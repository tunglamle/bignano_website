<?
function filter_duration($c, $value, $pval, $row)
{
    $html = "Vĩnh viễn";
    if ($value > 0) {
        $html = $value . ' Tháng';
    }
    return $html;
}

function filter_price($c, $value, $pval, $row)
{
    $html = "Miễn phí";
    if ($value > 0) {
        $html = number_format($value, 0, ',', '.') . '<sup>đ</sup>';
    }
    return $html;
}

function default_default()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $clsModule, $clsButtonNav, $arrYesNoOptions, $arrNewsSource, $arrTemplateOption;;
    global $_max_category_level, $lang_code;
    $classTable = "Courses";
    $clsClassTable = new $classTable;
    $tableName = $clsClassTable->tbl;
    $pkeyTable = $clsClassTable->pkey;

    if (isset($_GET["reset"])) {
        $arrVars = array("scatid", "skeyword", "sat_home");
        foreach ($arrVars as $key => $val) {
            getPost_remove($val);
        }
        unset($arrVars);
    }

    //get _GET, _POST
    $curPage = GET("page", 0);
    $btnSave = POST("btnSave", "");
    $sat_home = getPOST("sat_home", -1);
    $return = (isset($_GET["return"])) ? base64_decode($_GET["return"]) : "";
    if ($return == "") $return = "mod=$mod";
    $returnExp = "return=" . base64_encode($_SERVER['QUERY_STRING']);
    $rowsPerPage = 20;

    //init Button
    $clsButtonNav->set("Save...", "/icon/disks.png", "Save and Continue", 1, "save");
    $clsButtonNav->set("New", "/icon/add2.png", "?mod=$mod&act=add&$returnExp", 1);
    $clsButtonNav->set("Clone", "/icon/copy.png", "Nhân đôi", 1, "confirmClone", "$mod,$returnExp");
    $clsButtonNav->set("Edit", "/icon/edit2.png", "Edit", 1, "confirmEdit");
    if ($core->hasPermiss("news_default_delete")) {
        $clsButtonNav->set("Delete", "/icon/delete2.png", "Delete", 1, "confirmDelete");
    }
    $clsButtonNav->set("Cancel", "/icon/undo.png", "?");
    //################### CHANGE BELOW CODE ###################
    $arrOptionsCategory = array();
    makeArrayListCategory(0, 0, $_max_category_level, $arrOptionsCategory, "ctype=" . CTYPE_KH);
    makeArrayListAuthor($arrAuthor, 1);
    //init Grid
    $baseUrl = "?mod=$mod";
    $cond = "lang_code='$lang_code'";
    if ($_GET["return"] != "") {
        $baseUrl .= "&return=" . $_GET["return"];
    }
    //Begin Added 20080704
    $clsCategory = new Category();
    $clsCategory->getParentArray();
    $skeyword = getPOST("skeyword", "");
    $scatid = getPOST("scatid", "");
    $scatid_options = "";
    if (is_array($arrOptionsCategory))
        foreach ($arrOptionsCategory as $k => $v) {
            $selected = ($k == $scatid) ? "selected" : "";
            $scatid_options .= "<option value='$k' $selected >$v</option>";
        }
    if ($skeyword != "") {
        $cond .= " AND (title LIKE '%$skeyword%' OR sapo LIKE '%$skeyword%' OR content LIKE '%$skeyword%')";
        $baseUrl = preg_replace("/\&skeyword=(\w+)/e", "", $baseUrl);
        $baseUrl .= "&skeyword=$skeyword";
    }
    if ($sat_home >= 0) {
        $cond .= " AND (at_home=$sat_home)";
        $baseUrl = preg_replace("/\&at_home=(\w+)/e", "", $baseUrl);
        $baseUrl .= "&sat_home=$sat_home";
    }
    if ($scatid != "" && $scatid != "0") {
        $strcatid = $clsCategory->getAllCatStr($scatid) . "$scatid";
        $cond .= " AND cat_id in ($strcatid)";
        $baseUrl = preg_replace("/\&scatid=(\w+)/e", "", $baseUrl);
        $baseUrl .= "&scatid=$scatid";
    }
    $assign_list["skeyword"] = $skeyword;
    $assign_list["scatid_options"] = $scatid_options;
    //End
    $clsDataGrid = new DataGrid($curPage, $rowsPerPage);
    $clsDataGrid->setBaseURL($baseUrl);
    $clsDataGrid->setReturnExp($returnExp);
    $clsDataGrid->setDbTable($tableName, $cond);
    $clsDataGrid->setPkey($pkeyTable);
    $clsDataGrid->setFormName("theForm");
    $clsDataGrid->setOrderBy("reg_date DESC");
    $clsDataGrid->setTitle($core->getLang("Courses"));
    $clsDataGrid->setTableAttrib('cellpadding="0" cellspacing="0" width="100%" border="0" class="girdtable"');
    $clsDataGrid->addColumnLabel("name", "name", "width='auto'");
    $clsDataGrid->addColumnImage("image", "Image", "width='100px' border=0", "width='10%' align='center'");
    $clsDataGrid->addColumnSelect("cat_id", "Category", "width='5%' align='center'", $arrOptionsCategory);
    $clsDataGrid->addColumnMoney("price", "Price", "width='5%' align='center'");;
    $clsDataGrid->addColumnLabel("duration", "Thời hạn", "width='5%' align='center'");;
    $clsDataGrid->addColumnLabel("introduce", "Giới thiệu khóa học", "width='auto'");
    $clsDataGrid->addColumnDate("reg_date", "Created_at", "width='5%' align='center' nowrap ", "%d/%m/%Y %H:%M");
    $clsDataGrid->addColumnText("order_no", "OrderNo", "width='5%' align='center'");
    $clsDataGrid->addColumnSelect("at_home", "Nổi bật?", "width='5%' align='center'", $arrYesNoOptions);
    $clsDataGrid->addColumnSelect("is_online", "Publish?", "width='5%' align='center'", array("<b style='color:red'>Không</b>", "<b style='color:blue'>Có</b>"), 0, 1);
    $clsDataGrid->addColumnSelect("user_id", "Người đăng", "width='5%' align='center'", $arrAuthor, 0, 1);
    $clsDataGrid->addColumnUrl($pkeyTable, "Lộ trình", "width='3%' align='center' nowrap", "<a href='?mod=stage&course_id=%1%&$returnExp' class='abutton1'>Lộ trình &raquo;</a>");
    $clsDataGrid->addFilter("price", "filter_price");
    $clsDataGrid->addFilter("duration", "filter_duration");
    //####################### ENG CHANGE ######################
    if ($btnSave != "") {
        if ($clsDataGrid->saveData()) {
            if ($sat_home == -1 && $scatid == "0" && $scatid == "") {
                $query = $_SERVER['QUERY_STRING'];
                header("location: ?$query");
                exit();
            }
        }
    }
    $base_url1 = preg_replace("/\&skeyword=(\w*)/i", "", $_SERVER['QUERY_STRING']);
    $assign_list["sat_home"] = $sat_home;
    $assign_list["base_url1"] = "?" . $base_url1;
    $assign_list["base_url"] = "?" . preg_replace("/\&reset/i", "", $_SERVER['QUERY_STRING']);
    $assign_list["clsDataGrid"] = $clsDataGrid;
    $assign_list["htmlOptionsLang"] = makeListLang($lang_code);
}

function default_add()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $clsModule, $clsButtonNav, $arrYesNoOptions, $arrNewsSource, $arrTemplateOption;;
    global $_max_category_level, $lang_code;
    $classTable = "Courses";
    $clsClassTable = new $classTable; //$clsClassTable->setDebug();
    $tableName = $clsClassTable->tbl;
    $pkeyTable = $clsClassTable->pkey;

    require_once DIR_COMMON . "/clsForm.php";
    //get _GET, _POST
    $pvalTable = GET($pkeyTable, "");
    $btnSave = POST("btnSave", "");
    $return = (isset($_GET["return"])) ? base64_decode($_GET["return"]) : "";
    if ($return == "") $return = "mod=$mod";
    $returnExp = "return=" . base64_encode($return);
    //get Mode
    $mode = ($pvalTable != "") ? "Edit" : "New";
    $base_url = "?mod=$mod&act=$act&$returnExp";
    //init Button
    if ($mode == "Edit") {
        $arrNews = $clsClassTable->getOne($pvalTable);

        if ($core->hasPermiss("news_default_delete") || $arrNews['user_id'] == $core->_USER['user_id']) {
            $clsButtonNav->set("Save...", "/icon/disks.png", "Save", 1, "savecontinue");
            $clsButtonNav->set("Save", "/icon/disks.png", "Save", 1, "save");
        }
        $clsButtonNav->set("New", "/icon/add2.png", "?mod=$mod&act=add&$returnExp", 1);

        if ($core->hasPermiss("news_default_delete") || $arrNews['user_id'] == $core->_USER['user_id']) {
            $clsButtonNav->set("Delete", "/icon/delete2.png", "?mod=$mod&act=delete&$pkeyTable=$pvalTable&$returnExp");
        }
    } else {
        $clsButtonNav->set("Save", "/icon/disks.png", "Save", 1, "save");
    }
    $clsButtonNav->set("Cancel", "/icon/undo.png", "?$return");
    //################### CHANGE BELOW CODE ###################
    $arrOptionsCategory = array();
    makeArrayListCategory(0, 0, $_max_category_level, $arrOptionsCategory, "ctype=" . CTYPE_KH);
    makeArrayListAuthor($arrAuthor);
    //init Form
    $clsForm = new Form();
    $clsForm->setDbTable($tableName, $pkeyTable, $pvalTable);
    $clsForm->setTitle($core->getLang("Courses"));
    $clsForm->setTextAreaType("full");
    $clsForm->addInputSelect("cat_id", "", "Category", $arrOptionsCategory, 0, "style='font-size:12px'");
    $clsForm->addInputText("name", "", "Name", 255, 0, "style='width:99%'");
    $clsForm->addInputFile("image", "", "Image", "jpg, jpeg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("list_image", "", "Ảnh slide trang chủ", "jpg, jpeg, gif, png", 1, "style='width:300px'", "", 1);
    $clsForm->addInputFile("attachment", "", "Upload Video", "mp4", 1, "style='width:300px'");
    $clsForm->addInputText("video_id", "", "Youtube Video ID", 255, 1, "style='width:23%'");
    $clsForm->addInputText("price", "", "Giá", 255, 0, "style='width:23%'");
    $clsForm->addInputText("duration", "", "Thời hạn (tháng)", 255, 1, "style='width:23%'");
    $clsForm->addAttachInput('price', 'duration');
    $clsForm->addInputText("introduce", "", "Giới thiệu khóa học", 255, 1, "style='width:23%'");
    $clsForm->addInputTextArea("des", "", "Description", 9999999999, 10, 5, 1, "style='width:100%; height:300px'");
    $clsForm->addInputTextArea("detail", "", "Chi tiết khóa học", 9999999999, 10, 5, 0, "style='width:100%; height:300px'");
    $clsForm->addInputText("order_no", "99999", "OrderNo", 5, 0, "style='width:100px'");
    $clsForm->addInputDate("reg_date", time(), "Created_at", "%d/%m/%Y %H:%M");
    $clsForm->addInputSelect("user_id", 0, "Người đăng", $arrAuthor, 0, "style='font-size:12px'");
    $clsForm->addInputRadio("at_home", "", "Nổi bật?", $arrYesNoOptions, 0, "style='font-size:12px'");
    $clsForm->addInputRadio("is_online", 1, "Publish?", $arrYesNoOptions, 0, "style='font-size:12px'");
    $clsForm->addInputHidden("lang_code", $lang_code);

    $clsForm->addInputText("page_title", "", "[SEOmoz] PageTitle", 255, 1, "style='width:99%'");
    $clsForm->addInputText("meta_keywords", "", "[SEOmoz] MetaKeywords", 255, 1, "style='width:99%'");
    $clsForm->addInputText("meta_des", "", "[SEOmoz] MetaDescription", 255, 1, "style='width:99%'");
    //####################### ENG CHANGE ######################
    //do Action
    if ($btnSave != "") {
        $slug = utf8_nosign_noblank($_POST["name"]);
        $clsForm->addInputHidden("slug", $slug);
        if ($clsForm->validate()) {
            if ($clsForm->saveData($mode)) {
                if ($btnSave == "SaveContinue") {
                    $return = $_SERVER['QUERY_STRING'];
                }
                header("location: ?$return");
                exit();
            }
        }
    }

    $assign_list["base_url"] = $base_url;
    $assign_list["clsModule"] = $clsModule;
    $assign_list["clsForm"] = $clsForm;
    $assign_list[$pkeyTable] = $pvalTable;
}

function default_delete()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $clsModule, $clsButtonNav;
    $classTable = "Courses";
    $clsClassTable = new $classTable;
    $tableName = $clsClassTable->tbl;
    $pkeyTable = $clsClassTable->pkey;
    $return = (isset($_GET["return"])) ? base64_decode($_GET["return"]) : "";
    if ($return == "") $return = "mod=$mod";
    //################### CAN NOT MODIFY BELOW CODE ###################
    $pvalTable = isset($_GET[$pkeyTable]) ? $_GET[$pkeyTable] : "";
    if ($pvalTable != "") {
        //Begin RecycleBin
        $clsRecycleBin = new RecycleBin();
        $clsRecycleBin->AddNew($classTable, $pvalTable, "title", "Courses");
        //End RecycleBin
        $clsClassTable->deleteOne($pvalTable);
        header("location: ?$return");
    }
    $checkList = isset($_POST["checkList"]) ? $_POST["checkList"] : "";
    if (is_array($checkList)) {
        $clsRecycleBin = new RecycleBin();
        foreach ($checkList as $key => $val) {
            //Begin RecycleBin
            $clsRecycleBin->AddNew($classTable, $val, "title", "Courses");
            //End RecycleBin
            $clsClassTable->deleteOne($val);
        }
        header("location: ?$return");
    }
    unset($clsTable);
}

//gọi function clone
function default_clone()
{
    global $core;
    $core->_Clone("Courses");
}


?>
