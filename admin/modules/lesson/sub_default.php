<?
function default_default()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $clsModule, $clsButtonNav, $arrYesNoOptions, $arrNewsSource;
    global $_max_category_level, $lang_code;
    $classTable = "Lessons";
    $clsClassTable = new $classTable;
    $tableName = $clsClassTable->tbl;
    $pkeyTable = $clsClassTable->pkey;

    //get _GET, _POST
    $curPage = GET("page", 0);
    $btnSave = POST("btnSave", "");
    $return = (isset($_GET["return"])) ? base64_decode($_GET["return"]) : "";
    if ($return == "") $return = "mod=$mod";
    $returnExp = "return=" . base64_encode($_SERVER['QUERY_STRING']);
    $rowsPerPage = 50;
    $stage_id = GET("stage_id", 0);
    //init Button
    $clsButtonNav->set("Save...", "/icon/disks.png", "Save and Continue", 1, "save");
    $clsButtonNav->set("New", "/icon/add2.png", "?mod=$mod&act=add&stage_id=$stage_id&$returnExp", 1);
    $clsButtonNav->set("Edit", "/icon/edit2.png", "Edit", 1, "confirmEdit");
    $clsButtonNav->set("Clone", "/icon/copy.png", "Nhân đôi", 1, "confirmClone", "$mod,$returnExp");
    //$clsButtonNav->set("Paste", "/icon/paste.png", "", 0);
    if ($core->hasPermiss("news_default_delete")) {
        $clsButtonNav->set("Delete", "/icon/delete2.png", "Delete", 1, "confirmDelete");
    }
    if ($stage_id != 0) {
        $clsButtonNav->set("Cancel", "/icon/undo.png", "?$return");
    } else {
        $clsButtonNav->set("Cancel", "/icon/undo.png", "?");
    }
    //################### CHANGE BELOW CODE ###################
    makeArrayListAuthor($arrAuthor, 1);
    //init Grid
    $baseUrl = "?mod=$mod";
    if ($stage_id > 0) {
        $baseUrl .= "&stage_id=$stage_id";
    }
    $cond = "lang_code='$lang_code' AND stage_id=$stage_id";
    if ($_GET["return"] != "") {
        $baseUrl .= "&return=" . $_GET["return"];
    }
    //Begin Added 20080704
    $clsCategory = new Category();
    $clsCategory->getParentArray();
    $skeyword = isset($_REQUEST["skeyword"]) ? $_REQUEST["skeyword"] : "";
    if ($skeyword != "") {
        $cond .= " AND (title LIKE '%$skeyword%' OR sapo LIKE '%$skeyword%' OR content LIKE '%$skeyword%')";
        $baseUrl = preg_replace("/\&skeyword=(\w+)/e", "", $baseUrl);
        $baseUrl .= "&skeyword=$skeyword";
    }
    $assign_list["skeyword"] = $skeyword;
    //End
    $clsDataGrid = new DataGrid($curPage, $rowsPerPage);
    $clsDataGrid->setBaseURL($baseUrl);
    $clsDataGrid->setReturnExp($returnExp);
    $clsDataGrid->setDbTable($tableName, $cond);
    $clsDataGrid->setPkey($pkeyTable);
    $clsDataGrid->setFormName("theForm");
    $clsDataGrid->setOrderBy("reg_date DESC");
    $clsDataGrid->setTitle($core->getLang("Bài giảng"));
    $clsDataGrid->setTableAttrib('cellpadding="0" cellspacing="0" width="100%" border="0" class="girdtable"');
    $clsDataGrid->addColumnLabel("name", "Name", "width='10%'");
    $clsDataGrid->addColumnImage("image", "Image", "width='100%' height='auto' border=0", "width='5%' align='center'");
    $clsDataGrid->addColumnDate("reg_date", "Posted_at", "width='5%' align='center' nowrap ", "%m/%d/%Y %H:%M");
    $clsDataGrid->addColumnLabel("total_view", "Xem", "width='5%' align='center'");
    $clsDataGrid->addColumnSelect("is_trial", "Học thử?", "width='5%' align='center'", array("<b style='color:red'>Không</b>", "<b style='color:blue'>Có</b>"), 0, 1);
    $clsDataGrid->addColumnSelect("is_online", "Online?", "width='5%' align='center'", array("<b style='color:red'>Không</b>", "<b style='color:blue'>Có</b>"), 0, 1);
    $clsDataGrid->addColumnSelect("user_id", "Người đăng", "width='5%' align='center'", $arrAuthor, 0, 1);
    //####################### ENG CHANGE ######################
    if ($btnSave != "") {
        if ($btnSave == "Copy") {

        } else
            if ($clsDataGrid->saveData()) {
                $query = $_SERVER['QUERY_STRING'];
                header("location: ?$query");
                exit();
            }
    }
    $base_url1 = preg_replace("/\&skeyword=(\w*)/i", "", $_SERVER['QUERY_STRING']);
    $assign_list["base_url1"] = "?" . $base_url1;
    $assign_list["clsDataGrid"] = $clsDataGrid;
}

function default_add()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act, $dbconn;
    global $core, $clsModule, $clsButtonNav, $arrYesNoOptions, $arrNewsSource;
    global $_max_category_level, $lang_code;
    $classTable = "Lessons";
    $clsClassTable = new $classTable;
    $tableName = $clsClassTable->tbl;
    $pkeyTable = $clsClassTable->pkey;
    $clsTags = new Tags();

    require_once DIR_COMMON . "/clsForm.php";
    //get _GET, _POST
    $pvalTable = GET($pkeyTable, "");
    $stage_id = GET("stage_id", 0);
    $btnSave = POST("btnSave", "");
    $return = (isset($_GET["return"])) ? base64_decode($_GET["return"]) : "";
    if ($return == "") $return = "mod=$mod";
    $returnExp = "return=" . base64_encode($return);
    //get Mode
    $mode = ($pvalTable != "") ? "Edit" : "New";
    $base_url = "?mod=$mod&act=$act&$returnExp";
    //init Button
    if ($mode == "Edit") {
        $clsButtonNav->set("Save...", "/icon/disks.png", "Save", 1, "savecontinue");
        $clsButtonNav->set("Save", "/icon/disks.png", "Save", 1, "save");
        $clsButtonNav->set("Delete", "/icon/delete2.png", "?mod=$mod&act=delete&$pkeyTable=$pvalTable&$returnExp");
    } else {
        $clsButtonNav->set("Save", "/icon/disks.png", "Save", 1, "save");
    }
    $clsButtonNav->set("Cancel", "/icon/undo.png", "?$return");
    //################### CHANGE BELOW CODE ###################
    makeArrayListAuthor($arrAuthor);
    //init Form
    $clsForm = new Form();
    $clsForm->setDbTable($tableName, $pkeyTable, $pvalTable);
    $clsForm->setTitle($core->getLang("Bài giảng"));
    $clsForm->setTextAreaType("full");
    $clsForm->addInputText("name", "", "Name", 255, 0, "style='width:99%'");
    $clsForm->addInputFile("image", "", "Image", "jpg, jpeg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("attachment", "", "Tập tin đính kèm<br>Định dạng <b>.mp3 hoặc .mp4.</b>", "mp3,mp4", 1, "style='width:300px'");
    $clsForm->addInputText("video_id", "", "Youtube Video ID<br>vd: https://www.youtube.com/watch?v=<b>ycGfvA1vkR8</b>", 255, 1, "style='width:23%' placeholder='ycGfvA1vkR8'");
    $clsForm->addInputTextArea("des", "", "Mô tả ngắn gọn", 9999999999, 10, 5, 1, "style='width:100%; height:150px'", "SAPO");
    $clsForm->addInputTextArea("detail", "", "Chi tiết bài giảng", 9999999999, 10, 5, 1, "style='width:100%; height:300px'");
    $clsForm->addInputText("tags", "", "Tags (các tag cách nhau dấu ,)", 255, 1, "style='width:99%'");
    $clsForm->addInputSelect("user_id", 0, "Người đăng", $arrAuthor, 0, "style='font-size:12px'");
    $clsForm->addInputRadio("is_trial", 1, "Học thử?", $arrYesNoOptions, 0, "style='font-size:12px'");
    $clsForm->addInputRadio("is_online", 1, "Hiển thị?", $arrYesNoOptions, 0, "style='font-size:12px'");
    $clsForm->addInputText("page_title", "", "[SEOmoz] PageTitle", 255, 1, "style='width:99%'");
    $clsForm->addInputText("meta_keywords", "", "[SEOmoz] MetaKeywords", 255, 1, "style='width:99%'");
    $clsForm->addInputText("meta_des", "", "[SEOmoz] MetaDescription", 255, 1, "style='width:99%'");
    //####################### ENG CHANGE ######################
    //do Action
    if ($btnSave != "") {
        if ($mode == "New") {
            $clsForm->addInputHidden("stage_id", $stage_id);
            $clsForm->addInputHidden("lang_code", $lang_code);
            $clsForm->addInputHidden("reg_date", time());
        }
        $clsForm->addInputHidden("slug", utf8_nosign_noblank($_POST["name"]));
        if ($clsForm->validate()) {
            if ($clsForm->saveData($mode)) {
                $clsTags->insertTags($_POST['tags'], 'lesson_id', $pvalTable);

                if ($btnSave == "Save" || $mode == "New") {
                    header("location: ?$return");
                    exit();
                } else {
                    $query = $_SERVER['QUERY_STRING'];
                    header("location: ?$query");
                    exit();
                }
            }
        }
    }
    $assign_list["sampleTags"] = $clsTags->getStrSampleTags('lesson_id');

    $assign_list["base_url"] = $base_url;
    $assign_list["clsModule"] = $clsModule;
    $assign_list["clsForm"] = $clsForm;
    $assign_list[$pkeyTable] = $pvalTable;
}

function default_delete()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $clsModule, $clsButtonNav;
    $classTable = "Lessons";
    $clsClassTable = new $classTable;
    $tableName = $clsClassTable->tbl;
    $pkeyTable = $clsClassTable->pkey;
    $return = (isset($_GET["return"])) ? base64_decode($_GET["return"]) : "";
    if ($return == "") $return = "mod=$mod";
    //################### CAN NOT MODIFY BELOW CODE ###################
    $pvalTable = isset($_GET[$pkeyTable]) ? $_GET[$pkeyTable] : "";
    $checkList = isset($_POST["checkList"]) ? $_POST["checkList"] : "";
    if (is_array($checkList)) {
        $clsRecycleBin = new RecycleBin();
        foreach ($checkList as $key => $val) {
            //Begin RecycleBin
            $clsRecycleBin->AddNew($classTable, $val, "title", "Lessons");
            //End RecycleBin
            $clsClassTable->deleteOne($val);
        }
        header("location: ?$return");
    } else
        if ($pvalTable != "") {
            //Begin RecycleBin
            $clsRecycleBin = new RecycleBin();
            $clsRecycleBin->AddNew($classTable, $pvalTable, "title", "Lessons");
            //End RecycleBin
            $clsClassTable->deleteOne($pvalTable);
            header("location: ?$return");
        }

    unset($clsTable);
}

//gọi function clone
function default_clone()
{
    global $core;
    $core->_Clone("Lessons");
}

?>
