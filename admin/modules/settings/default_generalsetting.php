<?php
/**
 * Module: [settings]
 * Home function with $sub=default, $act=generalsetting
 * Display General Setting Page
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_generalsetting()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $clsModule, $clsButtonNav, $lang_code;
    $classTable = "Settings";
    $clsClassTable = new $classTable;
    $tableName = $clsClassTable->tbl;
    $pkeyTable = $clsClassTable->pkey;
    //get _GET, _POST
    $pvalTable = isset($_GET[$pkeyTable]) ? $_GET[$pkeyTable] : 1;
    $btnSave = isset($_POST["btnSave"]) ? $_POST["btnSave"] : "";
    //init Button
    $clsForm = new Form();
    $clsButtonNav->set("Save", "/icon/disks2.png", "Save", 1, "save");
    $clsButtonNav->set("Cancel", "/icon/undo.png", "?");

    $arrListKey = array(
        "site_title",
        "site_logo",
        "site_logo2",
        "site_logo_footer",
        "site_favicon",
        "site_name",
        "webmaster_email",
        "smtp_server",
        "smtp_port",
        "smtp_user",
        "smtp_pass",
        "copyright",
        "is_close_site",
        "close_site_notice",
        "site_phone",
        "site_email",
        "site_address",
        "site_address2",
        "site_addressvp",
        "site_hotline",
        "site_comname",
        "site_website",
        "about",
        "contact_info",
        "footer_info",
        "contact_map",
        "contact_map2",
        "site_facebook",
        "home_intro",
        "home_intro2",
        "home_image",
        "video_image",
        "video_image_mobile",
        "video_link",
        "header_address",
        "site_welcome",
        "site_welcome2",
        "qrcode",
        "search_image",
        "marquee_title",
        "marquee_link",
        "product_note",
        "banner_link",
        "download_zip",
        "video",
        "right_slide_image",
        "right_slide_alt",
        "right_slide_link",
        "content_email",
        "content_slider",
        "link_email"
    );

    foreach ($arrListKey as $key => $val) {
        ${$val} = isset($_POST[$val]) ? $_POST[$val] : "";
    }
    if ($btnSave != "") {
        $about = br2nl($about);
        $close_site_notice = br2nl($close_site_notice);
        $contact_info = br2nl($contact_info);
        $content_slider = br2nl($content_slider);
        $contact_map = br2nl($contact_map);
        $contact_map2 = br2nl($contact_map2);
        $content_email = br2nl($content_email);
        $footer_info = br2nl($footer_info);
        $home_intro = br2nl($home_intro);
        $home_intro2 = br2nl($home_intro2);
        $home_image = br2nl($home_image);
        $video_image = br2nl($video_image);
        $video_image_mobile = br2nl($video_image_mobile);
        $right_slide_image = br2nl($right_slide_image);
        $video_link = br2nl($video_link);
        $search_image = br2nl($search_image);
        $marquee_title = br2nl($marquee_title);
        $product_note = br2nl($product_note);
        $download_zip = br2nl($download_zip);
        $video = br2nl($video);
        foreach ($arrListKey as $key => $val) {
            $clsClassTable->setValue($val, ${$val}, $lang_code);
        }
//        header("location: ?mod=$mod&act=$act");
//        exit();
    } else {
        foreach ($arrListKey as $key => $val) {
            ${$val} = $clsClassTable->getValue($val, $lang_code);
        }
    }

    $clsForm->setTextAreaType("full");
    $clsForm->addInputFile("site_logo", $site_logo, "Image (jpg, gif, png)", "jpg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("site_logo2", $site_logo2, "Image (jpg, gif, png)", "jpg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("site_favicon", $site_favicon, "Image (jpg, gif, png)", "jpg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("site_logo_footer", $site_logo_footer, "Image (jpg, gif, png)", "jpg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("download_zip", $download_zip, "Zip (zip, rar)", "zip, rar", 1, "style='width:300px'");
    $clsForm->addInputFile("video", $video, "Video mp4", "mp4", 1, "style='width:300px'");
    $clsForm->addInputFile("qrcode", $qrcode, "Image (jpg, gif, png)", "jpg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("home_image", $home_image, "Image (jpg, gif, png)", "jpg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("search_image", $search_image, "Image (jpg, gif, png)", "jpg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("video_image", $video_image, "Image (jpg, gif, png)", "jpg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("video_image_mobile", $video_image_mobile, "Image (jpg, gif, png)", "jpg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("right_slide_image", $right_slide_image, "Image (jpg, gif, png)", "jpg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputTextArea("about", $about, "", 1000, 10, 5, 1, "style='width:100%; height:300px'", "SMALL");
    $clsForm->addInputTextArea("contact_info", $contact_info, "", 1000, 10, 5, 1, "style='width:100%; height:200px'", "full");
    $clsForm->addInputTextArea("content_slider", $content_slider, "", 1000, 10, 5, 1, "style='width:100%; height:200px'", "full");
    $clsForm->addInputTextArea("contact_map", $contact_map, "", 1000, 10, 5, 1, "style='width:100%; height:300px'", "none");
    $clsForm->addInputTextArea("content_email", $content_email, 1000, 10, 5, 1, "style='width:100%; height:200px'", "Full");
    $clsForm->addInputTextArea("contact_map2", $contact_map2, "", 1000, 10, 5, 1, "style='width:100%; height:300px'", "none");
    $clsForm->addInputTextArea("footer_info", $footer_info, "", 1000, 10, 5, 1, "style='width:100%; height:300px'", "full");
    $clsForm->addInputTextArea("home_intro", $home_intro, "", 1000, 10, 5, 1, "style='width:100%; height:300px'", "full");
    $clsForm->addInputTextArea("home_intro2", $home_intro2, "", 1000, 10, 5, 1, "style='width:100%; height:300px'", "full");
    $clsForm->addInputTextArea("product_note", $product_note, "", 1000, 10, 5, 1, "style='width:100%; height:300px'", "full");
    $clsForm->addInputTextArea("marquee_title", $marquee_title, "", 1000, 10, 5, 1, "style='width:100%; height:200px'", "full");
    $clsForm->addInputTextArea("close_site_notice", $close_site_notice, "", 1000, 10, 5, 1, "style='width:100%; height:300px'", "SMALL");

    foreach ($arrListKey as $key => $val) {
        $assign_list[$val] = ${$val};
    }
    $assign_list["clsModule"] = $clsModule;
    $assign_list["clsForm"] = $clsForm;
    $assign_list[$pkeyTable] = $pvalTable;
}

?>