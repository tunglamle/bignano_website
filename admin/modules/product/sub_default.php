<?
function cat_filter($c, $value, $pval, $row)
{
    return str_replace(array('&brvbar;', '-'), '', $c['arrOptions'][$value]);
}

function default_default()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $clsModule, $clsButtonNav, $arrYesNoOptions, $arrNewsSource;
    global $_max_category_level, $lang_code;
    $classTable = "Product";
    $clsClassTable = new $classTable;
    $tableName = $clsClassTable->tbl;
    $pkeyTable = $clsClassTable->pkey;
    $clsLanguage = new Language();

    //get _GET, _POST
    $curPage = GET("page", 0);
    $btnSave = POST("btnSave", "");
    $return = (isset($_GET["return"])) ? base64_decode($_GET["return"]) : "";
    if ($return == "") $return = "mod=$mod";
    $returnExp = "return=" . base64_encode($_SERVER['QUERY_STRING']);
    $rowsPerPage = 50;

    //init Button
    $clsButtonNav->set("Save...", "/icon/disks.png", "Save and Continue", 1, "save");
    $clsButtonNav->set("New", "/icon/add2.png", "?mod=$mod&act=add&$returnExp", 1);
    $clsButtonNav->set("Edit", "/icon/edit2.png", "Edit", 1, "confirmEdit");
    $clsButtonNav->set("Clone", "/icon/copy.png", "Clone", 1, "confirmClone");
    //$clsButtonNav->set("Paste", "/icon/paste.png", "", 0);
    if ($core->hasPermiss("news_default_delete")) {
        $clsButtonNav->set("Delete", "/icon/delete2.png", "Delete", 1, "confirmDelete");
    }
    $clsButtonNav->set("Cancel", "/icon/undo.png", "?");
    //################### CHANGE BELOW CODE ###################
    $arrOptionsCategory = array();
    makeArrayListCategory(0, 0, $_max_category_level, $arrOptionsCategory, "ctype=" . CTYPE_SP);
    makeArrayListAuthor($arrAuthor, 1);
    //init Grid
    $baseUrl = "?mod=$mod";
    $cond = "lang_code='$lang_code'";
    if ($_GET["return"] != "") {
        $baseUrl .= "&return=" . $_GET["return"];
    }
    //Begin Added 20080704
    $clsCategory = new Category();
    $clsCategory->getParentArray();
    $skeyword = isset($_REQUEST["skeyword"]) ? $_REQUEST["skeyword"] : "";
    $scatid = isset($_REQUEST["scatid"]) ? $_REQUEST["scatid"] : "";
    $scatid_options = "";
    if (is_array($arrOptionsCategory))
        foreach ($arrOptionsCategory as $k => $v) {
            $selected = ($k == $scatid) ? "selected" : "";
            $scatid_options .= "<option value='$k' $selected >$v</option>";
        }
    if ($skeyword != "") {
        $cond .= " AND (title LIKE '%$skeyword%' OR sapo LIKE '%$skeyword%' OR content LIKE '%$skeyword%')";
        $baseUrl = preg_replace("/\&skeyword=(\w+)/e", "", $baseUrl);
        $baseUrl .= "&skeyword=$skeyword";
    }
    if ($scatid != "" && $scatid != "0") {
        $strcatid = $clsCategory->getAllCatStr($scatid) . "$scatid";
        $cond .= " AND cat_id in ($strcatid)";
        $baseUrl = preg_replace("/\&scatid=(\w+)/e", "", $baseUrl);
        $baseUrl .= "&scatid=$scatid";
    }
    $assign_list["skeyword"] = $skeyword;
    $assign_list["scatid_options"] = $scatid_options;
    //End
    $clsDataGrid = new DataGrid($curPage, $rowsPerPage);
    $clsDataGrid->setBaseURL($baseUrl);
    $clsDataGrid->setReturnExp($returnExp);
    $clsDataGrid->setDbTable($tableName, $cond);
    $clsDataGrid->setPkey($pkeyTable);
    $clsDataGrid->setFormName("theForm");
    $clsDataGrid->setOrderBy("order_no ASC");
    $clsDataGrid->setTitle($core->getLang("Product"));
    $clsDataGrid->setTableAttrib('cellpadding="0" cellspacing="0" width="100%" border="0" class="girdtable"');
    $clsDataGrid->addColumnLabel("name", "Name", "width='10%'");
//    $clsDataGrid->addColumnImage("image", "Image", "width=70px border=0", "width='5%' align='center'");
    $clsDataGrid->addColumnSelect("cat_id", "Category", "width='5%' align='center'", $arrOptionsCategory, 0, 0);
    $clsDataGrid->addColumnDate("reg_date", "Posted_at", "width='5%' align='center' nowrap ", "%m/%d/%Y %H:%M");
    $clsDataGrid->addColumnLabel("view_num", "Lượt xem", "width='5%' align='center'");
    $clsDataGrid->addColumnSelect("at_home", "Hiển thị trang chủ?", "width='5%' align='center'", $arrYesNoOptions, 0, 0);
    $clsDataGrid->addColumnSelect("is_new", "SP New Launching ?", "width='5%' align='center'", $arrYesNoOptions, 0, 0);
    $clsDataGrid->addColumnSelect("is_online", "Online?", "width='5%' align='center'", $arrYesNoOptions, 0, 0);
//    $clsDataGrid->addColumnSelect("is_ofs", "Tình trang?", "width='5%' align='center'", array("Còn hàng", "Hết hàng"), 0, 0);
    $clsDataGrid->addColumnText("order_no", "STT", "width='3%' align='center'");
    // $clsDataGrid->addColumnSelect("user_id", "Người đăng", "width='5%' align='center'", $arrAuthor, 0, 1);
    // $clsDataGrid->addFilter("cat_id", "cat_filter");
    //####################### ENG CHANGE ######################
    if ($btnSave != "") {
        if ($btnSave == "Copy") {

        } else
            if ($clsDataGrid->saveData()) {
                if ($sis_hot == -1 && $scatid == "0" && $scat_id == "") {
                    $query = $_SERVER['QUERY_STRING'];
                    header("location: ?$query");
                    exit();
                }
            }
    }
    $base_url1 = preg_replace("/\&skeyword=(\w*)/e", "", $_SERVER['QUERY_STRING']);
    $base_url1 = preg_replace("/\&scatid=(\w*)/e", "", $base_url1);
    $assign_list["sis_hot"] = $sis_hot;
    $assign_list["base_url1"] = "?" . $base_url1;
    $assign_list["clsDataGrid"] = $clsDataGrid;
    $assign_list["htmlOptionsLang"] = makeListLang($lang_code);
}

function default_add()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $clsModule, $clsButtonNav, $arrYesNoOptions, $arrNewsSource;
    global $_max_category_level, $lang_code, $dbconn;
    $classTable = "Product";
    $clsClassTable = new $classTable;
//    $clsClassTable->setDebug();
    $tableName = $clsClassTable->tbl;
    $pkeyTable = $clsClassTable->pkey;
    $clsTags = new Tags();

    require_once DIR_COMMON . "/clsForm.php";
    //get _GET, _POST
    $pvalTable = GET($pkeyTable, "");
    $btnSave = POST("btnSave", "");
    $return = (isset($_GET["return"])) ? base64_decode($_GET["return"]) : "";
    if ($return == "") $return = "mod=$mod";
    $returnExp = "return=" . base64_encode($return);
    //get Mode
    $mode = ($pvalTable != "") ? "Edit" : "New";
    $base_url = "?mod=$mod&act=$act&$returnExp";

    if ($mode == "Edit") {
        $arrOneProduct = $clsClassTable->getOne($pvalTable);
        $clsButtonNav->set("Save...", "/icon/disks.png", "Save", 1, "savecontinue");
        $clsButtonNav->set("Save", "/icon/disks.png", "Save", 1, "save");
        $clsButtonNav->set("Delete", "/icon/delete2.png", "?mod=$mod&act=delete&$pkeyTable=$pvalTable&$returnExp");
    } else {
        $clsButtonNav->set("Save", "/icon/disks.png", "Save", 1, "save");
    }
    $clsButtonNav->set("Cancel", "/icon/undo.png", "?$return");
    //################### CHANGE BELOW CODE ###################
    $arrOptionsCategory = array();
    makeArrayListCategory(0, 0, $_max_category_level, $arrOptionsCategory, "ctype=" . CTYPE_SP);
    makeArrayListAuthor($arrAuthor);
    //Xử lý phần bộ lọc
    $arrOptionsApplication = array(
        0 => '',
    );
    makeArrayListApplication(0, 0, $_max_category_level, $arrOptionsApplication, "");

    $arrOptionsProperties = array(
        0 => '',
    );
    makeArrayListProperties(0, 0, $_max_category_level, $arrOptionsProperties, "");
    //End bộ lọc
    //init Form
    $clsForm = new Form();
    $clsForm->setDbTable($tableName, $pkeyTable, $pvalTable);
    $clsForm->setTitle($core->getLang("Product"));
    $clsForm->setTextAreaType("full");
    $clsForm->addInputSelect("cat_id", 0, "ProductCategory", $arrOptionsCategory, 0, "style='font-size:12px' onchange='ajax_get_cat_id_selected($modeInt)'");
//    $clsForm->addInputSelect("application_id", 0, "Search Application", $arrOptionsApplication, 0, "style='font-size:12px'");
//    $clsForm->addInputSelect("properties_id", 0, "Search Properties", $arrOptionsProperties, 0, "style='font-size:12px'");
    $clsForm->addInputText("name", "", "Name", 255, 0, "style='width:99%'");
    $clsForm->addInputText("link", "", "Link", 255, 1, "style='width:99%'");
    $clsForm->addInputText("embed", "", "Mã nhúng YTB / lấy mã nhúng sau dấu = ", 255, 1, "style='width:200px'");
    $clsForm->addInputFile("image", "", "Image", "jpg, jpeg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("image_newlaunching", "", "Image New-Launching", "jpg, jpeg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputFile("list_image", "", "Ảnh Slide", "jpg, jpeg, gif, png", 1, "style='width:300px'", "", 1);
    $clsForm->addInputFile("uploads_document", "", "Tải File Cataloge", "pdf, .doc, .xlsx", 1, "style='width:300px'", "", 1);
    $clsForm->addInputFile("list_icon", "", "List Icon", "jpg, jpeg, gif, png", 1, "style='width:300px'", "", 1);
    $clsForm->addInputFile("product_logo", "", "Product logo", "jpg, jpeg, gif, png", 1, "style='width:300px'");
    $clsForm->addInputTextArea("sapo", "", "Mô tả", 9999999999, 10, 5, 1, "style='width:100%; height:200px'");
    $clsForm->addInputTextArea("content", "", "Nội dung", 9999999999, 10, 5, 1, "style='width:100%; height:300px'");
//    $clsForm->addInputTextArea("content2", "", "Nội dung tab 2", 9999999999, 10, 5, 1, "style='width:100%; height:300px'");
//    $clsForm->addInputTextArea("content3", "", "Nội dung tab 3", 9999999999, 10, 5, 1, "style='width:100%; height:300px'");
    $clsForm->addInputText("order_no", "999999", "STT", 255, 0, "style='width:300px'");
//    $clsForm->addInputText("tags", "", "Tags (các tag cách nhau dấu ,)", 255, 1, "style='width:99%'");
    $clsForm->addInputSelect("user_id", 0, "Người đăng", $arrAuthor, 0, "style='font-size:12px'");
//    $clsForm->addInputRadio("is_ofs", 0, "Tình trạng?", array('Còn hàng', 'Hết hàng'), 0, "style='font-size:12px'");
    $clsForm->addInputRadio("is_online", 1, "Hiển thị?", $arrYesNoOptions, 0, "style='font-size:12px'");
//    $clsForm->addInputRadio("is_hot", 0, "SP nổi bật?", $arrYesNoOptions, 0, "style='font-size:12px'");
    $clsForm->addInputRadio("at_home", 0, "Hiển thị trang chủ?", $arrYesNoOptions, 0, "style='font-size:12px'");
    $clsForm->addInputRadio("is_new", 0, "SP New Launching ?", $arrYesNoOptions, 0, "style='font-size:12px'");
    $clsForm->addInputText("page_title", "", "[SEOmoz] PageTitle", 255, 1, "style='width:99%'");
    $clsForm->addInputText("meta_keywords", "", "[SEOmoz] MetaKeywords", 255, 1, "style='width:99%'");
    $clsForm->addInputText("meta_des", "", "[SEOmoz] MetaDescription", 255, 1, "style='width:99%'");
    //####################### ENG CHANGE ######################
    //do Action
    if ($btnSave != "") {
        if ($mode == "New") {
            $clsForm->addInputHidden("lang_code", $lang_code);
            $clsForm->addInputHidden("reg_date", time());
        }
        $clsForm->addInputHidden("slug", utf8_nosign_noblank($_POST["name"]));
        if ($clsForm->validate()) {
            if ($clsForm->saveData($mode)) {
                $clsTags->insertTags($_POST['tags'], 'product_id', $pvalTable);
                if ($btnSave == "Save" || $mode == "New") {
                    header("location: ?$return");
                    exit();
                } else {
                    $query = $_SERVER['QUERY_STRING'];
                    header("location: ?$query");
                    exit();
                }
            }
        }
    }
    $assign_list["sampleTags"] = $clsTags->getStrSampleTags('news_id');
    $assign_list["relatedProducts"] = $clsClassTable->getAllProductStr();

    $assign_list["base_url"] = $base_url;
    $assign_list["clsModule"] = $clsModule;
    $assign_list["clsForm"] = $clsForm;
    $assign_list[$pkeyTable] = $pvalTable;
}

function default_delete()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $clsModule, $clsButtonNav;
    $classTable = "Product";
    $clsClassTable = new $classTable;
    $tableName = $clsClassTable->tbl;
    $pkeyTable = $clsClassTable->pkey;
    $return = (isset($_GET["return"])) ? base64_decode($_GET["return"]) : "";
    if ($return == "") $return = "mod=$mod";
    //################### CAN NOT MODIFY BELOW CODE ###################
    $pvalTable = isset($_GET[$pkeyTable]) ? $_GET[$pkeyTable] : "";
    $checkList = isset($_POST["checkList"]) ? $_POST["checkList"] : "";
    if (is_array($checkList)) {
        $clsRecycleBin = new RecycleBin();
        foreach ($checkList as $key => $val) {
            //Begin RecycleBin
            $clsRecycleBin->AddNew($classTable, $val, "title", "News");
            //End RecycleBin
            $clsClassTable->deleteOne($val);
        }
        header("location: ?$return");
    } else
        if ($pvalTable != "") {
            //Begin RecycleBin
            $clsRecycleBin = new RecycleBin();
            $clsRecycleBin->AddNew($classTable, $pvalTable, "title", "News");
            //End RecycleBin
            $clsClassTable->deleteOne($pvalTable);
            header("location: ?$return");
        }

    unset($clsTable);
}

function default_clone()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $clsModule, $clsButtonNav;
    $classTable = "Product";
    $clsClassTable = new $classTable;
    $tableName = $clsClassTable->tbl;
    $pkeyTable = $clsClassTable->pkey;
    $return = (isset($_GET["return"])) ? base64_decode($_GET["return"]) : "";
    if ($return == "") $return = "mod=$mod";
    //################### CAN NOT MODIFY BELOW CODE ###################
    $pvalTable = isset($_GET[$pkeyTable]) ? $_GET[$pkeyTable] : "";
    $checkList = isset($_POST["checkList"]) ? $_POST["checkList"] : "";
    if (is_array($checkList)) {
        $clsRecycleBin = new RecycleBin();
        foreach ($checkList as $key => $val) {
            $clsClassTable->cloneOne($val);
        }
        header("location: ?$return");
        exit();
    } else
        if ($pvalTable != "") {
            $clsClassTable->cloneOne($pvalTable);
            header("location: ?$return");
            exit();
        }

    unset($clsTable);
}

?>
