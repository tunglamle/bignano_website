<?
/******************************************************
 * Admin Control Panel File
 * Control to show Menu & Icon of Dashboard
 * 
 * Project Name               :  Learning Themes DVS
 * Package Name            		:  
 * Program ID                 :  header.php
 * Environment                :  PHP  version version 4, 5
 * Author                     :  TuanTA
 * Version                    :  2.0
 * Creation Date              :  2014/02/10
 *
 * Modification History     :
 * Version    Date            Person Name  		Chng  Req   No    Remarks
 * 1.0       	2014/02/10    	TuanTA          -  		-     -     -
 * 2.0			2018/10/25		TuanTA			new style
 *
 ********************************************************/
class ControlPanel{
	var $arrSection		=	array();
	var $onLoadFunc		=	"";
	var $active_section = 	"";
	var $active_site_name = "";
	
	function ControlPanel(){
		//do nothing
	}
	/**
	 * Add Section
	 * 
	 * @param unknown $name
	 * @param string $title
	 * @param string $short_title
	 * @param string $imgsrc
	 * @param string $width
	 */
	function addSection($name, $title="Title of Section", $short_title = "Short Title", $imgsrc="", $width="120px"){
		$this->arrSection[$name] = array();
		$this->arrSection[$name]["title"] = $title;
		$this->arrSection[$name]["short_title"] = $short_title;
		$this->arrSection[$name]["imgsrc"] = $imgsrc;
		$this->arrSection[$name]["width"] = $width;
		$this->arrSection[$name]["link"] = array();
		$this->arrSection[$name]["totallink"] = 0;
	}
	/**
	 * Add link
	 * 
	 * @param unknown $section_name
	 * @param unknown $site_name
	 * @param string $link_name
	 * @param string $link_href
	 * @param string $imgsrc
	 */
	function addLink($section_name, $site_name, $link_name="", $link_href="", $imgsrc="", $badge=""){
		global $_SITE_ROOT, $core;
		if ($core->isSuper() || $core->isRoot() || $core->hasPermiss($site_name)){
			$arr = array();
			$arr["site_name"] = $site_name;
			$arr["link_name"] = $link_name;
			$arr["link_href"] = $link_href;
			$arr["imgsrc"] = $imgsrc;
			$arr["badge"] = $badge;
			$this->arrSection[$section_name]["link"][] = $arr;
			$this->arrSection[$section_name]["totallink"]++;
		}
	}
	/**
	 * Get current section
	 * 
	 * @param string $site_name
	 * @return Ambigous <string, unknown>
	 */
	function getCurrentSection($site_name=""){
		global $mod;
		if ($this->active_section!=""){
 			return $this->active_section;
		}
		$section_name = "";
		if ($site_name==""){
			$sub = GET("sub", "default");
			$act = GET("act", "default");
			$site_name = $mod."_".$sub."_".$act;
		}
		$site_name_default = $mod."_default_default";
		if (is_array($this->arrSection))
			foreach ($this->arrSection as $key => $val){
			if (is_array($val["link"]))
				foreach ($val["link"] as $key1 => $val1){
				if ($val1['site_name']==$site_name){
					$section_name = $key;
					$this->active_site_name = $site_name;
				}
				if ($val1['site_name']==$site_name_default){
					$section_name = $key;
					$this->active_site_name = $site_name_default;
				}
			}
		}
		$this->active_section = $section_name;
		return $section_name;
	}
	/**
	 * Get image Src
	 * 
	 * @param string $site_name
	 * @return string
	 */
	function getImgSrc($site_name=""){
		global $mod;
		if ($site_name==""){			
			$sub = GET("sub", "default");
			$act = GET("act", "default");
			$site_name = $mod."_".$sub."_".$act;
		}
		$site_name_default = $mod."_default_default";
		$img1 = $img2 = "";
		if (is_array($this->arrSection))
		foreach ($this->arrSection as $key => $val){
			if (is_array($val["link"]))
			foreach ($val["link"] as $key1 => $val1){
				if ($val1['site_name']==$site_name){
					$img1 = ADMIN_URL_IMAGES."/".$val1['imgsrc'];
				}
				if ($val1['site_name']==$site_name_default){
					$img2 = ADMIN_URL_IMAGES."/".$val1['imgsrc'];
				}
			}
		}
		if ($img1!="") return $img1;
		if ($img2!="") return $img2;
		return ADMIN_URL_IMAGES."/largeicon/no_icon.png";
	}
	/**
	 * Old Function: show Section
	 * 
	 * @param unknown $section_name
	 * @return string
	 */
	function showSection($section_name){
		global $_SITE_ROOT, $core;
		$html = "<!--------------->\n";
		$i = 0;
		$totalLink = $this->arrSection[$section_name]["totallink"];
		if (is_array($this->arrSection[$section_name]["link"]) && ($this->arrSection[$section_name]["totallink"]>0))
		foreach ($this->arrSection[$section_name]["link"] as $key => $val){
			$section_title = $this->arrSection[$section_name]["title"];
			if ($i==0){
				$html.= "<div id='div_$section_name' style=\"font-size:12px; padding:5px 0px 5px 10px;cursor:pointer\" onClick=\"changeSection('tbl_$section_name', 'img_$section_name')\" >
<strong><img id='img_$section_name' src='".ADMIN_URL_IMAGES."/nolines_minus.gif' border=0 align=left style='cursor:pointer'>".$section_title."</strong><br />
<img src='".ADMIN_URL_IMAGES."/bline.jpg' align='top'/>
</div>";
				$html.= "<table id='tbl_$section_name' cellpadding=\"5\" cellspacing=\"10\" style=\"font-size:12px;display:\">\n";
				$html.= "<tr>";
			}
			$i++;
			
			$html.= "<td class=\"tdlargeicon\" onmousemove=\"this.className='tdlargeiconActive'\"  onmouseout=\"this.className='tdlargeicon'\" onClick=\"gotoUrl('".$val["link_href"]."')\" nowrap>\n";
			$html.= "<div align='center' nowrap class='bigicon'>";
			if ($val['imgsrc']!="")
				$html.= "<img src='".ADMIN_URL_IMAGES."/".$val["imgsrc"]."'/><br />";
			$html.= $val["link_name"]."";
			$html.= "</div>";
			$html.= "</td>";
			if ($i%6==0 && $totalLink>$i){
				$html.= "</tr>";
				$html.= "<tr>";
			}
		}
		if ($i>0 && $i%6==0 && $totalLink>$i){
			$html.="<td></td>";
		}
		if ($i>0){
			$html.= "</tr>\n";
			$html.= "</table>\n<!--------------->\n";
		}
		return $html;
	}
	/**
	 * New Function: Show Section
	 * 
	 * @param unknown $section_name
	 * @return string
	 */
	function showSectionDiv($section_name){
		global $_SITE_ROOT, $core;
		$html = "<!--------------->\n";
		$i = 0;
		$totalLink = $this->arrSection[$section_name]["totallink"];
		if (is_array($this->arrSection[$section_name]["link"]) && ($this->arrSection[$section_name]["totallink"]>0)){
			$section_title = $this->arrSection[$section_name]["title"];
			$html.= "<div id='div_$section_name' class='section_div_name' onClick=\"changeSection('tbl_$section_name', 'img_$section_name')\" >
			<strong><img id='img_$section_name' src='".ADMIN_URL_IMAGES."/nolines_minus.gif' border=0 align=left style='cursor:pointer'>".$section_title."</strong><br />
			<img src='".ADMIN_URL_IMAGES."/bline.jpg' align='top'/></div>";
			$html.= "<div id='tbl_$section_name' class='section_tbl'>";
			foreach ($this->arrSection[$section_name]["link"] as $key => $val){
				$badge = ($val['badge']!="")? "<span class='badge badge-danger'>".$val['badge']."</span>" : "";
				$html.= "<div align='center' nowrap class='float-left m-3 icon-box'>$badge<a href='".$val["link_href"]."'>";
				if ($val['imgsrc']!="")
					$html.= "<img src='".ADMIN_URL_IMAGES."/".$val["imgsrc"]."'/><br />";
				$html.= "<span>".$val["link_name"]."</span>";
				$html.= "</a></div>";
			}
			$html.= "<div class='clearfix'></div></div>";		
		}
		return $html;
	}
	/**
	 * Old Function: Expand Section
	 * 
	 * @param unknown $section_name
	 */
	function expandSection($section_name){
		/* if ($this->arrSection[$section_name]["totallink"]>0)
		$this->onLoadFunc.= "expandSection('tbl_$section_name', 'img_$section_name');"; */
	}
	/**
	 * Old Function: Collapse Section
	 * 
	 * @param unknown $section_name
	 */
	function collapseSection($section_name){
		/* if ($this->arrSection[$section_name]["totallink"]>0)	
		$this->onLoadFunc.= "collapseSection('tbl_$section_name', 'img_$section_name');"; */
	}
	/**
	 * Show onload function
	 * 
	 * @return string
	 */
	function showOnLoadFunc(){
		$html.= "<script language='javascript'>";
		$html.= "function onLoadFunc(){";
		$html.= $this->onLoadFunc;
		$html.= "}"; 
		$html.= "</script>";
		return $html;
	}
	/**
	 * Show all Section
	 * 
	 * @return string
	 */
	function showAllSection(){
		if (is_array($this->arrSection))
		foreach ($this->arrSection as $key => $val){
			$html.= $this->showSectionDiv($key);
		}
		return $html;
	}
	/**
	 * Display Menu of Admin
	 * 
	 * @return string
	 */
	function showMenuHeader(){
		global $_SITE_ROOT, $core;
		$html = "";
		$active_section = $this->getCurrentSection();
		if (is_array($this->arrSection))
		foreach ($this->arrSection as $section_name => $val)
		if ($val['totallink']>0){
			$img = ($val['imgsrc']!="")? "<img src='".ADMIN_URL_IMAGES."/".$val['imgsrc']."' border='0' align='left' >" : "";
			
			$submenu = "";
			if (is_array($this->arrSection[$section_name]["link"])){				
				foreach ($this->arrSection[$section_name]["link"] as $k => $v){
					$img = ($val['imgsrc']!="")? "<img src='".ADMIN_URL_IMAGES."/".$v['imgsrc']."' border='0' align='middle' style='width:14px;height:14px;' >" : "";
					$active_sub = ($this->active_site_name==$v['site_name'])? "active" : "";
					$badge = ($v['badge']!="")? "(".$v['badge'].")" : "";
					$submenu.= "<a class='dropdown-item $active_sub' href='".$v["link_href"]."'>$img ".$v["link_name"]." $badge</a>";
				}
			}
			$img = ($val['imgsrc']!="")? "<img src='".ADMIN_URL_IMAGES."/".$val['imgsrc']."' border='0' align='middle' >" : "";
			$active = ($active_section==$section_name)? "active" : "" ;
			$html .= '<li class="nav-item dropdown '.$active.'">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink'.$section_name.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          '.$img.' '.$core->getLang($val['title']).' 
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink'.$section_name.'">
				          '.$submenu.'
				        </div>
				      </li>';		
			
		}
	
		return $html;
	}
	
	/**
	 * Old Function
	 * 
	 * @return string
	 */
	function showNaviHeader(){
		global $_SITE_ROOT, $core;
		$html = "";
		if (is_array($this->arrSection))
		foreach ($this->arrSection as $key => $val)
		if ($val['totallink']>0){		
			$img = ($val['imgsrc']!="")? "<img src='".ADMIN_URL_IMAGES."/".$val['imgsrc']."' border='0' align='left' >" : "";
			$html .= "<td class='menubutton' id='m_".$key."' nowrap  width='".$val['width']."' onmouseover=\"this.className='menubuttonActive';menuLayers.show('menu_".$key."', event, 'm_".$key."')\" onmouseout=\"this.className='menubutton';menuLayers.hide()\" title='".$core->getLang($val['title'])."'>".$img."&nbsp;&nbsp;".$core->getLang($val['short_title'])."</td>";
		}
		
		return $html;
	}
	/**
	 * Old Function
	 * 
	 * @param unknown $section_name
	 * @param string $class_name
	 * @return string
	 */
	function showDivHeader($section_name, $class_name='menu'){
		global $_SITE_ROOT, $core;
		$html= "
<div id='menu_$section_name' class='$class_name'>
  <ul>";
		if (is_array($this->arrSection[$section_name]["link"]))
		foreach ($this->arrSection[$section_name]["link"] as $key => $val){
			$html.= "<li><a href='".$val["link_href"]."'>&curren; ".$val["link_name"]."</a></li>";
		}	
		$html.= "
  </ul>
</div>		
		";
		return $html;
	}
}
?>