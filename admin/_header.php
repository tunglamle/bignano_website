<?
/******************************************************
 * Admin Header File
 * Load before module file called
 *
 * Project Name               :  Learning Themes DVS
 * Package Name                    :
 * Program ID                 :  header.php
 * Environment                :  PHP  version version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  2014/02/10
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        2014/02/10        TuanTA          -        -     -     -
 *
 ********************************************************/
/*Language START*/
$clsLanguage = new Language();
$lang_code = getPOST("lang_code", LANG_DEFAULT);//Ngôn ngữ dữ liệu
$lang_code_name = $clsLanguage->getName($lang_code);
$_LANG_ID_NAME = $clsLanguage->getName($_LANG_ID);//Ngôn ngữ của giao diện
/*Language END*/

#Button Navigation
$clsButtonNav = new  ButtonNav();

$default_permiss_name = array(
    "category_default_default" => "[" . $core->getLang("ContentManagement") . "] " . $core->getLang("Categories"),
    "articles_default_default" => "[" . $core->getLang("ContentManagement") . "] " . $core->getLang("Articles"),
    "comments_default_default" => "[" . $core->getLang("ContentManagement") . "] " . $core->getLang("FeedbackNews"),
    "pages_default_default" => "[" . $core->getLang("ContentManagement") . "] " . $core->getLang("Pages"),

    "menu_default_default" => "[" . $core->getLang("SettingManagement") . "] " . $core->getLang("Menu"),
    "settings_default_generalsetting" => "[" . $core->getLang("SettingManagement") . "] " . $core->getLang("GeneralSettings"),
    "settings_default_catsetting" => "[" . $core->getLang("SettingManagement") . "] " . $core->getLang("FrontEndSettings"),
    "settings_default_seosetting" => "[" . $core->getLang("SettingManagement") . "] " . $core->getLang("SEOSettings"),
    "settings_default_adsense" => "[" . $core->getLang("SettingManagement") . "] " . $core->getLang("WebmasterTools"),

    "accessstats_default_default" => "[" . $core->getLang("SystemManagement") . "] " . $core->getLang("AccessStats"),
    "adminmanager_default_default" => "[" . $core->getLang("SystemManagement") . "] " . $core->getLang("AdminManagement"),

);
$default_permiss_key = array_keys($default_permiss_name);
$default_permiss_array = array();
foreach ($default_permiss_key as $key => $val) {
    $default_permiss_array[$val] = 0;
}
$clsLessons = new Lessons();
$clsPromotion = new Promotion();
$clsUsers = new Users();
$clsOrders = new Orders();
$clsCategory = new Category();
$clsCourses = new Courses();
$clsArticles = new Articles();
$clsFeedBacks = new FeedBacks();
$clsComments = new Comments();
$clsPages = new Pages();
$clsStage = new Stage();
$clsRegion = new Region();
$clsCountry = new Country();
$clsProvince = new Province();
$clsDistrict = new District();
$clsSliders = new Sliders();
$clsProduct = new Product();
$clsAdver = new Adver();
$clsLibrary = new Library();
$clsAbout = new About();
$clsCsr = new Csr();
$clsFor_investor = new For_investor();
$clsJob = new Job();

$total_lessons = $clsLessons->countItem();
$total_promotion = $clsPromotion->countItem();
$total_orders = $clsOrders->countItem("status=0");
$total_users = $clsUsers->countItem("is_active>=0 AND user_group_id != 6");
$total_cates = $clsCategory->countItem("is_online=1 AND lang_code='$lang_code'");
$total_courses = $clsCourses->countItem("lang_code='$lang_code'");
$total_stages = $clsStage->countItem("is_online=1 AND lang_code='$lang_code'");
$total_articles = $clsArticles->countItem("is_online=1 AND lang_code='$lang_code' ");
$total_library = $clsLibrary->countItem("is_online=1 AND lang_code='$lang_code' ");
$total_product = $clsProduct->countItem("is_online=1 AND lang_code='$lang_code' ");
$total_feedbacks = $clsFeedBacks->countItem("is_read=0 AND lang_code='$lang_code'");
$total_comments = $clsComments->countItem("is_online=1 AND lang_code='$lang_code'");
$total_pages = $clsPages->countItem("is_online=1 AND lang_code='$lang_code'");
$total_about = $clsAbout->countItem("is_online=1 AND lang_code='$lang_code'");
$total_csr = $clsCsr->countItem("is_online=1 AND lang_code='$lang_code'");
$total_for_investor = $clsFor_investor->countItem("is_online=1 AND lang_code='$lang_code'");
$total_job = $clsJob->countItem("is_online=1 AND lang_code='$lang_code'");
$total_regions = $clsRegion->countItem("is_online=1");
$total_country = $clsCountry->countItem("is_online=1");
$total_provinces = $clsProvince->countItem("is_online=1");
$total_districts = $clsDistrict->countItem("1");
$total_sliders = $clsSliders->countItem("is_online=1");
$total_advers = $clsAdver->countItem("is_online=1");
unset($clsArticles, $clsFeedBacks, $clsComments, $clsPages);

$clsCP->addSection("course", $core->getLang("ContentManagement"), "Courses", "product.png");
//$clsCP->addLink("course", "course_default_default", $core->getLang("Khóa học"), "?mod=course&reset", "largeicon/courses.png", $total_courses);
//$clsCP->addLink("course", "library_default_default", $core->getLang("Dự án"), "?mod=library", "largeicon/slider.png", $total_library);
//$clsCP->addLink("course", "promotion_default_default", $core->getLang("Khuyến mại"), "?mod=promotion", "largeicon/promotion.png", $total_promotion);
//$clsCP->addLink("course", "orders_default_default", $core->getLang("Orders"), "?mod=orders", "largeicon/order.png", $total_orders);
$clsCP->addLink("course", "product_default_default", $core->getLang("Product"), "?mod=product", "largeicon/icon_lamp.png", $total_product);
//$clsCP->addLink("course", "filter_default_default", $core->getLang("Filter"), "?mod=filter", "largeicon/filter_data.png");
$clsCP->addLink("course", "articles_default_default", $core->getLang("News"), "?mod=articles&reset", "largeicon/articles.png", $total_articles);
$clsCP->addLink("course", "about_default_default", $core->getLang("About"), "?mod=about&reset", "largeicon/articles.png", $total_about);
//$clsCP->addLink("course", "csr_default_default", $core->getLang("Csr"), "?mod=csr&reset", "largeicon/articles.png", $total_csr);
$clsCP->addLink("course", "for_investor_default_default", $core->getLang("For Investor"), "?mod=for_investor&reset", "largeicon/articles.png", $total_for_investor);
//$clsCP->addLink("course", "job_default_default", $core->getLang("Job"), "?mod=job&reset", "largeicon/articles.png", $total_job);

$clsCP->addSection("content", $core->getLang("ContentManagement"), "Content", "content.png");
$clsCP->addLink("content", "pages_default_default", $core->getLang("Pages"), "?mod=pages", "largeicon/page.png", $total_pages);
$clsCP->addLink("content", "adver_default_default", $core->getLang("Advertisment"), "?mod=adver", "largeicon/adver.png", $total_advers);
$clsCP->addLink("content", "feedbacks_default_default", $core->getLang("Contact"), "?mod=feedbacks", "largeicon/feedback.png", $total_feedbacks);
$clsCP->addLink("content", "search_app_default_default", $core->getLang("Search Application"), "?mod=search_app", "largeicon/filter_data.png");
$clsCP->addLink("content", "search_pro_default_default", $core->getLang("Search Properties"), "?mod=search_pro", "largeicon/filter_data.png");
$clsCP->addLink("content", "year_default_default", $core->getLang("Thêm Năm"), "?mod=year", "largeicon/diembanhang.png");
$clsCP->addLink("content", "typical_default_default", $core->getLang("Typycal Application"), "?mod=typical&reset", "largeicon/articles.png");
$clsCP->addLink("content", "email_default_default", $core->getLang("D/S tìm hiểu sản phẩm"), "?mod=email", "largeicon/newsletter-icon.png");
//$clsCP->addLink("content", "regis_agency_default_default", $core->getLang("Đăng ký làm đại lý"), "?mod=regis_agency", "largeicon/newsletter-icon.png");
//$clsCP->addLink("content", "agency_default_default", $core->getLang("Đại lý"), "?mod=agency", "largeicon/diembanhang.png");
//$clsCP->addLink("content", "comments_default_default", $core->getLang("Feedback"), "?mod=comments", "largeicon/comment.png", $total_comments);
//$clsCP->addLink("content", "gallery_default_default", $core->getLang("Gallery"), "?mod=gallery", "largeicon/thuvienanh.png");


$clsCP->addSection("settings", $core->getLang("Setting"), "Setting", "setting.png", "140px");
$clsCP->addLink("settings", "category_default_default", $core->getLang("Categories"), "?mod=category&reset", "largeicon/folder.png");
//$clsCP->addLink("settings", "country_default_default", $core->getLang("Country"), "?mod=country&reset", "largeicon/country.png", $total_country);
//$clsCP->addLink("settings", "region_default_default", $core->getLang("Region"), "?mod=region&reset", "largeicon/region.png", $total_regions);
//$clsCP->addLink("settings", "province_default_default", $core->getLang("Province"), "?mod=province&reset", "largeicon/province.png", $total_provinces);
//$clsCP->addLink("settings", "district_default_default", $core->getLang("District"), "?mod=district", "largeicon/district.png", $total_districts);
$clsCP->addLink("settings", "menu_default_default", $core->getLang("Menu"), "?mod=menu&reset", "largeicon/menu.png");
//$clsCP->addLink("settings", "sliders_default_default", $core->getLang("Sliders"), "?mod=sliders", "largeicon/slider.png", $total_sliders);
$clsCP->addLink("settings", "settings_default_generalsetting", $core->getLang("GeneralSettings"), "?mod=settings&act=generalsetting", "largeicon/config.png");
$clsCP->addLink("settings", "settings_default_catsetting", $core->getLang("FrontEndSettings"), "?mod=settings&act=catsetting", "largeicon/configfront.png");
$clsCP->addLink("settings", "settings_default_seosetting", $core->getLang("SEOSettings"), "?mod=settings&act=seosetting", "largeicon/settingseo.png");
$clsCP->addLink("settings", "settings_default_admintool", $core->getLang("WebmasterTools"), "?mod=settings&act=admintool", "largeicon/webmaster_tools.png");
// $clsCP->addLink("settings", "settings_default_email", $core->getLang("EmailConfigs"), "?mod=settings&act=email", "largeicon/inbox.png");

$clsCP->addSection("system", $core->getLang("System"), "System", "system.png", "130px");
//$clsCP->addLink("system", "users_default_default", $core->getLang("Học viên"), "?mod=users", "largeicon/user.png", $total_users);
$clsCP->addLink("system", "settings_default_editlang", $core->getLang("Edit_Language"), "?mod=settings&act=editlang", "largeicon/translate.png");
$clsCP->addLink("system", "accessstats_default_default", $core->getLang("AccessStats"), "?mod=accessstats", "largeicon/websitestats.png");
$clsCP->addLink("system", "adminmanager_default_default", $core->getLang("AdminManagement"), "?mod=adminmanager&reset", "largeicon/useradmin.png");

$clsRecycleBin = new RecycleBin();
$totalItem = $clsRecycleBin->Count();
$icon_recyclebin = $clsRecycleBin->getIcon($totalItem);
unset($clsRecycleBin);
if ($core->isSuper()) {
    $clsCP->addLink("system", "recyclebin_default_default", $core->getLang("RecycleBin"), "?mod=recyclebin", "largeicon/$icon_recyclebin");
}
/*Setting Loader START*/
$clsSettings = new Settings();
$_CONFIG = $clsSettings->getAllSettings($lang_code);
unset($clsSettings);
/*Setting Loader END*/
?>
