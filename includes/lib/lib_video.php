<?
/******************************************************
 * Library Video
 *
 * Contain validate functions for project
 *
 * Project Name               :  ClientWebsite
 * Package Name                    :
 * Program ID                 :  lib_video.php
 * Environment                :  PHP  version 4, 5 ,7
 * Author                     :  Banglcb
 * Version                    :  1.0
 * Creation Date              :  17/04/2019
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        17/04/2019        banglcb          -        -     -     -
 *
 ********************************************************/

function getVideoInfo($filename)
{
    if (!file_exists(DIR_INCLUDES . "/getid3/getid3.php")) {
        return 0;
    }
    require_once(DIR_INCLUDES . "/getid3/getid3.php");
    $getID3 = new getID3();
    return (object)$getID3->analyze(DIR_UPLOADS . "/$filename");
}

function getDuration($videoID)
{
    $apikey = "AIzaSyB65fH4nol-AJtx6QIpEQzzPdOmt_mQ2y0"; // Like this AIcvSyBsLA8znZn-i-aPLWFrsPOlWMkEyVaXAcv
    $dur = get_html_content("https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=$videoID&key=$apikey");
    $VidDuration = json_decode($dur, true);
    if (is_array($VidDuration['items']) && count($VidDuration['items']) > 0) {
        foreach ($VidDuration['items'] as $vidTime) {
            $VidDuration = $vidTime['contentDetails']['duration'];
        }
        $date = new DateTime('2000-01-01');
        $date->add(new DateInterval($VidDuration));
        $vid_durH = $date->format('H');
        if ($vid_durH == "00") {
            $vid_dur = $date->format('i:s');
        } else {
            $vid_dur = $date->format('H:i:s');
        }
        return $vid_dur;
    }
    return null;
}

?>