<?

/******************************************************
 * Class Cart
 *
 * Cart Handling
 *
 * Project Name               :  DVS Project
 * Package Name                    :
 * Program ID                 :  clsCart.php
 * Environment                :  PHP  version 5,7
 * Author                     :  banglcb
 * Version                    :  1.0
 * Creation Date              :  2019/20/05
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        2019/20/05        banglcb          -        -     -     -
 *
 ********************************************************/
class Cart
{
    var $totalItem = 0;
    var $arrItem = array();
    var $arrItemId = array();
    function Cart()
    {
        if (vnSessionExist("cart_totalItem")) {
            $this->totalItem = vnSessionGetVar("cart_totalItem");
            if ($this->totalItem > 0) {
                $s_arrItem = vnSessionGetVar("cart_arrItem");
                $s_arrItemId = vnSessionGetVar("cart_arrItemId");
                $this->arrItem = @unserialize($s_arrItem);
                $this->arrItemId = @unserialize($s_arrItemId);
            }
        }
    }

    /**
     * Add an item to cart
     *
     * @param string $_itemId
     * @param string $_name
     * @param string $_image
     * @param number $_price
     * @param number $_discount_value
     * @param number $_discount_type
     * @param number $_VAT
     * $_itemId="", $_name="", $_image="", $_price=0, $_quantity=0, $_discount_value=0, $_discount_type=0, $_VAT=10
     * $_itemId, $_name, $_image, $_price, $_quantity, $_discount_value, $_discount_type, $_VAT
     */
    function addCart($item)
    {
        extract($item);
        $_SESSION['a'][] = 1;
        if ($this->arrItemId[$itemId] > -1) {
            $i = $this->arrItemId[$itemId];
            $this->arrItem[$i]->quantity += $quantity;
            vnSessionSetVar("cart_arrItem", @serialize($this->arrItem));
        } else {
            $objCartItem = new CartItem($item);
            $this->arrItem[$this->totalItem] = $objCartItem;
            $this->arrItemId[$itemId] = $this->totalItem;
            $this->totalItem++;
            vnSessionSetVar("cart_arrItem", @serialize($this->arrItem));
            vnSessionSetVar("cart_totalItem", $this->totalItem);
            vnSessionSetVar("cart_arrItemId", @serialize($this->arrItemId));
        }
        unset($objCartItem);
    }

    /**
     * Update quantity of 1 course
     *
     * @param number $_itemId
     * @param number $_quantity
     */
    function updateCart($quantity){
        if (is_array($quantity))
            foreach ($quantity as $key => $val){
                $this->arrItem[$key]->quantity = $val;
            }
        vnSessionSetVar("cart_arrItem", @serialize($this->arrItem));
        vnSessionSetVar("cart_totalItem", $this->totalItem);
    }

    /**
     * Update quantity of all courses in cart
     *
     * @param unknown $quantity , Mảng $quantity[key] => value, với key là ItemId, value là số lượng
     */
    function updateCartQuantity($quantity)
    {
        $has_zero = FALSE;
        if (is_array($quantity))
            foreach ($quantity as $key => $val) {
                $i = $this->arrItemId[$key];
                $this->arrItem[$i]->quantity = $val;
                if ($val == 0) $has_zero = TRUE;
            }
        if ($has_zero && $this->totalItem > 0) {
            $tmp = array();
            $this->arrItemId = array();
            $i = 0;
            foreach ($this->arrItem as $key => $val) {
                if ($val->quantity > 0) {
                    $tmp[$i] = $val;
                    $this->arrItemId[$val->itemId] = $i;
                    $i++;
                }
            }
            $this->arrItem = $tmp;
            $this->totalItem = $i;
            vnSessionSetVar("cart_arrItemId", @serialize($this->arrItemId));
        }
        vnSessionSetVar("cart_arrItem", @serialize($this->arrItem));
        vnSessionSetVar("cart_totalItem", $this->totalItem);
    }

    /**
     * Remove item_id from cart
     *
     * @param string $_itemId
     */
    function removeCart($_itemId=""){
        if ($this->arrItemId[$_itemId]>-1){
            $k = $this->arrItemId[$_itemId];
            unset($this->arrItem[$k]);
            unset($this->arrItemId);
            $this->arrItem = array_values($this->arrItem);
            $this->totalItem--;
            foreach ($this->arrItem as $k => $v){
                $itemId = $v->itemId;
                $this->arrItemId[$itemId] = $k;
            }
            vnSessionSetVar("cart_arrItem", @serialize($this->arrItem));
            vnSessionSetVar("cart_totalItem", $this->totalItem);
            vnSessionSetVar("cart_arrItemId", @serialize($this->arrItemId));
        }
    }

    /**
     * Clear all item from cart
     */
    function clearCart()
    {
        $this->totalItem = 0;
        $this->arrItem = array();
        vnSessionDelVar("cart_arrItem");
        vnSessionDelVar("cart_totalItem");
        vnSessionDelVar("cart_arrItemId");
    }

    /**
     * Count quantity in cart
     *
     * @return number
     */
    function getTotalQuantity()
    {
        if ($this->totalItem == 0) return 0;
        $q = 0;
        foreach ($this->arrItem as $key => $val) {
            $q += intval($val->quantity);
        }
        return $q;
    }

    /**
     * Count total price in cart
     *
     * @param string $incl_VAT
     * @return number
     */
    function getTotalPrice($incl_VAT = true)
    {
        if ($this->totalItem == 0) return 0;
        $sum = 0;
        foreach ($this->arrItem as $key => $val) {
            $price = floatval($val->price) - floatval($val->discount_value);
            if ($val->discount_type == 0) {
                $price = floatval($val->price) - ((floatval($val->price) * floatval($val->discount_value)) / 100);
            }
            $price = $price * $val->quantity;
            if ($incl_VAT)
                $price = $price + ($price * $val->VAT) / 100;
            $sum += $price;
        }
        return $sum;
    }

    /**
     * Get array of price
     *
     * @param string $incl_VAT
     * @return multitype:number
     */
    function getPrices($incl_VAT = true)
    {
        $arr = array();
        $sum = 0;
        if ($this->totalItem > 0) {
            foreach ($this->arrItemId as $key => $i) {
                $val = $this->arrItem[$i];
                $price = floatval($val->price) - floatval($val->discount_value);
                if ($val->discount_type == 0) {
                    $price = floatval($val->price) - ((floatval($val->price) * floatval($val->discount_value)) / 100);
                }
                $price = $price * $val->quantity;
                if ($incl_VAT)
                    $price = $price + ($price * $val->VAT) / 100;
                $arr[$key] = $price;
                $sum += $price;
            }
        }
        $arr['sum'] = $sum;
        return $arr;
    }

    /**
     * Get all item in cart
     *
     * @return array
     */
    function getAllItem()
    {
        return $this->arrItem;
    }

    /**
     * Get all item_ID in cart
     *
     * @return array
     */
    function getAllItemId()
    {
        return $this->arrItemId;
    }

    function getOneItem($itemId)
    {
        foreach ($this->arrItem as $item) {
            if ($itemId == $item->itemId) {
                return $item;
            }
        }
        return 0;
    }
}

class CartItem
{
    var $itemId = "";
    var $name = "";
    var $image = "";
    var $price = 0;
    var $quantity = 0;
    var $discount_value = 0;//discount value
    var $discount_type = 0;//0: percent, 1:cash
    var $VAT = 10;

    /**
     * Init class
     *
     * @param string $_itemId
     * @param string $_name
     * @param string $_image
     * @param number $_price
     * @param number $_quantity
     * @param number $_discount_value
     * @param number $_discount_type
     * @param number $_VAT
     * $_itemId="", $_name="", $_image="", $_price=0, $_quantity=0, $_discount_value=0, $_discount_type=0, $_VAT=10
     */
    function CartItem($item)
    {
        extract($item);
        $this->itemId = $itemId;
        $this->name = $name;
        $this->image = $image;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->discount_value = $discount_value;
        $this->discount_type = $discount_type;
        $this->VAT = $VAT;
    }

    /**
     * Copy from an object
     *
     * @param unknown $objCartItem
     */
    function copyObj($objCartItem)
    {
        $this->itemId = $objCartItem->itemId;
        $this->name = $objCartItem->name;
        $this->image = $objCartItem->image;
        $this->price = $objCartItem->price;
        $this->quantity = $objCartItem->quantity;
        $this->discount_value = $objCartItem->discount_value;
        $this->discount_type = $objCartItem->discount_type;
        $this->VAT = $objCartItem->VAT;
    }
}

?>