<?

/******************************************************
 * Class Product
 *
 * Post Handling
 *
 * Project Name               :  mamnemdican.vn
 * Package Name                    :
 * Program ID                 :  class_News.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  04/10/2017
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        04/10/2017        Ducnh          -        -     -     -
 *
 ********************************************************/
class Product extends DbBasic
{
    function Product()
    {
        $this->pkey = "product_id";
        $this->tbl = "_product";
    }

    //SELECT
    function getAllSimple($cond)
    {
        global $dbconn;
        $sql = "SELECT a.product_id, a.slug, a.sapo, a.lang_code, a.cat_id, a.name, a.image, a.image_newlaunching, a.reg_date, a.view_num, a.is_online, a.is_new, a.user_id,
						b.user_name, b.fullname, c.cat_id, c.name AS cat_name, c.slug AS cat_slug
				FROM $this->tbl AS a
				INNER JOIN _users AS b ON a.user_id = b.user_id
				INNER JOIN _category AS c ON a.cat_id = c.cat_id
				WHERE $cond";
        return $dbconn->GetAll($sql);
    }

    function getAllSimple2($cond)
    {
        global $dbconn;
        $sql = "SELECT product_id, slug, lang_code, cat_id, name, image, reg_date, view_num, is_online, user_id, sapo, link, list_image, list_icon
                    FROM $this->tbl	
                    WHERE $cond";
        return $dbconn->GetAll($sql);
    }

    function getOneSimple2($product_id)
    {
        global $dbconn;
        $sql = "SELECT product_id, slug, lang_code, cat_id, product_code, name, image, reg_date, view_num, is_ofs, is_online, is_hot, user_id
				FROM $this->tbl
				WHERE product_id=$product_id";
        return $dbconn->GetRow($sql);
    }

    function getOderBy($order)
    {
        switch ($order) {
            case 'nameaz':
                $orderby = " ORDER BY sapo ASC";
                break;
            case 'datedesc':
                $orderby = " ORDER BY reg_date DESC";
                break;
            case 'dateasc':
                $orderby = " ORDER BY reg_date ASC";
                break;
            default:
                $orderby = " ORDER BY reg_date DESC";
                break;
        }
        return $orderby;
    }

    function checkProductID($product_id)
    {
        $arrOneProduct = $this->getOne($product_id);
        if (is_array($arrOneProduct)) {
            return true;
        } else {
            return false;
        }
    }

    function getIDProductViewed($product_id)
    {
        $settimecookie = time() + (86400 * 15);//15 ngày
        if ($this->checkProductID($product_id)) {
            $pro_viewing = getCookie("pro_viewing");
            if ($pro_viewing == '') {
                setcookie("pro_viewing", $product_id, $settimecookie, "/");
            } else {
                if (!strpos("$pro_viewing", "$product_id") && strpos("$pro_viewing", "$product_id") !== 0) {//Nếu Product ID chưa xem
                    $cookie_value = $pro_viewing . "," . $product_id;
                    setcookie("pro_viewing", $cookie_value, $settimecookie, "/");
                }
            }
        }
    }

    function getIdFromName($name = "")
    {
        global $dbconn;
        $slug = utf8_nosign_noblank($name);
        $sql = "SELECT product_id FROM _product WHERE name='$name' || slug='$slug' || product_code = '$name'";
        $arr = $dbconn->GetRow($sql);
        return (is_array($arr) && $arr[$this->pkey] > 0) ? $arr[$this->pkey] : 0;
    }

    function getSmallImage($product_id)
    {
        $arr = $this->getOne($product_id);
        return ($arr['image'] != "") ? $arr['image'] : "nopic.jpg";
    }

    function getCatUrl($product_id)
    {
        global $dbconn;
        $arr = $this->getOne($product_id);
        $clsCategory = new Category();
        $oneCat = $clsCategory->getOne($arr['cat_id']);
        return url_category($oneCat);
    }

    function getCatName($product_id)
    {
        global $dbconn;
        $arr = $this->getOne($product_id);
        $clsCategory = new Category();
        return $clsCategory->getCatName($arr['cat_id']);
    }

    /**
     * Get product string
     */
    function getAllProductStr()
    {
        global $dbconn;
        $sql = "SELECT product_code, name, slug FROM _product WHERE is_online=1";
        $arr = $dbconn->GetAll($sql);
        $str = "";
        if (is_array($arr))
            foreach ($arr as $key => $val) {
                $str .= ($str == "") ? "'" . $val['name'] . "'" : ",'" . $val['name'] . "'";
                $str .= ($str == "") ? "'" . $val['product_code'] . "'" : ",'" . $val['product_code'] . "'";
            }
        return $str;
    }

    /**
     * Get list id of related product from string
     * @param string $str
     */
    function getRelatedId($str = "")
    {
        global $dbconn;
        $arr = explode(',', $str);
        $strid = "";
        if (is_array($arr))
            foreach ($arr as $key => $val) {
                $name = trim($val);
                if ($name == "") continue;
                $id = $this->getIdFromName($name);
                if ($id > 0) {
                    $strid .= ($strid == "") ? $id : "," . $id . "";
                }
            }
        return $strid;
    }

    function getListRelated($str = 0)
    {
        global $dbconn;
        $arr = explode(',', $str);
        $arrItem = array();
        if (is_array($arr))
            foreach ($arr as $key => $val) {
                $id = trim($val);
                if ($id == "") continue;
                $tmp = $this->getOneSimple2($id);
                if (is_array($tmp)) {
                    $arrItem[] = $tmp;
                }
            }
        return $arrItem;
    }
    //INSERT
    //UPDATE
    //Cap nhat luot xem
    function incView($product_id = 0)
    {
        return $this->updateOne($product_id, "view_num=view_num+1");
    }
    //DELETE
    //CLONE
    function cloneOne($product_id = 0)
    {
        global $dbconn;
        $arr = $this->getOne($product_id);
        $fields = "";
        $values = "";
        if (is_array($arr))
            foreach ($arr as $k => $v)
                if ($k != $this->pkey) {
                    $fields .= ($fields == "") ? "$k" : ", $k";
                    $values .= ($values == "") ? "'$v'" : ", '$v'";
                }
        $sql = "INSERT INTO " . $this->tbl . "($fields) VALUES($values)";
        $dbconn->Execute($sql);
        return 1;
    }

    //Lấy danh sách bộ lọc từ id sản phẩm
//    function getRootIdFilterByProduct($product_id)
//    {
//        global $dbconn;
//        $clsFilter = new Filter();
//        $clsCategory = new Category();
//        $clsCategory->getParentArray();
//        $arrOneProduct = $this->getOne($product_id);
//        if (is_array($arrOneProduct)) {
//            $cat_id = $arrOneProduct['cat_id'];
//            $cat_id_root = $clsCategory->getRootCat($cat_id);
//            $filter_id = $clsFilter->getIdFilterByCatId($cat_id_root);
//            return $filter_id;
//        } else {
//            return 0;
//        }
//    }
}

?>