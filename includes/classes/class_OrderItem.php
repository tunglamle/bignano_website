<?

/******************************************************
 * Class Cougory
 *
 * Cougory Handling
 *
 * Project Name               :  Learning Themes DVS
 * Package Name                    :
 * Program ID                 :  class_OrderItem.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  Banglcb
 * Version                    :  1.0
 * Creation Date              :  2019/05/17
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        2014/02/10        Banglcb          -        -     -     -
 *
 ********************************************************/
class OrderItem extends DbBasic
{
    function OrderItem()
    {
        $this->pkey = "oi_id";
        $this->tbl = "_order_item";
    }

    function getAllSimple($cond = "")
    {
        global $dbconn;
        $sql = "SELECT a.*,o.user_id,c.name,c.image,c.slug 
                FROM $this->tbl AS a 
                INNER JOIN _product AS c ON a.product_id = c.product_id
                INNER JOIN _orders AS o ON a.order_code = o.order_code";
        if ($cond != "") {
            $sql .= " WHERE $cond";
        }
        $sql .= " GROUP BY a.product_id";
        return $dbconn->GetAll($sql);

    }


    function activeCourse($active_key)
    {
        $arrOneActiveKey = $this->getByCond("active_key='$active_key'");
        if (is_array($arrOneActiveKey) && count($arrOneActiveKey) > 0) {
            if ($arrOneActiveKey['is_active'] == 0) {
                $clsCourses = new Courses();
                $arrOneCourse = $clsCourses->getOne($arrOneActiveKey['course_id']);
                $active_time = time();
                $expired_time = 0;
                if ($arrOneCourse['duration'] > 0){
                    $expired_time = strtotime("+$arrOneCourse[duration] month", $active_time);
                }
                $this->updateByCond("active_key = '$active_key'", "active_time = $active_time, expired_time = $expired_time, is_active = 1");
                return 0;
            }
            return 1;
        }
        return 2;
    }

    /**
     * insert item
     * @param string $order_code
     * @param int $course_id
     * @param int $quantity
     * @param int $price
     * @param int $discount_type
     * @param int $discount_value
     * @param int $order_date
     * @return int
     */
    function addOrderItem($order_code, $product_id, $quantity, $price, $discount_type, $discount_value, $order_date)
    {
        $active_key = $this->generateSerialNumber();
        $fields = "order_code, product_id, quantity, price, discount_type, discount_value, active_key, order_date, is_active";
        $values = "'$order_code', $product_id, $quantity, $price, $discount_type, $discount_value, '$active_key', $order_date, 0";
        return $this->insertOne($fields, $values);
    }

    /**
     * Create new active key
     */
    function generateSerialNumber()
    {
        $template = 'XX99-99XX-9XXX-XXXX-XX99-X99X-9XX9';
        $k = strlen($template);
        $serialNumber = '';
        for ($i = 0; $i < $k; $i++) {
            switch ($template[$i]) {
                case 'X':
                    $serialNumber .= chr(rand(65, 90));
                    break;
                case '9':
                    $serialNumber .= rand(0, 9);
                    break;
                case '-':
                    $serialNumber .= '-';
                    break;
            }
        }
        $arrOneActiveKey = $this->getByCond("active_key='$serialNumber'");
        if (is_array($arrOneActiveKey) && count($arrOneActiveKey) > 0) {
            return $this->generateSerialNumber();
        }
        return $serialNumber;
    }
}

?>