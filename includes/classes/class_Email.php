<?
/******************************************************
 * Class Page
 *
 * Static Page Handling
 * 
 * Project Name               :  FTS-USSH
 * Package Name            		:  
 * Program ID                 :  class_Email.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  2014/02/10
 *
 * Modification History     :
 * Version    Date            Person Name  		Chng  Req   No    Remarks
 * 1.0       	2014/02/10    	TuanTA          -  		-     -     -
 *
 ********************************************************/
class Email extends dbBasic{
	function Email(){
		$this->pkey = "mail_id";
		$this->tbl 	= "_mail";
	}
	
	//INSERT
	//UPDATE
	//DELETE
	//OTHER
}

?>