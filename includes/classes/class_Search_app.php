<?
/******************************************************
 * Class Page
 *
 * Static Page Handling
 * 
 * Project Name               :  FTS-USSH
 * Package Name            		:  
 * Program ID                 :  class_Email.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  2014/02/10
 *
 * Modification History     :
 * Version    Date            Person Name  		Chng  Req   No    Remarks
 * 1.0       	2014/02/10    	TuanTA          -  		-     -     -
 *
 ********************************************************/
class Search_app extends dbBasic{
	function Search_app(){
		$this->pkey = "search_app_id";
		$this->tbl 	= "_search_app";
	}
}

function makeArrayListApplication($search_app_id = 0, $level = 0, $maxlevel = 5, &$ret, $cond = "")
{
    if ($level == $maxlevel) return "";
    global $dbconn, $lang_code;
    $sql = "SELECT search_app_id, name FROM _search_app WHERE is_online = 1 AND lang_code = '$lang_code'";
    if ($cond != "") $sql .= " AND $cond";
    $sql .= " ORDER BY order_no ASC";
    $arrListApplication11 = $dbconn->GetAll($sql);
    $html = "";
    if (is_array($arrListApplication11)) {
        foreach ($arrListApplication11 as $k => $v) {
            $value = $v["search_app_id"];
            $option = $v["name"];
            $ret["$value"] = str_repeat("&brvbar;--- ", 1) . $option;
            makeArrayListApplication($v["search_app_id"], $level + 1, $maxlevel, $ret, $cond);
        }
        unset($arrListApplication11);
        return "";
    }
    unset($arrListApplication11);
    return "";
}

?>