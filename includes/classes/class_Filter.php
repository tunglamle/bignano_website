<?
/******************************************************
 * Class Filter
 *
 * Filter Handling
 * 
 * Project Name               :  HSgaminggear.com
 * Package Name            		:  
 * Program ID                 :  class_Filter.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  03/01/2018
 *
 * Modification History     :
 * Version    Date            Person Name  		Chng  Req   No    Remarks
 * 1.0       	03/01/2018    	Ducnh          -  		-     -     -
 *
 ********************************************************/
class Filter extends DbBasic{
	function Filter(){
		$this->pkey = "filter_id";
		$this->tbl = "_filter";
	}	
}
?>