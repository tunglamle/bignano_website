<?
/******************************************************
 * Class Filter
 *
 * Filter Handling
 *
 * Project Name               :  HSgaminggear.com
 * Package Name            		:
 * Program ID                 :  class_Filter.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  03/01/2018
 *
 * Modification History     :
 * Version    Date            Person Name  		Chng  Req   No    Remarks
 * 1.0       	03/01/2018    	Ducnh          -  		-     -     -
 *
 ********************************************************/
class Year extends DbBasic{
    function Year(){
        $this->pkey = "year_id";
        $this->tbl = "_year";
    }
}
function makeArrayListYear($year_id = 0, $level = 0, $maxlevel = 5, &$ret, $cond = "")
{
    if ($level == $maxlevel) return "";
    global $dbconn, $lang_code;
    $sql = "SELECT year_id, name FROM _year WHERE is_online = 1";
    if ($cond != "") $sql .= " AND $cond";
    $sql .= " ORDER BY order_no ASC";
    $arrListYear11 = $dbconn->GetAll($sql);
    $html = "";
    if (is_array($arrListYear11)) {
        foreach ($arrListYear11 as $k => $v) {
            $value = $v["name"];
            $option = $v["name"];
            $ret["$value"] = str_repeat("", 1) . $option;
            makeArrayListYear($v["year_id"], $level + 1, $maxlevel, $ret, $cond);
        }
        unset($arrListYear11);
        return "";
    }
    unset($arrListYear11);
    return "";
}
?>