<?

/******************************************************
 * Class Articles (Post)
 *
 * Post Handling
 *
 * Project Name               :  Learning Themes DVS
 * Package Name                    :
 * Program ID                 :  class_Articles.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  Banglcb
 * Version                    :  1.0
 * Creation Date              :  2014/02/10
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        2014/02/10        Banglcb          -        -     -     -
 *
 ********************************************************/
class Job extends DbBasic
{
    function Job()
    {
        $this->pkey = "job_id";
        $this->tbl = "_job";
    }

    //SELECT
    function getAllSimple($cond)
    {
        global $dbconn;
        $sql = "SELECT a.job_id, a.slug, a.lang_code, a.cat_id, a.title, a.link, a.sapo, a.image, a.reg_date, a.view_num, a.is_online, a.is_verify, a.user_id, a.exp_date,				
						b.user_name, b.fullname, b.user_group_id, c.cat_id, c.name AS cat_name, c.slug AS cat_slug
				FROM $this->tbl AS a
				INNER JOIN _users AS b ON a.user_id = b.user_id
				INNER JOIN _category AS c ON a.cat_id = c.cat_id
				WHERE $cond";
        return $dbconn->GetAll($sql);
    }

    function getAllSimple2($cond)
    {
        global $dbconn;
        $sql = "SELECT job_id, slug, lang_code, cat_id, title, sapo, image, reg_date, view_num, is_online, is_verify, user_id, year, menu_id
				FROM $this->tbl	
				WHERE $cond";
        return $dbconn->GetAll($sql);
    }
    //Lấy 1 tin mới nhất
}

?>