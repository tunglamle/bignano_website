<?php

class Youtube
{
    /*
     * Video Id for the given url
     */
    private $video_id;

    /*
     * Video title for the given video
     */
    private $video_title;

    /*
     * Video itag for the given video
     */
    private $itag_info;

    /*
     * Video quality for the given video
     */
    private $quality;

    public function __construct()
    {
        $this->itag_info = array(
            5 => "FLV 400x240",
            6 => "FLV 450x240",
            13 => "3GP Mobile",
            17 => "3GP 144p",
            18 => "MP4 360p",
            22 => "MP4 720p (HD)",
            34 => "FLV 360p",
            35 => "FLV 480p",
            36 => "3GP 240p",
            37 => "MP4 1080",
            38 => "MP4 3072p",
            43 => "WebM 360p",
            44 => "WebM 480p",
            45 => "WebM 720p",
            46 => "WebM 1080p",
            59 => "MP4 480p",
            78 => "MP4 480p",
            82 => "MP4 360p 3D",
            83 => "MP4 480p 3D",
            84 => "MP4 720p 3D",
            85 => "MP4 1080p 3D",
            91 => "MP4 144p",
            92 => "MP4 240p HLS",
            93 => "MP4 360p HLS",
            94 => "MP4 480p HLS",
            95 => "MP4 720p HLS",
            96 => "MP4 1080p HLS",
            100 => "WebM 360p 3D",
            101 => "WebM 480p 3D",
            102 => "WebM 720p 3D",
            120 => "WebM 720p 3D",
            127 => "TS Dash Audio 96kbps",
            128 => "TS Dash Audio 128kbps"
        );
        $this->quality = array(
            5 => "LD",
            6 => "LD",
            13 => "LD",
            17 => "LD",
            18 => "LD",
            22 => "HD",
            34 => "LD",
            35 => "SD",
            36 => "LD",
            37 => "FHD",
            38 => "4K",
            43 => "LD",
            44 => "SD",
            45 => "HD",
            46 => "FHD",
            59 => "SD",
            78 => "SD",
            82 => "LD 3D",
            83 => "SD 3D",
            84 => "HD 3D",
            85 => "FHD 3D",
            91 => "LD",
            92 => "LD",
            93 => "LD",
            94 => "LD",
            95 => "SD",
            96 => "FHD",
            100 => "LD 3D",
            101 => "SD 3D",
            102 => "HD 3D",
            120 => "HD 3D",
            127 => "96kbps",
            128 => "128kbps"
        );
    }

    /*
     * Set the ID
     * @param string
     */
    public function setVideoID($video_id)
    {
        $this->video_id = $video_id;
        return $this;
    }

    /*
     * Get the video information
     * return string
     */
    private function getVideoInfo()
    {
        return get_html_content("https://www.youtube.com/get_video_info?video_id=$this->video_id&cpn=CouQulsSRICzWn5E&eurl&el=adunit", 0);
    }

    /**
     * Get all video stream array
     *
     * @param string $format
     * @return array
     */
    public function getAllStream($format = "")
    {
        //parse the string separated by '&' to array
        parse_str($this->getVideoInfo(), $data);

        //set video title
        $this->video_title = $data["title"];

        //Get the youtube root link that contains video information
        $stream_map_arr = $this->getStreamMap();
        $final_stream_map_arr = array();

        //Create array containing the detail of video
        foreach ($stream_map_arr as $stream) {
            parse_str($stream, $stream_data);
            $stream_data["title"] = $this->video_title;
            $stream_data["mime"] = $stream_data["type"];
            $mime_type = explode(";", $stream_data["mime"]);
            $stream_data["mime"] = $mime_type[0];
            $stream_data["squality"] = $this->quality[$stream_data["itag"]];
            $start = stripos($mime_type[0], "/");
            $stream_data["format"] = ltrim(substr($mime_type[0], $start), "/");
            unset($stream_data["type"]);
            if ($format != "") {
                if ($stream_data["format"] == $format) {
                    $final_stream_map_arr [] = $stream_data;
                }
            } else {
                $final_stream_map_arr [] = $stream_data;
            }
        }
        return $final_stream_map_arr;
    }

    /**
     * Get first video stream array
     * @param string $format
     * @return mixed
     */
    public function getOneStream($format = "")
    {
        $arrStream = $this->getAllStream($format);
        return $arrStream[0];
    }

    /*
     * Get the youtube root data that contains the video information
     * return array
     */
    private function getStreamMap()
    {
        parse_str($this->getVideoInfo(), $data);
        $stream_link = $data["url_encoded_fmt_stream_map"];
        return explode(",", $stream_link);
    }

    /*
     * Validate the given video url
     * return bool
     */
    public function hasVideo()
    {
        $valid = true;
        parse_str($this->getVideoInfo(), $data);
        if ($data["status"] == "fail") {
            $valid = false;
        }
        return $valid;
    }

}