<?
/******************************************************
 * Class Slider
 *
 * Slider Handling
 * 
 * Project Name               :  Shopping Themes DVS
 * Package Name            		:  
 * Program ID                 :  class_Slider.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  2014/02/10
 *
 * Modification History     :
 * Version    Date            Person Name  		Chng  Req   No    Remarks
 * 1.0       	2014/02/10    	TuanTA          -  		-     -     -
 *
 ********************************************************/
class Online extends dbBasic{
	function Online(){
		$this->pkey = "online_id";
		$this->tbl 	= "_online";
	}
	function SetUpOnline($session_id,&$totalOnline,&$isOnline){
	    $curTime = time();
	    $day = date("d",$curTime);
	    $month = date("m",$curTime);
	    $year = date("Y",$curTime);
	    $timeStartDayCurrent = @mktime(0,0,0,"$month","$day","$year");
        $min_time = $curTime - 600;
        $number = $this->countItem("session_id = '$session_id' AND time_start >= '$min_time'");
        if ($number <= 0){
            $this->insertOne("session_id,time_start","'$session_id','$curTime'");
        }
        $isOnline = $this->countItem("time_start >= '$min_time'");
        $totalOnline = $this->countItem("");
    }
}
?>