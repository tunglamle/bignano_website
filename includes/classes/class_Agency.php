<?
/******************************************************
 * Class FeedBacks (Tour + Contact + ...)
 *
 * FeedBack Handling
 *
 * Project Name               :  Learning Themes DVS
 * Package Name            		:
 * Program ID                 :  class_FeedBack.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  Banglcb
 * Version                    :  1.0
 * Creation Date              :  2014/02/10
 *
 * Modification History     :
 * Version    Date            Person Name  		Chng  Req   No    Remarks
 * 1.0       	2014/02/10    	Banglcb          -  		-     -     -
 *
 ********************************************************/
class Agency extends dbBasic{
	function Agency(){
		$this->pkey = "agency_id";
		$this->tbl = "_agency";
	}
}
?>