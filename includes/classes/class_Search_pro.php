<?
/******************************************************
 * Class Page
 *
 * Static Page Handling
 * 
 * Project Name               :  FTS-USSH
 * Package Name            		:  
 * Program ID                 :  class_Email.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  2014/02/10
 *
 * Modification History     :
 * Version    Date            Person Name  		Chng  Req   No    Remarks
 * 1.0       	2014/02/10    	TuanTA          -  		-     -     -
 *
 ********************************************************/
class Search_pro extends dbBasic{
	function Search_pro(){
		$this->pkey = "search_pro_id";
		$this->tbl 	= "_search_pro";
	}
}

function makeArrayListProperties($search_pro_id = 0, $level = 0, $maxlevel = 5, &$ret, $cond = "")
{
    if ($level == $maxlevel) return "";
    global $dbconn, $lang_code;
    $sql = "SELECT search_pro_id, name FROM _search_pro WHERE is_online = 1 AND lang_code = '$lang_code'";
    if ($cond != "") $sql .= " AND $cond";
    $sql .= " ORDER BY order_no ASC";
    $arrListProperties11 = $dbconn->GetAll($sql);
    $html = "";
    if (is_array($arrListProperties11)) {
        foreach ($arrListProperties11 as $k => $v) {
            $value = $v["search_pro_id"];
            $option = $v["name"];
            $ret["$value"] = str_repeat("&brvbar;--- ", 1) . $option;
            makeArrayListProperties($v["search_pro_id"], $level + 1, $maxlevel, $ret, $cond);
        }
        unset($arrListProperties11);
        return "";
    }
    unset($arrListProperties11);
    return "";
}

?>