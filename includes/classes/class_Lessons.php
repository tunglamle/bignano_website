<?

/******************************************************
 * Class Lessons
 *
 * Post Handling
 *
 * Project Name               :  Learning Themes DVS
 * Package Name                    :
 * Program ID                 :  class_Lessons.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  Banglcb
 * Version                    :  1.0
 * Creation Date              :  2014/02/10
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        2014/02/10        Banglcb          -        -     -     -
 *
 ********************************************************/
class Lessons extends DbBasic
{
    function Lessons()
    {
        $this->pkey = "lesson_id";
        $this->tbl = "_lessons";
    }

    //SELECT
    function getAllSimple($cond)
    {
        global $dbconn;
        $sql = "SELECT a.lesson_id, a.slug, a.lang_code, a.cat_id, a.name, a.image, a.reg_date, a.total_view, a.is_online, a.user_id, a.attachment, a.video_id,
						b.user_name, b.fullname, c.cat_id, c.name AS cat_name, c.slug AS cat_slug
				FROM $this->tbl AS a
				INNER JOIN _users AS b ON a.user_id = b.user_id
				INNER JOIN _category AS c ON a.cat_id = c.cat_id
				WHERE $cond";
        return $dbconn->GetAll($sql);
    }

    function getAllSimple2($cond)
    {
        global $dbconn;
        $sql = "SELECT lesson_id, slug, lang_code, cat_id, name, image, reg_date, total_view, is_online, user_id
				FROM $this->tbl	
				WHERE $cond";
        return $dbconn->GetAll($sql);
    }

    function getOneSimple2($lesson_id)
    {
        global $dbconn;
        $sql = "SELECT lesson_id, slug, lang_code, cat_id, name, image, reg_date, price, total_view, is_online, user_id
				FROM $this->tbl
				WHERE lesson_id=$lesson_id";
        return $dbconn->GetRow($sql);
    }

    /**
     * Get lesson ID from name
     *
     * @param string $name
     * @return Ambigous <number, unknown>
     */
    function getIdFromName($name = "")
    {
        global $dbconn;
        $slug = utf8_nosign_noblank($name);
        $sql = "SELECT lesson_id FROM _lessons WHERE name='$name' || slug='$slug'";
        $arr = $dbconn->GetRow($sql);
        return (is_array($arr) && $arr[$this->pkey] > 0) ? $arr[$this->pkey] : 0;
    }

    /**
     * Get List lesson by Best Seller
     *
     * @param number $cat_id
     * @param number $limit
     * @param number $start
     */
    function getListBestSeller($cat_id = 0, $limit = 10, $start = 0)
    {
        $cond = "is_online=1";
        if ($cat_id > 0) $cond .= " AND cat_id=$cat_id";
        $cond .= " ORDER BY total_checkout DESC";
        $cond .= " LIMIT $start, $limit";
        return $this->getAllSimple2($cond);
    }

    /**
     * Get List lesson by New Arrival
     *
     * @param number $cat_id
     * @param number $limit
     * @param number $start
     */
    function getListNewArrival($cat_id = 0, $limit = 10, $start = 0)
    {
        $cond = "is_online=1";
        if ($cat_id > 0) $cond .= " AND cat_id=$cat_id";
        $cond .= " ORDER BY reg_date DESC";
        $cond .= " LIMIT $start, $limit";
        return $this->getAllSimple2($cond);
    }

    /**
     * Get List lesson by Featured lessons
     *
     * @param number $cat_id
     * @param number $limit
     * @param number $start
     */
    function getListFeatured($cat_id = 0, $limit = 10, $start = 0)
    {
        $cond = "is_online=1";
        if ($cat_id > 0) $cond .= " AND cat_id=$cat_id";
        $cond .= " ORDER BY total_view DESC";
        $cond .= " LIMIT $start, $limit";
        return $this->getAllSimple2($cond);
    }
    //INSERT
    //UPDATE
    /**
     * Increase total Views
     *
     * @param number $lesson_id
     * @return none
     */
    function incView($lesson_id = 0)
    {
        return $this->updateOne($lesson_id, "total_view=total_view+1");
    }

    /**
     * Increase total checkout
     *
     * @param number $lesson_id
     * @return none
     */
    function incCheckout($lesson_id = 0)
    {
        return $this->updateOne($lesson_id, "total_checkout=total_checkout+1");
    }

    /**
     * Increase total add cart
     *
     * @param number $lesson_id
     * @return none
     */
    function incAddCart($lesson_id = 0)
    {
        return $this->updateOne($lesson_id, "total_addcart=total_addcart+1");
    }
    //DELETE
}

?>