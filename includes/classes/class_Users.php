<?

/******************************************************
 * Class Users
 *
 * User Handling
 *
 * Project Name               :  Learning Themes DVS
 * Package Name                    :
 * Program ID                 :  class_User.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  Banglcb
 * Version                    :  1.0
 * Creation Date              :  2014/02/10
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        2014/02/10        Banglcb          -        -     -     -
 *
 ********************************************************/
class Users extends DbBasic
{
    var $user_id = "";
    var $user_name = "";
    var $user_pass = "";
    var $user_group_id = "";
    var $fullname = "";
    var $valid = 0;

    /**
     * Init class
     */
    function Users()
    {
        $this->pkey = "user_id";
        $this->tbl = "_users";
    }

    //SELECT
    static function encrypt($password)
    {
        return md5(md5($password));
    }

    function get_user_name($user_id)
    {
        global $dbconn;
        $sql = "SELECT user_name, user_id FROM $this->tbl WHERE user_id=$user_id";
        $res = $dbconn->GetRow($sql, false, 0);
        if (is_array($res) && count($res) > 0) {
            return $res['user_name'];
        }
        return "";
    }

    function get_users_id($user_name)
    {
        global $dbconn;
        $sql = "SELECT user_name, user_id FROM $this->tbl WHERE user_name='$user_name'";
        $res = $dbconn->GetRow($sql);
        if (is_array($res) && count($res) > 0) {
            return $res['user_id'];
        }
        return 0;
    }

    /**
     * Get list members by department
     *
     * @param number $department_id
     * @param number $limit
     * @param number $start
     * @return Ambigous <multitype:, number, unknown>
     */
    function getListMembers($department_id = 0, $limit = 10, $start = 0)
    {
        $cond = "user_group_id=2 AND is_active=1";
        if ($department_id > 0) $cond .= " AND department_id=$department_id";
        $arr = $this->getAll($cond);

        if (is_array($arr)) {
            $arrDepartmentOptions = $arrPositionOptions = array();
            makeArrayListCategory(0, 0, 1, $arrDepartmentOptions, "ctype=" . CTYPE_PB);//Phòng ban
            makeArrayListCategory(0, 0, 1, $arrPositionOptions, "ctype=" . CTYPE_CV);//Chức vụ
            foreach ($arr as $key => $val) {
                $arr[$key]['department_name'] = $arrDepartmentOptions[$val['department_id']];
                $arr[$key]['position_name'] = $arrPositionOptions[$val['position_id']];
            }
        } else {
            $arr = array();
        }
        return $arr;
    }
    //UPDATE

    /**
     * Update new avatar
     *
     * @param unknown $pkey
     * @param string $new_avatar
     * @return Ambigous <void, number>
     */
    function doUpdateNewAvatar($pkey, $new_avatar = "")
    {
        extract($_POST);
        $set = "avatar='avatar/$new_avatar'";
        return $this->updateOne($pkey, $set);
    }

    /**
     * Update edit profile form
     *
     * @param number $user_id
     * @return Ambigous <void, number>
     */
    function doUpdateProfile($user_id = 0)
    {
        $gender = 0;
        extract($_POST);
        $birthday = ($birthday != "") ? mkDayTime($birthday) : "";
        $set = "fullname='$fullname', gender=$gender, email='$email', birthday='$birthday', phone='$phone', mobile='$mobile', address='$address'";
        return $this->updateOne($user_id, $set);
    }

    /**
     * Update edit email form
     *
     * @param number $user_id
     * @return Ambigous <void, number>
     */
    function doUpdateEmail($user_id = 0)
    {
        $gender = 0;
        extract($_POST);
        $set = "email='$email'";
        return $this->updateOne($user_id, $set);
    }

    /**
     * Update edit phone form
     *
     * @param number $user_id
     * @return Ambigous <void, number>
     */
    function doUpdatePhone($user_id = 0)
    {
        $gender = 0;
        extract($_POST);
        $set = "phone='$phone'";
        return $this->updateOne($user_id, $set);
    }

    /**
     * Update password form
     *
     * @param number $user_id
     * @param string $new_pass
     * @return number
     */
    function updatePassword($user_id = 0)
    {
        global $core;
        extract($_POST);
        $set = "user_pass='" . $this->encrypt($user_pass) . "'";
        $cond = "user_id=$user_id";

        $ok = $this->updateByCond($cond, $set);
        $core->_SESS->doLogout();
        return $ok;
    }

    /**
     *
     * @param unknown $user_id
     * @return Ambigous <void, number>
     */
    function doActive($user_id)
    {
        return $this->updateOne($user_id, "is_active=1");
    }

    /**
     *
     * @param unknown $user_id
     * @return Ambigous <void, number>
     */
    function doReject($user_id)
    {
        return $this->updateOne($user_id, "is_active=0");
    }

    //VALIDATE
    function checkValidLogin($_user_name = "", $_users_pass = "")
    {
        $_users_pass = $this->encrypt($_users_pass);
        $res = $this->getByCond("user_name='$_user_name' AND is_active=1");
        if (is_array($res) && count($res) > 0) {
            if ($res["user_pass"] == $_users_pass) {
                return 1;
            } else {
                return -1;
            }
        }
        return 0;
    }

    /**
     * Check Oauth_uid is exists or not?
     *
     * @param $oauth_uid
     * @param $oauth_provider
     * @return int
     */
    function check_authid($oauth_uid, $oauth_provider)
    {
        $cond = "oauth_uid='$oauth_uid' AND oauth_provider='$oauth_provider'";
        $res = $this->getByCond($cond);
        if (is_array($res) && count($res) > 0) {
            return 1;
        }
        return 0;
    }

    function checkValidUserPass($_user_name = "", $_users_pass = "")
    {
        $_users_pass = $this->encrypt($_users_pass);
        $res = $this->getByCond("user_name='$_user_name'");
        if (is_array($res) && count($res) > 0) {
            if ($res["user_pass"] == $_users_pass) {
                return 1;
            } else {
                return -1;
            }
        }
        return 0;
    }

    //Begin Added 06/04/2014
    function validateUserName($user_name = "", $user_name_old = "", &$msg_error)
    {
        global $core;
        if ($user_name == "") {
            $msg_error = $core->getLang("Tên đăng nhập không được trống");
            return 0;
        }
        if (strlen($user_name) < 6 || strlen($user_name) > 16) {
            $msg_error = $core->getLang("Tên đăng nhập phải ít nhất 6 ký tự và tối đa 16 ký tự");
            return 0;
        }
        if (!isAlphabetNumber($user_name)) {
            $msg_error = $core->getLang("Tên đăng nhập chỉ được phép có các ký tự [A-Z]]a-z][0-9] và dấu _");
            return 0;
        }
        if ($this->check_user_name($user_name, $user_name_old)) {
            $msg_error = $core->getLang("Tên đăng nhập đã được dùng cho người khác");
            return 0;
        }
        return 1;
    }

    function validatePassword($user_pass = "", $user_pass_confirm = "", &$msg_error)
    {
        global $core;
        $msg_error = array();
        if ($user_pass == "") {
            $msg_error[0] = $core->getLang("Mật khẩu không được trống");
            return 0;
        }
        if (strlen($user_pass) < 6 || strlen($user_pass) > 32) {
            $msg_error[0] = $core->getLang("Mật khẩu phải ít nhất 6 ký tự và tối đa 32 ký tự");
            return 0;
        }
        if ($user_pass_confirm == "") {
            $msg_error[1] = $core->getLang("Mật khẩu nhắc lại không được để trống");
            return 0;
        }
        if ($user_pass != $user_pass_confirm) {
            $msg_error[1] = $core->getLang("Mật khẩu nhắc lại phải giống mật khẩu");
            return 0;
        }
        return 1;
    }

    function validateFullName($fullname = "", &$msg_error, $min = 3, $max = 30)
    {
        global $core;
        if ($fullname == "") {
            $msg_error = $core->getLang("Họ tên không được trống");
            return 0;
        }
        if (strlen($fullname) < $min || strlen($fullname) > $max) {
            $msg_error = $core->getLang("Họ tên phải ít nhất $min ký tự và tối đa $max ký tự");
            return 0;
        }
        return 1;
    }

    function validateEmail($email = "", $email_old = "", &$msg_error)
    {
        global $core;
        if ($email == "") {
            $msg_error = $core->getLang("Email không được trống");
            return 0;
        }
        if (!isValidEmail($email)) {
            $msg_error = $core->getLang("Email không đúng định dạng");
            return 0;
        }
        if ($this->check_email_exists($email, $email_old)) {
            $msg_error = $core->getLang("Email đã được dùng cho tài khoản khác");
            return 0;
        }
        return 1;
    }

    function validateAddress($address = "", &$msg_error)
    {
        global $core;
        if ($address == "") {
            $msg_error = $core->getLang("Email_is_not_empty");
            return 0;
        }
        return 1;
    }

    function validatePhone($phone = "", &$msg_error)
    {
        global $core;
        if (strlen($phone) < 9) {
            $msg_error = $core->getLang("Số điện thoại phải ít nhất 9 ký tự");
            return 0;
        }
        if (!isValidMobile($phone)) {
            $msg_error = $core->getLang("Số điện thoại không đúng đinh dạng");
            return 0;
        }
        return 1;
    }

    function validateMobile($mobile = "", &$msg_error)
    {
        global $core;
        if ($mobile == "") {
            $msg_error = $core->getLang("Số di động không được trống");
            return 0;
        }
        if (strlen($mobile) < 10) {
            $msg_error = $core->getLang("Số di động phải 10 hoặc 11 ký tự bắt đầu bằng 09 hoặc 08");
            return 0;
        }
        if (!isValidMobile($mobile)) {
            $msg_error = $core->getLang("Số di động không đúng đinh dạng");
            return 0;
        }
        return 1;
    }

    function validateSecurityCode($securitycode = "", &$msg_error)
    {
        global $core;
        if ($securitycode == "") {
            $msg_error = $core->getLang("Mã xác nhận chưa nhập");
            return 0;
        }
        require_once(DIR_COMMON . "/class.Securimage.php");
        $si = new Securimage(session_id());
        if (!$si->check($_POST['securitycode'])) {
            $msg_error = $core->getLang("Mã xác nhận không đúng");
            return 0;
        }
        return 1;
    }

    function validateAgree($f = "", &$msg_error)
    {
        global $core;
        if (!isset($_POST[$f])) {
            $msg_error = $core->getLang("Bạn phải đồng ý với điều kiện và điều khoản sử dụng");
            return 0;
        }
        return 1;
    }

    //validate register form
    function validateRegister(&$arr_error)
    {
        extract($_POST);
        $ok1 = $this->validateUserName($user_name, "", $arr_error['user_name']);
        if ($ok1) unset($arr_error['user_name']);
        $ok2 = $this->validatePassword($user_pass, $user_pass_confirm, $arr_error['user_pass']);
        if ($ok2) unset($arr_error['user_pass']);
        $ok3 = 1;//$this->validateFullName($fullname, $arr_error['fullname'], 3, 30);
        $ok4 = $this->validateEmail($email, "", $arr_error['email']);
        if ($ok4) unset($arr_error['email']);
        $ok5 = 1;//$this->validateMobile($mobile, $arr_error['mobile']);
        $ok6 = 1;//$this->validateSecurityCode($securitycode, $arr_error['securitycode']);
        $ok7 = 1;//$this->validateAgree($agree, $arr_error['agree']);
        $ok = ($ok1 && $ok2 && $ok3 && $ok4 && $ok5 && $ok6 && $ok7);
        return $ok;
    }

    //validate edit profile form
    function validateEditProfile(&$arr_error)
    {
        global $core;
        extract($_POST);
        $ok0 = 1; //if ($company_name==""){ $arr_error['company_name'] = $core->getLang("Company_is_not_empty"); $ok0 = 0;}
        $ok1 = $this->validateFullName($fullname, $arr_error['fullname'], 3, 30);
        if ($ok1) unset($arr_error['fullname']);
        $ok2 = 1;//$this->validateEmail($email, $email_old, $arr_error['email']); if ($ok2) unset($arr_error['email']);
        $ok3 = 1;//$this->validateMobile($mobile, $arr_error['mobile']); if ($ok3) unset($arr_error['mobile']);
        $ok4 = 1;//$this->validateSecurityCode($securitycode, $arr_error['securitycode']);
        $ok = ($ok0 && $ok1 && $ok2 && $ok3 && $ok4);
        return $ok;
    }

    //validate edit email
    function validateEditEmail(&$arr_error)
    {
        global $core;
        extract($_POST);
        $ok = $this->validateEmail($email, $email_old, $arr_error['email']);
        if ($ok) unset($arr_error['email']);
        return $ok;
    }

    //validate edit phone
    function validateEditPhone(&$arr_error)
    {
        global $core;
        extract($_POST);
        $ok = $this->validateMobile($phone, $arr_error['phone']);
        if ($ok) unset($arr_error['phone']);
        return $ok;
    }

    //validate change password form
    function validateChangePass(&$arr_error)
    {
        global $core;
        extract($_POST);
        $ok1 = ($core->_USER['user_pass'] == $this->encrypt($user_pass_old));
        if (!$ok1) $arr_error['user_pass_old'] = $core->getLang("Mật khẩu hiện tại không chính xác");
        $ok2 = $this->validatePassword($user_pass, $user_pass_confirm, $arr_error['user_pass']);
        $ok3 = 1;
        if ($user_pass_old == $user_pass) {
            $ok3 = 0;
            $arr_error['user_pass'][0] = $core->getLang("Mật khẩu mới phải khác mật khẩu cũ");
        }
        $ok4 = 1;//$this->validateSecurityCode($securitycode, $arr_error['securitycode']);
        $ok = ($ok1 && $ok2 && $ok3 && $ok4);
        return $ok;
    }

    //validate reset/create new password form
    function validateResetPass(&$arr_error)
    {
        global $core;
        extract($_POST);
        $ok1 = 1;
        $ok2 = $this->validatePassword($user_pass, $user_pass_confirm, $arr_error['user_pass']);
        $ok3 = 1;
        if ($user_pass_old == $user_pass) {
            $ok3 = 0;
            $arr_error['user_pass'][0] = $core->getLang("Mật khẩu mới phải khác mật khẩu cũ");
        }
        $ok4 = 1;//$this->validateSecurityCode($securitycode, $arr_error['securitycode']);
        $ok = ($ok1 && $ok2 && $ok3 && $ok4);
        return $ok;
    }

    //validate login form
    function validateLogin(&$arr_error)
    {
        global $core;
        extract($_POST);
        $ok1 = 1;
        if ($user_name == "") {
            $arr_error['user_name'] = $core->getLang("Tài khoản nhập không được trống");
            $ok1 = 0;
        } else {
            if (!$this->check_user_name($user_name)) {
                $arr_error['user_name'] = $core->getLang("Tài khoản nhập không tồn tại");
                $ok1 = 0;
            }
        }
        $ok2 = 1;
        if ($user_pass == "") {
            $arr_error['user_pass'] = $core->getLang("Mật khẩu không được trống");
            $ok2 = 0;
        } else {
            $i = $this->checkValidLogin($user_name, $user_pass);
            if ($i == -1) {
                $arr_error['user_pass'] = $core->getLang("Mật khẩu không chính xác");
                $ok2 = 0;
            } elseif ($i == 0) {
                $arr_error['user_name'] = $core->getLang("Tài khoản không tồn tại");
                $ok1 = 0;
            }
        }
        $ok = ($ok1 && $ok2);
        return $ok;
    }

    //validate forgot form
    function validateForgot(&$arr_error)
    {
        global $core;
        extract($_POST);
        $ok1 = $this->check_user_name($user_name);
        if (!$ok1) {
            $arr_error["user_name"] = $core->getLang("Nhập tài khoản");
        }
        $ok2 = $this->check_email_exists($email);
        if (!$ok2) {
            $arr_error['email'] = $core->getLang("Nhập địa chỉ email");
        }
        $ok = $ok1 * $ok2;
        return $ok;
    }
    //Check user_name is exists or not?
    //return 1 is exists, 0 is not
    function check_user_name($_user_name, $_user_name_old = "")
    {
        if (isEmpty($_user_name))
            return 0;
        $cond = "user_name='$_user_name'";
        if ($_user_name_old != "") $cond .= " AND user_name!='$_user_name_old'";
        $res = $this->getByCond($cond);
        if (is_array($res) && count($res) > 0) {
            return 1;
        }
        return 0;
    }

    function check_user_name2($site_id, $_user_name, $_user_name_old = "")
    {
        if (isEmpty($_user_name))
            return 0;
        $cond = "site_id=$site_id AND user_name='$_user_name'";
        if ($_user_name_old != "") $cond .= " AND user_name!='$_user_name_old'";
        $res = $this->getByCond($cond, 0);
        if (is_array($res) && count($res) > 0) {
            return 1;
        }
        return 0;
    }
    //Check email is exists or not? Email must be unique
    //return 1 is exists, 0 is not
    function check_email_exists($_email, $email_old = "")
    {
        if (isEmpty($_email))
            return 0;
        $cond = "email='$_email'";
        if ($email_old != "") $cond .= " AND email!='$email_old'";
        $res = $this->getByCond($cond, 0);
        if (is_array($res) && count($res) > 0) {
            return 1;
        }
        return 0;
    }
    //INSERT

    /**
     * Register new User from Form
     *
     * @return number
     */
    function doRegister()
    {
        global $_CONFIG, $_LANG_ID, $clsRewrite;
        extract($_POST);
        $md5_users_pass = $this->encrypt($user_pass);
        $user_group_id = 2;
        $reg_date = time();
        if ($fullname == "") $fullname = ucfirst($user_name);
        $active_key = simpleRandString(16);
        $is_active = 1;//0: inactive; 1: active
        $gender = ($gender) ? $gender : 0;
        $fields = "user_name, user_pass, fullname, gender, user_group_id, email, mobile, is_active, active_key, reg_date";
        $values = "'$user_name', '$md5_users_pass', '$fullname', $gender, $user_group_id, '$email', '$mobile', $is_active, '$active_key', $reg_date";
        if ($this->insertOne($fields, $values) == 1) {
            /*
            Begin Send Mail to user the activation link
            %SITE_NAME% : tên của website
            %SITE_TITLE% : tiêu đề của website
            %SITE_HOTLINE% : hotline của website
            %FULL_NAME% : họ tên người đăng ký
            %USER_NAME% : tên đăng nhập
            %USER_PASS% : mật khẩu
            %URL_ACTIVE% : link kích hoạt tài khoản
             */
            $active_link = $clsRewrite->url_active() . "?k=" . base64_encode($active_key) . "&e=" . base64_encode($email) . "&lang=" . $_LANG_ID;
            $post = array("FULL_NAME" => $fullname, "USER_NAME" => $user_name, "USER_PASS" => $user_pass, "URL_ACTIVE" => $active_link);
            if ($_CONFIG['mail_configs']["mail_register"] == 1) {
                send_mail_form("mail_register", $email, $post);
            } elseif ($_CONFIG['mail_configs']["mail_register_success"] == 1) {
                send_mail_form("mail_register_success", $email, $post);
            }
            //End Send Mail
            //send_mail_register($email, $fullname, $user_name, $user_pass, $active_link);
            return 1;
        }
        return 0;
    }


    /**
     * Register new user when login with Oauth
     * @param $user
     * @param $oauth_provider
     * @param int $user_group_id
     * @return int
     */
    function doOauthRegister($user, $oauth_provider, $user_group_id = 2)
    {
        $user_name = $user['id'];
        $fullname = ($user['first_name'] != "") ? $user['first_name'] : $user['given_name'] . " " . ($user['last_name'] != "") ? $user['last_name'] : $user['family_name'];
        $avatar = $user['picture'];
        $email = $user['email'];
        $gender = $user['gender'];
        $is_active = 1;
        $reg_date = time();
        $user_pass = "";
        $last_visit = $last_activity = $reg_date;
        $oauth_uid = $user['id'];
        $fields = "user_name, user_pass, fullname, avatar, email, gender, user_group_id, reg_date, is_active, oauth_provider, oauth_uid, last_visit, last_activity";
        $values = "'$user_name', '$user_pass', '$fullname', '$avatar', '$email', '$gender', '$user_group_id', $reg_date, $is_active, '$oauth_provider', '$oauth_uid', $last_visit, $last_activity";
        if ($this->check_user_name($user_name) == 0 && $this->check_authid($oauth_uid, $oauth_provider) == 0) {
            $this->insertOne($fields, $values);
        } else {
            $set = "email='$email', avatar='$avatar', oauth_provider='$oauth_provider', gender='$gender', oauth_uid='$oauth_uid', last_visit=$last_visit, last_activity=$last_activity";
            $this->updateByCond("user_name='$user_name' OR (oauth_uid='$oauth_uid' AND oauth_provider='$oauth_provider')", $set);
        }
        return 1;
    }


    /**
     * Resend activation email if lost mail
     *
     * @param number $user_id
     * @return number
     */
    function resendActivationMail($user_id = 0)
    {
        $arrOneUser = $this->getOne($user_id);
        if ($arrOneUser['is_active'] == 1) return 1;
        extract($arrOneUser);
        $active_link = VNCMS_URL . "/activeacc?k=" . base64_encode($active_key) . "&e=" . base64_encode($email) . "&lang=" . $_LANG_ID;
        send_mail_register($email, $fullname, $user_name, "******", $active_link);
        return 1;
    }
    //OTHER

    /**
     * Forgot password: send forgot link to email of user
     *
     * @return number
     */
    function doForgot()
    {
        global $_CONFIG, $_LANG_ID, $clsRewrite;
        extract($_POST);
        $clsPublicKey = new PublicKey();
        //Get a user
        $arrOneUser = $this->getByCond("user_name='$user_name' AND email='$email'");
        $forgotkey = $clsPublicKey->getForgotKey();
        /*
         Begin Send Mail to user to notice that activation has been successfully
         %SITE_NAME% : tên của website
         %SITE_TITLE% : tiêu đề của website
         %SITE_HOTLINE% : hotline của website
         %FULL_NAME% : họ tên người đăng ký
         %USER_NAME% : tên đăng nhập
        */
        $forgot_link = $clsRewrite->url_resetpass() . "?k=" . base64_encode($forgotkey) . "&e=" . base64_encode($email) . "&lang=" . $_LANG_ID;
        $post = array("FULL_NAME" => $arrOneUser['fullname'], "USER_NAME" => $arrOneUser['user_name'], "URL_FORGOT" => $forgot_link);
        send_mail_form("mail_forgot", $email, $post);
        //End Send Mail
        return 1;
    }

    /**
     * Create new password after forgot action
     *
     * @return number
     */
    function doResetPass()
    {
        global $_CONFIG, $_LANG_ID;
        extract($_POST);
        $clsPublicKey = new PublicKey();
        $reset = GET("k", "");
        $email = GET("e", "");
        $keyid = base64_decode($reset);
        $email = base64_decode($email);
        //Get a user
        $arrOneUser = $this->getByCond("email='$email'");
        $newpass = $this->encrypt($_POST["user_pass"]);
        $set = "user_pass='$newpass'";
        if ($this->updateOne($arrOneUser['user_id'], $set) == 1) {
            /*
            Begin Send Mail to user to notice that new password was created successfully
            %SITE_NAME% : tên của website
            %SITE_TITLE% : tiêu đề của website
            %SITE_HOTLINE% : hotline của website
            %FULL_NAME% : họ tên người đăng ký
            %USER_NAME% : tên đăng nhập
            %USER_PASS% : mật khẩu
             */
            $post = array("FULL_NAME" => $arrOneUser['fullname'], "USER_NAME" => $arrOneUser['user_name'], "USER_PASS" => $user_pass);
            send_mail_form("mail_forgot_success", $email, $post);
            //Delete public key
            $clsPublicKey->deleteKey($keyid);
            //End Send Mail
        }
        return 1;
    }
}

function makeListUser($selectedid = "", $cond = "", $field = "user_name")
{
    global $dbconn, $lang_code;
    $sql = "SELECT * FROM _users WHERE user_group_id=2 OR user_group_id=4";
    if ($cond != "") $sql .= " AND $cond";
    $arrListUser11 = $dbconn->GetAll($sql);
    $html = "";
    if (is_array($arrListUser11)) {
        foreach ($arrListUser11 as $k => $v) {
            $value = $v["user_id"];
            $option = $v[$field];
            $selected = ($value == $selectedid) ? "selected" : "";
            $html .= "<option value=\"$value\" $selected>" . $option . "</option>";
        }
        return $html;
    } else {
        return "";
    }
}

function makeArrayListUser(&$ret, $cond = "", $field = "user_name")
{
    global $dbconn;
    $sql = "SELECT * FROM _users WHERE user_group_id=2 OR user_group_id=4";
    if ($cond != "") $sql .= " AND $cond";
    $sql .= " ORDER BY user_name";
    $arrListUser11 = $dbconn->GetAll($sql);
    $html = "";
    if (is_array($arrListUser11)) {
        foreach ($arrListUser11 as $k => $v) {
            $value = $v["user_id"];
            $option = $v[$field];
            $ret["$value"] = $option;
        }
        unset($arrListUser11);
        return "";
    }
    unset($arrListUser11);
    return "";
}

function makeArrayListAuthor(&$ret, $short = 0)
{
    global $dbconn;
    $sql = "SELECT * FROM _users WHERE user_group_id!=2";
    $sql .= " ORDER BY user_group_id DESC, user_name";
    $arrListUser11 = $dbconn->GetAll($sql);
    $html = "";
    if (is_array($arrListUser11)) {
        foreach ($arrListUser11 as $k => $v) {
            $value = $v["user_id"];
            $option = ($short == 0) ? $v["user_name"] . " &lt;" . $v["fullname"] . "&gt;" : $v["user_name"];
            $ret["$value"] = $option;
        }
        unset($arrListUser11);
        return "";
    }
    unset($arrListUser11);
    return "";
}

?>