<?

/******************************************************
 * Class Cougory
 *
 * Cougory Handling
 *
 * Project Name               :  Learning Themes DVS
 * Package Name                    :
 * Program ID                 :  class_Courses.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  Banglcb
 * Version                    :  1.0
 * Creation Date              :  2019/05/17
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        2014/02/10        Banglcb          -        -     -     -
 *
 ********************************************************/
class Courses extends DbBasic
{
    function Courses()
    {
        $this->pkey = "course_id";
        $this->tbl = "_courses";
    }

    //SELECT

    function getOneSimple($product_id)
    {
        global $dbconn;
        $sql = "SELECT a.*,p.title, p.discount_type, p.discount_value, p.start_date, p.end_date, p.is_start
				FROM $this->tbl	AS a
				LEFT JOIN _promotion AS p ON a.course_id = p.course_id
				WHERE a.course_id=$product_id";
        return $dbconn->GetRow($sql);
    }

    function getAllSimple($cond = "")
    {
        global $dbconn;
        $sql = "SELECT a.*,p.title, p.discount_type, p.discount_value, p.start_date, p.end_date, p.is_start
				FROM $this->tbl	AS a
				LEFT JOIN _promotion AS p ON a.course_id = p.course_id";
        if ($cond != "") {
            $sql .= " WHERE $cond";
        }
        return $dbconn->GetAll($sql);
    }

    /**
     * Get random promotion course
     * @return mixed
     */
    function getOnePromotion()
    {
        global $dbconn;
        $now = time();
        $sql = "SELECT c.*, p.title, p.des as promotion_des, p.discount_type, p.discount_value, p.start_date, p.end_date, p.is_start
				FROM $this->tbl	AS c
				INNER JOIN _promotion AS p ON c.course_id = p.course_id
				WHERE p.is_start = 1 AND c.is_online = 1 AND l.is_online = 1 AND p.start_date <= $now AND p.end_date >= $now
				ORDER BY rand()";
        return $dbconn->GetRow($sql);
    }

    /**
     * Get list Courses by ID or array ID
     *
     * @param number $id
     */
    function getListCouById($id)
    {
        $cond = "";
        if (is_numeric($id)) {
            $cond = "course_id=$id";
        } else
            if (is_array($id)) {
                $s = implode(',', $id);
                $cond = "course_id IN ($s)";
            } else
                if (strpos(',', $id) !== false) {
                    $cond = "course_id IN ($id)";
                }
        $cond .= " ORDER BY order_no";
        return $this->getAll($cond);
    }

    /**
     * Get Cougory by slug
     *
     * @param string $slug
     * @return Ambigous <number, unknown>
     */
    function getBySlug($slug = "")
    {
        global $lang_code;
        return $this->getByCond("slug='$slug' AND lang_code='$lang_code'");
    }

    /**
     * Get slug by Pkey ID
     *
     * @param number $pkey_id
     * @return string
     */
    function getSlug($pkey_id = 0)
    {
        $arr = $this->getOne($pkey_id, 0);
        return (is_array($arr) && $arr[$this->pkey] > 0) ? $arr['slug'] : '';
    }

    /**
     * Get name by Pkey ID
     *
     * @param number $pkey_id
     * @return string|unknown
     */
    function getName($pkey_id = 0)
    {
        global $dbconn;
        if ($pkey_id == "" || $pkey_id == 0) return "";
        $sql = "SELECT name FROM " . $this->tbl . " WHERE " . $this->pkey . "=$pkey_id";
        $aCougory = $dbconn->GetRow($sql, false, 0);
        return $aCougory["name"];
    }

    /**
     * Check slug is exists or not?
     *
     * @param string $slug
     * @param string $old_slug
     * @return Ambigous <number, unknown>
     */
    function isExistsSlug($slug = "", $old_slug = "")
    {
        global $dbconn, $lang_code;
        $sql = "SELECT COUNT(course_id) AS total_item 
						FROM " . $this->tbl . " 
						WHERE lang_code='$lang_code' AND slug!='$old_slug' AND (slug='$slug' OR slug REGEXP '^" . $slug . "[0-9]+$')";
        $aCougory = $dbconn->GetRow($sql);
        return (is_array($aCougory) && $aCougory['total_item'] > 0) ? $aCougory['total_item'] : 0;
    }

    //INSERT
    //UPDATE
    //DELETE
    //OTHER

    //export slug to array(course_id => slug)
    function exportArraySlug()
    {
        global $dbconn, $lang_code;
        $sql = "SELECT course_id, name, slug FROM " . $this->tbl . " WHERE is_online=1";
        $arrListCougory1 = $dbconn->GetAll($sql);
        $arr = array();
        if (is_array($arrListCougory1)) {
            foreach ($arrListCougory1 as $k => $v) {
                $arr[$v['course_id']] = $v['slug'];
            }
        }
        return $arr;
    }

    function export2array($cond = "")
    {
        $arr1 = $this->getAll($cond);
        $arr = array();
        foreach ($arr1 as $key => $val) {
            $arr[$val[$this->pkey]] = $val['name'];
        }
        return $arr;
    }
}

?>