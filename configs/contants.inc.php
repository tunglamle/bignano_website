<?
/******************************************************
 * Vars&Conts Definition
 *
 * Define some variables & contants for project
 *
 *
 * Project Name               :  ClientWebsite
 * Package Name                    :
 * Program ID                 :  contants.inc.php
 * Environment                :  PHP  version 5.3
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  20/01/2018
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        20/01/2018        banglcb          -        -     -     -
 *
 ********************************************************/
$arrBBCodeImg = array();
$arrAdsPositionOptions = array(
//    "SL" => "Slide",
//    "NN" => "Link ngôn ngữ",
//    "RCSR" => "Related Csr",
    "DT" => "Đối tác",
//    "LFI" => "Link Trang For Investor",
//    "LV" => "Lĩnh vực kinh doanh",
//    "RSL" => "Nhóm bên phải slide",
//    "RSL2" => "Ảnh nhóm bên phải slide",
);
$arrMod2NamePosition = array(
    "All" => array("NN", "RCSR", "DT", "LFI", "LV", "RSL", "RSL2"),
);
$arrAdsPositionOptionsSize = array(
    "L" => "Text",
);
$arrMod2Name = array(
    "All" => "Tất cả các trang",
);

$arrTargetOptions = array(
    '_blank' => "Blank page",
    '_parent' => "Self page",
);
//config slides format
$_arr_slider_format = array(
    "t_title" => array("Title", 255, "", ""), //title, length, default value, place holder
    "t_des" => array("Description", 255, "", ""),
    "t_url" => array("URL", 255, "", "")
);
$_arr_slider_type = array(
    "main" => "Sldie PC",
);
//config category
$_max_category_level = 4;
define("MAX_LEVEL_CATEGORY", 4);
//config menu type
$_arr_menu_type = array(
//    "top" => "Menu đầu trang",
    "main" => "Menu Chính",
    "bottom" => "Menu Cuối Trang",
    "right" => "Menu bên phải slide",
);
$_max_menu_level = 2;

define("CTYPE_BV", 0);
define("CTYPE_SP", 1);
define("CTYPE_GT", 2);
//define("CTYPE_CSR", 3);
define("CTYPE_FI", 4);
//define("CTYPE_JOB", 5);
define("CTYPE_TA", 6);
//define("CTYPE_VL", 7);

$arrCtypeOptions = array(
    CTYPE_BV => "Nhóm Tin Tức",
    CTYPE_SP => "Nhóm Sản Phẩm",
    CTYPE_GT => "Nhóm Giới Thiệu",
//    CTYPE_CSR => "Nhóm CRS",
    CTYPE_FI => "Nhóm FI",
//    CTYPE_JOB => "Nhóm Job",
    CTYPE_TA => "Nhóm Ứng dụng",
//    CTYPE_VL => "Nhóm Việc làm",
);


$arrTemplateOption[CTYPE_BV] = array('default' => "Mặc định", 'construction' => "Công trình tiêu biểu");
$arrTemplateOption[CTYPE_GT] = array('default' => "Mặc định", 'page' => "Giao diện Research");

$paymentMethodOptions = array(1 => "Thanh toán chuyển khoản ngân hàng", 2 => "Thanh toán qua thẻ visa hoặc thẻ tín dụng");
$shippingMethodOptions = array(1 => "Vận chuyển qua bưu điện", 2 => "Đăng ký trực tiếp tại Văn Phòng");
$orderStatusOptions = array(0 => "Chờ thanh toán", 1 => "Chưa thanh toán", 2 => "Đã thanh toán");
$arrInboxPriority = array(0 => "Bình thường", 1 => "Quan trọng", 2 => "Khẩn cấp");
$arrYesNoOptions = array(0 => "Không", 1 => "Có");
$arrYesNoVisits = array(0 => "Trái", 1 => "Phải");
$arrGenderOptions = array(0 => "Nam", 1 => "Nữ");
$arrActiveOptions = array(0 => "Không hoạt động", 1 => "Hoạt động");
$arrGroupOptions = array(2 => "Người dùng", 4 => "Moderator");
$arrDiscountTypeOptions = array(0 => "Phần trăm (%)", 1 => "Số tiền");

//Facebook App definition
$FB_APP = array(
    "appId" => "160149684683194",
    "secret" => "9cbb1a1d9ce01da835ba03064496bead",
    "cookie" => true,
    "version" => "v3.2"
);

//Google API definition
$GG_API = array(
    "client_id" => "608085969286-8fpbk5cceuh6c7f7p8hsqf8ggftsund9.apps.googleusercontent.com",
    "client_secret" => "xC-eIlEMZq2EmxrlKnZipPLm"
);
?>