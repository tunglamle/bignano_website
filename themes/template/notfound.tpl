{literal}
    <style>
        .notfound-wrap {
            padding: 50px 0;
            text-align: center;
        }

        .notfound-des {
            padding: 10px 0;
        }

        .notfound-title {
            font-weight: bold;
        }

        .notfound-link {
            background: #0082C4;
            color: white;
            padding: 10px 15px;
            border-radius: 8px;
            margin: 0 15px;
        }

        .notfound-box {
            margin-top: 30px;
        }
    </style>
{/literal}
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="notfound-wrap my-30" style="background-color: #edf2f2;">
                <h1 class="text-center text-uppercase notfound-title"
                    style="color:#0082C4;">{$core->getLang("PAGE NOT FOUND... !!!")} !!!</h1>
                <div class="notfound-des">
                    {$core->getLang("SORRY NOT FOUND...")}
                </div>
                <div class="notfound-box">
                    <a href="{$VNCMS_URL}" class="text-default notfound-link">
                        {$core->getLang("HOME PAGE")}
                    </a>
                    <a href="{$VNCMS_URL}/lien-he" class="text-default notfound-link">
                        {$core->getLang("CONTACT")}
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
