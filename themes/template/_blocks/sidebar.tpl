<section class="aside mb-30">
    <h2 class="aside__title">Danh mục sản phẩm</h2>
    <ul class="as-menu">
        {foreach from=$arrListCatProduct key=k item=cat}
        <li class="as-menu__item">
            <a class="as-menu__link" href="{$Rewrite->url_category($cat)}">{$cat.name}</a>
        </li>
        {/foreach}
    </ul>
</section>
<section class="aside mb-30">
    <h2 class="aside__title">Hỗ trợ trực tuyến</h2>
    <ul class="as-support">
        {foreach from=$arrListSupport key=k item=adver}
        <li class="as-support__item media">
            <i class="fa fa-fw fa-2x fa-phone mr-2 text-primary"></i>
            <div class="media-body">
                <div class="as-support__label">{$adver.title}</div>
                <div class="as-support__number"><a href="tel:{$adver.phone}">{$adver.phone}</a></div>
            </div>
        </li>
        {/foreach}
    </ul>
</section>
<section class="aside">
    <h2 class="aside__title">Tin tức nổi bật</h2>
    <ul class="as-news">
        {foreach from=$arrListArticleHot key=k item=news}
        <li class="as-news__item">
            <a class="as-news__link" href="{$Rewrite->url_article($news)}">{$news.title}</a>
        </li>
        {/foreach}
    </ul>
</section>