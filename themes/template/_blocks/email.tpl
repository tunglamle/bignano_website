<section class="email_sale">
    <div class="container">
        <h3 class="title_email">Đăng ký nhận tin khuyến mại !</h3>
        <form class="follow__form" id="save_email_form" onsubmit="ajax_email();return false;">
            <div class="input-group e_input">
                <input class="form-control" type="text" name="email" placeholder="Nhập email của bạn"/>
                <div class="input-group-append">
                    <button class="input-group-text" type="submit"><i
                                class="fa fa-envelope fa-2x"></i></button>
                </div>
            </div>
        </form>
    </div>

</section>