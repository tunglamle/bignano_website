﻿<!DOCTYPE html>
<html lang="vi">
<head>
    <title>{if $_CONFIG.page_title}{$_CONFIG.page_title}{else}{$_CONFIG.site_title}{/if}</title>
    <!-- REQUIRED meta tags -->
    <meta charset="utf-8"/>
    <meta name="viewport"
          content="width=device-width, height=device-height, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <!-- Favicon tag -->
    {if $_CONFIG.site_favicon ne ""}
        <link rel="shortcut icon" href="{$URL_UPLOADS}/{$_CONFIG.site_favicon}" type="image/x-icon"/>
        <link rel="icon" href="{$URL_UPLOADS}/{$_CONFIG.site_favicon}" type="image/x-icon">
    {else}
        <link rel="shortcut icon" href="{$VNCMS_URL}/favicon.ico" type="image/x-icon">
        <link rel="icon" href="{$URL_UPLOADS}/{$_CONFIG.site_favicon}" type="image/x-icon">
    {/if}
    <!-- SEO meta tags -->
    <meta name="keywords" content="{$_CONFIG.page_keywords}"/>
    <meta name="description" content="{$_CONFIG.page_description}"/>
    <meta name="author" content="{$_CONFIG.site_title}"/>
    <meta property="og:image" content="{$URL_UPLOADS}/{$_CONFIG.thumb}"/>
    <meta property="og:image:alt" content="{$URL_UPLOADS}/{$_CONFIG.thumb}"/>
    <meta property="og:title" content="{$_CONFIG.title}"/>
    <meta property="og:description" content="{$_CONFIG.description}"/>
    <meta property="og:url" content="{$_CONFIG.url}"/>
    <meta property="og:type" content="{$_CONFIG.type}"/>
    <!-- Social meta tags -->
    {if $og.title!=""}
        <meta name="DC.title" content="{$og.title}"/>
    {/if}
    {if $og.fbadmin!=""}
        <meta property="fb:admins" content="{$og.fbadmin}"/>
    {/if}
    {if $og.url!=""}
        <meta itemprop="url" content="{$og.url}"/>
    {/if}
    {if $og.image!=""}
        <meta itemprop="image" content="{$og.image}"/>
        <meta property="og:image" content="{$og.image}"/>
    {/if}
    {if $og.title!=""}
        <meta property="og:title" content="{$og.title}"/>
    {/if}
    {if $og.description!=""}
        <meta property="og:description" content="{$og.description}"/>
    {/if}
    {if $og.type!=""}
        <meta property="og:type" content="{$og.type}"/>
    {/if}
    <meta property="og:locale" content="vi_vn">
    {if $og.published ne ""}
        <meta property="article:published_time" content="{$og.published}"/>
        <meta property="article:modified_time" content="{$og.modified}"/>
        <meta property="article:author" content="{$_CONFIG.site_name}"/>
        <meta property="article:section" content="{$og.section}"/>
        {foreach key=k item=tag from=$og.tag}
            <meta property="article:tag" content="{$tag}"/>
        {/foreach}
        <meta name="twitter:card" content="summary"/>
    {/if}
    {if $og.site_name!=""}
        <meta property="og:site_name" content="{$og.site_name}"/>
    {/if}
    {if $og.url!=""}
        <meta property="og:url" content="{$og.url}"/>
    {/if}
    {if $og.alternate1!=""}
        <link rel="alternate" href="{$og.alternate1}" media="handheld"/>
    {/if}
    {if $og.canonical}
        <link rel="canonical" href="{$og.canonical}"/>
    {/if}

    <link rel="stylesheet" href="{$URL_VENDOR}/font-awesome-4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="{$URL_VENDOR}/swiper/swiper.min.css"/>
    <link rel="stylesheet" href="{$URL_VENDOR}/fancybox/jquery.fancybox.min.css"/>
    {*    <link rel="stylesheet" href="{$URL_VENDOR}/mmenu/css/mmenu.css"/>*}
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css'>
    <link rel="stylesheet" href="{$URL_CSS}/style.css"/>
    <link rel="stylesheet" href="{$URL_CSS}/custom.css"/>


    <script src="{$URL_VENDOR}/jquery/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js'></script>

    <!-- ![endif]-->
    {$_CONFIG.jscode_head|htmlDecode}


    <script>
        var VNCMS_URL = "{$VNCMS_URL}";
        var URL_IMAGES = "{$URL_IMAGES}";
        var URL_MEDIAS = "{$URL_MEDIAS}";
        var URL_CSS = "{$URL_CSS}";
        var URL_UPLOAD = "{$URL_UPLOADS}";
        var IS_LOGIN = {$isLogin};
    </script>
    <!-- :::::-[ Vendors JS ]-:::::: -->

    <script src="{$URL_VENDOR}/popper/popper.min.js" defer></script>
    <script src="{$URL_VENDOR}/bootstrap/bootstrap.min.js" defer></script>
    <script src="{$URL_VENDOR}/swiper/swiper.min.js" defer></script>
    <script src="{$URL_VENDOR}/jquery-zoom/jquery.zoom.min.js" defer></script>
    <script src="{$URL_VENDOR}/fancybox/jquery.fancybox.min.js" defer></script>

    {*<script src="{$URL_VENDOR}/mmenu/js/mmenu.polyfills.js"></script>*}
    {*<script src="{$URL_VENDOR}/mmenu/js/mmenu.js"></script>*}
    {*<script src="{$URL_VENDOR}/html5shiv/html5shiv.min.js"></script>*}
    <!-- custom script-->
    {*<script src="{$URL_JS}/_bundle.js"></script>*}
    <script src="{$URL_JS}/cart.js" defer></script>
    <script src="{$URL_JS}/globals.js" defer></script>
    <script src="{$URL_JS}/custom.js" defer></script>
    <script src="{$URL_JS}/publish.js" defer></script>
    <!-- custom script-->
    <!-- Custom JS Bottom -->
</head>
<body {if $mod eq 'home' && $act eq 'default'}class="menu-show"{/if}>
<!-- Custom JS Body -->
{$_CONFIG.jscode_openbody|htmlDecode}
<div class="page">
    {include file="_header.tpl"}
    {include file="$mod/index.tpl"}
    {include file="_footer.tpl"}
</div>
<div class="loader-overlay">
    <div class="loader-box">
        <div class="loader"></div>
    </div>
</div>

<!-- modals + sticky widgets-->
<div class="sticky">
    <div class="sticky__btns">
        <ul class="sticky-btns">
            <li class="sticky-btns__item"><a class="sticky-btns__link sticky-btns__link--up js-movetop" href="#!"><span
                            class="d-none">Move Top</span></a></li>
            <li class="sticky-btns__item"><a class="sticky-btns__link sticky-btns__link--search js-md-search" href="#!"><span
                            class="d-none">Search</span></a></li>
            <li class="sticky-btns__item"><a class="sticky-btns__link sticky-btns__link--menu js-menu-toggle" href="#!"><span
                            class="d-none">Menu Toggle</span><span></span><span></span><span></span></a></li>
        </ul>
    </div>
    <div class="sticky__sitemap">
        <div class="sticky__sitemap-body">
            <div class="sitemap">
                <div class="sitemap__wrapper">
                    <ul class="sitemap__list">
                        {foreach from=$arrListMainMenu key=k item=menu}
                            {if $menu.children}
                                <li class="sitemap__item sitemap__dropdown">
                                    <a class="sitemap__link sitemap__dropdown-toggle {if $menu.cat_id eq $curCat.cat_id}active{/if}"
                                       href="{$menu.href}">
                                        <span>{$menu.title}</span>
                                    </a>
                                    <div class="sitemap__dropdown-menu"{if $menu.cat_id eq $curCat.cat_id} style="display: block" {else} style="display: none"{/if}>
                                        <a class="sitemap__dropdown-title {if $menu.custom_link}js-menu-scroll{/if}"
                                           href="{$menu.href}">{$menu.title}</a>
                                        <ul class="sitemap__sub">
                                            {foreach from=$menu.children key=j item=submenu}
                                                <li class="sitemap__sub-item">
                                                    <a class="sitemap__sub-link"
                                                       href="{$Rewrite->url_category($submenu)}">{$submenu.title}</a>
                                                </li>
                                            {/foreach}
                                        </ul>
                                    </div>
                                </li>
                            {else}
                                <li class="sitemap__item">
                                    <a class="sitemap__link {if $menu.cat_id eq $curCat.cat_id}active{/if}"
                                       href="{$menu.href}">
                                        <span>{$menu.title}</span>
                                    </a>
                                </li>
                            {/if}
                        {/foreach}
                    </ul>
                    <nav class="sitemap__nav">
                        {*                                <a class="sitemap__nav-link" href="{$Rewrite->url_category($arrNews)}">News</a>*}
                        {*                                <a class="sitemap__nav-link" href="{$Rewrite->url_category($arrJobCareers)}">Jobs & Careers</a>*}
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<article class="md-search">
    <div class="md-search__wrapper">
        <div class="md-search__container">
            <div class="md-search__inner">
                <form class="search" action="{$VNCMS_URL}/search">
                    <div class="input-group">
                        <label for="fixed-key" class="d-none">Search</label>
                        <input class="form-control" id="fixed-key" type="text" name="key"
                               placeholder="Search our site..."/>
                        <div class="input-group-append">
                            <button class="input-group-text" type="submit">
                                <i class="fa fa-search"></i>
                                <span class="d-none">Search</span>
                            </button>
                        </div>
                    </div>
                </form>
                <button class="md-search__close">
                    <span class="d-none">Search</span>
                </button>
            </div>
        </div>
    </div>
</article>
<a class="phone-ring" href="tel:+84879808080">
    <span class="phone-ring__icon"><i class="fa fa-phone"></i></span>
    <span class="phone-ring__text">+84879808080</span>
</a>

{$_CONFIG.jscode_closebody|htmlDecode}
{if $arr_error}
    <script>
        Swal.queue([{
            title: "Thông báo",
            type: "{$arr_error.status}",
            text: "{$arr_error.message}",
            showLoaderOnConfirm: true,
            preConfirm: () => {
                var location = "{$arr_error.location}";
                if (location !== "") {
                    return window.location.href = "{$arr_error.location}";
                }
            }
        }])
    </script>
{/if}
</body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=538897933198331&autoLogAppEvents=1"></script>


{*mạng xa hội *}

<script type="text/javascript" language="javascript">
    function share_twitter() {
        u = location.href;
        t = document.title;
        window.open("http://twitter.com/home?status=" + encodeURIComponent(u));
    }

    function share_facebook() {
        u = location.href;
        t = document.title;
        window.open("http://www.facebook.com/share.php?u=" + encodeURIComponent(u) + "&t=" + encodeURIComponent(t));
    }

    function share_google() {
        u = location.href;
        t = document.title;
        window.open("http://www.google.com/bookmarks/mark?op=edit&bkmk=" + encodeURIComponent(u) + "&title=" + t + "&annotation=" + t);
    }
</script>
{*End*}
</html>
