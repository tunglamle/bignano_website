{if $curCat.parent_id eq 0}
    {if $curCat.template eq 'page'}
        <div class="page__content">
            <div class="banner">
                <div class="banner__wrapper">
                    <div class="banner__title">{$curCat.name}</div>
                </div>
                <img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt="{$curCat.name}"/>
            </div>
            <section class="section-2">
                <div class="research_developement_des mb-30">
                    {$curCat.des|htmlDecode}
                </div>
                {*                <div class="mb-40"><a class="banner-2"*}
                {*                                      href="{if $arrListCatChildren.0.link}{$arrListCatChildren.0.link}{else}{$Rewrite->url_category($arrListCatChildren.0)}{/if}">*}
                {*                        <h2 class="banner-2__title">{$arrListCatChildren.0.name}</h2>*}
                {*                        <div class="banner-2__frame"><img src="{$URL_UPLOADS}/{$arrListCatChildren.0.banner}" alt=""/>*}
                {*                        </div>*}
                {*                    </a>*}
                {*                </div>*}
                <div class="row gutter-under-md-16">
                    {foreach from=$arrListCatChildren key=k item=cat}
                        <div class="col-6 col-md-4 col-lg-4 mb-3 mb-sm-30">
                            <div class="about">
                                <a class="about__frame"
                                   href="{if $cat.link}{$cat.link}{else}{$Rewrite->url_category($cat)}{/if}">
                                    <img src="{$URL_UPLOADS}/{$cat.image}" alt="{$cat.name}"/></a>
                                <div class="about__body">
                                    <h3 class="about__title"><a
                                                href="{if $cat.link}{$cat.link}{else}{$Rewrite->url_category($cat)}{/if}">{$cat.name}</a>
                                    </h3>
                                    <div class="mt-2">
                                        {$cat.des|htmlDecode}
                                    </div>
                                </div>
                            </div>
                        </div>
                    {/foreach}
                </div>
            </section>
        </div>
    {else}
        <div class="page__content">
            <div class="banner">
                <div class="banner__wrapper">
                    <div class="banner__title">{$curCat.name}</div>
                </div>
                <img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt="{$curCat.name}"/>
            </div>
            <section class="section-2">
                <div class="row gutter-under-md-16 gutter-over-xl-60">
                    {foreach from=$arrListCatChildren key=k item=cat}
                        <div class="col-6 col-md-4 col-lg-4 mb-3 mb-sm-30">
                            <div class="about">
                                <a class="about__frame"
                                   href="{if $cat.link}{$cat.link}{else}{$Rewrite->url_category($cat)}{/if}">
                                    <img src="{$URL_UPLOADS}/{$cat.image}" alt="{$cat.name}"/></a>
                                <div class="about__body">
                                    <h3 class="about__title text-center">
                                        <a href="{if $cat.link}{$cat.link}{else}{$Rewrite->url_category($cat)}{/if}">{$cat.name}</a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    {/foreach}
                </div>
            </section>
        </div>
    {/if}
{else}
    {if $curCat.template eq 'page'}
        <div class="page__content">
            <div class="banner">
                <div class="banner__wrapper">
                    <div class="banner__title">{$curCat.name}</div>
                </div>
                <img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt="{$curCat.name}"/>
            </div>
            <section class="section-2">
                <div class="research_developement_des mb-30">
                    {$curCat.des|htmlDecode}
                </div>

                <div class="row gutter-under-md-16">
                    {foreach from=$arrListCatChildren key=k item=cat}
                        <div class="col-6 col-md-4 col-lg-4 mb-3 mb-sm-30">
                            <div class="about">
                                <a class="about__frame"
                                   href="#!">
                                    <img src="{$URL_UPLOADS}/{$cat.image}" alt="{$cat.name}"/></a>
                                <div class="about__body">
                                    <h3 class="about__title">
                                        <a href="#!">{$cat.name}</a>
                                    </h3>
                                    <div class="mt-2">
                                        {$cat.des|htmlDecode}
                                    </div>
                                </div>
                            </div>
                        </div>
                    {/foreach}
                </div>
            </section>
        </div>
    {else}
        <div class="page__content">
            <!-- main content-->
            <section class="section-2 pt-20">
                <article class="post mb-40">
                    {if $countListAbout > 1}
                        <ul class="post-nav">
                            {foreach from=$arrListAbout key=k item=about}
                                <li class="post-nav__item"><a class="post-nav__link"
                                                              href="#{$about.slub}-{$k}">{$about.title}</a></li>
                            {/foreach}
                        </ul>
                    {/if}
                    {if $curCat.is_show_contact eq 1}
                        <div class="post-content">
                            {foreach from=$arrListAbout key=k item=about}
                                <h2 class="post-heading text-primary" id="{$about.slub}-{$k}">{$about.title}</h2>
                                <div class="row">
                                    <div class="col-lg-6 mb-30">
                                        <form id="form-contact" method="post">
                                            <h2 class="text-20 mb-3">{'gui-thong-tin-cho-chung-toi'|lang}</h2>
                                            <div class="form-group">
                                                <label for="ct-name" class="d-none">Name</label>
                                                <input id="ct-name" name="name" type="text" class="form-control" placeholder="{'Full_name'|lang}">
                                            </div>
                                            <div class="form-group">
                                                <label for="ct-email" class="d-none">Email</label>
                                                <input id="ct-email" name="email" type="email" class="form-control" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="ct-phone" class="d-none">{'Phone'|lang}</label>
                                                <input id="ct-phone" name="phone" type="text" class="form-control" placeholder="{'Phone'|lang}">
                                            </div>
                                            <div class="form-group">
                                                <label for="ct-title" class="d-none">Title</label>
                                                <input id="ct-title" name="title" type="text" class="form-control" placeholder="{'tieu-de'|lang}">
                                            </div>
                                            <div class="form-group">
                                                <label for="ct-content" class="d-none">{'Content'|lang}</label>
                                                <textarea id="ct-content" name="content" class="form-control" rows="4"
                                                          placeholder="{'Content'|lang}"></textarea>
                                            </div>
                                            <button class="btn btn-primary" type="button" onclick="submitContact()">{'gui-cau-hoi'|lang}</button>
                                        </form>
                                    </div>
                                    <div class="col-lg-6 mb-30">
                                        <div class="d-none d-lg-block text-20 mb-2">&nbsp;</div>
                                        {$about.content|htmlDecode}
                                    </div>
                                    <div class="col-12">
                                        <div class="embed-responsive" style="height: 350px;">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d931.1138261316368!2d105.7757454!3d21.0144606!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345302b915c71d%3A0xbca488cecc456c5d!2sBIG%20CAPITAL.%2C%20JSC!5e0!3m2!1svi!2s!4v1580900249007!5m2!1svi!2s"
                                                    width="600" height="450" frameborder="0" style="border:0;"
                                                    allowfullscreen=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    {else}
                        <div class="post-content">
                            {foreach from=$arrListAbout key=k item=about}
                                <h2 class="post-heading text-primary" id="{$about.slub}-{$k}">{$about.title}</h2>
                                <div class="">
                                    {$about.content|htmlDecode}
                                </div>
                            {/foreach}
                        </div>
                    {/if}
                </article>

            </section>
        </div>
    {/if}
{/if}
