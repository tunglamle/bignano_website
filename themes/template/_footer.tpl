<!-- footer-->
<div class="sitemap d-lg-none">
    <div class="sitemap__wrapper">
        <ul class="sitemap__list">
            {foreach from=$arrListMainMenu key=k item=menu}
                 {if $menu.children}
                     <li class="sitemap__item sitemap__dropdown">
                        <a class="sitemap__link sitemap__dropdown-toggle  {if $menu.cat_id eq $curCat.cat_id}active{/if}" href="{$menu.href}">
                            <span>{$menu.title}</span>
                        </a>
                        <div class="sitemap__dropdown-menu"{if $menu.cat_id eq $curCat.cat_id} style="display: block" {else} style="display: none"{/if}>
                            <a class="sitemap__dropdown-title" href="{$menu.href}" >{$menu.title}</a>
                            <ul class="sitemap__sub">
                                {foreach from=$menu.children key=j item=submenu}
                                <li class="sitemap__sub-item">
                                    <a class="sitemap__sub-link" href="{$Rewrite->url_category($submenu)}">{$submenu.title}</a>
                                </li>
                                {/foreach}
                            </ul>
                        </div>
                     </li>
                     {else}
                     <li class="sitemap__item">
                        <a class="sitemap__link {if $menu.cat_id eq $curCat.cat_id}active{/if}" href="{$menu.href}">
                            <span>{$menu.title}</span>
                        </a>
                    </li>
                 {/if}
            {/foreach}
        </ul>
{*        <nav class="sitemap__nav">*}
{*            <a class="sitemap__nav-link" href="{$Rewrite->url_category($arrNews)}">News</a>*}
{*            <a class="sitemap__nav-link" href="{$Rewrite->url_category($arrJobCareers)}">Jobs & Careers</a>*}
{*        </nav>*}
    </div>
</div>

<footer class="footer">
    <div class="footer__wrapper">
        <div class="footer__content">
            <ul class="footer__nav">
                 {foreach from=$arrListBottomMenu key=k item=menu}
                <li class="footer__nav-item">
                    <a class="footer__nav-link" href="{$menu.href}">{$menu.title}</a>
                </li>
                {/foreach}
            </ul>
            <div class="footer__copyright">{$_CONFIG.copyright}</div>
        </div>
        <nav class="footer__elements">
            <div class="mr-3">
                <ul class="langs langs--white">
                    <li class="langs__item"><a class="langs__link {if $_LANG_ID == 'vn'}active{/if}" href="?lang=vn">VI</a></li>
                    <li class="langs__item"><a class="langs__link {if $_LANG_ID == 'en'}active{/if}" href="?lang=en">EN</a></li>
                </ul>
            </div>
            <a class="footer__element" href="{$VNCMS_URL}/{$_CONFIG.link_email}"><i class="icon icon-envelope"></i>  <span class="d-none">Contact</span></a>
        </nav>
    </div>
    <a class="footer__btn js-movetop" href="#!"><span class="d-none">Move Top</span></a>
</footer>
