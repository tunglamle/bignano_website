<div class="page__content">
    <!-- main content-->
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title">{$curCat.name}</div>
        </div><img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt="" />
    </div>
    <section class="section-2">
        <ul class="nav pd-tabs">
             {foreach from=$arrCatTypicalApplication key=k item=cat}
            <li class="nav-item">
                <a class="nav-link {if $cat.cat_id eq $id_typical}active{/if}{if !$id_typical && $k eq 0} active{/if}" href="#product-tab-{$k}" data-toggle="tab">{$cat.name}</a></li>
            {/foreach}
        </ul>
        <div class="tab-content">
            {foreach from=$arrCatTypicalApplication key=k item=cat}
            <div class="tab-pane {if $cat.cat_id eq $id_typical}active{/if}{if !$id_typical && $k eq 0} active{/if}" id="product-tab-{$k}">
                <div class="launching-slider">
                    <div class="launching-slider__pagination"></div>
                    <div class="swiper-container launching-slider__container">
                        <div class="swiper-wrapper">
                             {foreach from=$cat.arrTypical key=j item=typical}
                            <div class="swiper-slide">
                                <div class="launching-slider__item">
                                    <div class="launching-slider__frame">
                                        <img src="{$URL_UPLOADS}/{$typical.image}" alt="" />
                                    </div>
                                    <div class="launching-slider__desc"> {$typical.title} </div>
                                </div>
                            </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
            {/foreach}
        </div>
{*        <div class="mt-40"><a class="banner-2" href="#!">*}
{*                <h2 class="banner-2__title">Focus Products</h2>*}
{*                <div class="banner-2__frame"><img src="./images/banner-2.jpg" alt="" /></div>*}
{*            </a>*}
{*        </div>*}
    </section>
</div>