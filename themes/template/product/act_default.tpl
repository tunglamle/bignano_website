{if $curCat.parent_id eq 0}
<div class="page__content">
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title">{$curCat.name}</div>
        </div>
        {if $curCat.banner}
        <img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt="{$curCat.name}" />
        {/if}
    </div>
    <section class="section-2">
        <!--  <h2 class="heading">Product Search</h2> -->
        <ul class="nav tabs">
            {foreach from=$arrListCat key=k item=cat}
            <li class="nav-item">
                {if $cat.is_show_logo}
                <a class="nav-link" href="{$Rewrite->url_category($cat)}">{$cat.name}</a></li>
            {else}
            <a class="nav-link {if $cat.cat_id eq $show}active{/if}{if !$show && $k eq 0}active{/if}" href="#product-tab-{$k}" data-toggle="tab">{$cat.name}</a></li>
            {/if}
            {/foreach}
        </ul>
        <div class="tab-content">
            {foreach from=$arrListCat key=k item=cat}
            <div class="tab-pane {if $cat.cat_id eq $show}active{/if}{if !$show && $k eq 0} active{/if}" id="product-tab-{$k}">
                <div class="row gutter-under-sm-16">
                    {foreach from=$cat.arrCat key=j item=subcat}
                    <div class="col-6 col-md-3 mb-20 mb-md-40">
                        <div class="about">
                            <a class="about__frame" href="{$Rewrite->url_category($subcat)}">
                                {if $subcat.image}
                                <img src="{$URL_UPLOADS}/{$subcat.image}" alt="{$subcat.name}" />
                                {/if}
                            </a>
                            <div class="about__body">
                                <h3 class="about__title">
                                    <a href="{$Rewrite->url_category($subcat)}">{if $subcat.name}{$subcat.name}{else}Bignanotech{/if}</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                    {/foreach}
                </div>
            </div>
            {/foreach}
        </div>
        {if $curCat.phone}
        <div class="mt-30 p-30 p-lg-40 bg-light text-18 text-center">
            Contact: <a class="text-default phone" href="tel:{$curCat.phone}">{$curCat.phone}</a>
        </div>
        {/if}
    </section>
</div>
{elseif $curCat.parent_id eq $arrProduct.cat_id && $curCat.is_show_logo eq 0}
<div class="page__content">
    <!-- main content-->
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title">{$curCat.name}</div>
        </div>
        {if $curCat.banner}
        <img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt="{$curCat.name}" />
        {/if}
    </div>
    <section class="section-2">
        <h2 class="heading">{$curCat.name}</h2>
        <div class="row gutter-under-sm-16">
            {foreach from=$arrListCatByCurCat key=k item=cat}
            <div class="col-6 col-md-3 mb-20 mb-md-40">
                <div class="about">
                    <a class="about__frame" href="{$Rewrite->url_category($cat)}">
                        <img src="{$URL_UPLOADS}/{$cat.image}" alt="{$cat.name}" />
                    </a>
                    <div class="about__body">
                        <h3 class="about__title">
                            <a href="{$Rewrite->url_category($cat)}">{$cat.name}</a></h3>
                    </div>
                </div>
            </div>
            {/foreach}
        </div>
        {if $curCat.phone}
        <div class="mt-30 p-30 p-lg-40 bg-light text-18 text-center">
            Contact: <a class="text-default phone" href="tel:{$curCat.phone}">{$curCat.phone}</a>
        </div>
        {/if}
    </section>
</div>
{else}
{if $curCat.is_show_logo eq 1}
<div class="container mt-40">
    <section class="show_logo_text">
        {$curCat.des|htmlDecode}
    </section>
    <section class="partner-section pt-0">
        <div class="partner-section__body">
            <ul class="partners">
                {foreach from=$arrListPartner key=k item=adver}
                <li class="partners__item"><a class="partners__link" href="{$adver.link}">
                        <img src="{$URL_UPLOADS}/{$adver.image}" alt="{$adver.title}"></a></li>
                {/foreach}
            </ul>
        </div>
    </section>
</div>
{else}
<div class="page__content">
    <!-- main content-->
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title">{$curCat.name}</div>
        </div>
        {if $curCat.banner}
        <img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt="{$curCat.name}" />
        {/if}
    </div>
    <section class="section-2 pt-20">
        <ul class="nav pd-tabs my-30">
             {if $arrListProductNewLaunching}
            <li class="nav-item"><a class="nav-link active" href="#pd-tab-0" data-toggle="tab">NEW LAUNCHING</a>
            </li>
            {/if}
            {foreach from=$arrListCatProduct key=k item=cat}
            <li class="nav-item">
                <a class="nav-link" href="#pd-tab-{$k+2}" data-toggle="tab">{$cat.name}</a>
            </li>
            {/foreach}
        </ul>
        <div class="tab-content mb-40">
            {if $arrListProductNewLaunching}
            <div class="tab-pane active" id="pd-tab-0">
                <div class="launching-slider">
                    <div class="launching-slider__pagination"></div>
                    <div class="swiper-container launching-slider__container">
                        <div class="swiper-wrapper">
                            {foreach from=$arrListProductNewLaunching key=k item=product}
                            <div class="swiper-slide">
                                <a href="{$Rewrite->url_product($product)}" class="text-default">
                                    <div class="launching-slider__item">
                                        <div class="launching-slider__frame"><img src="{$URL_UPLOADS}/{$product.image_newlaunching}" alt="{$product.name}" />
                                        </div>
                                        <div class="launching-slider__desc">{$product.name}</div>
                                    </div>
                                </a>
                            </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
            {/if}
            {foreach from=$arrListCatProduct key=k item=cat}
            <div class="tab-pane" id="pd-tab-{$k+2}">
                <article class="post mb-40">
                    <div class="product-intro media">
                        <div class="product-intro__left">
                            {if $cat.image}
                            <img class="d-block w-100" src="{$URL_UPLOADS}/{$cat.image}" alt="{$cat.name}">
                            {/if}
                        </div>
                        <div class="product-intro__body media-body">
                            {$cat.des|htmlDecode}
                            {if $cat.catalouge}
                            <div class="pd-download ml-auto" style="max-width: 400px;">
                                <div class="pd-download__title">{'download-catalogue'|lang}</div>
                                <a class="pd-download__btn" href="{$URL_UPLOADS}/{$cat.catalouge}" download><i class="fa fa-arrow-circle-o-down mr-2"></i><span>{'download-catalogue'|lang}</span></a>
                                <p></p>
                            </div>
                            {/if}
                        </div>
                    </div>
                </article>
                {if $cat.arrProduct.0.product_id > 0}
                <div class="accordion">
                    <div class="accordion__top">
                        <a class="accordion__switch active" href="#!">{'all-open'|lang}</a>
                    </div>
                    {foreach from=$cat.arrProduct key=j item=product}
                    <div class="accordion__item">
                        <div class="accordion__header">
                            <h3 class="accordion__title">
                                <a href="{$Rewrite->url_product($product)}">{$product.name}</a></h3>
                            <div class="accordion__toggle active"></div>
                        </div>
                        <div class="accordion__body" style="display: block">
                            <div class="accordion__wrapper media">
                                <div class="accordion__left">
                                    {if $product.image}
                                    <img class="d-block w-100" src="{$URL_UPLOADS}/{$product.image}" alt="{$product.name}" />
                                    {/if}
                                </div>
                                <div class="media-body">
                                    <div class="accordion__content">
                                        {$product.sapo|htmlDecode}
                                    </div>
                                    <div class="accordion__btns">
                                        <a class="accordion__btn button button--red button--sm" href="{$Rewrite->url_product($product)}">{'product-details'|lang}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/foreach}
                </div>
                {/if}
            </div>
            {/foreach}
        </div>
    </section>
</div>
{/if}
{/if}