<div class="page__content">
    <!-- main content-->
    {* <nav class="navigation">
        <div class="navigation__wrapper">
            <div class="navigation__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="{$VNCMS_URL}">Home</a></li>
                    <li class="breadcrumb-item"><a class="link-unstyled" href="{$Rewrite->url_category($arrProduct)}">{$arrProduct.name}</a></li>
                    <li class="breadcrumb-item active">{$arrSearch.name}</li>
                </ol>
            </div>
            <div class="navigation__lang">
                <div class="langs-2">
                    <a class="langs-2__item" href="?lang=vn">
                        <img src="{$URL_IMAGES}/vn.svg" alt="">
                    </a>
                    <a class="langs-2__item" href="?lang=en">
                        <img src="{$URL_IMAGES}/gb.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </nav> *}
    <section class="section-2 pt-20">
        <article class="post mb-40">
            <div class="post-subtitle">Products Search Results</div>
            <h1 class="post-title">{$arrSearch.name}</h1>
            <span><b class="text-18">{$countSearchResult}</b> product</span>
            <div class="post-content">
                <h2 class="post-heading text-center" id="heading-1">Search Results</h2>
                <div class="accordion">
                    <div class="accordion__top">
                        <a class="accordion__switch active" href="#!">All open</a>
                    </div>
                     {foreach from=$arrListSearchResult key=k item=product}
                    <div class="accordion__item">
                        <div class="accordion__header">
                            <h3 class="accordion__title"><a href="{if $product.link}{$product.link}{else}{$Rewrite->url_product($product)}{/if}">{$product.name}</a></h3>
                            <div class="accordion__toggle active"></div>
                        </div>
                        <div class="accordion__body" style="display: block;">
                            <div class="accordion__wrapper media">
                                <div class="accordion__left">
                                    {if $product.list_image}
                                    <div class="image-slider">
                                        <div class="image-slider__container swiper-container">
                                            <div class="swiper-wrapper">
                                                {assign var="arrSlide" value=","|explode:$product.list_image}
                                                 {foreach from=$arrSlide key=j item=slide}
                                                <div class="swiper-slide">
                                                    <div class="image-slider__frame"><img src="{$URL_UPLOADS}/{$slide}" alt="" /></div>
                                                </div>
                                                {/foreach}
                                            </div>
                                        </div>
                                        <div class="image-slider__pagination"></div>
                                        <div class="image-slider__nav">
                                            <div class="image-slider__prev"><i class="fa fa-angle-left"></i></div>
                                            <div class="image-slider__next"><i class="fa fa-angle-right"></i></div>
                                        </div>
                                    </div>
                                        {else}
                                        <div class="image-slider__img"><img src="{$URL_UPLOADS}/{$product.image}" alt="" /></div>
                                    {/if}
                                    <div class="accordion__icons">
                                        {assign var="arrIcon" value=","|explode:$product.list_icon}
                                        {foreach from=$arrIcon key=j item=icon}
                                        <a class="accordion__icon" href="#!">
                                            <img src="{$URL_UPLOADS}/{$icon}" alt="" /></a>
                                        {/foreach}
                                    </div>
                                </div>
                                <div class="media-body">
                                    <div class="accordion__content">
                                        {$product.sapo|htmlDecode}
                                    </div>
                                    <div class="accordion__btns">
                                        <a class="accordion__btn button button--red" href="{if $product.link}{$product.link}{else}{$Rewrite->url_product($product)}{/if}">Product details</a>
                                        <a class="accordion__btn button button--dark" href="#!">Inquiry</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/foreach}
                </div>

            </div>
        </article>

    </section>
</div>
