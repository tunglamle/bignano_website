<div class="page__content">
    <!-- main content-->
    {* <nav class="navigation">
        <div class="navigation__wrapper">
            <div class="navigation__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="{$VNCMS_URL}">Home</a></li>
                    <li class="breadcrumb-item"><a class="link-unstyled" href="{$Rewrite->url_category($parCat)}">{$parCat.name}</a></li>
                    <li class="breadcrumb-item active">{$curCat.name}</li>
                </ol>
            </div>
            <div class="navigation__lang">
                <div class="langs-2">
                    <a class="langs-2__item" href="?lang=vn">
                        <img src="{$URL_IMAGES}/vn.svg" alt="">
                    </a>
                    <a class="langs-2__item" href="?lang=en">
                        <img src="{$URL_IMAGES}/gb.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </nav> *}
    {* <div class="banner">*}
        {* <div class="banner__wrapper">*}
            {* <div class="banner__title">Detail</div>*}
            {* </div><img class="banner__bg" src="{$URL_IMAGES}/banner-about.jpg" alt="" />*}
        {* </div>*}
    <section class="section-2">
        <div class="row">
            <div class="col-lg-6 mb-30 floating-container">
                <div class="floating">
                    {if $arrOneProduct.list_image}
                    <article class="preview-2">
                        <div class="preview-2__main">
                            <div class="preview-slider-2">
                                {if $arrOneProduct.list_image|is_array}
                                <div class="preview-slider-2__prev"><i class="fa fa-angle-left"></i></div>
                                <div class="preview-slider-2__next"><i class="fa fa-angle-right"></i></div>
                                <div class="preview-slider-2__pagination"></div>
                                {/if}
                                <div class="preview-slider-2__container swiper-container">
                                    <div class="swiper-wrapper">
                                        {foreach from=$arrOneProduct.list_image key=k item=slide}
                                        <div class="swiper-slide">
                                            <a class="preview-slider-2__frame js-zoom" href="{$URL_UPLOADS}/{$slide}" data-fancybox="product image">
                                                {if $slide}
                                                <img src="{$URL_UPLOADS}/{$slide}" alt="{$arrOneProduct.name}" />
                                                {/if}
                                            </a>
                                        </div>
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="preview-2__thumbs">
                            <div class="thumb-slider-2">
                                <div class="thumb-slider-2__container swiper-container">
                                    <div class="swiper-wrapper">
                                        {foreach from=$arrOneProduct.list_image key=k item=slide}
                                        <div class="swiper-slide">
                                            <div class="thumb-slider-2__frame">
                                                {if $slide}
                                                <img src="{$URL_UPLOADS}/{$slide}" alt="{$arrOneProduct.name}" />
                                                {/if}
                                            </div>
                                        </div>
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    {elseif $arrOneProduct.embed && $arrOneProduct.list_image ne ''}
                    <iframe width="100%" height="350" src="https://www.youtube.com/embed/{$arrOneProduct.embed}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    {else}
                    {/if}
                </div>
            </div>
            <div class="col-lg-6 mb-30">
                <section class="pd-detail">
                    <h1 class="pd-detail__title">{$arrOneProduct.name}</h1>
                    <div class="pd-detail__desc">
                        {$arrOneProduct.sapo|htmlDecode}
                    </div>
                    {* <div class="pd-detail__subtitle">VIDEO</div>*}
                    {* <div class="pd-detail__content">*}
                        {* <div class="embed-responsive embed-responsive-16by9"><iframe src='https://www.youtube.com/embed/SItFPrgEITM' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></div>*}
                        {* </div>*}
                </section>
                <div class="pd-tools">
                    <div class="pd-tools__header">
                        <a class="pd-tools__item" href=".md-sendmail" data-toggle="modal"><i class="fa fa-envelope mr-2"></i><span>Send mail</span>
                        </a>
                        <a class="pd-tools__item" href="#!" onclick="myFunction()"><i class="fa fa-print mr-2"></i><span>Print</span>
                        </a>
                        <script>
                            function myFunction() {
                                window.print();
                            }
                        </script>
                    </div>
                    <nav class="pd-tools__social">
                        <a class="pd-tools__social-item" href="javascript:share_facebook();"><i class="fa fa-facebook"></i></a>
                        <a class="pd-tools__social-item" href="{$social.youtube}"><i class="fa fa-youtube"></i></a>
                        <a class="pd-tools__social-item" href="https://zalo.me/{$social.zalo}"><img src="{$URL_IMAGES}/icon-zalo.png" alt="icon zalo" /></a>
                    </nav>
                </div>
            </div>
        </div>
        {if $arrOneProduct.content}
        <section class="pd-detail mb-30">
            <!-- <h2 class="pd-detail__title">Thông tin sản phẩm</h2>-->
            <div class="row">
                <div class="col-lg-8">
                    <div class="pd-detail__content">
                        {$arrOneProduct.content|htmlDecode}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="pd-download">
                        {if $arrOneProduct.embed}
                        <iframe width="100%" height="250" src="https://www.youtube.com/embed/{$arrOneProduct.embed}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        {/if}
                        
                        <!--   <div class="pd-download__subtitle">UNIVERSAL SORBENT SDS</div>
                            <div class="pd-download__text">Safety Data Sheet for Universal Meltblown Absorbent
                                Products
                            </div>-->
                        {if $arrOneProduct.uploads_document}
                        <div class="pd-download__title">{'download-catalogue'|lang}</div>
                        {foreach from = $arrOneProduct.uploads_document key=i item = dowloads}
                        <a class="pd-download__btn" href="{$URL_UPLOADS}/{$dowloads}"><i class="fa fa-arrow-circle-o-down mr-2"></i><span>{$dowloads}</span></a>
                        <p></p>
                        {/foreach}
                        {/if}
                        
                    </div>
                </div>
            </div>
        </section>
        {/if}
        <section class="pd-detail">
            <h2 class="pd-detail__title">Sản phẩm liên quan</h2>
            <div class="row gutter-20">
                {foreach from=$arrListOtherProduct key=k item=product}
                <div class="col-xl-2 col-lg-3 col-md-4 col-6 mb-20">
                    <div class="product-3">
                        <a class="product-3__frame" href="{$Rewrite->url_product($product)}">
                            {if $product.image}
                            <img src="{$URL_UPLOADS}/{$product.image}" alt="{$product.name}" />
                            {/if}
                        </a>
                        <h3 class="product-3__title">
                            <a href="{$Rewrite->url_product($product)}">{$product.name}</a></h3>
                        <!-- <div class="product-3__desc">
                                {$product.sapo|htmlDecode}
                            </div>-->
                        <div class="product-3__footer">
                            <a class="product-3__link" href="{$Rewrite->url_product($product)}">
                                <span>Xem chi tiết</span>
                                <i class="fa fa-long-arrow-right ml-2"></i></a></div>
                    </div>
                </div>
                {/foreach}
            </div>
        </section>
    </section>
</div>
<div class="md-sendmail modal fade" tabindex="-1">
    <div class="modal-dialog modal-md">
        <form class="modal-content" id="save_email_form" onsubmit="ajax_email();return false;">
            <div class="modal-body md-sendmail__body">
                <button class="md-sendmail__close" data-dismiss="modal"></button>
                <div class="md-sendmail__title">Send mail:</div>
                <div class="media mb-20"><img class="md-sendmail__img" src="{$URL_UPLOADS}/{$arrOneProduct.image}" alt="{$arrOneProduct.name}" />
                    <div class="md-sendmail__name media-body">{$arrOneProduct.name}
                    </div>
                    <input type="hidden" name="product" value="{$arrOneProduct.name}">
                </div>
                <div class="form-group">
                    <label>Họ và tên (*):</label>
                    <input class="form-control" name="name" type="text"/ required="">
                </div>
                <div class="form-group">
                    <label>Địa chỉ email (*):</label>
                    <input class="form-control" name="email" type="email" />
                </div>
                <div class="form-group">
                    <label>Số điện thoại (*):</label>
                    <input class="form-control" name="phone" type="text" />
                </div>
                <div class="text-muted">(*) Những trường bắt buộc</div>
                <div class="md-sendmail__footer">
                    <button class="md-sendmail__btn" type="submit">Gửi</button>
                    <span class="md-sendmail__btn-divider">hoặc</span>
                    <button class="md-sendmail__btn2" type="button" data-dismiss="modal">Huỷ bỏ</button>
                </div>
            </div>
        </form>
    </div>
</div>