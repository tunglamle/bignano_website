<!-- header-->
<header class="header">
    <div class="header__inner">
        <a class="header__logo" href="/">
            <img src="{$URL_UPLOADS}/{$_CONFIG.site_logo}" alt="{$_CONFIG.site_title}"></a>
        <a class="header__logo-2" href="/">
            <img src="{$URL_UPLOADS}/{$_CONFIG.site_logo2}" alt="{$_CONFIG.site_title}" />
        </a>
        <div class="header__elements">
            <form class="h-search" action="{$VNCMS_URL}/search">
                <div class="input-group">
                    <label for="header-search" class="d-none">{'Search'|lang}</label>
                    <input class="form-control" id="header-search" type="text" name="key" placeholder="{'search-our-site'|lang}..." />
                    <div class="input-group-append">
                        <button class="input-group-text" type="submit">
                            <i class="fa fa-search"></i>
                            <span class="d-none">{'Search'|lang}</span>
                        </button>
                    </div>
                </div>
            </form>
            <ul class="langs">
                <li class="langs__item"><a class="langs__link {if $_LANG_ID == 'vn'}active{/if}" href="?lang=vn">VI</a></li>
                <li class="langs__item"><a class="langs__link {if $_LANG_ID == 'en'}active{/if}" href="?lang=en">EN</a></li>
            </ul>
            <a class="header__element" href="{$VNCMS_URL}/{$_CONFIG.link_email}">
                <i class="icon icon-envelope">
                    <span class="d-none">Contact</span>
                </i>
            </a>
            <div class="header__hotline">
                <div class="media">
                    <div class="media-body">
                    	<div class="d-flex justify-content-between">
                            <span class="mr-1">Hotline:</span>
                            <a href="tel:+84868939595">(+84) 879.808.080</a>
                        </div>
                        <div class="d-flex justify-content-between">
                            <span class="mr-1">For importer:</span>
                            <a href="tel:+84913560223">(+84) 913.560.223</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="header__menu">
         {foreach from=$arrListMainMenu key=k item=menu}
        <a class="header__menu-link" href="{$menu.href}">
            <span class="header__menu-text">{$menu.title}</span>
        </a>
        {/foreach}
    </nav>
</header>
