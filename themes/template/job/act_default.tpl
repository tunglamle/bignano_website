    <div class="page__content">
        <!-- main content-->
        <div class="banner">
            <div class="banner__wrapper">
                <div class="banner__title">{$curCat.name}</div>
            </div><img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt="" />
        </div>
        <section class="section-2">
            <section class="news-section mb-40">
{*                <div class="news-section__header">*}
{*                    <h2 class="news-section__title">CSR News</h2>*}
{*                    <div class="news-section__elements">*}
{*                        <a class="news-section__element news-section__element--rss" href="#!">RSS</a>*}
{*                        <a class="news-section__element news-section__element--arrow" href="{$Rewrite->url_category($arrCatNewsCsr)}">CSR News Archives</a></div>*}
{*                </div>*}
                {foreach from=$arrListJob key=k item=job}
                    <a class="news-section__item" href="{$Rewrite->url_job($job)}">
                        <div class="news-section__date">{$job.reg_date|date_format:"%B %d, %Y"}</div>
{*                        <div class="news-section__icon">{$job.cat_name}</div>*}
                        <h3 class="news-section__text">{$job.title}</h3>
                        <span class="news-section__exp-date">Expiration Date: {$job.exp_date|date_format:"%d/ %m/ %Y"}</span>
                    </a>
                {/foreach}
            </section>
            <div class="row gutter-under-md-16">
                {foreach from=$arrListCatChildren key=k item=cat}
                    <div class="col-sm-6 col-md-4 col-lg-3 mb-30">
                        <a class="news-2" href="{$Rewrite->url_category($cat)}">
                            <div class="news-2__frame"><img src="{$URL_UPLOADS}/{$cat.image}" alt="" /></div>
                            <div class="news-2__overlay">
                                <div class="news-2__title">{$cat.name}</div>
                            </div>
                        </a>
                        {if $cat.arrCsr}
                            <div class="mt-30">
                                <ul class="news-list">
                                    {foreach from=$cat.arrCsr key=j item=csr}
                                        <li class="news-list__item"><a class="news-list__link" href="{$Rewrite->url_csr($csr)}">{$csr.title}</a></li>
                                    {/foreach}
                                </ul>
                            </div>
                        {/if}
                    </div>
                {/foreach}
            </div>
        </section>
    </div>
