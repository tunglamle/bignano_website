<div class="news-ajax-content">
    {if $arrListArticles.0 eq ''}
        <span class="text-center d-block mt-3">Please wait for new "News".</span>
    {else}
        {foreach from=$arrListArticles key=k item=news}
            <a class="news-section__item" href="{if $news.link}{$news.link}{else}{$Rewrite->url_article($news)}{/if}">
                <div class="news-section__date">{$news.reg_date|date_format:"%B %d, %Y"}</div>
                <div class="news-section__icon">{$news.cat_name}</div>
                <h3 class="news-section__text">{$news.title}</h3>
            </a>
        {/foreach}
    {/if}
</div>