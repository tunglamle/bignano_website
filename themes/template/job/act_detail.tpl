<div class="page__content">
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title">{$curCat.name}</div>
        </div><img class="banner__bg" src="{if $curCat.parent_id eq 0}{$URL_UPLOADS}/{$curCat.banner}{else}{$URL_UPLOADS}/{$parCat.banner}{/if}" alt="" />
    </div>
    <section class="section-2">
        <article class="post mb-40">
            <h1 class="post-title">{$arrOneJob.title}</h1>
            <div class="post-content">
                {$arrOneJob.content|htmlDecode}
            </div>
        </article>
    </section>
</div>
