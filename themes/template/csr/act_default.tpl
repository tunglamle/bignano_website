{if $curCat.parent_id eq 0}
    <div class="page__content">
    <!-- main content-->
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title">{$curCat.name}</div>
        </div><img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt="{$curCat.name}" />
    </div>
    <section class="section-2">
        <section class="news-section mb-40">
            <div class="news-section__header">
                <h2 class="news-section__title">CSR News</h2>
                <div class="news-section__elements">
                    <a class="news-section__element news-section__element--rss" href="#!">RSS</a>
                    <a class="news-section__element news-section__element--arrow" href="{$Rewrite->url_category($arrCatNewsCsr)}">CSR News Archives</a></div>
            </div>
             {foreach from=$arrListNewsCsr key=k item=news}
            <a class="news-section__item" href="{if $news.link}{$news.link}{else}{$Rewrite->url_article($news)}{/if}">
                <div class="news-section__date">{$news.reg_date|date_format:"%B %d, %Y"}</div>
                <div class="news-section__icon">{$news.cat_name}</div>
                <h3 class="news-section__text">{$news.title}</h3>
            </a>
            {/foreach}
        </section>
        <div class="row gutter-under-md-16">
             {foreach from=$arrListCatChildren key=k item=cat}
            <div class="col-sm-6 col-md-4 col-lg-3 mb-30">
                <a class="news-2" href="{$Rewrite->url_category($cat)}">
                    <div class="news-2__frame"><img src="{$URL_UPLOADS}/{$cat.image}" alt="{$cat.name}" /></div>
                    <div class="news-2__overlay">
                        <div class="news-2__title">{$cat.name}</div>
                    </div>
                </a>
                {if $cat.arrCsr}
                <div class="mt-30">
                    <ul class="news-list">
                         {foreach from=$cat.arrCsr key=j item=csr}
                        <li class="news-list__item"><a class="news-list__link" href="{$Rewrite->url_csr($csr)}">{$csr.title}</a></li>
                        {/foreach}
                    </ul>
                </div>
                {/if}
            </div>
            {/foreach}
        </div>
    </section>
    <section class="relate">
        <div class="relate__body">
            <h2 class="relate__title">Related information</h2>
            <div class="relate__grid">
                 {foreach from=$arrListRelatedCsr key=k item=adver}
                <div class="relate__col"><a class="relate__frame" href="{$adver.link}"><img src="{$URL_UPLOADS}/{$adver.image}" alt="{$adver.title}"></a></div>
                {/foreach}
            </div>
        </div>
    </section>
    <section class="partner-section">
        <div class="partner-section__body"><a class="partner-section__btn" href="#!">Ethical Reporting Desk</a>
            <div class="partner-section__list">
                <ul class="partners">
                    {foreach from=$arrListPartner key=k item=adver}
                    <li class="partners__item"><a class="partners__link" href="{$adver.link}"><img src="{$URL_UPLOADS}/{$adver.image}" alt="{$adver.title}"></a></li>
                    {/foreach}
                </ul>
            </div>
        </div>
    </section>
</div>
{else}
    <div class="page__content">
        <!-- main content-->
        <section class="section-2 py-20">
            <article class="post mb-40">
                <h1 class="post-title">{$curCat.name}</h1>
                <div class="post-content">
                    {$curCat.des|htmlDecode}
                </div>
            </article>
        </section>
    </div>
{/if}