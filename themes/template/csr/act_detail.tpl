<div class="page__content">
    <!-- main content-->
    <nav class="navigation">
        <div class="navigation__wrapper">
            <div class="navigation__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="{$VNCMS_URL}">Home</a></li>
                    <li class="breadcrumb-item"><a class="link-unstyled" href="{$Rewrite->url_category($parCat)}"">{$parCat.name}</a></li>
                    <li class="breadcrumb-item"><a class="link-unstyled" href="{$Rewrite->url_category($curCat)}">{$curCat.name}</a></li>
                    <li class="breadcrumb-item active">{$arrOneCsr.title}</li>
                </ol>
            </div>
            <div class="navigation__lang">
                <div class="langs-2">
                    <a class="langs-2__item" href="?lang=vn">
                        <img src="{$URL_IMAGES}/vn.svg" alt="">
                    </a>
                    <a class="langs-2__item" href="?lang=en">
                        <img src="{$URL_IMAGES}/gb.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </nav>
    <section class="section-2 py-20">
        <article class="post mb-40">
            <h1 class="post-title">{$arrOneCsr.title}</h1>
            <div class="post-content">
                {$arrOneCsr.content|htmlDecode}
            </div>
        </article>
    </section>
</div>