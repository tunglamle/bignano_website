<section class="partners">
    <div class="container">
        <h2 class="section__title"><span>Các đối tác</span></h2>
        <div class="partner-slider">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    {foreach from=$arrListAdver key=k item=adver}
                        <div class="swiper-slide">
                            <a class="partner" href="{$adver.link}">
                                <img src="{$URL_UPLOADS}/{$adver.image}" alt="{$adver.name}"/>
                            </a>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</section>