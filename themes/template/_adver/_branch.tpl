{if $arrListAdver}
    {foreach key=k item=adver from=$arrListAdver}
        <div class="contact__map mb-20 embed-responsive">
            {$adver.embed|htmlDecode}
        </div>
        <div style="padding: 6px 12px;border-left: 4px solid #4166b0;font-size: 12px;background-color: rgba(65, 102, 176, 0.1);">
            <strong>{'Base'|lang} {$k+1}:</strong>
            <span>{$adver.title}</span>
        </div>
    {/foreach}
{/if}
