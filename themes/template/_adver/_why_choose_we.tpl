{if $arrListAdver}
    <section class="section mb-3 pt-40">
        <div class="container">
            <div class="section__header">
                <h2 class="section__title">Tại sao bạn nên học cùng chúng tôi ?</h2>
            </div>
            <div class="row gutter-under-sm-16">
                {foreach key=k item=adver from=$arrListAdver}
                    <div class="col-6 col-lg-3 mb-30">
                        <div class="pros">
                            <div class="pros__iwrap">
                                <a class="text-default" href="{$adver.link}">
                                    <img class="pros__img" src="{$URL_UPLOADS}/{$adver.image}" onerror="this.src='{$URL_IMAGES}/nopic.png'" alt="{$adver.title}" />
                                </a>
                            </div>
                            <div class="pros__title">
                                <a class="text-default" href="{$adver.link}">{$adver.title}</a>
                            </div>
                            <div class="pros__desc">{$adver.des|htmlDecode|strip_tags}</div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </section>
{/if}