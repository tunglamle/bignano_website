{if $arrListAdver}
    <section class="section">
        <div class="container">
            <div class="section__header">
                <h2 class="section__title">Học viên nói gì</h2>
                <div class="section__desc">Cảm nhận của học viên sau khi học</div>
            </div>
            <div class="testimonial-slider js-testimonial-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        {foreach key=k item=adver from=$arrListAdver}
                            <div class="swiper-slide">
                                <div class="testimonial">
                                    <div class="testimonial__text">
                                        “ {$adver.des|htmlDecode|strip_tags|truncate:650:"..."}. ”
                                    </div>
                                    <div class="media align-items-center">
                                        <div class="testimonial__avatar">
                                            <img src="{$URL_UPLOADS}/{$adver.image}" alt="{$adver.title}" />
                                        </div>
                                        <div class="media-body">
                                            <div class="testimonial__name">{$adver.title}</div>
                                            <div class="testimonial__pos">{$adver.occupations}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                    <div class="swiper-pagination">
                    </div>
                </div>
            </div>
        </div>
    </section>
{/if}