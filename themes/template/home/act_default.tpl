<h1 class="d-none">{$_CONFIG.site_title}</h1>
<div class="page__content">
    <div class="home-movie">
        <div class="home-movie__body">
            <div class="embed-responsive embed-responsive-16by9 home-video">
                <video class="embed-responsive-item" src="{$URL_MEDIA}/video5.mp4" poster="" autoplay="autoplay"
                       muted="muted" playsinline="playsinline" loop="loop">
            </div>
        </div>
        <div class="home-movie__aside">
            <div class="home-movie__close"><i class="fa fa-angle-double-right"></i></div>
            <div class="home-movie__aside-item">
                {$_CONFIG.content_slider|htmlDecode}
                <!-- div class="product-5">
                    <div class="product-5__part">
                        <div class="product-5__frame">
                            <a href="{$_CONFIG.right_slide_link}" class="text-default">
                                <img src="{$URL_UPLOADS}/{$_CONFIG.right_slide_image}" alt="{$_CONFIG.right_slide_alt}"/>
                            </a>
                        </div>
                    </div>
                    <div class="product-5__part">
                        <div class="product-5__body">
                            {foreach from=$arrListRightMenu key=k item=right}
                                <div class="product-5__item">
                                    <a class="product-5__link text-uppercase" href="{$right.href}">
                                        <i class="fa fa-angle-right mr-2"></i>
                                        <span>{$right.title}</span></a>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <div style="border-top: 1px solid white"></div>
    <!-- notification-->
    <section class="section" id="products">
        <h2 class="section__title">{'Product'|lang}</h2>
        <div class="section__body">
            <div class="row gutter-under-sm-16 gutter-over-xl-60">
                {foreach from=$arrCatProductHome key=k item=cat}
                    <div class="col-6 col-md-4 mb-20 mb-md-40">
                        <div class="about">
                            <a class="about__frame"
                               href="{$Rewrite->url_category($cat)}">
                                <img src="{$URL_UPLOADS}/{$cat.image}" alt="{$cat.name}"/>
                            </a>
                            <div class="about__body">
                                <h3 class="about__title text-center">
                                    <a href="{$Rewrite->url_category($cat)}">{$cat.name}</a>
                                </h3>
                            </div>
                        </div>
                        {* <div class="service">
                            <a class="service__frame" href="{if $cat.is_show_logo eq 1}{$Rewrite->url_category($cat)}{else}{$Rewrite->url_category($arrProduct)}?show={$cat.cat_id}{/if}">
                                <img src="{$URL_UPLOADS}/{$cat.image}" alt="" />
                            </a>
                            <h3 class="service__title">
                                <a href="{$Rewrite->url_category($cat)}">
                                    {$cat.name}
                                </a>
                            </h3>
                        </div> *}
                    </div>
                {/foreach}
            </div>
        </div>
    </section>
    <section class="section section--gray">
        <h2 class="section__title">{'typical-application'|lang}</h2>
        <div class="section__body">
            <div class="row gutter-under-sm-16">
                {foreach from=$arrListApplocationHome key=k item=cat}
                    <div class="col-6 col-lg-3 mb-3 mb-sm-30">
                        <div class="application">
                            <a class="application__frame"
                               href="{$Rewrite->url_category($cat)}">
                                <img src="{$URL_UPLOADS}/{$cat.image}" alt="{$cat.name}"/></a>
                            <div class="application__desc">
                                {$cat.name}
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </section>
    <section class="section">
        <h2 class="section__title">{'about_us'|lang}</h2>
        <div class="section__body">
            <div class="row gutter-under-md-16 gutter-over-xl-60">
                {foreach from=$arrCatAboutHome key=k item=cat}
                    <div class="col-sm-6 col-lg-4 col-lg-4 mb-20">
                        <div class="about about--2">
                            {* <a class="about__frame"
                            href="{if $cat.link}{$cat.link}{else}{$Rewrite->url_category($cat)}{/if}">
                                <img src="{$URL_UPLOADS}/{$cat.image}" alt="{$cat.name}"/></a> *}
                            <div class="about__body">
                                <h3 class="about__title text-center">
                                    <a href="{if $cat.link}{$cat.link}{else}{$Rewrite->url_category($cat)}{/if}">{$cat.name}</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </section>
{*    <section class="section section--gray">*}
{*        <h2 class="section__title">Latest Information</h2>*}
{*        <div class="section__body">*}
{*            <ul class="nav home-news-tabs">*}
{*                <li class="nav-item"><a class="nav-link js-news-tab active" href="#news-tab-1"*}
{*                                        data-toggle="tab">News</a></li>*}
{*                <li class="nav-item"><a class="nav-link js-news-tab" href="#news-tab-2" data-toggle="tab">IR News</a>*}
{*                </li>*}
{*                <li class="nav-item"><a class="nav-link js-news-tab" href="#news-tab-3"*}
{*                                        data-toggle="tab">Notifications</a></li>*}
{*            </ul>*}
{*            <div class="tab-content">*}
{*                <div class="tab-pane active" id="news-tab-1">*}
{*                    <div class="d-none d-md-block">*}
{*                        <div class="news-slider">*}
{*                            <div class="news-slider__prev"></div>*}
{*                            <div class="news-slider__next"></div>*}
{*                            <div class="news-slider__pagination"></div>*}
{*                            <div class="news-slider__container swiper-container">*}
{*                                <div class="swiper-wrapper">*}
{*                                    {foreach from=$arrListNews key=k item=news}*}
{*                                        <div class="swiper-slide">*}
{*                                            <a class="news" href="{$Rewrite->url_article($news)}">*}
{*                                                <div class="news__date">{$news.reg_date|date_format:"%B %d, %Y"}</div>*}
{*                                                <div class="news__frame">*}
{*                                                    <img src="{$URL_UPLOADS}/{$news.image}" alt="{$news.title}"/>*}
{*                                                </div>*}
{*                                                <h3 class="news__title">{$news.title}</h3>*}
{*                                                *}{*                                        <div class="news__icon news__icon--pdf"></div>*}
{*                                            </a>*}
{*                                        </div>*}
{*                                    {/foreach}*}
{*                                </div>*}
{*                            </div>*}
{*                        </div>*}
{*                    </div>*}
{*                    <div class="d-md-none">*}
{*                        {foreach from=$arrListNews key=k item=news}*}
{*                            <div class="mt-2">*}
{*                                <div class="news-3 media">*}
{*                                    <a class="news-3__frame" href="{$Rewrite->url_article($news)}">*}
{*                                        <img src="{$URL_UPLOADS}/{$news.image}" alt="{$news.title}"/>*}
{*                                    </a>*}
{*                                    <div class="media-body">*}
{*                                        <div class="news-3__time">{$news.reg_date|date_format:"%B %d, %Y"}</div>*}
{*                                        <h3 class="news-3__title">*}
{*                                            <a href="{$Rewrite->url_article($news)}">{$news.title}</a>*}
{*                                        </h3>*}
{*                                    </div>*}
{*                                </div>*}
{*                            </div>*}
{*                        {/foreach}*}
{*                    </div>*}
{*                </div>*}
{*                <div class="tab-pane" id="news-tab-2">*}
{*                    <div class="d-none d-md-block">*}
{*                        <div class="news-slider">*}
{*                            <div class="news-slider__prev"></div>*}
{*                            <div class="news-slider__next"></div>*}
{*                            <div class="news-slider__pagination"></div>*}
{*                            <div class="news-slider__container swiper-container">*}
{*                                <div class="swiper-wrapper">*}
{*                                    {foreach from=$arrListNewsIr key=k item=news}*}
{*                                        <div class="swiper-slide">*}
{*                                            <a class="news" href="{$Rewrite->url_article($news)}">*}
{*                                                <div class="news__date">{$news.reg_date|date_format:"%B %d, %Y"}</div>*}
{*                                                <div class="news__frame"><img src="{$URL_UPLOADS}/{$news.image}"*}
{*                                                                              alt="{$news.title}"/></div>*}
{*                                                <h3 class="news__title">{$news.title}</h3>*}
{*                                                *}{*                                        <div class="news__icon news__icon--pdf"></div>*}
{*                                            </a>*}
{*                                        </div>*}
{*                                    {/foreach}*}
{*                                </div>*}
{*                            </div>*}
{*                        </div>*}
{*                    </div>*}
{*                    <div class="d-md-none">*}
{*                        {foreach from=$arrListNewsIr key=k item=news}*}
{*                            <div class="mt-2">*}
{*                                <div class="news-3 media">*}
{*                                    <a class="news-3__frame" href="{$Rewrite->url_article($news)}">*}
{*                                        <img src="{$URL_UPLOADS}/{$news.image}" alt="{$news.title}"/>*}
{*                                    </a>*}
{*                                    <div class="media-body">*}
{*                                        <div class="news-3__time">{$news.reg_date|date_format:"%B %d, %Y"}</div>*}
{*                                        <h3 class="news-3__title">*}
{*                                            <a href="{$Rewrite->url_article($news)}">{$news.title}</a>*}
{*                                        </h3>*}
{*                                    </div>*}
{*                                </div>*}
{*                            </div>*}
{*                        {/foreach}*}
{*                    </div>*}
{*                </div>*}
{*                <div class="tab-pane" id="news-tab-3">*}
{*                    <div class="d-none d-md-block">*}
{*                        <div class="news-slider">*}
{*                            <div class="news-slider__prev"></div>*}
{*                            <div class="news-slider__next"></div>*}
{*                            <div class="news-slider__pagination"></div>*}
{*                            <div class="news-slider__container swiper-container">*}
{*                                <div class="swiper-wrapper">*}
{*                                    {foreach from=$arrListNewsNotifications key=k item=news}*}
{*                                        <div class="swiper-slide">*}
{*                                            <a class="news" href="{$Rewrite->url_article($news)}">*}
{*                                                <div class="news__date">{$news.reg_date|date_format:"%B %d, %Y"}</div>*}
{*                                                <div class="news__frame"><img src="{$URL_UPLOADS}/{$news.image}"*}
{*                                                                              alt="{$news.title}"/></div>*}
{*                                                <h3 class="news__title">{$news.title}</h3>*}
{*                                                *}{*                                        <div class="news__icon news__icon--pdf"></div>*}
{*                                            </a>*}
{*                                        </div>*}
{*                                    {/foreach}*}
{*                                </div>*}
{*                            </div>*}
{*                        </div>*}
{*                    </div>*}
{*                    <div class="d-md-none">*}
{*                        {foreach from=$arrListNewsNotifications key=k item=news}*}
{*                            <div class="mt-2">*}
{*                                <div class="news-3 media">*}
{*                                    <a class="news-3__frame" href="{$Rewrite->url_article($news)}">*}
{*                                        <img src="{$URL_UPLOADS}/{$news.image}" alt="{$news.title}"/>*}
{*                                    </a>*}
{*                                    <div class="media-body">*}
{*                                        <div class="news-3__time">{$news.reg_date|date_format:"%B %d, %Y"}</div>*}
{*                                        <h3 class="news-3__title">*}
{*                                            <a href="{$Rewrite->url_article($news)}">{$news.title}</a>*}
{*                                        </h3>*}
{*                                    </div>*}
{*                                </div>*}
{*                            </div>*}
{*                        {/foreach}*}
{*                    </div>*}
{*                </div>*}
{*            </div>*}
{*            <div class="text-center mt-30"><a class="my-button" href="{$Rewrite->url_category($arrNews)}">News*}
{*                    Archives</a></div>*}
{*        </div>*}
{*    </section>*}
</div>
