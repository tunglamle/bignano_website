<article class="banner">
    <img class="banner__bg w-100" src="{$URL_UPLOADS}/{$arrOnePage.image}" alt="{$arrOnePage.name}" />
    <div class="container">
        <h2 class="banner__title f-osb">{$arrOnePage.name}</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a class="link-unstyled" href="{$VNCMS_URL}">Trang chủ</a></li>
            <li class="breadcrumb-item active">{$arrOnePage.name}</li>
        </ol>
    </div>
</article>

<main class="agency-main py-40">
    <div class="container">
        <h1 class="agency-title f-osb">DANH SÁCH ĐẠI LÝ HUY BẢO</h1>
        <div class="row gutter-20">
            <div class="col-lg-6 order-1 order-lg-0">
                <div class="wrap_ggmap w-100 h-100" style="min-height: 500px" id="wrap_ggmap">
                    Google map
                </div>
            </div>
            <div class="col-lg-6 mb-30 mb-lg-0">
                <div class="maps-title f-osb">
                    <img src="{$URL_IMAGES}/agency-icon.png" class="mr-6" alt="">
                    Tìm kiếm theo địa chỉ
                </div>
                <form action="" class="agency-form" id="agency_id">
                    <div class="row gutter-20">
                        <div class="col-md-6">
                            <select class="agency-select form-control" name="province_id" id="province" onchange="this.form.submit();">
                                <option value="">-- Chọn tỉnh/thành phố --</option>
                                {$htmlOptionProvince}
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select class="agency-select form-control" name="district_id" id="district" onchange="this.form.submit();">
                                <option value="">-- Chọn quận/huyện --</option>
                                {$htmlOptionDistrict}
                            </select>
                        </div>
                    </div>
                </form>
                <div class="location-wrap">
                    {if $arrListAgency[0].name ne ''}
                    {foreach from=$arrListAgency key=k item=agency}
                    <div class="location item-agency cf coord" data-id="{$agency.agency_id}" data-lat="{$agency.latitude}" data-lng="{$agency.longitude}" data-address="{$agency.address}" data-title="{$agency.name}">
                        <div class="location-name f-osb">
                            <a href="#!" class="location-link text-default">
                                {$agency.name}{$agency.agency_id}
                            </a>
                        </div>
                        <div class="location-address">
                            <i class="fa fa-map-marker"></i>
                            {$agency.address}
                        </div>
                        <div class="location-phone">
                            <i class="fa fa-phone"></i>
                            {$agency.phone}
                        </div>
                    </div>
                    {/foreach}
                    {else}
                        <div class="text-danger mt-4">Hiện chưa có đại lý nào ở địa điểm này</div>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript " src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwpwR40C5TNuJyPqK1czYk1vL0gNXAn3M "></script>
<script src="{$URL_JS}/_bundle.js " type="text/javascript "></script>