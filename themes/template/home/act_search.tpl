<div class="container my-40">
    <div class="accordion">
        <div class="accordion__top justify-content-md-between flex-md-row flex-column">
            <h3 class="">Kết quả tìm kiếm cho từ khóa: <span style="color:#cf0212;">{$key}</span></h3>
            <a class="accordion__switch active" href="#!" style="max-width: 136px; margin-left: auto">All open</a>
        </div>
        {foreach from=$arrListProduct key=j item=product}
            <div class="accordion__item">
                <div class="accordion__header">
                    <h3 class="accordion__title">
                        <a href="{$Rewrite->url_product($product)}">{$product.name}</a></h3>
                    <div class="accordion__toggle active"></div>
                </div>
                <div class="accordion__body" style="display: block">
                    <div class="accordion__wrapper media">
                        <div class="accordion__left">
                            <div class="">
                                {if $product.image}
                                <img class="d-block w-100" src="{$URL_UPLOADS}/{$product.image}" alt="{$product.name}"/>
                                {/if}
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="accordion__content">
                                {$product.sapo|htmlDecode}
                            </div>
                            <div class="accordion__btns">
                                <a class="accordion__btn button button--red button--sm"
                                   href="{$Rewrite->url_product($product)}">Product
                                    details</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
</div>