<article class="banner">
    <img class="banner__bg" src="{$URL_UPLOADS}/{$arrOnePage.banner}" alt=""/>
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a class="link-unstyled" href="/">{'Home'|lang}</a></li>
            <li class="breadcrumb-item active">{$arrOnePage.name}</li>
        </ol>
    </div>
</article>

<main class="contact-main">
    <div class="container mb-30">
        <div class="row">
            <div class="col-lg-6 mb-30">
                <section>
                    <form action="" id="form-contact">
                    <h2 class="text-30 mb-20 font-weight-bold">{'gui-lien-he'|lang}</h2>
                    <div class="row">
                        <div class="col-xl-6 col-lg-12 col-md-6">
                            <div class="form-group mb-30">
                                <input class="form-control rounded-0"  name="name" type="text" placeholder="{'Full_name'|lang}">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-12 col-md-6">
                            <div class="form-group mb-30">
                                <input class="form-control rounded-0" name="email" type="email" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-30">
                        <input class="form-control rounded-0" name="phone" type="text" placeholder="{'Phone'|lang}">
                    </div>
                    <div class="form-group mb-30">
                        <textarea class="form-control rounded-0" name="content" rows="7" placeholder="{'Content'|lang}"></textarea>
                    </div>
                    <button class="btn bg-main cl-w font-weight-bold text-uppercase rounded-0 pl-5 pr-5"
                            type="button" onclick="submitContact()">
                        {'lien-he-ngay'|lang}
                    </button>
                    </form>
                </section>
            </div>
            <div class="col-lg-6 mb-30">
                <section>
                    <h2 class="text-30 mb-20 font-weight-bold">{'thong-tin'|lang}</h2>
                    <div class="text-18 text-uppercase mb-4 font-weight-bold">
                        {$_CONFIG.site_name}
                    </div>
                    <ul class="contact-info">
                        <li class="media align-items-center mb-20">
                            <div class="contact-info__iwrap"><i class="fa fa-map-marker fa-2x"></i></div>
                            <div class="media-body">
                                <div class="text-18 font-weight-bold">{'Address'|lang}</div>
                                <div>{$_CONFIG.site_address}</div>
                            </div>
                        </li>
                        <li class="media align-items-center mb-20">
                            <div class="contact-info__iwrap"><i class="fa fa-envelope fa-2x"></i></div>
                            <div class="media-body">
                                <div class="text-18 text-700">Email</div>
                                <div>{$_CONFIG.site_email}</div>
                            </div>
                        </li>
                        <li class="media align-items-center mb-20">
                            <div class="contact-info__iwrap"><i class="fa fa-phone fa-2x"></i></div>
                            <div class="media-body">
                                <div class="text-18 font-weight-bold">Tel / Fax</div>
                                <div>{$_CONFIG.site_hotline}</div>
                            </div>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
        <div class="contact-map embed-responsive">
            {$_CONFIG.contact_map|htmlDecode}
        </div>
    </div>
</main>