<div class="page__content">
    <!-- main content-->
    <nav>
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a class="link-unstyled" href="{$VNCMS_URL}">Trang chủ</a></li>
                <li class="breadcrumb-item active">Catalog</li>
            </ol>
        </div>
    </nav>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-9 mb-30 order-lg-1">
                    <article class="post mb-30">
                        <form action="" method="post">
                        <h1 class="post-title">{$arrOneAdver.sub_title}</h1>
                        <div class="post-info">
                            <div class="mr-3">
                                <div class="fb-like" data-href="{$VNCMS_URL}/cataloge?cataloge_id={$arrOneAdver.adver_id}" data-width="" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                            </div>
                            <div><a class="download-btn" href="#!"><i class="fa fa-download mr-2"></i><button name="btnDownload" value="button" class="bg-transparent text-white border-0">Tải về</button></a>
                            </div>
                        </div>
                        <div class="post-content">
                            {foreach from=$arrOneAdver.list_image key=k item=adver}
                            <p><img src="{$URL_UPLOADS}/{$adver}" alt="" class="w-100" style="display: block; margin: 0 auto;"></p>
                            {/foreach}
                        </div>
                        <div class="text-center mt-30"><a class="download-btn" href="#!"><i class="fa fa-download mr-2"></i><button name="btnDownload" value="button" class="bg-transparent text-white border-0">Tải về</button></a>
                        </div>
                        </form>
                    </article>
                </div>
                <div class="col-lg-4 col-xl-3 mb-30">
                    {include file="_blocks/sidebar.tpl"}
                </div>
            </div>
        </div>
    </section>
</div>