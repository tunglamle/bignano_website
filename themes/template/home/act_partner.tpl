<div class="page-content">
    <!-- main content-->
    <section class="banner"><img class="banner__bg" src="{$URL_UPLOADS}/{$arrOnePage.image}" alt="" />
        <div class="container">
            <div class="banner__title">{$arrOnePage.name}</div>
            <div class="banner__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="{$VNCMS_URL}">Trang chủ</a></li>
                    <li class="breadcrumb-item active">{$arrOnePage.name}</li>
                </ol>
            </div>
        </div>
    </section>
    <div class="container pt-40 pb-60">
        <div class="mb-4">
            <p>{$arrOnePage.content|htmlDecode}</p>
        </div>
        <div class="row gutter-under-sm-16">
            {foreach from=$arrListPartner key=k item=partner}
            <div class="col-lg-2 col-md-3 col-4 mb-3 mb-sm-30">
                <div class="partner">
                    <a class="partner__frame" href="{$partner.link}">
                        <img src="{$URL_UPLOADS}/{$partner.image}" alt="{$partner.title}" /></a>
                    <h3 class="partner__title">{$partner.title}</h3>
                </div>
            </div>
            {/foreach}
        </div>
        <nav class="d-flex justify-content-center">
            <ul class="pagination">
                {$clsPaging->showPagingNew2()}
            </ul>
        </nav>
    </div>
</div>