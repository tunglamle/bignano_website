<div class="page__content">
    <!-- main content-->
    {* <nav class="navigation">
        <div class="navigation__wrapper">
            <div class="navigation__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="{$VNCMS_URL}">Home</a></li>
                    <li class="breadcrumb-item active">{$arrOnePage.name}</li>
                </ol>
            </div>
            <div class="navigation__lang">
                <div class="langs-2">
                    <a class="langs-2__item" href="?lang=vn">
                        <img src="{$URL_IMAGES}/vn.svg" alt="">
                    </a>
                    <a class="langs-2__item" href="?lang=en">
                        <img src="{$URL_IMAGES}/gb.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </nav> *}
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title">{$arrOnePage.name}</div>
        </div><img class="banner__bg" src="{$URL_UPLOADS}/{$arrOnePage.banner}" alt="" />
    </div>
    <section class="section-2">
        <article class="post">
            <div class="post-content">
                <h2 class="post-heading"><a href="#!">{$arrOnePage.name}</a></h2>
                {$arrOnePage.content|htmlDecode}

            </div>
        </article>
{*        <section class="relate-info">*}
{*            <h2 class="relate-info__title">Related information</h2>*}
{*            <ul class="relate-info__list">*}
{*                <li class="relate-info__item"><a class="relate-info__link" href="#!">Corporate Data</a></li>*}
{*                <li class="relate-info__item"><a class="relate-info__link" href="#!">Our Philosophy</a></li>*}
{*                <li class="relate-info__item"><a class="relate-info__link" href="#!">Facilities Worldwide</a></li>*}
{*                <li class="relate-info__item"><a class="relate-info__link" href="#!">History</a></li>*}
{*            </ul>*}
{*        </section>*}
    </section>
</div>
