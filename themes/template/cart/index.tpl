{if $sub ne "default"}
	{if $core->template_exists("$mod/sub_$sub.tpl")}
		{include file="$mod/sub_$sub.tpl"}
	{else}
		Sub Module File not Found!
	{/if}
{else}
	{if $act ne "default"}
		{if $core->template_exists("$mod/act_$act.tpl")}
			{include file="$mod/act_$act.tpl"}
		{else}
			Action File not Found! {$act}
		{/if}
	{else}
		{include file="$mod/act_default.tpl"}
	{/if}
{/if}