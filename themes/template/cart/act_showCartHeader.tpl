<div class="media-wrap-js">
    {if $allItem.0->itemId eq 0}
        <span class="d-block" style="color:#CB2028;">Bạn chưa thêm sản phẩm nào !</span>
    {else}
        <div class="cart-media-wrap" id="cart-media-wrap">
            {foreach from=$allItem key=k item=product}
                {assign var=itemId value=$product->itemId}
                {assign var=oneProduct value=$clsItem->getOne($itemId)}
                <div class="media header-media">
                    <div class="media-img">
                        <a href="{$Rewrite->url_product($oneProduct)}" class="text-default">
                            <img class="" src="{$URL_UPLOADS}/{$product->image}" style="width: 70px;" alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <a href="{$Rewrite->url_product($oneProduct)}" class="text-default media-link d-block">
                            {$product->name}
                        </a>
                        <span class="media-span d-block">
                                              <span class="item-amount">{$product->quantity}</span>
                                              x
                                              <span class="font-weight-bold item-money">
                                                  {$core->callfunc("number_format", $product->price, 0, ',', '.')}đ
                                              </span>
                                            </span>
                    </div>
                </div>
            {/foreach}
        </div>
        <div class="cart-total-money font-weight-bold">
            <span style="color:#666666;">Tổng phụ:</span>
            <span style="color:#CB2028;">{$core->callfunc("number_format", $totalPrice, 0, ',', '.')}đ</span>
        </div>
        <a href="{$VNCMS_URL}/show-cart" class="text-default cart-view-cart">
            GIỎ HÀNG
        </a>
        <a href="{$VNCMS_URL}/cart" class="text-default cart-pay">
            THANH TOÁN
        </a>
    {/if}
</div>