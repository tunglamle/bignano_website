<main class="page-content">
    <!-- main content-->
    <div class="breadcrumb-bg">
        <div class="container">
            <div aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-black">
                    <li class="breadcrumb-item"><a href="{$VNCMS_URL}">{$core->getLang("Home")}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Giỏ hàng</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container mb-60">
        {if $arrListItem}
            <article class="page-show-cart">
                <div class="cart__header">
                    <h2 class="cart__title">Đơn hàng của bạn</h2>
                </div>
                <form action="" id="cart">
                    <div class="cart__body table-responsive">
                        <table class="table">
                            <thead>
                            <tr class="text-nowrap">
                                <th>STT</th>
                                <th>Hình ảnh</th>
                                <th>Tên sản phẩm</th>
                                <th class="text-center">Số lượng</th>
                                <th class="text-right">Đơn giá</th>
                                <th class="text-right">Tổng (VNĐ)</th>
                                <th class="text-center">Xoá</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach from=$arrListItem key=k item=product}
                                {assign var=itemId value=$product->itemId}
                                {assign var=oneProduct value=$clsItem->getOne($itemId)}
                                <tr class="js-cart-parent item_r{$itemId}">
                                    <td class="text-center" width="1%">{$k+1}</td>
                                    <td width="1%">
                                        <a class="cart__frame" href="{$Rewrite->url_product($oneProduct)}">
                                            <img src="{$URL_UPLOADS}/{$oneProduct.image}"
                                                 alt="{$oneProduct.name}"/></a></td>
                                    <td>
                                        <a class="text-default" href="{$Rewrite->url_product($oneProduct)}">{$oneProduct.name}</a>
                                    </td>
                                    <td width="1%">
                                        <div class="input-group input-group-sm product-quantity js-quantity"><span
                                                    class="input-group-prepend js-quantity-btn" data-plus="-1">
                                                    <button class="input-group-text" type="button"><i
                                                                class="fa fa-minus fa-fw"></i></button></span>
                                            <input id="quantity_r{$product->itemId}" class="form-control js-quantity-value quantity" type="text" value="{$product->quantity}" name="quantity[]" data-unit="{$oneProduct.price}"
                                                   data-parent=".js-cart-parent" data-target=".js-cart-target"
                                                   data-total=".js-cart-total" data-itemId="{$product->itemId}" data-quantity="{$product->quantity}"/><span
                                                    class="input-group-append js-quantity-btn" data-plus="1">
                                                    <button class="input-group-text" type="button"><i
                                                                class="fa fa-plus fa-fw"></i></button></span>
                                        </div>
                                    </td>
                                    <td class="text-right text-nowrap text-700" style="color:#BE1E2D;" width="1%">
                                        <span id="price_r{$product->itemId}" data-price="{$oneProduct.price}">{$core->callfunc("number_format", $oneProduct.price, 0, ',', '.')}</span>
                                    </td>
                                    {math equation="x * y" x=$oneProduct.price y=$product->quantity assign="z"}
                                    <td class="text-right text-nowrap text-700" style="color:#BE1E2D;" width="1%">
                                        <span id="total_r{$itemId}" class="js-cart-target total_r_item" data-val="{$z}">{$core->callfunc("number_format", $z, 0, ',', '.')}</span>
                                    </td>
                                    <td class="text-center" width="1%">
                                        <a class="" style="color:#BE1E2D;" href="javascript:removeCartItem({$product->itemId},'item_r{$itemId}');">
                                            <i class="fa fa-minus-circle fa-lg"></i>
                                        </a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                    <div class="cart__footer">
                        <div class="text-16 text-700 text-center mb-10"><span class="mr-3">Tổng cộng (VNĐ)</span>
                            <span class="js-cart-total" style="color: #BE1E2D" id="total_order">{$core->callfunc("number_format", $total_price, 0, ',', '.')}đ</span></div>
                    </div>
                    <div class="cart__footer">
                        <a class="btn btn-default px-30 rounded-18 mb-10" style="color:#BE1E2D; border-color: #BE1E2D; " href="{$VNCMS_URL}">Mua hàng tiếp</a>
                        <a class="btn btn-light px-30 rounded-18 mb-10 ml-2" href="#!" onclick="deleteAll()">Xoá hết</a>
                        <a class="btn px-30 rounded-18 mb-10 text-700 ml-2 text-white" style="background-color: #BE1E2D;" href="{$VNCMS_URL}/cart">Thanh toán</a>
                    </div>
                </form>
            </article>
        {else}
            <h2 class="text-center">BẠN CHƯA CHỌN SẢN PHẨM NÀO !!!</h2>
        {/if}
    </div>
</main>