<div class="bg-gray my-2 payment-info-page">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 offset-xl-1">
                <ul class="payment-step list-unstyled">
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">1</span>
		                    <strong>{'Shopping_Cart'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">2</span>
		                    <strong>{'Client_Information'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">3</span>
		                    <strong>{'Payment_Methods'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">4</span>
		                    <strong>{'Payment_Confirm'|lang}</strong>
		                </a>
		            </li>
		        </ul>               
                <div class="border border-gray bg-white px20">
                    <div class="bank-acc">
                        <div class="row">

<div class="box-form-vali">
{'Payment successfull'|lang}!
</div>

                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>