<div class="page__content">
    <!-- main content-->
    <nav>
        <div class="container">
            <ol class="breadcrumb breadcrumb-black">
                <li class="breadcrumb-item"><a class="link-unstyled" href="{$VNCMS_URL}">Trang chủ</a></li>
                <li class="breadcrumb-item active">Đặt hàng</li>
            </ol>
        </div>
    </nav>
    <section class="section mb-60">
        <div class="container">
            <h2 class="section__title wow fadeInDown text-uppercase" data-wow-delay="0.3s">Đặt hàng</h2>
            {*            <div class="section__desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad architecto eos neque provident cum quasi, doloribus illum veritatis. Molestiae odio possimus aspernatur omnis deleniti adipisci dolorum praesentium doloremque laboriosam beatae.</div>*}
            <form id="order" >
                <article class="order">
                    <div class="row">
                        <div class="col-lg-4 mb-30">
                            <h3 class="order__subtitle">Thông tin khách hàng</h3>
                            <div class="form-group">
                                <input class="form-control" required name="fullname" value="{$core->_USER.fullname}" type="text" placeholder="Họ và tên" />
                            </div>
                            <div class="form-group form-inline">
                                <label>Giới tính</label>
                                <div class="ml-3">
                                    <label class="radio-styled">
                                        <input class="radio-styled__input" type="radio" checked name="check_info" value="Nam" /><span class="radio-styled__icon"></span><span>Nam</span>
                                    </label>
                                </div>
                                <div class="ml-3">
                                    <label class="radio-styled">
                                        <input class="radio-styled__input" type="radio" name="check_info" value="Nữ" /><span class="radio-styled__icon"></span><span>Nữ</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="form-control js-number" name="phone" value="{$core->_USER.phone}" required type="text" placeholder="Số điện thoại" />
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="email" name="email" value="{$core->_USER.email}" required placeholder="Email" />
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Địa chỉ nhận hàng"  name="address" value="{$core->_USER.address}" required />
                            </div>
                            <div class="form-text">Bạn cần hỗ trợ online? Hotline<a class="text-700 ml-1" style="color:#BE1E2D;" href="tel:{$_CONFIG.site_hotline}">{$_CONFIG.site_hotline}</a></div>
                        </div>
                        <div class="col-lg-4 mb-30">
                            <h3 class="order__subtitle">Thời gian nhận hàng</h3>
                            <div class="form-group">
                                <div class="mb-2">
                                    <label class="radio-styled">
                                        <input class="radio-styled__input" type="radio" checked name="check_time" value="Trong giờ hành chính"/><span class="radio-styled__icon"></span><span>Trong giờ hành chính</span>
                                    </label>
                                </div>
                                <div class="mb-2">
                                    <label class="radio-styled">
                                        <input class="radio-styled__input" type="radio" name="check_time" value="Ngoài giờ hành chính" /><span class="radio-styled__icon"></span><span>Ngoài giờ hành chính</span>
                                    </label>
                                </div>
                            </div>
                            <h3 class="order__subtitle mt-3">Hình thức thanh toán</h3>
                            <div class="form-group">
                                <div class="mb-2">
                                    <label class="radio-styled">
                                        <input class="radio-styled__input" type="radio" checked name="check_pay" value="Thanh toán khi nhận hàng (COD)" /><span class="radio-styled__icon"></span><span>Thanh toán khi nhận hàng (COD)</span>
                                    </label>
                                </div>
                                <div class="mb-2">
                                    <label class="radio-styled">
                                        <input class="radio-styled__input" type="radio"name="check_pay" value="Chuyển khoản qua ngân hàng" /><span class="radio-styled__icon"></span><span>Chuyển khoản ngân hàng</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <textarea class="form-control" name="content" value="{$core->_USER.content} rows="4" placeholder="Viết chú ý, yêu cầu hoá đơn GTGT..."></textarea>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-30">
                            <h3 class="order__subtitle">Đơn hàng ({$totalQuantity} sản phẩm)</h3>
                            <ul class="order__list">
                                {foreach from=$arrListItem key=k item=product}
                                    {assign var=itemId value=$product->itemId}
                                    {assign var=oneProduct value=$clsProduct->getOne($itemId)}
                                    <li class="item_r{$product->itemId}">
                                        <div class="cart-product media js-order-product">
                                            <a class="cart-product__remove" href="#!" onclick="removeCartItem({$product->itemId},'item_r{$product->itemId}')">&times;</a>
                                            <a class="cart-product__frame" href="{$Rewrite->url_product($oneProduct)}">
                                                <img src="{$URL_UPLOADS}/{$oneProduct.image}" style="width: 70px;" alt="{$oneProduct.name}" />
                                            </a>
                                            <div class="media-body">
                                                <h3 class="cart-product__title mb-10">
                                                    <a href="{$Rewrite->url_product($oneProduct)}" class="text-default">{$oneProduct.name}</a>
                                                </h3>
                                                <div class="d-flex align-items-center">
                                                    <div class="input-group input-group-sm product-quantity js-quantity">
                                                <span class="input-group-prepend js-quantity-btn" data-plus="-1">
                                                                <button class="input-group-text" type="button">
                                                                    <i class="fa fa-minus fa-fw"></i>
                                                                </button>
                                                </span>
                                                        <input id="quantity_orderR{$product->itemId}" class="form-control js-quantity-value order_quantity" name="order_quantity[]" type="text" value="{$product->quantity}" data-orderItemId="{$product->itemId}" data-orderPrice="{$product->price}" />
                                                        <span class="input-group-append js-quantity-btn" data-plus="1">
                                                                <button class="input-group-text" type="button">
                                                                    <i class="fa fa-plus fa-fw"></i>
                                                                </button>
                                                </span>
                                                    </div>
                                                    {math equation="x * y" x=$product->price y=$product->quantity assign="z"}
                                                    <div class="cart-product__price"><span class="total_r_orderItem" id="total_orderR{$product->itemId}" data-orderVal={$z}>{$core->callfunc("number_format", $z, 0, ',', '.')}</span><span> VNĐ</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                {/foreach}
                            </ul>
                            <div class="order__total">
                                <span class="mr-3">Tổng cộng:</span>
                                <span class="js-order-product-total" id="totalOrder" style="color:#da282d;">{$core->callfunc("number_format", $totalPrice, 0, ',', '.')}</span><span style="color:#da282d"> VNĐ</span></div>
                            <button class="btn btn-block btn-dark text-uppercase text-700 rounded-18" type="button" onclick="createOrder();">ĐẶT HÀNG</button>
                            <input type="hidden" name="btn" value="Order">
                        </div>
                    </div>
                </article>
            </form>
        </div>
    </section>
</div>