<!-- main content-->
<div class="container pt-30">
    <h2 class="cart-title">Giỏ hàng của bạn</h2>
    <div class="cart-desc">( 1 sản phẩm )</div>
    <ul class="cart-nav nav" role="tablist">
        <li class="nav-item">
            <a class="nav-link" href="#san-pham-chinh-ngach" role="tab" data-toggle="tab">Sản phẩm chính ngạch</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="#san-pham-xuat-hoa-don" role="tab" data-toggle="tab">Sản phẩm xuất hoá đơn</a>
        </li>
    </ul>
    <div class="tab-content mb-40">
        <div class="tab-pane fade" id="san-pham-chinh-ngach" role="tabpanel">
            <h1>sản phẩm chính ngạch</h1>
        </div>
        <div class="tab-pane fade active show" id="san-pham-xuat-hoa-don" role="tabpanel">
            <div class="table-responsive">
                <table class="table cart-table">
                    <thead>
                        <tr>
                            <th>Sản phẩm</th>
                            <th>Số lượng</th>
                            <th>Thành tiền</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="js-cart-row">
                            <td width="50%">
                                <div class="product-2 media">
                                    <a class="product-2__iwrap" href="#!">
                                        <img src="images/product-2-img.png" alt="" />
                                    </a>
                                    <div class="media-body d-flex flex-column align-self-stretch">
                                        <div class="product-2__title">
                                            <a class="link-unstyled" href="#!">[ヌノネット] レディース チュニック ロング丈 サイドスリット ゆったり 体型カバー トップス 白 グレー M L XL 2XL</a>
                                        </div>
                                        <div class="text-16 text-danger mb-1 js-unitprice">90000 đ</div>
                                        <div class="product-2__info">
                                            <p>Nơi bán: amazon.com</p>
                                            <p>Mô tả: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td width="1%">
                                <div class="input-group input-group-sm" style="width: 108px;">
                                    <span class="input-group-prepend js-number-btn" data-plus="-1">
                                        <button class="input-group-text bg-white text-10" type="button">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </span>
                                    <input class="form-control text-center text-14 js-number-value" type="text" value="1">
                                    <span class="input-group-append js-number-btn" data-plus="1">
                                        <button class="input-group-text bg-white text-10" type="button">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </span>
                                </div>
                            </td>
                            <td class="text-16 text-danger js-totalprice" width="1%" style="white-space: nowrap;"></td>
                            <td width="1%">
                                <a class="text-16 font-weight-bold font-italic text-purple" href="#!" data-toggle="tooltip" title="Xoá khỏi giỏ hàng">X</a>
                            </td>
                        </tr>
                        <tr class="js-cart-row">
                            <td width="50%">
                                <div class="product-2 media">
                                    <a class="product-2__iwrap" href="#!">
                                        <img src="images/product-2-img.png" alt="" />
                                    </a>
                                    <div class="media-body d-flex flex-column align-self-stretch">
                                        <div class="product-2__title">
                                            <a class="link-unstyled" href="#!">[ヌノネット] レディース チュニック ロング丈 サイドスリット ゆったり 体型カバー トップス 白 グレー M L XL 2XL</a>
                                        </div>
                                        <div class="text-16 text-danger mb-1 js-unitprice">90000 đ</div>
                                        <div class="product-2__info">
                                            <p>Nơi bán: amazon.com</p>
                                            <p>Mô tả: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td width="1%">
                                <div class="input-group input-group-sm" style="width: 108px;">
                                    <span class="input-group-prepend js-number-btn" data-plus="-1">
                                        <button class="input-group-text bg-white text-10" type="button">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </span>
                                    <input class="form-control text-center text-14 js-number-value" type="text" value="1">
                                    <span class="input-group-append js-number-btn" data-plus="1">
                                        <button class="input-group-text bg-white text-10" type="button">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </span>
                                </div>
                            </td>
                            <td class="text-16 text-danger js-totalprice" width="1%" style="white-space: nowrap;"></td>
                            <td width="1%">
                                <a class="text-16 font-weight-bold font-italic text-purple" href="#!" data-toggle="tooltip" title="Xoá khỏi giỏ hàng">X</a>
                            </td>
                        </tr>
                        <tr class="js-cart-row">
                            <td width="50%">
                                <div class="product-2 media">
                                    <a class="product-2__iwrap" href="#!">
                                        <img src="images/product-2-img.png" alt="" />
                                    </a>
                                    <div class="media-body d-flex flex-column align-self-stretch">
                                        <div class="product-2__title">
                                            <a class="link-unstyled" href="#!">[ヌノネット] レディース チュニック ロング丈 サイドスリット ゆったり 体型カバー トップス 白 グレー M L XL 2XL</a>
                                        </div>
                                        <div class="text-16 text-danger mb-1 js-unitprice">90000 đ</div>
                                        <div class="product-2__info">
                                            <p>Nơi bán: amazon.com</p>
                                            <p>Mô tả: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td width="1%">
                                <div class="input-group input-group-sm" style="width: 108px;">
                                    <span class="input-group-prepend js-number-btn" data-plus="-1">
                                        <button class="input-group-text bg-white text-10" type="button">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </span>
                                    <input class="form-control text-center text-14 js-number-value" type="text" value="1">
                                    <span class="input-group-append js-number-btn" data-plus="1">
                                        <button class="input-group-text bg-white text-10" type="button">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </span>
                                </div>
                            </td>
                            <td class="text-16 text-danger js-totalprice" width="1%" style="white-space: nowrap;"></td>
                            <td width="1%">
                                <a class="text-16 font-weight-bold font-italic text-purple" href="#!" data-toggle="tooltip" title="Xoá khỏi giỏ hàng">X</a>
                            </td>
                        </tr>
                        <tr class="js-cart-row">
                            <td width="50%">
                                <div class="product-2 media">
                                    <a class="product-2__iwrap" href="#!">
                                        <img src="images/product-2-img.png" alt="" />
                                    </a>
                                    <div class="media-body d-flex flex-column align-self-stretch">
                                        <div class="product-2__title">
                                            <a class="link-unstyled" href="#!">[ヌノネット] レディース チュニック ロング丈 サイドスリット ゆったり 体型カバー トップス 白 グレー M L XL 2XL</a>
                                        </div>
                                        <div class="text-16 text-danger mb-1 js-unitprice">90000 đ</div>
                                        <div class="product-2__info">
                                            <p>Nơi bán: amazon.com</p>
                                            <p>Mô tả: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td width="1%">
                                <div class="input-group input-group-sm" style="width: 108px;">
                                    <span class="input-group-prepend js-number-btn" data-plus="-1">
                                        <button class="input-group-text bg-white text-10" type="button">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </span>
                                    <input class="form-control text-center text-14 js-number-value" type="text" value="1">
                                    <span class="input-group-append js-number-btn" data-plus="1">
                                        <button class="input-group-text bg-white text-10" type="button">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </span>
                                </div>
                            </td>
                            <td class="text-16 text-danger js-totalprice" width="1%" style="white-space: nowrap;"></td>
                            <td width="1%">
                                <a class="text-16 font-weight-bold font-italic text-purple" href="#!" data-toggle="tooltip" title="Xoá khỏi giỏ hàng">X</a>
                            </td>
                        </tr>
                        <tr class="js-cart-row">
                            <td width="50%">
                                <div class="product-2 media">
                                    <a class="product-2__iwrap" href="#!">
                                        <img src="images/product-2-img.png" alt="" />
                                    </a>
                                    <div class="media-body d-flex flex-column align-self-stretch">
                                        <div class="product-2__title">
                                            <a class="link-unstyled" href="#!">[ヌノネット] レディース チュニック ロング丈 サイドスリット ゆったり 体型カバー トップス 白 グレー M L XL 2XL</a>
                                        </div>
                                        <div class="text-16 text-danger mb-1 js-unitprice">90000 đ</div>
                                        <div class="product-2__info">
                                            <p>Nơi bán: amazon.com</p>
                                            <p>Mô tả: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td width="1%">
                                <div class="input-group input-group-sm" style="width: 108px;">
                                    <span class="input-group-prepend js-number-btn" data-plus="-1">
                                        <button class="input-group-text bg-white text-10" type="button">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </span>
                                    <input class="form-control text-center text-14 js-number-value" type="text" value="1">
                                    <span class="input-group-append js-number-btn" data-plus="1">
                                        <button class="input-group-text bg-white text-10" type="button">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </span>
                                </div>
                            </td>
                            <td class="text-16 text-danger js-totalprice" width="1%" style="white-space: nowrap;"></td>
                            <td width="1%">
                                <a class="text-16 font-weight-bold font-italic text-purple" href="#!" data-toggle="tooltip" title="Xoá khỏi giỏ hàng">X</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="text-16 text-right mb-4">
                <strong class="mr-50">Tổng tiền:</strong>
                <span class="text-danger js-grandtotal"></span>
            </div>
            <div class="text-right mb-3">
                <a class="btn btn-md btn-circle btn-gradient mb-2" href="#!">Tiếp tục mua hàng</a>
                <a class="btn btn-md btn-circle btn-outline-gradient mb-2 ml-30" href="#!">Thanh toán đơn hàng</a>
            </div>
            <div class="text-right">
                <div class="font-italic">Đơn hàng của quý khách đã đủ điều kiện được miễn phí giao hàng trong nước</div>
            </div>
        </div>
    </div>
</div>