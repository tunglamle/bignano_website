<div class="bg-gray my-2 payment-info-page">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 offset-xl-1">
                <ul class="payment-step list-unstyled">
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">1</span>
		                    <strong>{'Shopping_Cart'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">2</span>
		                    <strong>{'Client_Information'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">3</span>
		                    <strong>{'Payment_Methods'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">4</span>
		                    <strong>{'Payment_Confirm'|lang}</strong>
		                </a>
		            </li>
		        </ul>               
				<div class="border border-gray bg-white px20 mb-4 pt-3 pb-3">
<p>{'Order_ID'|lang}: <strong>{$arrOneOrder.order_code}</strong></p>
<p>{'Grand_total'|lang} : <strong class='text-danger'>{$arrOneOrder.total_cost|number_format} VND</strong></p>
				</div>
                <div class="border border-gray bg-white px20">
                    <div class="bank-acc">
<p>{'You_chose_payment_over_visa'|lang}.</p>
<form action="" method="POST" id="fPrepayAtm">										
<div class="row">
	<div class="col-lg-12">
		<div class="form-group">
			<label>{'Choose_type_card'|lang}:</label>
			<ul id="list_bank_images">
			{$list_bank_images}
			</ul>
		</div>
	</div>
	<div class="col-lg-12 mt-3">
		<div class="form-group">
			<input type="hidden" name="btnSubmit" value="Submit">
			<button type="submit" class="btn btn-primary btn-danger">{'Process_payment'|lang}</button>
		</div>
	</div>											
</div>
<input type="hidden" name="total_amount" value="{$arrOneOrder.total_cost}">
<input type="hidden" name="payer_name" value="{$arrOneOrder.c_fullname}">
<input type="hidden" name="payer_phone_no" value="{$arrOneOrder.c_phone}">
<input type="hidden" name="payer_email" value="{$arrOneOrder.c_email}">
<input type="hidden" name="address" value="{$arrOneOrder.c_addess}">										
<input type="hidden" name="active_submit" value="submit"/>
<input type="hidden" name="bank_payment_method_id" id="bank_payment_method_id" value=""/>
<input type="hidden" name="shipping_address" size="30" value="Ha Noi"/>
<input type="hidden" name="payer_message" size="30" value="Ok"/>
<input type="hidden" name="extra_fields_value" size="30" value=""/>
<input type="hidden" name="extra_payment" id="extra_payment" value=""/>
</form>

                    </div>
                </div>
                <small><a href="{$url_prepay}">&larr; {'Choose_other_payment'|lang}</a></small>
            </div>
        </div>
    </div>
</div>