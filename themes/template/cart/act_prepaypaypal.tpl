<div class="bg-gray my-2 payment-info-page">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 offset-xl-1">
                <ul class="payment-step list-unstyled">
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">1</span>
		                    <strong>{'Shopping_Cart'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">2</span>
		                    <strong>{'Client_Information'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">3</span>
		                    <strong>{'Payment_Methods'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">4</span>
		                    <strong>{'Payment_Confirm'|lang}</strong>
		                </a>
		            </li>
		        </ul>               
				<div class="border border-gray bg-white px20 mb-4 pt-3 pb-3">
<p>{'Order_ID'|lang}: <strong>{$arrOneOrder.order_code}</strong></p>
<p>{'Grand_total'|lang} : <strong class='text-danger'>{$arrOneOrder.total_cost|number_format} VND</strong></p>
				</div>
                <div class="border border-gray bg-white px20">
                    <div class="bank-acc">
<p>{'You_chose_payment_over_paypal'|lang}
<div id="choosePrepayMethod" class="mb-5">
	<div id="paypal-button-container" style="display:;" class="btn btn-payment"></div>
</div>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
var order_code = "{$arrOneOrder.order_code}";
var url_prepay_process = "{$url_prepay_process}";
</script>
{literal}
<script>
        paypal.Button.render({           
            //env: 'production', 
            env: 'sandbox',
            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: {
                sandbox:    '{/literal}{$PayPal_CLIENT_ID}{literal}',
                production: '{/literal}{$PayPal_CLIENT_ID}{literal}'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {
                
                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: '{/literal}{$arrOneOrder.total_cost_usd}{literal}', currency: 'USD' }
                            }
                        ]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {

                // Make a call to the REST api to execute the payment
                return actions.payment.execute().then(function() {
                    console.log('Payment Complete!');
        
                    window.location = url_prepay_process+"?process=1000&paymentID="+data.paymentID+"&payerID="+data.payerID+"&paymentToken="+data.paymentToken+"&order_code="+order_code;

                });
            }


        }, '#paypal-button-container');

</script>
{/literal}
                    </div>
                </div>
                <small><a href="{$url_prepay}">&larr; {'Choose_other_payment'|lang}</a></small>                                            
            </div>
        </div>
    </div>
</div>