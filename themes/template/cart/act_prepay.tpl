<div class="bg-gray my-2 payment-info-page">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 offset-xl-1">
                <ul class="payment-step list-unstyled">
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">1</span>
		                    <strong>{'Shopping_Cart'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">2</span>
		                    <strong>{'Client_Information'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item active">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">3</span>
		                    <strong>{'Payment_Methods'|lang}</strong>
		                </a>
		            </li>
		            <li class="payment-step__item">
		                <a class="payment-step__link">
		                    <span class="payment-step__number">4</span>
		                    <strong>{'Payment_Confirm'|lang}</strong>
		                </a>
		            </li>
		        </ul>
		        <div class="border border-gray bg-white px20 mb-4 pt-3 pb-3">
<p>{'Order_ID'|lang}: <strong>{$arrOneOrder.order_code}</strong></p>
<p>{'Grand_total'|lang} : <strong class='text-danger'>{$arrOneOrder.total_cost|number_format} VND</strong></p>
				</div>
                <div class="row">
                    <div class="col-6 col-sm-4 mb30">
                        <a class="d-block border border-gray" href="{$url_prepaypaypal}">
                            <img class="w-100" src="{$URL_IMAGES}/paypal.png" alt="">
                        </a>
                    </div>
                    <div class="col-6 col-sm-4 mb30">
                        <a class="d-block border border-gray" href="{$url_prepayvisa}">
                            <img class="w-100" src="{$URL_IMAGES}/smart-link.png" alt="">
                        </a>
                    </div>
                    <div class="col-6 col-sm-4 mb30">
                        <a class="d-block border border-gray" href="{$url_prepayatm}">
                            <img class="w-100" src="{$URL_IMAGES}/smart-link-domestic.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="border border-gray bg-white px20">
                    <div class="bank-acc">
                        <div class="row dvsArticleContent">
                            <div class="col-lg-4 mb-3">
                                {$_CONFIG.list_banks.0|htmlDecode}
                            </div>
                            <div class="col-lg-4 mb-3">
                                {$_CONFIG.list_banks.1|htmlDecode}
                            </div>
                            <div class="col-lg-4 mb-3">
                                {$_CONFIG.list_banks.2|htmlDecode}
                            </div>
                        </div>
                    </div>
                    <ul class="payment-card list-unstyled">
                        <li class="payment-card__item">
                            <a href="#!">
                                <img class="payment-card__img" src="{$URL_IMAGES}/payment-card-1.png" alt="">
                            </a>
                        </li>
                        <li class="payment-card__item">
                            <a href="#!">
                                <img class="payment-card__img" src="{$URL_IMAGES}/payment-card-2.png" alt="">
                            </a>
                        </li>
                        <li class="payment-card__item">
                            <a href="#!">
                                <img class="payment-card__img" src="{$URL_IMAGES}/payment-card-3.png" alt="">
                            </a>
                        </li>
                        <li class="payment-card__item">
                            <a href="#!">
                                <img class="payment-card__img" src="{$URL_IMAGES}/payment-card-4.png" alt="">
                            </a>
                        </li>
                        <li class="payment-card__item">
                            <a href="#!">
                                <img class="payment-card__img" src="{$URL_IMAGES}/payment-card-5.png" alt="">
                            </a>
                        </li>
                    </ul>
                    <section class="bank-support">
                        <div class="bank-support__label">{'We_support_these_payment'|lang}</div>
                        <ul class="bank-support__list list-unstyled mb-0">
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-1.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-2.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-3.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-4.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-5.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-6.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-7.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-8.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-9.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-10.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-11.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-12.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-13.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-14.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-15.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-16.png" alt="">
                                </a>
                            </li>
                            <li class="bank-support__item">
                                <a class="d-block" href="#!">
                                    <img class="bank-support__img" src="{$URL_IMAGES}/bank-17.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>