<div class="page__content">
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title">{$curCat.name}</div>
        </div><img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt="" />
    </div>
    
    <section class="section-2">
        <article class="post mb-40">
            <h1 class="post-title">{$arrOneArticle.title}</h1>
            <div class="post-content">
                {$arrOneArticle.content|htmlDecode}
            </div>
        </article>
{*        <ul class="back-nav">*}
{*            <li class="back-nav__item"><a class="back-nav__link" href="{$Rewrite->url_category($arrNews)}">Back to 2019 News</a></li>*}
{*            <li class="back-nav__item"><a class="back-nav__link" onclick="goBack()" href="#!">Back to {$curCat.name}</a></li>*}
{*        </ul>*}
{*        <script>*}
{*            function goBack() {*}
{*                window.history.back();*}
{*            }*}
{*        </script>*}
    </section>
</div>
