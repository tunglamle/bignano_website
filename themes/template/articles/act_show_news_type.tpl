<div class="news-type_content">
    {foreach from=$arrListArticles key=k item=news}
        <a class="news-section__item"
           href="{if $news.link}{$news.link}{else}{$Rewrite->url_article($news)}{/if}">
            <div class="news-section__date">{$news.reg_date|date_format:"%B %d, %Y"}</div>
            <div class="news-section__icon">
                {if $news.news_type eq 0}
                    News
                {else}
                    Job
                {/if}
            </div>
            <h3 class="news-section__text">{$news.title}</h3>
        </a>
    {/foreach}
</div>