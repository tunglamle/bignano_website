{*<div class="page__content">*}
{*    <div class="banner">*}
{*        <div class="banner__wrapper">*}
{*            <div class="banner__title">{if $curCat.parent_id eq 0}{$curCat.name}{else}{$parCat.name}{/if}</div>*}
{*        </div><img class="banner__bg" src="{if $curCat.parent_id eq 0}{$URL_UPLOADS}/{$curCat.banner}{else}{$URL_UPLOADS}/{$parCat.banner}{/if}" alt="" />*}
{*    </div>*}
{*    <section class="section-2">*}
{*        <ul class="news-tabs">*}
{*            <li class="news-tabs__item"><a class="news-tabs__link {if $curCat.cat_id eq $arrNews.cat_id}active{/if}" href="{$Rewrite->url_category($arrNews)}">Show All</a></li>*}
{*             {foreach from=$arrListCatNewsChildren key=k item=cat}*}
{*            <li class="news-tabs__item"><a class="news-tabs__link {if $curCat.cat_id eq $cat.cat_id}active{/if}" href="{$Rewrite->url_category($cat)}">{$cat.name}</a></li>*}
{*            {/foreach}*}
{*        </ul>*}
{*        <section class="news-section mb-40">*}
{*            <div class="news-section__header">*}
{*                <form action="" id="form_news">*}
{*                    <input type="hidden" name="cat_id" value="{$curCat.cat_id}">*}
{*                <select class="form-control rounded-0 my-12" name="year" onchange="showNews()" style="width: 250px; max-width: 100%; height: 45px">*}
{*                     {foreach from=$arrListYear key=k item=year}*}
{*                    <option value="{$year.name}">{$year.name}</option>*}
{*                    {/foreach}*}
{*                </select>*}
{*                </form>*}
{*                <div class="news-section__elements"><a class="news-section__element news-section__element--rss" href="#!">RSS</a></div>*}
{*            </div>*}
{*            <div class="news-ajax-wrap">*}
{*                <div class="news-ajax-content">*}
{*                    {if $arrListArticles.0 eq ''}*}
{*                        <span class="text-center d-block mt-3">Please wait for new "News".</span>*}
{*                        {else}*}
{*                    {foreach from=$arrListArticles key=k item=news}*}
{*                        <a class="news-section__item" href="{if $news.link}{$news.link}{else}{$Rewrite->url_article($news)}{/if}">*}
{*                            <div class="news-section__date">{$news.reg_date|date_format:"%B %d, %Y"}</div>*}
{*                            <div class="news-section__icon">{$news.cat_name}</div>*}
{*                            <h3 class="news-section__text">{$news.title}</h3>*}
{*                        </a>*}
{*                    {/foreach}*}
{*                    {/if}*}
{*                </div>*}
{*            </div>*}
{*        </section>*}
{*    </section>*}
{*</div>*}


<div class="page__content">
    <!-- main content-->
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title">{$curCat.name}</div>
        </div>
        <img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt="{$curCat.name}"/>
    </div>
    <section class="section-2">
        <section class="news-section mb-40">
            
            <div class="news-type">
                <div class="news-type_content">
                {foreach from=$arrListArticles key=k item=news}
                    <a class="news-section__item"
                       href="{if $news.link}{$news.link}{else}{$Rewrite->url_article($news)}{/if}">
                        <div class="news-section__date">{$news.reg_date|date_format:"%B %d, %Y"}</div>
                        <div class="news-section__icon">
                            {if $news.news_type eq 0}
                                News
                            {else}
                                Job
                            {/if}
                        </div>
                        <h3 class="news-section__text">{$news.title}</h3>
                    </a>
                {/foreach}
                </div>
            </div>
        </section>
    </section>
</div>
