{if $curCat.parent_id eq 0}
    <div class="page__content">

        <div class="banner">
            <div class="banner__wrapper">
                <div class="banner__title">{$curCat.name}</div>
            </div>
            <img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.banner}" alt=""/>
        </div>
        {*        <section class="section-2">*}
        {*            <div class="ir-layout mb-40">*}
        {*                <div class="ir-layout__left">*}
        {*                    <a class="ir-layout__frame" href="{$Rewrite->url_for_investor($arrFISpecial)}">*}
        {*                        <img src="{$URL_UPLOADS}/{$arrFISpecial.image}" alt="">*}
        {*                        <h2 class="ir-layout__title">{$arrFISpecial.title}</h2>*}
        {*                    </a>*}
        {*                </div>*}
        {*                <div class="ir-layout__right">*}
        {*                    <div class="ir-layout__title2">Stock Price</div>*}
        {*                    <div class="ir-layout__content"><img src="{$URL_IMAGES}/ir-layout-img.png" alt=""></div>*}
        {*                    <div class="ir-layout__footer"><a class="ir-layout__link" href="#!">Chart</a></div>*}
        {*                </div>*}
        {*            </div>*}
        {*            <div class="row">*}
        {*                <div class="col-lg-8 mb-40">*}
        {*                    <div class="news-section__header">*}
        {*                        <h2 class="news-section__title">IR News</h2>*}
        {*                        <div class="news-section__elements">*}
        {*                            <a class="news-section__element news-section__element--rss" href="#!">RSS</a>*}
        {*                            <a class="news-section__element news-section__element--arrow" href="{$Rewrite->url_category($arrCatNewsFor_investor)}">view all</a>*}
        {*                        </div>*}
        {*                    </div>*}
        {*                    {foreach from=$arrListNewsFor_investor key=k item=news}*}
        {*                        <a class="news-section__item" href="{if $news.link}{$news.link}{else}{$Rewrite->url_article($news)}{/if}">*}
        {*                            <div class="news-section__date">{$news.reg_date|date_format:"%B %d, %Y"}</div>*}
        {*                            <div class="news-section__icon">{$news.cat_name}</div>*}
        {*                            <h3 class="news-section__text">{$news.title}</h3>*}
        {*                        </a>*}
        {*                    {/foreach}*}
        {*                </div>*}
        {*                <div class="col-lg-4 mb-40">*}
        {*                    <div class="download-zip">*}
        {*                        <a class="download-zip__btn" href="{$URL_UPLOADS}/{$_CONFIG.download_zip}" download>*}
        {*                            <img class="download-zip__icon" src="{$URL_IMAGES}/icon-zip-file.png" alt="">*}
        {*                            <div class="download-zip__text">*}
        {*                                <div>Download the latest IR materials in batch</div>*}
        {*                                <div>(ZIP: {$sizeFile} KB)</div>*}
        {*                            </div>*}
        {*                        </a>*}
        {*                        <div class="download-zip__detail">*}
        {*                            <p>The following materials are included</p>*}
        {*                            <ul>*}
        {*                                {foreach from=$arrListLinkForInvestor key=k item=adver}*}
        {*                                    <li><a href="{$adver.link}">{$adver.title}</a></li>*}
        {*                                {/foreach}*}
        {*                            </ul>*}
        {*                        </div>*}
        {*                    </div>*}
        {*                </div>*}
        {*            </div>*}
        {*            <div class="row">*}
        {*                <div class="col-lg-8 col-xl-9 mb-40">*}
        {*                    <div class="row gutter-under-md-16">*}
        {*                        {foreach from=$arrListCatChildrenLeft key=k item=ir}*}
        {*                            <div class="col-sm-6 col-md-4 col-lg-4 mb-30"><a class="news-2" href="{if $ir.link}{$ir.link}{else}{$Rewrite->url_category($ir)}{/if}">*}
        {*                                    <div class="news-2__frame"><img src="{$URL_UPLOADS}/{$ir.image}" alt="" /></div>*}
        {*                                    <div class="news-2__overlay">*}
        {*                                        <div class="news-2__title">{$ir.name}</div>*}
        {*                                    </div>*}
        {*                                </a>*}
        {*                                <div class="mt-30">*}
        {*                                    <ul class="news-list">*}
        {*                                        {foreach from=$ir.arrFor_investor key=j item=sub_ir}*}
        {*                                            <li class="news-list__item"><a class="news-list__link" href="{if $sub_ir.link}{$sub_ir.link}{else}{$Rewrite->url_for_investor($sub_ir)}{/if}">{$sub_ir.title}</a></li>*}
        {*                                        {/foreach}*}
        {*                                    </ul>*}
        {*                                </div>*}
        {*                            </div>*}
        {*                        {/foreach}*}
        {*                    </div>*}
        {*                </div>*}
        {*                <div class="col-lg-4 col-xl-3 mb-40">*}
        {*                    <ul class="as-menu">*}
        {*                        {foreach from=$arrListCatChildrenRight key=k item=ir}*}
        {*                            <li class="as-menu__item"><a class="as-menu__link" href="{if $ir.link}{$ir.link}{else}{$Rewrite->url_category($ir)}{/if}">*}
        {*                                    <div class="as-menu__frame">*}
        {*                                        <img src="{$URL_UPLOADS}/{$ir.icon}" alt="">*}
        {*                                    </div>*}
        {*                                    <div class="as-menu__text">{$ir.name}</div>*}
        {*                                </a>*}
        {*                            </li>*}
        {*                        {/foreach}*}
        {*                    </ul>*}
        {*                </div>*}
        {*            </div>*}
        {*        </section>*}
        <div class="container">
            <div class="for_investor_des mt-40">
                {$curCat.des|htmlDecode}
            </div>
        </div>
        <section class="partner-section pt-0">
            <div class="partner-section__body">
                <ul class="partners">
                    {foreach from=$arrListPartner key=k item=adver}
                        <li class="partners__item"><a class="partners__link" href="{$adver.link}"><img
                                        src="{$URL_UPLOADS}/{$adver.image}" alt=""></a></li>
                    {/foreach}
                </ul>
            </div>
        </section>
    </div>
{else}
    <div class="page__content">
        <!-- main content-->
        {* <nav class="navigation">
            <div class="navigation__wrapper">
                <div class="navigation__breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a class="link-unstyled" href="{$VNCMS_URL}">Home</a></li>
                        <li class="breadcrumb-item"><a class="link-unstyled" href="{$Rewrite->url_category($parCat)}"">{$parCat.name}</a></li>
                        <li class="breadcrumb-item active">{$curCat.name}</li>
                    </ol>
                </div>
                <div class="navigation__lang">
                    <div class="langs-2">
                        <a class="langs-2__item" href="?lang=vn">
                            <img src="{$URL_IMAGES}/vn.svg" alt="">
                        </a>
                        <a class="langs-2__item" href="?lang=en">
                            <img src="{$URL_IMAGES}/gb.svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </nav> *}
        <section class="section-2 py-20">
            <article class="post mb-40">
                <h1 class="post-title">{$curCat.name}</h1>
                <div class="post-content">
                    {$curCat.des|htmlDecode}
                </div>
            </article>
        </section>
    </div>
{/if}
