function addToCart(product_id) {
    let quantity = $("#product-quantity-value").val();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: VNCMS_URL + "/cart/ajax/addToCart",
        data: {product_id: product_id, quantity: quantity},
        cache: false,
        success: function (data) {
            update_total_item();
            show_cart_header();
            get_total_quantity();
            // window.location.href = VNCMS_URL + "/cart";
            Swal.fire({
                type: data.status,
                title: 'Thông báo',
                text: data.message,
                preConfirm: () => {
                    sessionStorage.setItem("reloading", "true");
                    window.location.reload();

                }
            });
        }
    });
}

window.onload = function () {
    var reloading = sessionStorage.getItem("reloading");
    if (reloading) {
        sessionStorage.removeItem("reloading");
        let $body = $('html, body');
        let $cart = $('.js-cart');
        $cart.addClass('is-show');
        $body.addClass('overflow-hidden');
    }
};

function showCart() {
    let $cart = $('.js-cart');
    $cart.addClass('is-show');
}

function byNow(product_id) {
    let quantity = $("#product-quantity-value").val();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: VNCMS_URL + "/cart/ajax/byNow",
        data: {product_id: product_id, quantity: quantity},
        cache: false,
        success: function (data) {
            update_total_item();
            show_cart_header();
            get_total_quantity();
            window.location.href = VNCMS_URL + "/cart";
        }
    });
}

function order(product_id) {
    $.ajax({
        type: "POST",
        url: VNCMS_URL + "/cart/ajax/addToCart",
        data: {product_id: product_id},
        cache: false,
        success: function (data) {
            update_total_item();
            window.location.href = VNCMS_URL + '/cart/checkout';
        }
    });
}

function createOrder() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: VNCMS_URL + "/cart/ajax/createOrder",
        data: $('#order').serialize(),
        cache: false,
        success: function (data) {
            update_total_item();
            Swal.queue([{
                title: 'Thông báo',
                type: data.status,
                text: data.message,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    if (data.location) {
                        return window.location.href = data.location;
                    }
                }
            }])
        }
    });
}

function removeCartItem(product_id, ItemRow) {

    if (product_id !== "" && ItemRow !== "") {
        $.ajax({
            type: "POST",
            url: VNCMS_URL + "/cart/remove-item",
            dataType: "json",
            data: {product_id: product_id}
        }).done(function (data) {
            console.log(12);
            $("." + ItemRow).remove();
            update_total_item();
            update_order_price();
            update_cart_price();
            if (data == 0){
                window.location.reload()
            }
        })
    } else {
        swal("Thông báo", "Sản phẩm này không tồn tại trong giỏ hàng!", "error");
    }
}

function deleteAll() {
    Swal.queue([{
        title: 'Thông báo',
        type: 'question',
        text: 'Bạn có muốn xóa tất cả sản phẩm?',
        showLoaderOnConfirm: true,
        preConfirm: () => {
            removeCart();
        }
    }])
}
function removeCart() {
    $.ajax({
        type: "POST",
        url: VNCMS_URL + "/remove-cart",
        dataType: "text",
    }).done(function () {
        let location = VNCMS_URL;
        window.location.href = location;
    })
}


function showCartContent(obj) {
    var toid = $(obj).data("toid");
    $.ajax({
        type: "GET",
        url: VNCMS_URL + "?mod=cart&act=showCart",
        dataType: "html",
        async: "true",
        cache: "false",
        success: function (data) {
            $(toid).html(data);
        }
    });
}

function show_cart_header() {
    $('.media-wrap-js').remove();
    $.ajax({
        type: "GET",
        url: VNCMS_URL + "/cart/ajax/showCartHeader",
        dataType: "text",
        async: "true",
        cache: "false",
        success: function (data) {
            $('#media_wrap').html(data);
        }
    });
}

function get_total_quantity() {
    $.ajax({
        type: "GET",
        url: VNCMS_URL + "/cart/ajax/getTotalQuantity",
        dataType: "text",
        async: "true",
        cache: "false",
        success: function (data) {
            $('#header_total_quantity').text(data);
        }
    });
}

function update_total_item() {
    $.ajax({
        type: "GET",
        url: VNCMS_URL + "/cart/ajax/getTotalItem",
        dataType: "text",
        async: "true",
        cache: "false",
        success: function (data) {
            $('#total_cart_item').text(data);
        }
    });
}

// Cập nhật khi thay đổi số lượng trong giỏ hàng
$('.quantity').change(function () {
    update_cart_price(this);
});

function update_cart_price(obj) {
    var itemId = $(obj).attr("data-itemId");
    var total_r = 0;
    var quantity = parseInt($('#quantity_r' + itemId).val());
    var price = parseInt($('#quantity_r' + itemId).attr("data-price"));
    total_r += quantity * price;
    $('#total_r' + itemId).attr("data-val", total_r);
    $('#total_r' + itemId).text(Number(total_r.toFixed(1)).toLocaleString());

    var total = 0;
    $('.total_r_item').each(function (index, item) {
        total_r = parseInt($(item).attr("data-val"));
        total += total_r;
    });

    $('#total').text(Number(total.toFixed(1)).toLocaleString());
    $('#total_payment').text(Number(total.toFixed(1)).toLocaleString());
    $('#total_order').text(Number(total.toFixed(1)).toLocaleString());

    $.ajax({
        type: "POST",
        url: VNCMS_URL + "/cart/update-cart",
        dataType: "text",
        data: $("#cart").serialize(),
        success: function (data) {
            $('#header-quantity').text(data);
        }
    })
}

// Cập nhật khi thay đổi số lượng trong khi thanh toán
$('.order_quantity').change(function () {
    update_order_price(this);
});

function update_order_price(obj) {
    var itemId = $(obj).attr("data-orderItemId");
    var total = 0;
    var total_r = 0;
    var quantity = parseInt($('#quantity_orderR' + itemId).val());
    var price = $(obj).attr("data-orderPrice");

    total_r += quantity * price;
    $('#total_orderR' + itemId).attr("data-orderVal", total_r);
    $('#total_orderR' + itemId).text(Number(total_r.toFixed(1)).toLocaleString());

    $('.total_r_orderItem').each(function (index, item) {
        total_r = parseInt($(item).attr("data-orderVal"));
        total += total_r;
    });

    $('#totalOrder').text(Number(total.toFixed(1)).toLocaleString());
    $('#total_orderPayment').text(Number(total.toFixed(1)).toLocaleString());

    $.ajax({
        type: "POST",
        url: VNCMS_URL + "/cart/update-order",
        dataType: "json",
        data: $('#order').serialize(),
        success: function (data) {
        }
    })
}
