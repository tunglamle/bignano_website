// $(document).ready(function () {
//     // setMenuActive();
//     $('.embed-responsive').bind('contextmenu', function (e) {
//         return false;
//     });
//     console.log(MediaElementPlayer);
//     if (! MediaElementPlayer) {
//         return;
//     }
//
//
//     var mediaElements = document.querySelectorAll('video, audio');
//     for (var i = 0, total = mediaElements.length; i < total; i++) {
//         new MediaElementPlayer(mediaElements[i], {
//             autoRewind: false,
//             features: ['playpause', 'current', 'progress', 'duration', 'volume', 'skipback', 'jumpforward', 'speed', 'fullscreen']
//         });
//     }
// });

function ajax_email() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: VNCMS_URL + "/ajax/email",
        data: $('#save_email_form').serialize(),
        cache: false,
        success: function (data) {
            Swal.queue([{
                title: 'Thông báo',
                type: "success",
                text: "Đăng ký email thành công"
            }])
            $('#save_email_form').trigger("reset");
            location.reload();
        }
    });
}

function ajax_login() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: VNCMS_URL + "/account/ajax/login",
        data: $('#login_form').serialize(),
        cache: false,
        success: function (data) {
            if (data) {
                Swal.queue([{
                    title: 'Thông báo',
                    type: data.status,
                    text: data.message
                }])
            } else {
                location.reload();
            }
        }
    });
}

function ajax_register() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: VNCMS_URL + "/account/ajax/register",
        data: $('#register_form').serialize(),
        cache: false,
        success: function (data) {
            Swal.queue([{
                title: 'Thông báo',
                type: data.status,
                text: data.message,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    if (data.location) {
                        return window.location.href = data.location;
                    }
                }
            }])
        }
    });
}

function ajax_forgot() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: VNCMS_URL + "/account/ajax/forgot",
        data: $('#forgot_form').serialize(),
        cache: false,
        success: function (data) {
            Swal.queue([{
                title: 'Thông báo',
                type: data.status,
                text: data.message,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    if (data.status === "success") {
                        $('#forgot_form').trigger('reset');
                        $('.md-recovery').modal('hide');
                    }
                }
            }])
        }
    });
}

function getRes(resid) {
    var player = document.getElementById('player');
    if ($.cookie("resid") !== resid) {
        $.cookie("resid", resid, {path: "/", expires: 1});
        player.pause();
        $.ajax({
            type: "POST",
            dataType: 'text',
            url: VNCMS_URL + "/ajax/res",
            data: {resid: resid},
            cache: false,
            success: function (data) {
                player.setSrc(data);
                player.load();
                player.play();
            }
        });
    } else {
        player.play();
    }
}

//Xem truoc anh avatar truoc khi upload
function setPreviewAvatar(input, imgobj) {
    if (!/(\.png|\.gif|\.jpg|\.jpeg)$/i.test(input.files[0].name)) {
        alert('Chỉ chấp nhận các tệp đuôi JPG, JPEG, GIF, PNG');
        input.value = "";
        return false;
    }
    if (input.files[0].size > 1 * 1024 * 1024) {
        alert("Tệp ảnh phải có dung lượng nhỏ hơn 1 MB");
        input.value = "";
        return false;
    }
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#' + imgobj).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
    return false;
}

function setMenuClicked(t) {
    $.cookie("menu_a_clicked", t, {path: "/", expires: 1})
}

// function getMenuClicked() {
//     return $.cookie("menu_a_clicked")
// }
//
// function deletetMenuClicked() {
//     $.cookie("menu_a_clicked", null, {path: "/", expires: 1})
// }

// function setMenuActive() {
//     var i;
//     var base_url = VNCMS_URL + '/';
//     i = getMenuClicked();
//     if (i == 0 || i == null || window.location == base_url) i = 0;
//     $("#menu_a_" + i).addClass("active");
// }

//Ajax Get options District from Province obj
function getDistrict(obj, to_id, t) {
    if (typeof t == 'undefined') {
        var t = 0;
    }
    var province_id, req1;
    province_id = $(obj).val();
    req1 = VNCMS_URL + "/ajax/getDistrict?type=" + t + "&province_id=" + province_id;
    $("#" + to_id).load(req1);
    $("#" + to_id).val(0);
    $("#" + to_id).change();
    return false;
}
