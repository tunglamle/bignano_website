$(function () {
    $('.menu-link').each(function (i) {
        var url = $(this).attr("href");
        var curUrl = window.location.href;
        var number = $(this).data('li');
        if (url == curUrl || url + '/' == curUrl) {
            $(".menu-" + number).addClass("active");
        }
    });
});

function submitContact() {
    $(".loader-overlay").addClass("show");
    $.ajax({
        type: "POST",
        url: VNCMS_URL + "/ajax/submit-Contact",
        data: $('#form-contact').serialize(),
        dataType: 'json',
        cache: false,
        success: function (data) {
            $(".loader-overlay").removeClass("show");
            Swal.fire({
                type: data.status,
                title: 'Thông báo',
                text: data.message,
                preConfirm: () => {
                    if (data.status == 'success') {
                        window.location.href = VNCMS_URL;
                    }
                }
            });
        }
    });
}

function showNews() {
    $('.news-ajax-content').remove();
    $.ajax({
        type: "POST",
        url: VNCMS_URL + "/ajax/show-news",
        data: $('#form_news').serialize(),
        dataType: 'html',
        cache: false,
        success: function (data) {
            $('.news-ajax-wrap').html(data);
        }
    });
}

function showNewsType(value, html) {
    var html = html;
    $.ajax({
        type: "POST",
        url: VNCMS_URL + "/ajax/show-news-type",
        data: {type:value},
        dataType: 'html',
        cache: false,
        success: function (data) {
            $('#news-title').html($(html).html());
            $('.menu-news').removeClass('active');
            $('.news-type_content').remove();
            $(html).addClass('active');
            $('.news-type').html(data);
        }
    });
}