<div class="page-content">
    <!-- main content-->
    <nav class="border-top">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a class="link-unstyled" href="{$VNCMS_URL}">Trang chủ</a></li>
                {if $parCat}
                    <li class="breadcrumb-item"><a class="link-unstyled" href="{$Rewrite->url_category($parCat)}">{$parCat.name}</a></li>
                {/if}
                <li class="breadcrumb-item"><a class="link-unstyled" href="{$Rewrite->url_category($curCat)}">{$curCat.name}</a></li>
                <li class="breadcrumb-item active">{$arrOneLibrary.title}</li>
            </ol>
        </div>
    </nav>
    <div class="container">
        <article class="post mb-40">
            <h1 class="post-title">{$arrOneLibrary.title}</h1>
            <div class="mb-20">
                <div class="fb-like" data-href="{$Rewrite->url_library($arrOneLibrary)}" data-width="" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
            </div>
            <h2 class="post-sapo">{$arrOneLibrary.sapo|htmlDecode}</h2>
            <div class="post-content">
                {$arrOneLibrary.content|htmlDecode}
            </div>
        </article>
        <div class="mb-40">
            <article class="preview">
                <div class="preview__main">
                    <div class="preview-slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                {foreach from=$arrOneLibrary.list_image key=k item=image}
                                <div class="swiper-slide">
                                    <div class="preview-slider__frame">
                                        <img src="{$URL_UPLOADS}/{$image}" alt="" />
                                    </div>
                                </div>
                                {/foreach}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="preview__thumbs">
                    <div class="thumb-slider">
                        <div class="thumb-slider__prev"><i class="fa fa-angle-left"></i></div>
                        <div class="thumb-slider__next"><i class="fa fa-angle-right"></i></div>
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                {foreach from=$arrOneLibrary.list_image key=k item=image}
                                <div class="swiper-slide">
                                    <div class="thumb-slider__frame">
                                        <img src="{$URL_UPLOADS}/{$image}" alt="" /></div>
                                </div>
                                {/foreach}
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="mb-40">
            <div class="facebook-comments fb-cmt">
                <div class="fb-comments" data-href="{$Rewrite->url_library($arrOneLibrary)}" data-width="100%" data-numposts="5"></div>
            </div>
        </div>
    </div>
    <section class="section overflow-hidden pb-80">
        <div class="container">
            <h2 class="section__title">Dự án liên quan</h2>
            <div class="project-slider">
                <div class="project-slider__prev"><i class="fa fa-angle-left"></i></div>
                <div class="project-slider__next"><i class="fa fa-angle-right"></i></div>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        {foreach from=$arrListOtherLibrary key=k item=library}
                        <div class="swiper-slide">
                            <div class="project">
                                <a class="project__frame" href="{$Rewrite->url_library($library)}">
                                    <img src="{$URL_UPLOADS}/{$library.image}" alt="{$library.title}" /></a>
                                <h3 class="project__title">
                                    <a href="{$Rewrite->url_library($library)}">{$library.title}</a></h3>
                            </div>
                        </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>