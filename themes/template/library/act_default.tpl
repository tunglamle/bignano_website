<div class="page-content">
    <!-- main content-->
    <section class="banner">
        <img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.image}" alt="{$curCat.name}" />
        <div class="container">
            <div class="banner__title">{$curCat.name}</div>
            <div class="banner__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="{$VNCMS_URL}">Trang chủ</a></li>
                    {if $parCat}
                        <li class="breadcrumb-item"><a class="link-unstyled" href="{$Rewrite->url_category($parCat)}">{$parCat.name}</a></li>
                    {/if}
                    <li class="breadcrumb-item active">{$curCat.name}</li>
                </ol>
            </div>
        </div>
    </section>
    <div class="container pt-40 pb-60">
        <div class="mb-4">
            {$curCat.des}
        </div>
        <div class="row gutter-under-sm-16">
            {foreach from=$arrListLibrary key=k item=library}
            <div class="col-lg-3 col-md-4 col-6 mb-3 mb-sm-30">
                <div class="project">
                    <a class="project__frame" href="{$Rewrite->url_library($library)}">
                        <img src="{$URL_UPLOADS}/{$library.image}" alt="{$library.title}" /></a>
                    <h3 class="project__title">
                        <a href="{$Rewrite->url_library($library)}">{$library.title}</a></h3>
                </div>
            </div>
            {/foreach}
        </div>
        <nav class="d-flex justify-content-center">
            <ul class="pagination">
                {$clsPaging->showPagingNew2()}
            </ul>
        </nav>
    </div>
</div>