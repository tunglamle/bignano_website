<section class="bg_slider">
    <div class="banner_slider">
        <div class="bd-example">
            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    {foreach from=$arrListSlider key=k item=slider}
                        <div class="carousel-item {if $k eq 0}active{/if}">
                            <img src="{$URL_UPLOADS}/{$slider.image}" class="bg_slider__img d-block w-100" alt="...">
                        </div>
                    {/foreach}
                </div>
                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>