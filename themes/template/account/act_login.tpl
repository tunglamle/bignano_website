<div class="container pt-40">
    <div class="row mb-30">
        <div class="col-xl-6 offset-xl-3">
            <div class="sign-in card border-primary">
                <div class="card-header bg-primary text-white">
                    <h2 class="card-title">Đăng nhập</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="modal-body" method="POST" id="fLogin" name="fLogin">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-white">
                                                <i class="fa fa-user fa-fw"></i>
                                            </span>
                                        </div>
                                        <input class="form-control" type="text" name="user_name" placeholder="Tên đăng nhập" required/>
                                    </div>
                                    {if $arr_error.user_name ne ''}<div class="text-danger">{$arr_error.user_name}</div>{/if}
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-white">
                                                <i class="fa fa-key fa-fw"></i>
                                            </span>
                                        </div>
                                        <input class="form-control" type="password" name="user_pass" placeholder="Mật khẩu" required/>
                                    </div>
                                    {if $arr_error.user_pass ne ''} <div class="text-danger">{$arr_error.user_pass}</div>{/if}
                                    <div class="form-text">
                                        <a class="text-muted js-switch-modal" href=".md-recovery">Quên mật khẩu?</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-block btn-primary" type="submit" value="Login" name="btnLogin">Đăng nhập</button>
                                </div>
                                <div class="form-group form-row">
                                    <div class="col-6">
                                        <a class="btn btn-block text-white btn-facebook" href="{$Rewrite->url_fbauth()}">
                                            <i class="fa fa-facebook mr-2"></i>
                                            <span>Facebook</span>
                                        </a>
                                    </div>
                                    <div class="col-6">
                                        <a class="btn btn-block text-white btn-google-plus" href="{$Rewrite->url_ggauth()}">
                                            <i class="fa fa-google mr-2"></i>
                                            <span>Google</span>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <a class="js-switch-modal" href=".md-register">ĐĂNG KÝ NGAY</a> nếu bạn chưa có tài khoản.
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>