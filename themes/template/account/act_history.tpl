<div class="container pt-40">
    <div class="row">
        <div class="col-xl-9 col-lg-8 mb-20 order-lg-1">
            <ul class="nav profile-course-tab">
                <li class="nav-item">
                    <a class="nav-link active" href="#profile-course-monthly" data-toggle="tab">Khoá học theo tháng</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#profile-course-forever" data-toggle="tab">Khoá học mãi mãi</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="profile-course-monthly">
                    {if $arrListCoursesMonthly}
                        <div class="row gutter-under-sm-16">
                            {foreach from=$arrListCoursesMonthly key=t item=course}
                                <div class="col-6 mb-3 mb-sm-30">
                                    <div class="course">
                                        <a class="course__iwrap" href="{$Rewrite->url_course($course)}">
                                            <img src="{$NVCMS_URL}/img.php?pic={$core->callfunc('base64_encode', $course.image)}&w=458&h=298&encode=1" onerror="this.src='{$URL_IMAGES}/nopic.png'" alt="{$course.name}"/>
                                        </a>
                                        <h3 class="course__title">{$course.name}n</h3>
                                        <a class="course__link" href="{$Rewrite->url_course($course)}"></a>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    {/if}
                </div>
                <div class="tab-pane" id="profile-course-forever">
                    {if $arrListCoursesForever}
                        <div class="row gutter-under-sm-16">
                            {foreach from=$arrListCoursesForever key=t item=course}
                                <div class="col-6 mb-3 mb-sm-30">
                                    <div class="course">
                                        <a class="course__iwrap" href="{$Rewrite->url_course($course)}">
                                            <img src="{$NVCMS_URL}/img.php?pic={$core->callfunc('base64_encode', $course.image)}&w=458&h=298&encode=1" onerror="this.src='{$URL_IMAGES}/nopic.png'" alt="{$course.name}"/>
                                        </a>
                                        <h3 class="course__title">{$course.name}n</h3>
                                        <a class="course__link" href="{$Rewrite->url_course($course)}"></a>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    {/if}
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 mb-50">
            <div class="sidebar card">
                <h2 class="sidebar__header card-header">THÔNG TIN CÁ NHÂN</h2>
                <ul class="sidebar__nav nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="javascript:;">
                            <i class="fa fa-file-text-o mr-3"></i>
                            <span>Khoá học của tôi</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{$Rewrite->url_account()}">
                            <i class="fa fa-user mr-3"></i>
                            <span>Thông tin cá nhân</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{$Rewrite->url_logout()}">
                            <i class="fa fa-power-off mr-3"></i>
                            <span>Đăng xuất</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>