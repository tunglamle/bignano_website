<div class="container pt-40">
    {if $smarty.get.success==1}
        <div class="alert alert-success" role="alert">
            <strong>Đặt lại mật khẩu thành công!</strong>
            <p>Vui lòng <a style="color: blue" href=".md-login" data-toggle="modal">Đăng nhập</a> để quản lý tài khoản.</p>
        </div>
    {elseif $existskey==0}
        <div class="alert alert-danger" role="alert">
            <strong>Đường dẫn không chính xác hoặc đã hết hạn!</strong>
        </div>
    {else}
        <div class="row mb-30">
            <div class="col-xl-6 offset-xl-3">
                <div class="sign-in card border-primary">
                    <div class="card-header bg-primary text-white">
                        <h2 class="card-title">Đặt lại mật khẩu</h2>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="modal-body" action="" method="POST" id="fForgot" name="fForgot"
                                      class="fLoginRegister">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text bg-white">
                                                    <i class="fa fa-key fa-fw"></i>
                                                </span>
                                            </div>
                                            <input class="form-control" type="password" placeholder="Mật khẩu mới" id="user_pass" name="user_pass" value="{$smarty.post.user_pass}" required>
                                        </div>
                                        {if $msg_error.user_pass.0 ne ''}<div class="text-danger">{$msg_error.user_pass.0}</div>{/if}
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text bg-white">
                                                    <i class="fa fa-key fa-fw"></i>
                                                </span>
                                            </div>
                                            <input class="form-control" type="password" placeholder="{'Confirm_password'|lang}" id="user_pass_confirm" name="user_pass_confirm" required>
                                        </div>
                                        {if $msg_error.user_pass.1 ne ''}<div class="text-danger">{$msg_error.user_pass.1}</div>{/if}
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-block btn-primary" type="submit" value="Login" name="btnReset" value="Create">Xác nhận</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {/if}
</div>