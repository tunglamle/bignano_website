<div class="container pt-40">
    <div class="row">
        <div class="col-xl-9 col-lg-8 mb-50 order-lg-1">
            <section class="profile-section">
                <h2 class="profile-section__title">Thông tin tài khoản</h2>
                <ul class="profile-info">
                    <li>
                        <span>Email:</span>
                        <span>{$core->_USER.email}</span>
                        <a class="profile-info__edit" href="{$Rewrite->url_changeinfo()}">Đổi Email</a>
                    </li>
                    <li>
                        <span>Số điện thoại:</span>
                        <a class="profile-info__number" href="tel: {$core->_USER.phone}">{$core->_USER.phone|phone_format}</a>
                        <a class="profile-info__edit" href="{$Rewrite->url_changeinfo()}">Đổi số điện thoại</a>
                    </li>
                    <li>
                        <span>Mật khẩu:</span>
                        <span>********</span>
                        <a class="profile-info__edit" href="{$Rewrite->url_changeinfo()}">Đổi mật khẩu</a>
                    </li>
                </ul>
            </section>
            <section class="profile-section">
                <h2 class="profile-section__title">Thông tin cá nhân</h2>
                <form class="profile" method="post">
                    <div class="media">
                        <div class="profile__thumbs">
                            <div class="profile__thumbs-iwrap">
                                <div class="profile__thumbs-placeholder">
                                    <span>Chưa cập nhật</span>
                                    <span>Hình Ảnh</span>
                                </div>
                                <img id="avatar" class="js-thumb-img" src="{$URL_UPLOADS}/{$core->_USER.avatar}" onerror="this.src='{$URL_IMAGES}/no_profile.png'" alt="avatar">
                            </div>
                            <label class="profile__thumbs-btn">
                                <input class="js-thumb-input" name="avatar" onChange="setPreviewAvatar(this, 'avatar');" accept=".jpg,.png,.bmp,.gif" type="file"/>
                                <i class="fa fa-picture-o"></i>
                                <span>Chọn avatar</span>
                            </label>
                        </div>
                        <div class="media-body">
                            <div class="form-group row">
                                <label class="col-form-label col-xl-3">Họ và tên</label>
                                <div class="col-xl-9">
                                    <input class="form-control" type="text" name="fullname" value="{$core->_USER.fullname}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-xl-3">Ngày sinh</label>
                                <div class="col-xl-9">
                                    <div class="form-row">
                                        {assign var=birthday value=$core->_USER.birthday|date_format:"%d/%m/%Y"}
                                        {assign var=arrBirthday value="/"|explode:$birthday}
                                        <div class="col-2">
                                            <select name="ngay" class="form-control">
                                                {for $ngay=1 to 31}
                                                    {if $ngay < 10}
                                                        {assign var=ngay value="0`$ngay`"}
                                                    {/if}
                                                    <option {if $ngay eq $arrBirthday[0]}selected{/if}>{$ngay}</option>
                                                {/for}
                                            </select>
                                        </div>
                                        <div class="col-2">
                                            <select name="thang" class="form-control">
                                                {for $thang=1 to 12}
                                                    {if $thang < 10}
                                                        {assign var=thang value="0`$thang`"}
                                                    {/if}
                                                    <option {if $thang eq $arrBirthday[1]}selected{/if}>{$thang}</option>
                                                {/for}
                                            </select>
                                        </div>
                                        <div class="col-2">
                                            <select name="nam" class="form-control">
                                                {for $nam=1900 to 'Y'|date}
                                                    <option {if $nam eq $arrBirthday[2]}selected{/if}>{$nam}</option>
                                                {/for}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-xl-3">Tỉnh/Thành phố</label>
                                <div class="col-xl-9">
                                    <select class="form-control" name="province_id">
                                        {$htmlOptionKhuvuc}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-xl-3">Chỗ ở hiện tại</label>
                                <div class="col-xl-9">
                                    <input class="form-control" type="text" name="address" value="{$core->_USER.address}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-xl-3">Giới tính</label>
                                <div class="col-xl-9">
                                    <select class="form-control" name="gender">
                                        <option value="0" {if $core->_USER.gender eq 0}selected{/if}>{'Male'|lang}</option>
                                        <option value="1" {if $core->_USER.gender eq 1}selected{/if}>{'Female'|lang}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-primary text-700 px-35 py-2" type="submit" name="btnSubmit" value="UpdateAccount">CẬP NHẬT</button>
                    </div>
                </form>
            </section>
        </div>
        <div class="col-xl-3 col-lg-4 mb-50">
            <div class="sidebar card">
                <h2 class="sidebar__header card-header">THÔNG TIN CÁ NHÂN</h2>
                <ul class="sidebar__nav nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{$Rewrite->url_history()}">
                            <i class="fa fa-file-text-o mr-3"></i>
                            <span>Khoá học của tôi</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="javascript:;">
                            <i class="fa fa-user mr-3"></i>
                            <span>Thông tin cá nhân</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{$Rewrite->url_logout()}">
                            <i class="fa fa-power-off mr-3"></i>
                            <span>Đăng xuất</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>