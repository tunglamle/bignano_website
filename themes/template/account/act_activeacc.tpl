<div class="bg-gray my-2">
    <div class="container">
        <div class="form-wrap">
            <section class="register">
                <div class="row">
                	<div class="col-md-12 mb-4">
                	<h2 class="text-danger">{'Activation_account'|lang}</h2>
					{if $success==1}
					<p class="text-success"><strong>{'Activation_account_successfully'|lang}!</strong></p>					
					<p class="mt-3">{'Thank_you_for_register_to'|lang} <a href="{$core->callfunc('url_login')}">{'Sign_in'|lang}</a> {'To_manage_your_account'|lang}.</p>
					{elseif $success==-1}
					<p class="text-danger"><strong>{'Activation_account_unsuccessfully'|lang}!</strong></p>							
					<p class="mt-3">{'Activation_fail_notice'|lang}.</p>
					{else}
					<p class="text-danger">{'Error_occur_when_activation'|lang}.</p>
					{/if}                	                	
                	</div>			
                </div>
            </section>
        </div>
    </div>
</div>