<div class="banner">
    <img class="banner__bg" src="{$URL_UPLOADS}/{$curCat.image}" onerror="this.src='{$URL_IMAGES}/banner-contact.jpg'"
         alt="{$curCat.name}"/>
    <div class="container">
        <div class="banner__title">{$curCat.name}</div>
    </div>
</div>
<div class="container pt-40 mb-50">
    <div class="row gutter-under-sm-16">
        {foreach from=$arrListCourses key=c item=course}
            <div class="col-lg-4 col-6 mb-3 mb-sm-30">
                <div class="course">
                    <a class="course__iwrap" href="{$Rewrite->url_course($course)}">
                        <img src="{$NVCMS_URL}/img.php?pic={$core->callfunc('base64_encode', $course.image)}&w=350&h=237&encode=1" onerror="this.src='{$URL_IMAGES}/nopic.png'" alt="{$course.name}"/>
                    </a>
                    <h3 class="course__title">{$course.name}</h3>
                    <a class="course__link" href="{$Rewrite->url_course($course)}"></a>
                </div>
            </div>
        {/foreach}
    </div>
    <nav class="d-flex justify-content-center">
        <ul class="pagination">
            {$clsPaging->showPagingNew2()}
        </ul>
    </nav>
</div>
<div class="border-top border-bottom border-primary"></div>