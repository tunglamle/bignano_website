<div class="container pt-40 pb-50">
    <section class="activate">
        <h1 class="activate__title">Kích hoạt khoá học</h1>
        <h2 class="activate__subtitle">Lưu ý: Mỗi khoá học chỉ cần kích hoạt một lần duy nhất</h2>
        <div class="activate__info">
            <p style="text-align: center;">Bạn <strong>chưa có</strong> tài khoản?
                <a href=".md-register" data-toggle="modal">Đăng ký ngay</a>
            </p>
            <p style="text-align: center;">Bạn <strong>đã có</strong> tài khoản?
                Vui lòng <a href=".md-login" data-toggle="modal">Đăng nhập</a>
            </p>
        </div>
        <form class="activate__form" method="post">
            <div class="input-group">
                <input class="form-control" type="text" name="active_key" autocomplete="Off" placeholder="Nhập mã kích hoạt" required>
                <div class="input-group-append">
                    <button class="input-group-text" type="submit">
                        <i class="fa fa-unlock"></i>
                    </button>
                </div>
            </div>
        </form>
        <div class="activate__support">
            {assign var=hotline value=";"|explode:$_CONFIG.site_hotline}
            <p>Hỗ trợ:
                {foreach from=$hotline key=h item=phone}
                    {if $h > 0}&nbsp;-&nbsp;{/if}
                    <a href="tel:{$phone}">{$phone|phone_format}</a>
                {/foreach}
            </p>
        </div>
    </section>
</div>