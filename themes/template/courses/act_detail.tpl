<section class="course-header">
    <div class="container">
        <div class="course-header__title">{$arrOneCourse.name}</div>
        <div class="course-header__desc">{$arrOneCourse.introduce}</div>
        <div class="course-header__info">
            <div class="course-header__author">
                <div class="course-header__avatar">
                    <!-- img(src="avatar here", alt="")-->
                </div>
                <span>{$arrAuthor.fullname}</span>
            </div>
            <div class="course-header__rating">
                <!-- .rating-stars--4, .rating-stars--4-5 => 4 stars, 4,5 stars-->
                <span class="course-header__rating-stars rating-stars rating-stars--4"></span>
                <span>160 lượt đánh giá</span>
            </div>
            <div>
                <i class="fa fa-users fa-lg mr-2">
                </i>
                <span>{$total_student} Học viên</span>
            </div>
        </div>
    </div>
</section>
<section class="pt-3">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 order-lg-1 mb-40">
                <section class="course-detail">
                    <div class="course-detail__price">
                        {if $arrOneCourse.start_date <= $smarty.now && $arrOneCourse.end_date >= $smarty.now && $arrOneCourse.discount_value > 0}
                            {if $arrOneCourse.discount_type ne 1}
                                {math equation="a-((a*b)/100)" a=$arrOneCourse.price b=$arrOneCourse.discount_value assign="discount_value"}
                                {assign var=price value=$discount_value}
                            {else}
                                {assign var=price value=$arrOneCourse.price - $arrOneCourse.discount_value}
                            {/if}
                            <div>{$core->callfunc("number_format", $price, 0, ',', '.')} VNĐ</div>
                            <del>{$core->callfunc("number_format", $arrOneCourse.price, 0, ',', '.')} VNĐ</del>
                        {else}
                            <span>{$core->callfunc("number_format", $arrOneCourse.price, 0, ',', '.')} VNĐ</span>
                        {/if}
                    </div>
                    <a class="btn btn-primary btn-lg btn-block text-700 mb-12"
                       href="javascript:order({$arrOneCourse.course_id});">ĐĂNG KÝ KHOÁ HỌC</a>
                    <a class="btn btn-default btn-lg btn-block border-primary mb-3"
                       href="javascript:addToCart({$arrOneCourse.course_id});">
                        <i class="fa fa-shopping-cart mr-2">
                        </i>
                        <span>Thêm vào giỏ hàng</span>
                    </a>
                    <ul class="list-unstyled mb-0 px-xl-20">
                        <li class="mb-14">Hoàn tiền trong 7 ngày nếu không hài lòng</li>
                        <li class="mb-14">
                            <i class="fa fa-fw fa-lg fa-clock-o mr-2">
                            </i>
                            <span>Thời lượng</span> <span class="text-700">01 giờ 50 phút</span>
                        </li>
                        <li class="mb-14">
                            <i class="fa fa-fw fa-lg fa-play-circle mr-2">
                            </i>
                            <span>Giáo trình</span> <span class="text-700">20 bài giảng</span>
                        </li>
                        <li class="mb-14">
                            <i class="fa fa-fw fa-lg fa-clock-o mr-2">
                            </i>
                            <span>Sở hữu khoá học {if $arrOneCourse.duration eq 0}trọn đời{else}{$arrOneCourse.duration} tháng{/if}
                            </span>
                        </li>
                        <li class="mb-14">
                            <i class="fa fa-fw fa-lg fa-globe mr-2">
                            </i>
                            <span>Học mọi lúc mọi nơi</span>
                        </li>
                        <li class="mb-14">
                            <i class="fa fa-fw fa-lg fa-mobile mr-2">
                            </i>
                            <span>Học trên mọi thiết bị: Mobile, TV, PC</span>
                        </li>
                    </ul>
                </section>
            </div>
            <div class="col-lg-8 mb-40">
                <img class="course-banner" src="{$URL_UPLOADS}/{$arrOneCourse.image}"
                     onerror="this.src='{$URL_IMAGES}/nopic.png'" alt="{$arrOneCourse.name}">
                <section class="course-section">
                    <h3 class="course-section__title">Bạn sẽ học được gì</h3>
                    <div class="course-features">
                        {$arrOneCourse.des|htmlDecode}
                    </div>
                </section>
                <section class="course-section">
                    <h3 class="course-section__title">Giới thiệu khoá học</h3>
                    <div class="course-section__intro">
                        {$arrOneCourse.detail|htmlDecode}
                    </div>
                </section>
                <section class="course-section">
                    <h3 class="course-section__title mb-20">Nội dung khoá học</h3>
                    {foreach from=$arrListStage key=s item=stage}
                        {if $stage.list_lesson}
                            <div class="course-menu mt-10">
                                <div class="course-menu__header js-course-menu-header">
                                    <div class="course-menu__item media">
                                        <div class="course-menu__btn">
                                            <i class="fa fa-minus-circle fa-lg"></i>
                                            <i class="fa fa-plus-circle fa-lg"></i>
                                        </div>
                                        <div class="media-body">
                                            <div class="course-menu__title">
                                                {$stage.name}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="course-menu__body js-course-menu-body">
                                    {foreach from=$stage.list_lesson key=l item=lesson}
                                        <div class="course-menu__item media" style="cursor: pointer">
                                            <div class="course-menu__btn js-video-modal-btn" href="{if $is_active eq 1 }{$lesson.video_id}{elseif $lesson.is_trial eq 1}{$lesson.video_id}{/if}">
                                                <i class="fa fa-play-circle fa-lg"></i>
                                            </div>
                                            <div class="media-body">
                                                <div class="course-menu__title js-video-modal-btn" href="{if $is_active eq 1 }{$lesson.video_id}{elseif $lesson.is_trial eq 1}{$lesson.video_id}{/if}">{$lesson.name}{if $is_active ne 1 && $lesson.is_trial eq 1} - Học thử{/if}</div>
                                            </div>
                                            <div class="course-menu__time">{$lesson.duration}</div>
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                        {/if}
                    {/foreach}
                </section>
                {if $arrListOtherCourses}
                    <section class="course-section">
                        <h3 class="course-section__title">Khoá học liên quan</h3>
                        <ul class="relate-list mt-4">
                            {foreach from=$arrListOtherCourses key=c item=course}
                                <li class="relate-list__item">
                                    <div class="course-2 media">
                                        <a class="course-2__iwrap" href="{$Rewrite->url_course($course)}">
                                            <img src="{$NVCMS_URL}/img.php?pic={$core->callfunc('base64_encode', $course.image)}&w=215&h=110&encode=1" onerror="this.src='{$URL_IMAGES}/nopic.png'" alt="{$course.name}"/>
                                        </a>
                                        <div class="course-2__body media-body">
                                            <div class="course-2__title">
                                                <a class="text-default"
                                                   href="{$Rewrite->url_course($course)}">{$course.name}</a>
                                            </div>
                                            <div class="course-2__price">{$core->callfunc("number_format", $course.price, 0, ',', '.')}
                                                đ
                                            </div>
                                            <div class="course-2__teacher">Giáo viên: {$course.author.fullname}</div>
                                            <div class="course-2__info">
                                                <div class="course-2__rating">
                                                    <span class="course-2__rating-stars rating-stars rating-stars--4"></span>
                                                    <span>160 lượt đánh giá</span>
                                                </div>
                                                <div class="course-2__member">
                                                    <i class="fa fa-users fa-lg mr-2"></i>
                                                    <span>{$course.total_student} học viên</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            {/foreach}
                        </ul>
                    </section>
                {/if}
                <section class="mt-3">
                    <div class="facebook-comments">
                        <!-- put this place-->
                        <div class="fb-comments"
                             data-href="{$Rewrite->url_course($arrOneCourse)}" data-width="100%" data-numposts="20">
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
<div class="border-top border-bottom border-primary"></div>