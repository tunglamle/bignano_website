// navbar mobile toggle
$(function () {
  var $body = $('html, body');
  var $navbar = $('.js-navbar');
  var $navbarOpen = $('.js-navbar-open');
  var $navbarClose = $('.js-navbar-close');

  $navbarOpen.on('click', function () {
    $navbar.addClass('is-show');
    $body.addClass('overflow-hidden');
  });

  $navbarClose.on('click', function () {
    $navbar.removeClass('is-show');
    $body.removeClass('overflow-hidden');
  });
});

// menu toggle
$(function () {
  $('.menu-toggle').on('click', function () {
    var $toggle = $(this);

    $toggle.toggleClass('active').siblings('.menu-sub').slideToggle();

    $toggle.parent().siblings('.menu-item-group').children('.menu-sub').slideUp();

    $toggle.parent().siblings('.menu-item-group').children('.menu-toggle').removeClass('active');
  });
});

$(function () {
  const sliders = addSwiper(".launching-slider", {
    loop: true,
    speed: 600,
    autoHeight: true,
    autoplay: {
      delay: 3500,
      disableOnInteraction: false
    },
    pagination: true
  });

  $(".pd-tabs .nav-link").on("shown.bs.tab", function () {
    sliders.map(function (slider) {
      slider.update();
    });
  });
});

$(function () {
  var imageSliders = addSwiper(".image-slider", {
    navigation: true,
    pagination: true,
    loop: true,
    autoHeight: true
  });

  $(".accordion__toggle").on("click", function (e) {
    e.preventDefault();

    $(this).toggleClass("active");
    $(this).closest(".accordion__header").siblings(".accordion__body").slideToggle();

    setTimeout(function () {
      if (imageSliders) {
        for (let i = 0, l = imageSliders.length; i < l; i++) {
          imageSliders[i].update();
        }
      }
    }, 300);
  });

  $(".accordion__switch").on("click", function () {
    var $switch = $(this);
    var $group = $switch.closest(".accordion");
    var $body = $group.find(".accordion__body");
    var $toggle = $group.find(".accordion__toggle");

    if ($switch.hasClass("active")) {
      $switch.removeClass("active");
      $body.slideUp();
      $toggle.removeClass("active");
    } else {
      $switch.addClass("active");
      $body.slideDown();
      $toggle.addClass("active");

      setTimeout(function () {
        if (imageSliders) {
          for (let i = 0, l = imageSliders.length; i < l; i++) {
            imageSliders[i].update();
          }
        }
      }, 300);
    }
  });
});

// horizontal preview sync slider
$(function () {
  if (!$(".preview").length) {
    return;
  }

  $(".preview").each(function () {
    var $preview = $(this);
    var $thumbEl = $preview.find(".thumb-slider__container")[0];
    var $previewEl = $preview.find(".preview-slider__container")[0];
    var $previewPrev = $preview.find(".preview-slider__prev");
    var $previewNext = $preview.find(".preview-slider__next");

    var thumbSlider = new Swiper($thumbEl, {
      slidesPerView: 3,
      freeMode: true,
      spaceBetween: 16,
      watchSlidesProgress: true,
      watchSlidesVisibility: true
    });

    var previewSlider = new Swiper($previewEl, {
      navigation: {
        prevEl: $previewPrev,
        nextEl: $previewNext
      },
      effect: "fade",
      thumbs: {
        swiper: thumbSlider
      }
    });

    $(".js-product-detail-tab").on("shown.bs.tab", function () {
      thumbSlider.update();
      previewSlider.update();
    });
  });
});

// vertical preview sync slider
$(function () {
  if (!$(".preview-slider-2, .thumb-slider-2").length) {
    return;
  }

  if (!window.addSwiper) {
    console.warn('"addSwiper" funtion is required!');
    return;
  }

  var thumbSlider = addSwiper(".thumb-slider-2", {
    direction: "vertical",
    slidesPerView: "auto",
    freeMode: true,
    watchSlidesProgress: true,
    watchSlidesVisibility: true,
    spaceBetween: 16,
    breakpoints: {
      576: {
        spaceBetween: 10
      }
    }
  })[0];

  var previewSlider = addSwiper(".preview-slider-2", {
    init: false,
    pagination: true,
    effect: "fade",
    navigation: true,
    allowTouchMove: false,
    thumbs: {
      swiper: thumbSlider
    }
  })[0];

  previewSlider.on("init", function () {
    $(".js-zoom").each(function () {
      const url = $(this).attr("href");
      $(this).zoom({
        url
      });
    });
  });

  previewSlider.init();
});

$(function () {
  var newsSlider = addSwiper(".news-slider", {
    navigation: true,
    pagination: true,
    slidesPerView: 5,
    loop: true,
    speed: 500,
    breakpoints: {
      1199: {
        slidesPerView: 4
      },
      991: {
        slidesPerView: 3
      },
      767: {
        slidesPerView: 2
      },
      575: {
        slidesPerView: 1
      }
    }
  });

  $(".js-news-tab").on("shown.bs.tab", function () {
    newsSlider.map(function (slider) {
      slider.update();
    });
  });
});

// swiper template
function addSwiper(selector, options = {}) {
  return Array.from(document.querySelectorAll(selector), function (item) {
    var $sliderContainer = $(item),
        $sliderEl = $sliderContainer.find(selector + "__container");

    if (options.navigation) {
      $sliderContainer.addClass("has-nav");
      options.navigation = {
        prevEl: $sliderContainer.find(selector + "__prev"),
        nextEl: $sliderContainer.find(selector + "__next")
      };
    }

    if (options.pagination) {
      $sliderContainer.addClass("has-pagination");
      options.pagination = {
        el: $sliderContainer.find(selector + "__pagination"),
        clickable: true
      };
    }

    return new Swiper($sliderEl, options);
  });
}

$(function () {
  const $searchContainer = $(".home-movie__search");
  const $searchBtn = $(".sticky-btns__link--search");

  if (!$searchContainer.length) {
    return;
  }

  $searchBtn.addClass("is-hide");

  $(window).on("scroll", function () {
    var offset = $searchContainer.offset().top;
    var scrollTop = $(window).scrollTop();

    if (offset > scrollTop) {
      $searchBtn.addClass("is-hide");
    } else {
      $searchBtn.removeClass("is-hide");
    }
  });
});

$(function () {
  $(".js-md-search").on("click", function () {
    $(".md-search").fadeIn();
  });

  $(".md-search, .md-search__close").on("click", function () {
    $(".md-search").fadeOut();
  });

  $(".md-search__container").on("click", function (e) {
    e.stopPropagation();
  });
});

$(function () {
  $(window).on("scroll", function () {
    var $sticky = $(".sticky");
    var $menuBtn = $(".js-menu-toggle");
    var offset = $(".sitemap").offset().top;
    var scrollTop = $(window).scrollTop();
    var windowHeight = $(window).height();

    if (scrollTop + windowHeight > offset) {
      $menuBtn.removeClass("active").slideUp("fast");
      $sticky.css("marginBottom", scrollTop + windowHeight - offset);
      $(".sticky__sitemap").hide();
    } else {
      $menuBtn.slideDown("fast");
      $sticky.css("marginBottom", 0);
    }
  });
});

$(function () {
  $(".js-menu-toggle").on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("active");
    $(".sticky__sitemap").slideToggle();
  });
});

$(function () {
  $(".js-md-lang").on("click", function (e) {
    e.preventDefault();

    $(".md-lang").fadeIn();
  });

  $(".md-lang, .md-lang__close").on("click", function () {
    $(".md-lang").fadeOut();
  });

  $(".md-lang__container").on("click", function (e) {
    e.stopPropagation();
  });
});

$(function () {
  $(".post-nav__link").on("click", function (e) {
    e.preventDefault();
    var target = $(this).attr("href");
    if ($(target).length) {
      $("html, body").animate({
        scrollTop: $(target).offset().top
      }, 800);
    }
  });
});

$(function () {
  $("html, body").on("click", function () {
    $(".home-card").removeClass("active");
  });

  $(".home-card").on("click", function (e) {
    e.stopPropagation();
    $(this).addClass("active");
    $(".home-card").not($(this)).removeClass("active");
  });

  $(".home-card__close").on("click", function (e) {
    e.stopPropagation();
    $(".home-card").removeClass("active");
  });
});

$(function () {
  $(".sitemap__dropdown-toggle").on("click", function (e) {
    e.preventDefault();

    if ($(window).width() >= 992) {
      return;
    }

    var $toggle = $(this);

    $toggle.toggleClass("active").siblings(".sitemap__dropdown-menu").slideToggle();

    $toggle.closest(".sitemap").find(".sitemap__dropdown-menu").not($toggle.siblings(".sitemap__dropdown-menu")).slideUp();

    $$toggle.closest(".sitemap").find(".sitemap__dropdown-toggle").not($toggle).removeClass("active");
  });

  $(".sitemap__item").on("mouseenter", function () {
    if ($(window).width() < 992) {
      return;
    }

    var $item = $(this);
    var $link = $item.children(".sitemap__link");
    var $menu = $item.children(".sitemap__dropdown-menu");

    $link.addClass("active");
    $(".sitemap__link").not($link).removeClass("active");

    $menu.show();

    $(".sitemap__dropdown-menu").not($menu).hide();
  });
});

$(function () {
  $(".js-movetop").on("click", function (e) {
    e.preventDefault();

    $("html, body").animate({
      scrollTop: 0
    }, 1000);
  });
});

$(function () {
  floating();
});

// floating
function floating() {
  $(".floating").each(function () {
    var $floating = $(this),
        width = $floating.width(),
        offsetLeft = $floating.offset().left,
        offsetTop = $floating.offset().top;

    $floating.data("offsetLeft", offsetLeft).data("offsetTop", offsetTop).css({
      width: width
    });
  });

  if ($(window).width() < 992) {
    return;
  }

  $(window).on("scroll", function () {
    $(".floating").each(function () {
      var $floating = $(this),
          offsetTop = $floating.data("offsetTop"),
          offsetLeft = $floating.data("offsetLeft"),
          height = $floating.outerHeight(),
          outerHeight = $floating.outerHeight(true),
          $container = $floating.closest(".floating-container"),
          dataTop = $floating.data("top"),
          top = dataTop !== undefined ? parseInt(dataTop) : 30,
          containerHeight = $container.outerHeight(),
          containerOffsetTop = $container.offset().top,
          scrollTop = $(window).scrollTop();

      if (outerHeight + offsetTop == containerHeight + containerOffsetTop) {
        return;
      } else if (scrollTop + top <= offsetTop) {
        $(this).css({
          position: "static"
        });
      } else if (scrollTop + height + top > containerHeight + containerOffsetTop) {
        $(this).css({
          position: "absolute",
          zIndex: 2,
          top: "auto",
          bottom: 0,
          left: "15px"
        });
      } else {
        $(this).css({
          position: "fixed",
          zIndex: 2,
          top: top,
          left: offsetLeft,
          bottom: "auto"
        });
      }
    });
  });
}