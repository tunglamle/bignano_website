<?
/******************************************************
 * Class Rewrite (URL Rewrite Controller)
 *
 * Parse URL string to corresponding vars
 *
 * Project Name               :  ClientWebsite
 * Package Name                    :
 * Program ID                 :  clsRewrite.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  20/01/2018
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        20/01/2018        banglcb          -        -     -     -
 *
 ********************************************************/

/**
 * Rewrite Class
 *
 * @author Tuanta
 *
 */
class Rewrite
{
    var $base = "";
    var $actual = "";
    var $path = "";
    var $rules = array();
    var $e404 = "error404.php";
    var $exec = 0;

    /**
     * Initialize class
     *
     * @param                : string $base
     * @return            : no
     */
    function Rewrite($base = "", $exec = 1)
    {
        $this->path = parse_url($_SERVER['REQUEST_URI']);
        $this->actual = $this->path == "/" ? array("") : explode("/", $this->path['path']);
        $this->base = $base;
        if ($exec == 1) {
            $this->execute();
        }
    }

    /**
     * Return page 404
     *
     * @param                : no
     * @return            : no
     */
    function error404()
    {
        header('HTTP/1.1 404 Not Found');
        header('Status: 404 Not Found');
        if ($this->e404 != '')
            require $this->e404;
        exit();
    }

    /**
     * Parse URL path to detect params and assign to $_GET
     *
     * @param                : no
     * @return            : no
     */
    function execute()
    {
        global $_CAT_SLUG;
        $this->exec = 1;
        if (strpos($this->path['path'], 'http:') !== false || strpos($this->path['path'], '?') !== false) {
            $this->error404();
        }
        $ok = preg_match("/\.(jpg|jpeg|gif|ico|png|js|css|txt|swf|tpl|ttf|xml|doc|zip|rar|xls|mp3|avi)$/i", $this->path['path'], $match);
        if ($ok) {
            $this->error404();
            return 0;
        }
        $this->url_article();
        $this->url_job();
        $this->url_about();
        $this->url_csr();
        $this->url_for_investor();
        $this->url_library();
        $this->url_product();
        $this->url_search();
        $this->url_product_search();
        $this->url_catalog();
        $this->url_tags();
        $this->url_course();
        $this->url_active_course();
        $this->url_checkout();
        $this->url_page();
        $this->url_video();
        $this->url_home();
        $this->url_submitcontact();
        $this->url_contact();
        $this->url_agency();
        $this->url_partner();
        $this->url_shownews();
        $this->url_shownewstype();
        $this->url_showcart();
        $this->url_viewcart();
        $this->url_updatecart();
        $this->url_updateorder();
        $this->url_removeitem();
        $this->url_removecart();
        $this->url_account();
        $this->url_history();
        $this->url_login();
        $this->url_fbauth();
        $this->url_ggauth();
        $this->url_register();
        $this->url_logout();
        $this->url_changeinfo();
        $this->url_resetpass();
        $this->url_active();
        $this->url_category();
        $this->url_order();
        $this->exec = 0;
    }

    /**
     * Find type of category
     *
     * @param unknown $query
     * @return number
     */
    function findCTYPE($query)
    {
        global $_CAT_SLUG;
        $ctype = -1;
        $maxlen = 0;
        foreach ($_CAT_SLUG as $key => $val) {
            $ok = (strpos('a' . $query, $key) == 1 || $query == $key);
            if ($ok && $maxlen < strlen($key)) {
                $ctype = $val;
                $maxlen = strlen($key);
                $slug = $key;
            }
        }
        return $ctype;
    }

    /**
     *
     * @param string $category
     * @return string
     */
    function url_category($category = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/category\/([a-zA-Z0-9]+[a-zA-Z\_0-9\.-]*)$/i", $this->path['path'], $regs);

            if ($ok) {
                $ctype = $this->findCTYPE($regs[1]);
                if ($ctype == CTYPE_BV) {
                    $_GET["mod"] = "articles";
                    $_GET["slug"] = $regs[1];
                } elseif ($ctype == CTYPE_SP) {
                    $_GET["mod"] = "product";
                    $_GET["slug"] = $regs[1];
                 } elseif ($ctype == CTYPE_GT) {
                    $_GET["mod"] = "about";
                    $_GET["slug"] = $regs[1];
                } elseif ($ctype == CTYPE_CSR) {
                    $_GET["mod"] = "csr";
                    $_GET["slug"] = $regs[1];
                } elseif ($ctype == CTYPE_FI) {
                    $_GET["mod"] = "for_investor";
                    $_GET["slug"] = $regs[1];
                } elseif ($ctype == CTYPE_TA) {
                    $_GET["mod"] = "typical";
                    $_GET["slug"] = $regs[1];
                } elseif ($ctype == CTYPE_VL) {
                    $_GET["mod"] = "job";
                    $_GET["slug"] = $regs[1];
                }   else {
                    $_GET["mod"] = "home";
                    $_GET["act"] = "notfound";
                }
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if (!is_array($category)) {
            $html = VNCMS_URL . "/category/" . $category;
        } else {
            if ($_CONFIG['enable_urlrewrite'] == 1) {
                $html = VNCMS_URL . "/category/" . $category['slug'];
            } else {
                $html = VNCMS_URL . "/?mod=news&view_cat_id=" . $category['cat_id'] . "&view_cat_name=" . $category['slug'];
            }
        }
        return $html;
    }

    /**
     *
     * @param string $article
     * @return string
     */
    function url_article($article = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/([a-zA-Z0-9]+[a-zA-Z\_0-9\.-]*)-news([0-9]+)([^0-9]*)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "articles";
                $_GET["act"] = "detail";
                $_GET["article_id"] = $regs[2];
                if ($regs[3] != '') {
                    $_GET["mod"] = "home";
                    $_GET["act"] = "notfound";
                }
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if (!is_array($article)) {
            $html = VNCMS_URL . "/" . $article;
        } else {
            if ($_CONFIG['enable_urlrewrite'] == 1) {
                $html = VNCMS_URL . "/" . $article['slug'] . "-news" . $article['article_id'];
            } else {
                $html = VNCMS_URL . "/?mod=articles&view_article_id=" . $article['article_id'] . "&view_slug=" . $article['slug'];
            }
        }
        return $html;
    }

    function url_job($job = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/([a-zA-Z0-9]+[a-zA-Z\_0-9\.-]*)-job([0-9]+)([^0-9]*)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "job";
                $_GET["act"] = "detail";
                $_GET["job_id"] = $regs[2];
                if ($regs[3] != '') {
                    $_GET["mod"] = "home";
                    $_GET["act"] = "notfound";
                }
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if (!is_array($job)) {
            $html = VNCMS_URL . "/" . $job;
        } else {
            if ($_CONFIG['enable_urlrewrite'] == 1) {
                $html = VNCMS_URL . "/" . $job['slug'] . "-job" . $job['job_id'];
            } else {
                $html = VNCMS_URL . "/?mod=job&view_job_id=" . $job['job_id'] . "&view_slug=" . $job['slug'];
            }
        }
        return $html;
    }

    function url_about($about = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/([a-zA-Z0-9]+[a-zA-Z\_0-9\.-]*)-ab([0-9]+)([^0-9]*)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "about";
                $_GET["act"] = "detail";
                $_GET["about_id"] = $regs[2];
                if ($regs[3] != '') {
                    $_GET["mod"] = "home";
                    $_GET["act"] = "notfound";
                }
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if (!is_array($about)) {
            $html = VNCMS_URL . "/" . $about;
        } else {
            if ($_CONFIG['enable_urlrewrite'] == 1) {
                $html = VNCMS_URL . "/" . $about['slug'] . "-ab" . $about['about_id'];
            } else {
                $html = VNCMS_URL . "/?mod=about&view_about_id=" . $about['about_id'] . "&view_slug=" . $about['slug'];
            }
        }
        return $html;
    }

    function url_csr($csr = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/([a-zA-Z0-9]+[a-zA-Z\_0-9\.-]*)-cs([0-9]+)([^0-9]*)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "csr";
                $_GET["act"] = "detail";
                $_GET["csr_id"] = $regs[2];
                if ($regs[3] != '') {
                    $_GET["mod"] = "home";
                    $_GET["act"] = "notfound";
                }
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if (!is_array($csr)) {
            $html = VNCMS_URL . "/" . $csr;
        } else {
            if ($_CONFIG['enable_urlrewrite'] == 1) {
                $html = VNCMS_URL . "/" . $csr['slug'] . "-cs" . $csr['csr_id'];
            } else {
                $html = VNCMS_URL . "/?mod=csr&view_csr_id=" . $csr['csr_id'] . "&view_slug=" . $csr['slug'];
            }
        }
        return $html;
    }

    function url_for_investor($for_investor = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/([a-zA-Z0-9]+[a-zA-Z\_0-9\.-]*)-fi([0-9]+)([^0-9]*)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "for_investor";
                $_GET["act"] = "detail";
                $_GET["for_investor_id"] = $regs[2];
                if ($regs[3] != '') {
                    $_GET["mod"] = "home";
                    $_GET["act"] = "notfound";
                }
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if (!is_array($for_investor)) {
            $html = VNCMS_URL . "/" . $for_investor;
        } else {
            if ($_CONFIG['enable_urlrewrite'] == 1) {
                $html = VNCMS_URL . "/" . $for_investor['slug'] . "-fi" . $for_investor['for_investor_id'];
            } else {
                $html = VNCMS_URL . "/?mod=for_investor&view_for_investor_id=" . $for_investor['for_investor_id'] . "&view_slug=" . $for_investor['slug'];
            }
        }
        return $html;
    }

    function url_library($library = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/([a-zA-Z0-9]+[a-zA-Z\_0-9\.-]*)-tv([0-9]+)([^0-9]*)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "library";
                $_GET["act"] = "detail";
                $_GET["library_id"] = $regs[2];
                if ($regs[3] != '') {
                    $_GET["mod"] = "home";
                    $_GET["act"] = "notfound";
                }
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if (!is_array($library)) {
            $html = VNCMS_URL . "/" . $library;
        } else {
            if ($_CONFIG['enable_urlrewrite'] == 1) {
                $html = VNCMS_URL . "/" . $library['slug'] . "-tv" . $library['library_id'];
            } else {
                $html = VNCMS_URL . "/?mod=library&view_library_id=" . $library['library_id'] . "&view_slug=" . $library['slug'];
            }
        }
        return $html;
    }

    /**
     *
     * @param string $product
     * @return string
     */
    function url_product($product = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/([a-zA-Z0-9]+[a-zA-Z\_0-9\.-]*)-pr([0-9]+)([^0-9]*)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "product";
                $_GET["act"] = "detail";
                $_GET["product_id"] = $regs[2];
                if ($regs[3] != '') {
                    $_GET["mod"] = "home";
                    $_GET["act"] = "notfound";
                }
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if (!is_array($product)) {
            $html = VNCMS_URL . "/" . $product;
        } else {
            if ($_CONFIG['enable_urlrewrite'] == 1) {
                $html = VNCMS_URL . "/" . $product['slug'] . "-pr" . $product['product_id'];
            } else {
                $html = VNCMS_URL . "/?mod=product&view_product_id=" . $product['product_id'] . "&view_slug=" . $product['slug'];
            }
        }
        return $html;
    }

    /**
     * @param string $Keyword
     * @return int|string
     */

    function url_search($Keyword = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/(tim-kiem|search)/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["act"] = "search";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/search?key=$Keyword";
        } else {
            $html = VNCMS_URL . "/?mod=home&act=search?key=$Keyword";
        }
        return $html;
    }

    function url_product_search($Keyword = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/(product-search)/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "product";
                $_GET["act"] = "search";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/product-search?key=$Keyword";
        } else {
            $html = VNCMS_URL . "/?mod=product&act=search?key=$Keyword";
        }
        return $html;
    }

    function url_catalog($catalog = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/catalog$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["act"] = "catalog";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/catalog";
        } else {
            $html = VNCMS_URL . "/?mod=home&act=catalog?";
        }
        return $html;
    }

    /**
     * @param string $Tags
     * @return int|string
     */

    function url_tags($Tags = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/articles\/tags$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "articles";
                $_GET["act"] = "tags";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/tags/" . utf8_nosign_noblank($Tags);
        } else {
            $html = VNCMS_URL . "/?mod=articles&act=tags&key=$Tags";
        }
        return $html;
    }

    /**
     *
     * @param string $course
     * @return string
     */
    function url_course($course = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/khoa-hoc\/([a-zA-Z0-9]+[a-zA-Z\_0-9\.-]*)-c([0-9]+)([^0-9]*)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "courses";
                $_GET["act"] = "detail";
                $_GET["course_id"] = $regs[2];
                if ($regs[3] != '') {
                    $_GET["mod"] = "home";
                    $_GET["act"] = "notfound";
                }
                $this->exec = 0;
                return 1;
            }
            $ok = preg_match("/^\/khoa-hoc\/search$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "courses";
                $_GET["act"] = "search";
                $this->exec = 0;
                return 1;
            }
            $ok = preg_match("/^\/khoa-hoc\/ajax\/([a-zA-Z0-9\_]+)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "courses";
                $_GET["sub"] = "ajax";
                $_GET["act"] = $regs[1];
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if (!is_array($course)) {
            $html = VNCMS_URL . "/khoa-hoc" . $course;
        } else {
            if ($_CONFIG['enable_urlrewrite'] == 1) {
                $html = VNCMS_URL . "/khoa-hoc/" . $course['slug'] . "-c" . $course['course_id'];
            } else {
                $html = VNCMS_URL . "/?mod=courses&view_course_id=" . $course['course_id'] . "&view_slug=" . $course['slug'];
            }
        }
        return $html;
    }

    function url_active_course()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/khoa-hoc\/active$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "courses";
                $_GET["act"] = "active";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/khoa-hoc/active";
        } else {
            $html = VNCMS_URL . "/?mod=courses&act=active";
        }
        return $html;
    }

    /**
     *
     * @return string
     */
    function url_checkout()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/cart\/checkout$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "cart";
                $_GET["act"] = "checkout";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/cart/checkout";
        } else {
            $html = VNCMS_URL . "/?mod=cart&act=checkout";
        }
        return $html;
    }

    /**
     *
     * @param string $arrOnePage
     * @return string
     */
    function url_page($arrOnePage = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/page\/([a-zA-Z0-9]+[a-zA-Z\_0-9\.-]*)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["act"] = "page";
                $_GET["slug"] = $regs[1];
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if (!is_array($arrOnePage)) {
            return VNCMS_URL . "/page/" . $arrOnePage;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/page/" . $arrOnePage['slug'];
        } else {
            $html = VNCMS_URL . "/?pid=" . $arrOnePage['slug'];
        }
        return $html;
    }

    /**
     *
     * @return string
     */
    function url_video()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/video$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["act"] = "video";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/video";
        } else {
            $html = VNCMS_URL . "/?mod=home&act=video";
        }
        return $html;
    }

    /**
     *
     * @return string
     */
    function url_home()
    {
        if ($this->exec) {
            $ok = preg_match("/api$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["sub"] = "api";
                return 1;
            }
            $ok = preg_match("/^\/ajax\/([a-zA-Z0-9]+)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["sub"] = "ajax";
                $_GET["act"] = $regs[1];
                return 1;
            }
            $ok = preg_match("/.+$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["act"] = "default";
                return 1;
            }
            return 0;
        }
        $html = VNCMS_URL;
        return $html;
    }


    function url_submitcontact()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/ajax\/submit-contact$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["sub"] = "ajax";
                $_GET["act"] = "submitContact";
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/submit-contact";
        } else {
            $html = VNCMS_URL . "/?mod=home&sub=ajax&act=submitContact";
        }
        return $html;
    }
    /**
     *
     * @return string
     */
    function url_contact()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/lien-he.html$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["act"] = "contact";
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/contact";
        } else {
            $html = VNCMS_URL . "/?mod=home&act=contact";
        }
        return $html;
    }

    function url_agency()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/agency$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["act"] = "agency";
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/agency";
        } else {
            $html = VNCMS_URL . "/?mod=home&act=agency";
        }
        return $html;
    }

    function url_partner()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/doi-tac*/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["act"] = "partner";
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/partner";
        } else {
            $html = VNCMS_URL . "/?mod=home&act=partner";
        }
        return $html;
    }


    function url_shownews()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/ajax\/show-news$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["sub"] = "ajax";
                $_GET["act"] = "showNews";
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/show-news";
        } else {
            $html = VNCMS_URL . "/?mod=home&sub=ajax&act=showNews";
        }
        return $html;
    }

    function url_shownewstype()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/ajax\/show-news-type$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "home";
                $_GET["sub"] = "ajax";
                $_GET["act"] = "showNewsType";
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/show-news-type";
        } else {
            $html = VNCMS_URL . "/?mod=home&sub=ajax&act=showNewsType";
        }
        return $html;
    }


    function url_showcart()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/show-cart$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "cart";
                $_GET["sub"] = "default";
                $_GET["act"] = "showcart";
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/show-cart";
        } else {
            $html = VNCMS_URL . "/?mod=cart&act=showcart";
        }
        return $html;
    }

    function url_viewcart()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/cart$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "cart";
                $_GET["sub"] = "default";
                $this->exec = 0;
                return 1;
            }
            $ok = preg_match("/^\/cart\/ajax\/([a-zA-Z0-9]+)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "cart";
                $_GET["sub"] = "ajax";
                $_GET["act"] = $regs[1];
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/cart";
        } else {
            $html = VNCMS_URL . "/?mod=cart";
        }
        return $html;
    }

    function url_updatecart()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/cart\/update-cart$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "cart";
                $_GET["sub"] = "ajax";
                $_GET["act"] = "updatecart";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/update-cart";
        } else {
            $html = VNCMS_URL . "/?mod=cart&act=updatecart";
        }
        return $html;
    }

    function url_updateorder()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/cart\/update-order$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "cart";
                $_GET["sub"] = "ajax";
                $_GET["act"] = "updateorder";
                print $regs;
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/update-order";
        } else {
            $html = VNCMS_URL . "/?mod=cart&act=updateorder";
        }
        return $html;
    }

    function url_showCartHeader()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/cart\/showcatheader$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "cart";
                $_GET["sub"] = "ajax";
                $_GET["act"] = "showcartheader";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/showcatheader";
        } else {
            $html = VNCMS_URL . "/?mod=cart&act=showcatheader";
        }
        return $html;
    }

    function url_removeitem()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/cart\/remove-item$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "cart";
                $_GET["sub"] = "ajax";
                $_GET["act"] = "removeitem";
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/remove-item";
        } else {
            $html = VNCMS_URL . "/?mod=cart&act=removeitem";
        }
        return $html;
    }

    function url_removecart()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/remove-cart/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "cart";
                $_GET["sub"] = "ajax";
                $_GET["act"] = "removecart";
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/remove-cart";
        } else {
            $html = VNCMS_URL . "/?mod=cart&act=removecart";
        }
        return $html;
    }

    function url_account()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/account$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["sub"] = "default";
                $this->exec = 0;
                return 1;
            }
            $ok = preg_match("/^\/account\/ajax\/([a-zA-Z0-9]+)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["sub"] = "ajax";
                $_GET["act"] = $regs[1];
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/account";
        } else {
            $html = VNCMS_URL . "/?mod=account";
        }
        return $html;
    }

    function url_history()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/account\/history$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["act"] = "history";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/account/history";
        } else {
            $html = VNCMS_URL . "/?mod=account&act=history";
        }
        return $html;
    }

    function url_login()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/login$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["act"] = "login";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/login";
        } else {
            $html = VNCMS_URL . "/?mod=account&act=login";
        }
        return $html;
    }

    function url_fbauth()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/facebook-authentication$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["act"] = "fbauth";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/facebook-authentication";
        } else {
            $html = VNCMS_URL . "/?mod=account&act=fbauth";
        }
        return $html;
    }

    function url_ggauth()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/google-authentication$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["act"] = "ggauth";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/google-authentication";
        } else {
            $html = VNCMS_URL . "/?mod=account&act=ggauth";
        }
        return $html;
    }

    function url_register()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/register$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["act"] = "register";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/register";
        } else {
            $html = VNCMS_URL . "/?mod=account&act=register";
        }
        return $html;
    }

    function url_logout()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/logout$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["act"] = "logout";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/logout";
        } else {
            $html = VNCMS_URL . "/?mod=account&act=logout";
        }
        return $html;
    }

    function url_changeinfo()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/account\/change-info$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["act"] = "changeinfo";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/account/change-info";
        } else {
            $html = VNCMS_URL . "/?mod=account&act=changeinfo";
        }
        return $html;
    }

    function url_resetpass()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/reset-password$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["act"] = "resetpass";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/reset-password";
        } else {
            $html = VNCMS_URL . "/?mod=account&act=resetpass";
        }
        return $html;
    }

    function url_active()
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/active$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["act"] = "activeacc";
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/active";
        } else {
            $html = VNCMS_URL . "/?mod=account&act=activeacc";
        }
        return $html;
    }

    /**
     *
     * @param string $arrOneOrder
     * @return string
     */
    function url_order($arrOneOrder = "")
    {
        global $_CONFIG;
        if ($this->exec) {
            $ok = preg_match("/^\/account\/order-managerment$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["sub"] = "order";
                $this->exec = 0;
                return 1;
            }
            $ok = preg_match("/^\/account\/order\/([a-zA-Z0-9]+)$/i", $this->path['path'], $regs);
            if ($ok) {
                $_GET["mod"] = "account";
                $_GET["sub"] = "order";
                $_GET["act"] = "detail";
                $_GET["order_code"] = $regs[1];
                $this->exec = 0;
                return 1;
            }
            return 0;
        }
        if (!is_array($arrOneOrder)) {
            return VNCMS_URL . "/account/order/" . $arrOneOrder;
        }
        if ($_CONFIG['enable_urlrewrite'] == 1) {
            $html = VNCMS_URL . "/account/order/" . $arrOneOrder['order_code'];
        } else {
            $html = VNCMS_URL . "?mod=account&sub=order&act=detail&order_code=" . $arrOneOrder['order_code'];
        }
        return $html;
    }

}

$base = trim(dirname(" " . $_SERVER['SCRIPT_NAME']));
if ($_SITE_ROOT == "root") {
    $clsRewrite = new Rewrite($base);
}
?>