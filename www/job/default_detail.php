<?php
/**
 * Module: [Jobs]
 * Posts function with $sub=default, $act=detail
 * Display detail of a post
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_detail()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $isMobile, $_LANG_ID;
    //Begin GetVars
    $job_id = isset($_GET["job_id"]) ? $_GET["job_id"] : "";
    $slug = isset($_GET["slug"]) ? $_GET["slug"] : "";
    if (isset($_GET["job_id"]) && "$job_id" != $_GET["job_id"] && $_GET["job_id"] != "") {
        $act = "notfound";
        return;
    }
    //End GetVars
    //Begin Init
    $clsCourses = new Courses();
    $clsCategory = new Category();
    $clsJob = new Job();
    $clsUsers = new Users();

    $clsCategory->getParentArray();
    if ($job_id == "" || $job_id == 0) {
        if ($slug != "") {
            $arrTmp = $clsJob->getByCond("slug='$slug'");
            if (is_array($arrTmp) && $arrTmp['job_id'] != 0) {
                $job_id = $arrTmp['job_id'];
            }
        } else {
            $act = "notfound";
            return;
        }
    }
    $job_id = intval($job_id);
    //End Init

    $arrOneJob = $clsJob->getOne($job_id);
    if (!is_array($arrOneJob) || $arrOneJob["job_id"] != $job_id) {
        $act = "notfound";
        return;
    }
    // Tags
    $tags = $arrOneJob['tags'];
    $htmlTags = "";
    $arrListTags = array_filter(explode(",", $tags));
    if (is_array($arrListTags)) {
        foreach ($arrListTags as $k => $v) {
            $url_tags = url_tags($v);
            $htmlTags .= "<li class='nav-item'><a class='nav-link' href='$url_tags'>$v</a></li>";
        }
    }

    //Tăng lượt xem
    $clsJob->updateOne($job_id, "view_num=view_num+1");
    $cat_id = $arrOneJob["cat_id"];
    if ($cat_id > 0) {
        $curCat = $clsCategory->getOne($cat_id);
        if ($curCat['parent_id'] > 0) {
            $parCat = $clsCategory->getOne($curCat['parent_id']);
        }
    }
    $arrListOtherJob = $clsJob->getAllSimple("c.cat_id=$cat_id AND a.job_id<>$job_id ORDER BY a.reg_date DESC LIMIT 0,6");
    $arrAuthor = ($arrOneJob['user_id'] > 0) ? $clsUsers->getOne($arrOneJob['user_id']) : array();

    $arrListJobByCat = array();
    if ($curCat['parent_id'] > 0) {
        $arrListJobByCat = $clsCategory->getSubCatNews($curCat['parent_id'], 0);
    }
    if ($curCat['image'] == "") {
        $curCat['image'] = $parCat['image'];
    }

    $arrListJobHot = $clsJob->getAllSimple2("is_online = 1 AND is_hot = 1 ORDER BY reg_date DESC");




    //Begin Right Sidebar



    //End Right Sidebar
    //Begin Assign
    $assign_list["curCat"] = $curCat;
    $assign_list["parCat"] = $parCat;
    $assign_list["arrOneJob"] = $arrOneJob;
    $assign_list["arrAuthor"] = $arrAuthor;
    $assign_list["htmlTags"] = $htmlTags;
    $assign_list["arrListOtherJob"] = $arrListOtherJob;
    $assign_list["arrListJobByCat"] = $arrListJobByCat;
    $assign_list["arrListJobHot"] = $arrListJobHot;
    //End Assign
    //Begin SEOmoz
    $page_title = ($arrOneJob['page_title'] != "") ? $arrOneJob['page_title'] : $arrOneJob['title'];
    $page_title .= " - " . $_CONFIG['site_title'];
    $tags = $arrOneJob['meta_keywords'];
    $page_keywords = ($tags != "") ? $tags : $_CONFIG['meta_keywords'];
    $des = $arrOneJob['meta_des'];
    $page_description = ($des != "") ? $des : $_CONFIG['site_description'];
    $_CONFIG['thumb'] = $arrOneJob['image'];
    $_CONFIG['page_title'] = $page_title;
    $_CONFIG['page_keywords'] = $page_keywords;
    $_CONFIG['page_description'] = $page_description;
    unset($tags, $des);
    //End SEOmoz
}

?>