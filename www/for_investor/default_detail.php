<?php
/**
 * Module: [For_investor]
 * Posts function with $sub=default, $act=detail
 * Display detail of a post
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_detail()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $isMobile, $_LANG_ID;
    //Begin GetVars
    $for_investor_id = isset($_GET["for_investor_id"]) ? $_GET["for_investor_id"] : "";
    $slug = isset($_GET["slug"]) ? $_GET["slug"] : "";
    if (isset($_GET["for_investor_id"]) && "$for_investor_id" != $_GET["for_investor_id"] && $_GET["for_investor_id"] != "") {
        $act = "notfound";
        return;
    }
    //End GetVars
    //Begin Init
    $clsCourses = new Courses();
    $clsCategory = new Category();
    $clsFor_investor = new For_investor();
    $clsUsers = new Users();

    $clsCategory->getParentArray();
    if ($for_investor_id == "" || $for_investor_id == 0) {
        if ($slug != "") {
            $arrTmp = $clsFor_investor->getByCond("slug='$slug'");
            if (is_array($arrTmp) && $arrTmp['for_investor_id'] != 0) {
                $for_investor_id = $arrTmp['for_investor_id'];
            }
        } else {
            $act = "notfound";
            return;
        }
    }
    $for_investor_id = intval($for_investor_id);
    //End Init

    $arrOneFor_investor = $clsFor_investor->getOne($for_investor_id);
    if (!is_array($arrOneFor_investor) || $arrOneFor_investor["for_investor_id"] != $for_investor_id) {
        $act = "notfound";
        return;
    }
    // Tags
    $tags = $arrOneFor_investor['tags'];
    $htmlTags = "";
    $arrListTags = array_filter(explode(",", $tags));
    if (is_array($arrListTags)) {
        foreach ($arrListTags as $k => $v) {
            $url_tags = url_tags($v);
            $htmlTags .= "<li class='nav-item'><a class='nav-link' href='$url_tags'>$v</a></li>";
        }
    }

    //Tăng lượt xem
    $clsFor_investor->updateOne($for_investor_id, "view_num=view_num+1");
    $cat_id = $arrOneFor_investor["cat_id"];
    if ($cat_id > 0) {
        $curCat = $clsCategory->getOne($cat_id);
        if ($curCat['parent_id'] > 0) {
            $parCat = $clsCategory->getOne($curCat['parent_id']);
        }
    }
    $arrListOtherFor_investor = $clsFor_investor->getAllSimple("c.cat_id=$cat_id AND a.for_investor_id<>$for_investor_id ORDER BY a.reg_date DESC LIMIT 0,6");
    $arrAuthor = ($arrOneFor_investor['user_id'] > 0) ? $clsUsers->getOne($arrOneFor_investor['user_id']) : array();

    $arrListFor_investorByCat = array();
    if ($curCat['parent_id'] > 0) {
        $arrListFor_investorByCat = $clsCategory->getSubCatNews($curCat['parent_id'], 0);
    }
    if ($curCat['image'] == "") {
        $curCat['image'] = $parCat['image'];
    }


    //Begin Right Sidebar



    //End Right Sidebar
    //Begin Assign
    $assign_list["curCat"] = $curCat;
    $assign_list["parCat"] = $parCat;
    $assign_list["arrOneFor_investor"] = $arrOneFor_investor;
    $assign_list["arrAuthor"] = $arrAuthor;
    $assign_list["htmlTags"] = $htmlTags;
    $assign_list["arrListOtherFor_investor"] = $arrListOtherFor_investor;
    $assign_list["arrListFor_investorByCat"] = $arrListFor_investorByCat;
    //End Assign
    //Begin SEOmoz
    $page_title = ($arrOneFor_investor['page_title'] != "") ? $arrOneFor_investor['page_title'] : $arrOneFor_investor['title'];
    $page_title .= " - " . $_CONFIG['site_title'];
    $tags = $arrOneFor_investor['meta_keywords'];
    $page_keywords = ($tags != "") ? $tags : $_CONFIG['meta_keywords'];
    $des = $arrOneFor_investor['meta_des'];
    $page_description = ($des != "") ? $des : $_CONFIG['site_description'];
    $_CONFIG['thumb'] = $arrOneFor_investor['image'];
    $_CONFIG['page_title'] = $page_title;
    $_CONFIG['page_keywords'] = $page_keywords;
    $_CONFIG['page_description'] = $page_description;
    unset($tags, $des);
    //End SEOmoz
}

?>