<?php
/**
 * Module: [Csr]
 * Posts function with $sub=default, $act=detail
 * Display detail of a post
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_detail()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $isMobile, $_LANG_ID;
    //Begin GetVars
    $csr_id = isset($_GET["csr_id"]) ? $_GET["csr_id"] : "";
    $slug = isset($_GET["slug"]) ? $_GET["slug"] : "";
    if (isset($_GET["csr_id"]) && "$csr_id" != $_GET["csr_id"] && $_GET["csr_id"] != "") {
        $act = "notfound";
        return;
    }
    //End GetVars
    //Begin Init
    $clsCourses = new Courses();
    $clsCategory = new Category();
    $clsCsr = new Csr();
    $clsUsers = new Users();

    $clsCategory->getParentArray();
    if ($csr_id == "" || $csr_id == 0) {
        if ($slug != "") {
            $arrTmp = $clsCsr->getByCond("slug='$slug'");
            if (is_array($arrTmp) && $arrTmp['csr_id'] != 0) {
                $csr_id = $arrTmp['csr_id'];
            }
        } else {
            $act = "notfound";
            return;
        }
    }
    $csr_id = intval($csr_id);
    //End Init

    $arrOneCsr = $clsCsr->getOne($csr_id);
    if (!is_array($arrOneCsr) || $arrOneCsr["csr_id"] != $csr_id) {
        $act = "notfound";
        return;
    }
    // Tags
    $tags = $arrOneCsr['tags'];
    $htmlTags = "";
    $arrListTags = array_filter(explode(",", $tags));
    if (is_array($arrListTags)) {
        foreach ($arrListTags as $k => $v) {
            $url_tags = url_tags($v);
            $htmlTags .= "<li class='nav-item'><a class='nav-link' href='$url_tags'>$v</a></li>";
        }
    }

    //Tăng lượt xem
    $clsCsr->updateOne($csr_id, "view_num=view_num+1");
    $cat_id = $arrOneCsr["cat_id"];
    if ($cat_id > 0) {
        $curCat = $clsCategory->getOne($cat_id);
        if ($curCat['parent_id'] > 0) {
            $parCat = $clsCategory->getOne($curCat['parent_id']);
        }
    }
    $arrListOtherCsr = $clsCsr->getAllSimple("c.cat_id=$cat_id AND a.csr_id<>$csr_id ORDER BY a.reg_date DESC LIMIT 0,6");
    $arrAuthor = ($arrOneCsr['user_id'] > 0) ? $clsUsers->getOne($arrOneCsr['user_id']) : array();

    $arrListCsrByCat = array();
    if ($curCat['parent_id'] > 0) {
        $arrListCsrByCat = $clsCategory->getSubCatNews($curCat['parent_id'], 0);
    }
    if ($curCat['image'] == "") {
        $curCat['image'] = $parCat['image'];
    }


    //Begin Right Sidebar



    //End Right Sidebar
    //Begin Assign
    $assign_list["curCat"] = $curCat;
    $assign_list["parCat"] = $parCat;
    $assign_list["arrOneCsr"] = $arrOneCsr;
    $assign_list["arrAuthor"] = $arrAuthor;
    $assign_list["htmlTags"] = $htmlTags;
    $assign_list["arrListOtherCsr"] = $arrListOtherCsr;
    $assign_list["arrListCsrByCat"] = $arrListCsrByCat;
    //End Assign
    //Begin SEOmoz
    $page_title = ($arrOneCsr['page_title'] != "") ? $arrOneCsr['page_title'] : $arrOneCsr['title'];
    $page_title .= " - " . $_CONFIG['site_title'];
    $tags = $arrOneCsr['meta_keywords'];
    $page_keywords = ($tags != "") ? $tags : $_CONFIG['meta_keywords'];
    $des = $arrOneCsr['meta_des'];
    $page_description = ($des != "") ? $des : $_CONFIG['site_description'];
    $_CONFIG['thumb'] = $arrOneCsr['image'];
    $_CONFIG['page_title'] = $page_title;
    $_CONFIG['page_keywords'] = $page_keywords;
    $_CONFIG['page_description'] = $page_description;
    unset($tags, $des);
    //End SEOmoz
}

?>