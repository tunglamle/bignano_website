<?php
/**
 * Module: [account]
 * Home function with $sub=default, $act=login
 * Display Home Page
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
global $mod;
function default_login()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act, $clsRewrite;
    global $core, $_LANG_ID;
    if ($core->_SESS->isLoggedin()) {
        redirectURL($clsRewrite->url_account());
    }
    $return = isset($_GET["return"]) ? base64_decode($_GET["return"]) : $clsRewrite->url_home();
    $_POST["user_name"] = trim(strtolower($_POST["user_name"]));
    $clsUser = new Users();
    $btnLogin = POST("btnLogin", "");
    if ($btnLogin != "") {
        $valid = $clsUser->validateLogin($arr_error);
        if ($valid) {
            $ok = $core->_SESS->doLogin(trim($_POST["user_name"]), trim($_POST["user_pass"]), $arr_error['user_id']);
            header("location:" . $return);
            exit();
        }
    }
    foreach ($_POST as $key => $val) {
        $assign_list[$key] = $val;
    }
    $assign_list["valid"] = $valid;
    $assign_list["arr_error"] = $arr_error;
    //Begin SeoMoz
    $page_title = $core->getLang("Login");
    $_CONFIG["page_title"] = $page_title . " - " . $_CONFIG["site_title"];
    $_CONFIG["page_description"] = $_CONFIG["site_description"];
    //End SeoMoz
}