<?php
/**
 * Module: [account]
 * Home function with $sub=default, $act=activeacc
 * Display activation page
 *
 */
function default_history()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $clsRewrite;
    global $core, $_LANG_ID;
    require_once DIR_COMMON . "/clsPaging.php";
    if (!$core->_SESS->isLoggedin()) {
        redirectURL($clsRewrite->url_home());
    }
    $clsCourses = new OrderItem();
    $user_id = $core->_USER['user_id'];
    $arrListCoursesMonthly = $clsCourses->getAllSimple("o.user_id = $user_id AND a.expired_time != 0 AND a.is_active = 1");
    $arrListCoursesForever = $clsCourses->getAllSimple("o.user_id = $user_id AND a.expired_time = 0 AND a.is_active = 1");

    $assign_list['arrListCoursesMonthly'] = $arrListCoursesMonthly;
    $assign_list['arrListCoursesForever'] = $arrListCoursesForever;

    //Begin SeoMoz
    $page_title = $core->getLang("Khóa học đã mua");
    $_CONFIG["page_title"] = $page_title . " - " . $_CONFIG["site_title"];
    $_CONFIG["page_description"] = $_CONFIG["site_description"];
    //End SeoMoz
}