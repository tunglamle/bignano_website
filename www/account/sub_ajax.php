<?
function ajax_default()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID;
    exit();
}

function ajax_login()
{
    global $core;
    $clsUsers = new Users();
    $btnLogin = isset($_POST["btnLogin"]) ? $_POST["btnLogin"] : "";
    $user_name = isset($_POST["user_name"]) ? $_POST["user_name"] : "";
    $user_pass = isset($_POST["user_pass"]) ? $_POST["user_pass"] : "";

    if ($btnLogin != "") {
        $valid = $clsUsers->checkValidUserPass($user_name, $user_pass);
        if ($valid == 1) {
            $core->_SESS->doLogin($user_name, $user_pass);
        } else {
            $arr_error = array('status' => 'error', 'message' => 'Tài khoản hoặc mật khẩu không chính xác!');
        }
    }
    echo json_encode($arr_error);
    exit();
}

function ajax_register()
{
    global $clsRewrite, $core;
    $arr_error = array('status' => 'error', 'message' => 'Bạn không thể thực hiện thao tác này!');
    if (!$core->_SESS->isLoggedin()) {
        $clsUsers = new Users();
        $btnRegister = POST("btnRegister", "");
        $msg_error = array();
        if ($btnRegister != "") {
            $valid = $clsUsers->validateRegister($msg_error);
            if ($valid) {
                $ok = $clsUsers->doRegister();
                if ($ok) {
                    $location = $clsRewrite->url_home();
                    $arr_error = array('status' => 'success', 'message' => 'Đăng ký tài khoản thành công!', 'location' => $location);
                } else {
                    $arr_error = array('status' => 'error', 'message' => 'Có lỗi sảy ra, vui lòng thử lại sau!');
                }
            } else {
                if (is_array($msg_error) && count($msg_error) > 0) {
                    foreach ($msg_error as $e => $error) {
                        $message = $error;
                    }
                    $arr_error = array('status' => 'error', 'message' => $message);
                }
            }
        }
    }
    echo json_encode($arr_error);
    exit();
}

function ajax_forgot()
{
    global $core;
    $arr_error = array('status' => 'error', 'message' => 'Bạn không thể thực hiện thao tác này!');
    if (!$core->_SESS->isLoggedin()) {
        $clsUsers = new Users();
        $btnForgot = POST("btnForgot", "");
        $msg_error = array();
        if ($btnForgot != "") {
            $valid = $clsUsers->validateForgot($msg_error);
            if ($valid) {
                $ok = $clsUsers->doForgot();
                if ($ok) {
                    $arr_error = array('status' => 'success', 'message' => 'Chúng tôi đã gửi liên kết thay đổi mật khẩu vào email của bạn!');
                } else {
                    $arr_error = array('status' => 'error', 'message' => 'Vui lòng nhập đúng thông tin đã đăng ký để có thể khôi phục mật khẩu!');
                }
            } else {
                if (is_array($msg_error) && count($msg_error) > 0) {
                    foreach ($msg_error as $e => $error) {
                        $message = $error;
                    }
                    $arr_error = array('status' => 'error', 'message' => $message);
                }
            }
        }
    }
    echo json_encode($arr_error);
    exit();
}

?>