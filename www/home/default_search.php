<?php
/**
 * Module: [Articles]
 * Posts function with $sub=default, $act=search
 * Display search of a articles
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_search()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID;
    $clsProduct = new Product();
    $clsCategory = new Category;
    $clsCategory->getParentArray();
    require_once DIR_COMMON . "/clsPaging.php";

    $cat = $_GET['cat'];
    $key = $_GET['key'];
    if ($key != '') {
        $slug = utf8_nosign_noblank($key);
        $cond = "is_online = 1 AND lang_code = '$_LANG_ID' AND  ( name LIKE '%$key%' OR slug LIKE '%$slug%') ";
    }
    if($cat != 0){
        $cond.= " AND cat_id = $cat";
    }
    $orderby = "  ORDER BY reg_date DESC";
    //Begin Paging
    $rowPerPage = 100;
    $curPage = (isset($_GET["page"]) && $_GET["page"] > 0) ? ($_GET["page"]) : 0;

    $clsPaging = new Paging($curPage, $rowPerPage, "product");
    $clsPaging->setBaseURL(VNCMS_URL . "/search?cat=$cat&key=$key");
    $totalItem = $clsProduct->countItem($cond);
    $arrListProduct = $clsProduct->getAllSimple2($clsPaging->getQueryLimit($cond.$orderby));

    $clsPaging->setTotalRows($totalItem);
    $clsPaging->setShowStatstic(false);
    $clsPaging->setShowGotoBox(false);

    $strpro_viewed = getCookie("pro_viewing");
    $arrListProductViewed = $clsProduct->getAllSimple2("is_online = 1 AND lang_code = '$_LANG_ID' AND product_id IN ($strpro_viewed) ORDER BY reg_date DESC LIMIT 0,4");
    //End Paging

    $assign_list["totalItem"] = $totalItem;
    $assign_list["arrListProduct"] = $arrListProduct;
    $assign_list["clsPaging"] = $clsPaging;
    $assign_list["key"] = $key;
    $assign_list["searchCat"] = $cat;
    $assign_list["clsCategory"] = $clsCategory;
    $assign_list["arrListProductViewed"] = $arrListProductViewed;

}

?>