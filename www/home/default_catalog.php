<?
/******************************************************
 * Child Module of module [home]
 *
 * Contain functions of child module: [default], each function has prefix is 'default_'
 *
 * Project Name               :  ClientWebsite
 * Package Name                    :
 * Program ID                 :  index.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  20/01/2018
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        20/01/2018        banglcb          -        -     -     -
 *
 ********************************************************/

/**
 * Module: [home]
 * Home function with $sub=default, $act=default
 * Display Home Page
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
global $mod;
function default_catalog()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID;

    $clsPage = new Pages();
    $clsAdver = new Adver();

    $catalog_id = $_GET['catalog_id'];
    $btnDownload = $_POST['btnDownload'];
    $_CONFIG['site_title'] = 'Catalog';

    $arrOneAdver = $clsAdver->getOne($catalog_id);
    $arrOneAdver['list_image'] = explode(',',$arrOneAdver['list_image']);

    $zip = new ZipArchive;
    $zipname = 'catalog';
    if ($zip->open($zipname, ZipArchive::CREATE| ZipArchive::OVERWRITE) === TRUE)
    {
        $files = $arrOneAdver['list_image'];
        foreach ($files as $file) {
            $zip->addFile(DIR_UPLOADS ."/". $file,substr($file,3));
        }
        $zip->deleteName(DIR_UPLOADS);
        $zip->close();
    }
    if( $btnDownload) {
        header("Content-type: application/zip");
        header("Content-Disposition: attachment; filename=$zipname");
        header("Content-length: " . filesize($zipname));
        header("Pragma: no-cache");
        header("Expires: 0");
        readfile("$zipname");
    }


    $assign_list['arrOneAdver'] = $arrOneAdver;
    $page_title = $core->getLang('Cataloge');
    $_CONFIG['page_title'] = $page_title;
    unset($tags, $des);
    //End SEOmoz
}

?>