<?
/******************************************************
 * Child Module of module [home]
 *
 * Contain functions of child module: [default], each function has prefix is 'default_'
 *
 * Project Name               :  ClientWebsite
 * Package Name                    :
 * Program ID                 :  index.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  20/01/2018
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        20/01/2018        banglcb          -        -     -     -
 *
 ********************************************************/

/**
 * Module: [home]
 * Home function with $sub=default, $act=video
 * Display Home video
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
global $mod;
function default_video()
{
    $resouce_id = $_SESSION['resid'];
    unset($_SESSION['resid']);
    if (isset($resouce_id) && !empty($resouce_id)) {
        $youtube = new Youtube();
        // Set the video ID
        $obj = $youtube->setVideoID(base64_decode(substr($resouce_id, 50)));
        if ($obj->hasVideo()) {
            header("Content-type: video/mp4");
            echo file_get_contents($obj->getOneStream("mp4")['url']);
        } else {
            echo "The document is not found!";
        }
    } else {
        header("HTTP/1.0 500 Internal Server Error");
    }
    exit;
}

?>