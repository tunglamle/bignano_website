<?
/******************************************************
 * Child Module of module [home]
 *
 * Contain functions of child module: [default], each function has prefix is 'default_'
 *
 * Project Name               :  ClientWebsite
 * Package Name                    :
 * Program ID                 :  index.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  20/01/2018
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        20/01/2018        banglcb          -        -     -     -
 *
 ********************************************************/

/**
 * Module: [home]
 * Home function with $sub=default, $act=default
 * Display Home Page
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
global $mod;
function default_agency()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID;

    $clsAgency = new Agency();
    $clsPage = new Pages();
    $_CONFIG['site_title'] = 'Danh sách đại lý';

    $arrOnePage = $clsPage->getBySlug('danh-sach-dai-ly');

    $province_id = getPOST('province_id',0);
    $district_id = getPOST('district_id',0);

    $cond = "is_online = 1 AND lang_code = '$_LANG_ID'";
    $orderby = "  ORDER BY order_no ASC";

    if ($province_id > 0) {
        $cond .=" AND province_id = '$province_id'";
        if ($district_id > 0) {
            $cond .=" AND district_id = '$district_id'";
        }
    }


    $arrListAgency = $clsAgency->getAll($cond.$orderby);

    $htmlOptionProvince = makeListProvince($province_id, "");
    $htmlOptionDistrict = makeListDistrict($district_id, "province_id=$province_id");

    $assign_list['htmlOptionProvince'] = $htmlOptionProvince;
    $assign_list['htmlOptionDistrict'] = $htmlOptionDistrict;
    $assign_list['arrListAgency'] = $arrListAgency;

    $assign_list['arrListAgency'] = $arrListAgency;
    $assign_list['arrOnePage'] = $arrOnePage;
    $page_title = $core->getLang('Agency');
    $_CONFIG['page_title'] = $page_title;
    unset($tags, $des);
    //End SEOmoz
}

?>