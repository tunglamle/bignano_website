<?
/******************************************************
 * Child Module of module [home]
 *
 * Contain functions of child module: [default], each function has prefix is 'default_'
 *
 * Project Name               :  ClientWebsite
 * Package Name                    :
 * Program ID                 :  index.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  20/01/2018
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        20/01/2018        banglcb          -        -     -     -
 *
 ********************************************************/

/**
 * Module: [home]
 * Home function with $sub=default, $act=default
 * Display Home Page
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
global $mod;
function default_partner()
{
    global $assign_list, $_CONFIG, $clsRewrite, $_SITE_ROOT, $mod, $act;
    global $core, $_LANG_ID;
    require_once DIR_COMMON . "/clsPaging.php";
    $clsAdver = new Adver();
    $clsPage = new Pages();

    $arrOnePage = $clsPage->getBySlug('doi-tac');
    $_CONFIG['site_title'] = 'Đối tác';

    $cond = "lang_code='$_LANG_ID' AND is_online=1 AND position = 'DT'";
    $orderby = " ORDER BY order_no ASC";
    //Begin Paging
    $rowPerPage = 18;
    $curPage = (isset($_GET["page"]) && $_GET["page"] > 0) ? ($_GET["page"]) : 0;
    $clsPaging = new Paging($curPage, $rowPerPage, "partner");
    $clsPaging->setBaseURL(VNCMS_URL.'/doi-tac.html');
    $totalItem = $clsAdver->countItem($cond);
    $clsPaging->setTotalRows($totalItem);
    $clsPaging->setShowStatstic(false);
    $clsPaging->setShowGotoBox(false);
    $assign_list["clsPaging"] = $clsPaging;
    //End Paging
    //Begin ListArticles
    $arrListPartner = $clsAdver->getAll($clsPaging->getQueryLimit($cond . $orderby));


    $assign_list['arrOnePage'] = $arrOnePage;
    $assign_list['arrListPartner'] = $arrListPartner;
    //End Comment

    $page_title = 'Đối tác';
    $_CONFIG['page_title'] = $page_title;
    unset($tags, $des);
    //End SEOmoz
}

?>