<?
/******************************************************
 * Child Module of module [home]
 *
 * Contain functions of child module: [default], each function has prefix is 'default_'
 *
 * Project Name               :  ClientWebsite
 * Package Name                    :
 * Program ID                 :  index.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  20/01/2018
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        20/01/2018        banglcb          -        -     -     -
 *
 ********************************************************/

/**
 * Module: [home]
 * Home function with $sub=default, $act=default
 * Display Home Page
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
global $mod;
function default_default()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $_LANG_ID;
    $clsArticles = new Articles();
    $clsPages = new Pages();
    $clsCategory = new Category();
    $clsProducts = new Product();
    $clsAdver = new Adver();
    $clsTypical = new Typical();

    $arrCatNews = $clsCategory->getByCond("ctype = 0");
    $arrCatProductHome = $clsCategory->getAll("ctype = 1 AND is_online = 1 AND at_home = 1 AND lang_code = '$_LANG_ID' ORDER BY order_no ASC");
    $arrLvHome = $clsAdver->getByPosition("LV");
    $arrCatProductHomeLeft = $clsCategory->getAll("ctype = 1 AND cat_visit = 0 AND is_online = 1 AND at_home = 1 AND lang_code = '$_LANG_ID' ORDER BY order_no ASC");
    $arrCatProductHomeRight = $clsCategory->getAll("ctype = 1 AND cat_visit = 1 AND is_online = 1 AND at_home = 1 AND lang_code = '$_LANG_ID' ORDER BY order_no ASC");
    $arrCatAboutHome = $clsCategory->getAll("ctype = 2 AND  is_online = 1 AND at_home = 1 AND lang_code = '$_LANG_ID' ORDER BY order_no ASC");
    $arrListNews = $clsArticles->getAllSimple2("is_online = 1 AND at_home = 1 AND lang_code = '$_LANG_ID' ORDER BY reg_date DESC");
    $arrListNewsIr = $clsArticles->getAllSimple2("is_online = 1 AND is_ir = 1 AND lang_code = '$_LANG_ID' ORDER BY reg_date DESC");
    $arrListNewsNotifications = $clsArticles->getAllSimple2("is_online = 1 AND is_notifications = 1 AND lang_code = '$_LANG_ID' ORDER BY reg_date DESC");
    $arrListRightSlide = $clsAdver->getByPosition("RSL");
    $arrRightSlide2 = $clsAdver->getByPosition("RSL2");
    $arrListProductHome = $clsProducts->getAllSimple2("is_online = 1 AND at_home = 1 AND lang_code = '$_LANG_ID' ORDER BY reg_date DESC");
    $arrCatTypicalApplication = $clsCategory->getAll("ctype = 6 AND is_online = 1 AND parent_id != 0 AND lang_code = '$_LANG_ID' ORDER BY order_no ASC");
    $arrTypical = $clsCategory->getByCond("ctype = 6 AND parent_id = 0 AND lang_code = '$_LANG_ID'");
    $arrProduct = $clsCategory->getByCond("ctype = 1 AND parent_id = 0 AND lang_code = '$_LANG_ID'");
    $arrCatAboutHome = $clsCategory->getAll("ctype = 2 AND at_home = 1 AND lang_code = '$_LANG_ID' ORDER BY order_no ASC");
    $arrCatSlideMenu = $clsCategory->getAll("ctype = 1 AND is_menu_slide = 1 AND lang_code = '$_LANG_ID' ORDER BY order_no ASC");
    
    $arrListApplocationHome = $clsCategory->getAll("ctype = 0 AND is_appcation = 1 AND lang_code = '$_LANG_ID' ORDER BY order_no ASC");
        
    
    $assign_list['clsCategory'] = $clsCategory;
    $assign_list['core'] = $core;
    $assign_list['arrCatNews'] = $arrCatNews;
    $assign_list['arrCatProductHome'] = $arrCatProductHome;
    $assign_list['arrLvHome'] = $arrLvHome;
    $assign_list['arrCatProductHomeLeft'] = $arrCatProductHomeLeft;
    $assign_list['arrCatProductHomeRight'] = $arrCatProductHomeRight;
    $assign_list['arrCatAboutHome'] = $arrCatAboutHome;
    $assign_list['arrListNews'] = $arrListNews;
    $assign_list['arrListNewsIr'] = $arrListNewsIr;
    $assign_list['arrListNewsNotifications'] = $arrListNewsNotifications;
    $assign_list['arrListRightSlide'] = $arrListRightSlide;
    $assign_list['arrRightSlide2'] = $arrRightSlide2;
    $assign_list['arrListProductHome'] = $arrListProductHome;
    $assign_list['arrCatTypicalApplication'] = $arrCatTypicalApplication;
    $assign_list['arrTypical'] = $arrTypical;
    $assign_list['arrProduct'] = $arrProduct;
    $assign_list['arrCatAboutHome'] = $arrCatAboutHome;
    $assign_list['arrCatSlideMenu'] = $arrCatSlideMenu;
    $assign_list['arrListApplocationHome'] = $arrListApplocationHome;
    //Begin SeoMoz
    $_CONFIG["page_title"] = $_CONFIG["site_title"];
    $_CONFIG["page_description"] = $_CONFIG["site_description"];
    //End SeoMoz
}


?>