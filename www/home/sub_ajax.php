<?
function ajax_default()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID;
    exit();
}

function ajax_email()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $_LANG_ID;
    require_once VNCMS_DIR . "/includes/mail/sendmail.php";

    $_email = isset($_POST['email']) ? $_POST['email'] : '';
    $_name = isset($_POST['name']) ? $_POST['name'] : '';
    $_phone = isset($_POST['phone']) ? $_POST['phone'] : '';
    $_product = isset($_POST['product']) ? $_POST['product'] : '';
    $reg_date = time();
    $mailto = $_CONFIG['webmaster_email'];
    $title_sendmail = htmlDecode($_CONFIG['content_email'])  ;
    $clsMail = new Email;
    if ($_email != '' && isEmail($_email) && $clsMail->insertOne('email,name,phone,product,reg_date', "'$_email','$_name',$_phone,'$_product',$reg_date")) {
        echo 1;
         $contentmail1 = "
         $title_sendmail
        
    ";
         $contentmail = "
                <ul style='padding-left: 0;'>
                    <li>Name: $_name</li>
                    <li>Email: $_email</li>
                    <li>Phone: $_phone</li>
                    <li>Tên Sản Phẩm: $_product</li>
                </ul>
            ";
            $mail = mail2($mailto, 'Thông đăng ký quan tâm sản phẩm', "$contentmail");
            $mail1 = mail2($_email, 'Thông đăng ký quan tâm sản phẩm', "$contentmail1");

    } else {
        echo 0;
    }
    exit();
}


function ajax_getDistrict()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID;
    $province_id = GET("province_id", 0);
    $type = GET("type", 0);
    $html = ($province_id > 0) ? makeListDistrict(0, "province_id=$province_id") : "";
    if ($type == 0) {
        $text0 = "Quận/Huyện";
    } else
        if ($type == 1) {
            $text0 = "Không thay đổi gì";
        } else {
            $text0 = "Tất cả";
        }
    $first = "<option value='0'>$text0</option>";
    $html = $first . $html;
    echo $html;
    exit();
}

function ajax_res()
{
    global $clsRewrite;
    $_SESSION['resid'] = $_POST['resid'];
    echo $clsRewrite->url_video();
    exit;
}

/*get search company*/
/**
 * Lấy khóa học
 */
function ajax_getcourses()
{
    global $dbconn, $lang_code;
    $keyword = utf8_nosign(POST("keyword", ""));
    $html = "";
    if ($keyword != "") {
        $slug = utf8_nosign_noblank($keyword);
        $sql = "SELECT course_id,cat_id,name,image FROM _courses WHERE is_online=1 AND lang_code='$lang_code' AND (name LIKE '%$keyword%' or slug LIKE '%$slug%') ORDER BY name LIMIT 0,20";
        $arr = $dbconn->GetAll($sql);
        if ($arr) {
            $html = "<ul>";
            foreach ($arr as $key => $val) {
                $html .= "<li class='course_id' tag-title='{$val['name']}' tag-value='{$val['course_id']}' onclick='return selectTag(this);'><img src='" . URL_UPLOADS . "/{$val['image']}'>{$val['name']}</li>";
            }
            $html .= "</ul>";
        }
    }
    echo $html;
    $dbconn->Close();
    exit();
}

function ajax_submitContact()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $_LANG_ID;
    require_once VNCMS_DIR . "/includes/mail/sendmail.php";
    $clsFeedbacks = new FeedBacks();

    $name = isset($_POST["name"]) ? $_POST["name"] : '';
//    $company = isset($_POST["company"]) ? $_POST["company"] : '';
    $phone = isset($_POST["phone"]) ? $_POST["phone"] : '';
    $email = isset($_POST["email"]) ? $_POST["email"] : '';
    $title = isset($_POST["title"]) ? $_POST["title"] : '';
    $content = isset($_POST["content"]) ? $_POST["content"] : '';
    $btnSend = isset($_POST["btnSend"]) ? $_POST["btnSend"] : "";
    $mailto = $_CONFIG['webmaster_email'];
    $title_sendmail = htmlDecode($_CONFIG['content_email'])  ;
    $arr_error = array('status' => 'error', 'message' => $core->getLang('Bạn chưa nhập thông tin !'));
    if ($name != "" && $content != "" && $phone != '' && $email != '') {
        $reg_date = time();
        $fields = "name, phone, email, title, content, lang_code, reg_date";
        $values = "'$name', '$phone', '$email', '$title', '$content','$_LANG_ID', $reg_date";
        if ($clsFeedbacks->insertOne($fields, $values)) {
            $arr_error = array('status' => 'success', 'message' => 'Bạn đã gửi thông tin thành công');

            $contentmail = "
                <ul style='padding-left: 0;'>
                    <li>Name: $name</li>
                    <li>Số điện thoại: $phone</li>
                    <li>Email: $email</li>
                    <li>Tiêu đề: $title</li>
                    <li>Nội dung liên hệ: $content</li>
                </ul>
            ";
             $contentmail1 = "
             $title_sendmail";

            $mail = mail2($mailto, 'Thông tin liên hệ', "$contentmail");
            $mail1 = mail2($email, 'Thông tin liên hệ', "$contentmail1");
        } else {
            $arr_error = array('status' => 'error', 'message' => 'Có lỗi xảy ra. Vui lòng thủ lại sau !');
        }
    } else {
        $arr_error = array('status' => 'error', 'message' => 'Có lỗi xảy ra. Vui lòng kiểm tra lại quá trình điền thông tin!');
    }
    echo json_encode($arr_error);
    exit();

}

function ajax_submitFormEmail()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $_LANG_ID;
    require_once VNCMS_DIR . "/includes/mail/sendmail.php";
    $clsEmail = new Email();

    $email = isset($_POST["email"]) ? $_POST["email"] : '';
    $mailto = $_CONFIG['webmaster_email'];

    $arr_error = array('status' => 'error', 'message' => $core->getLang('Bạn chưa nhập thông tin !'));

    if ($email != "") {
        $reg_date = time();
        $fields = "email, reg_date";
        $values = "'$email', $reg_date";
        if ($clsEmail->insertOne($fields, $values)) {
            $arr_error = array('status' => 'success', 'message' => 'Bạn đã gửi thông tin thành công');
//            $contentmail = "
//                <ul style='padding-left: 0;'>
//                    <li>Email: $email</li>
//                </ul>
//            ";
//                $mail = mail2($mailto, 'Thông tin liên hệ', "$contentmail");
        } else {
            $arr_error = array('status' => 'error', 'message' => 'Có lỗi xảy ra. Vui lòng thủ lại sau !');
        }
    } else {
        $arr_error = array('status' => 'error', 'message' => 'Có lỗi xảy ra. Vui lòng kiểm tra lại quá trình điền thông tin!');
    }
    echo json_encode($arr_error);
    exit();

}

function ajax_submitFormRegisAgency0()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $_LANG_ID;
    require_once VNCMS_DIR . "/includes/mail/sendmail.php";
    $clsRegis_agency = new Regis_agency();

    $name_agency = isset($_POST["name_agency"]) ? $_POST["name_agency"] : '';
    $surname = isset($_POST["surname"]) ? $_POST["surname"] : '';
    $name = isset($_POST["name"]) ? $_POST["name"] : '';
    $address = isset($_POST["address"]) ? $_POST["address"] : '';
    $phone = isset($_POST["phone"]) ? $_POST["phone"] : '';
    $email = isset($_POST["email"]) ? $_POST["email"] : '';
    $content = isset($_POST["content"]) ? $_POST["content"] : '';
    $mailto = $_CONFIG['webmaster_email'];

    $arr_error = array('status' => 'error', 'message' => $core->getLang('Bạn chưa nhập thông tin !'));

    if ($name != "" && $address != "" && $surname != "" && $content != "" && $phone != '' && $email != '' && $name_agency != '') {
        $reg_date = time();
        $fields = "name_agency, surname, name, address, phone, email, content, lang_code, reg_date";
        $values = "'$name_agency', '$surname', '$name', '$address', '$phone', '$email','$content','$_LANG_ID', $reg_date";
        if ($clsRegis_agency->insertOne($fields, $values)) {
            $arr_error = array('status' => 'success', 'message' => 'Bạn đã gửi thông tin thành công');
            $contentmail = "
                <ul style='padding-left: 0;'>
                    <li>Tên công ty: $name_agency</li>
                    <li>Họ: $surname</li>
                    <li>Tên: $name</li>
                    <li>Địa chỉ: $address</li>
                    <li>Số điện thoại: $phone</li>
                    <li>Email: $email</li>
                    <li>Nội dung liên hệ: $content</li>
                </ul>
            ";
            $mail = mail2($mailto, 'Đăng ký làm đại lý', "$contentmail");
        } else {
            $arr_error = array('status' => 'error', 'message' => 'Có lỗi xảy ra. Vui lòng thủ lại sau !');
        }
    } else {
        $arr_error = array('status' => 'error', 'message' => 'Có lỗi xảy ra. Vui lòng kiểm tra lại quá trình điền thông tin!');
    }
    echo json_encode($arr_error);
    exit();

}

function ajax_showNews()
{
    global $assign_list;
    global $core, $smarty, $arrNews;
    require_once DIR_COMMON . "/clsPaging.php";
    $clsArticles = new Articles();
    $clsRewrite = new Rewrite();
    $clsCategory = new Category();
    $year = isset($_POST["year"]) ? $_POST["year"] : "";
    $cat_id = isset($_POST["cat_id"]) ? $_POST["cat_id"] : "";

    $new_id = $arrNews['cat_id'];
    if ($cat_id != $new_id) {
        $cond = "cat_id = $cat_id AND year = $year AND is_online = 1";
    } else {
        $cond = "year = $year AND is_online = 1";
    }
    $orderby = " ORDER BY reg_date DESC";


    $arrListArticles = $clsArticles->getAllSimple2($cond . $orderby);

    $assign_list["clsArticles"] = $clsArticles;
    $assign_list["core"] = $core;
    $assign_list["URL_UPLOADS"] = URL_UPLOADS;
    $assign_list["VNCMS_URL"] = VNCMS_URL;
    $assign_list["URL_IMAGES"] = URL_IMAGES;
    $assign_list["Rewrite"] = $clsRewrite;
    $assign_list["arrListArticles"] = $arrListArticles;
    $smarty->assign($assign_list);
    echo $smarty->fetch("articles/act_show_news.tpl");
    exit();
}

function ajax_showNewsType()
{
    global $assign_list;
    global $core, $smarty, $arrNews, $lang_code;
    require_once DIR_COMMON . "/clsPaging.php";
    $clsArticles = new Articles();
    $clsRewrite = new Rewrite();
    $clsCategory = new Category();
    $type = isset($_POST["type"]) ? $_POST["type"] : "All";

    $cond = " lang_code = '$lang_code' AND is_online = 1";
    if ($type == 0) {
        $cond .= " AND news_type = 0";
    } elseif ($type == 1) {
        $cond .= " AND news_type = 1";
    }
    $orderby = " ORDER BY reg_date DESC";

    $arrListArticles = $clsArticles->getAllSimple2($cond . $orderby);

    $assign_list["clsArticles"] = $clsArticles;
    $assign_list["core"] = $core;
    $assign_list["URL_UPLOADS"] = URL_UPLOADS;
    $assign_list["VNCMS_URL"] = VNCMS_URL;
    $assign_list["URL_IMAGES"] = URL_IMAGES;
    $assign_list["Rewrite"] = $clsRewrite;
    $assign_list["arrListArticles"] = $arrListArticles;
    $smarty->assign($assign_list);
    echo $smarty->fetch("articles/act_show_news_type.tpl");
    exit();
}


?>