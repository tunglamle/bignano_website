<?
/******************************************************
 * Child Module of module [home]
 *
 * Contain functions of child module: [default], each function has prefix is 'default_'
 *
 * Project Name               :  ClientWebsite
 * Package Name                    :
 * Program ID                 :  index.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  20/01/2018
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        20/01/2018        banglcb          -        -     -     -
 *
 ********************************************************/

/**
 * Module: [home]
 * Home function with $sub=default, $act=page
 * Display Home Page
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
global $mod;
function default_page()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $_LANG_ID;
    $slug = GET("slug", "");
    $clsPages = new Pages();

    $arrOnePage = $clsPages->getBySlug($slug);
    $arrListPageChildren = $clsPages->getListSubPage($arrOnePage['page_id']);
    //Assign to smarty
    $assign_list["arrOnePage"] = $arrOnePage;
    $assign_list["arrListPageChildren"] = $arrListPageChildren;
    //Begin SEOmoz
    $parent_id = ($arrOnePage['parent_id'] == 0) ? $arrOnePage['page_id'] : $arrOnePage['parent_id'];
    $arrListSubPage = $clsPages->getListSubPage($parent_id);
    $assign_list["arrListSubPage"] = $arrListSubPage;
    $assign_list["hasSubPage"] = (is_array($arrListSubPage) && count($arrListSubPage) > 0);
    $page_title = ($arrOnePage['page_title'] != "") ? $arrOnePage['page_title'] . " - " . $_CONFIG['site_title'] : $arrOnePage['name'] . " - " . $_CONFIG['site_title'];
    $tags = $arrOnePage['meta_keywords'];
    $page_keywords = ($tags != "") ? $tags : $_CONFIG['meta_keywords'];
    $des = $arrOnePage['meta_des'];
    $page_description = ($des != "") ? $des : $_CONFIG['site_description'];
    $_CONFIG['thumb'] = $arrOnePage['image'];
    $_CONFIG['page_title'] = $page_title;
    $_CONFIG['page_keywords'] = $page_keywords;
    $_CONFIG['page_description'] = $page_description;
    unset($tags, $des);
    //End SEOmoz
}

?>