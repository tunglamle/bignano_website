<?
/******************************************************
 * Child Module of module [Typical]
 *
 * Contain functions of child module: [default], each function has prefix is 'default_'
 *
 * Project Name               :  ClientWebsite
 * Package Name                    :
 * Program ID                 :  index.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  20/01/2018
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        20/01/2018        banglcb          -        -     -     -
 *
 ********************************************************/
/**
 * Module: [Typical]
 * Category function with $sub=default, $act=default
 * Display Category Page, display list of posts
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_default()
{
    global $assign_list, $_CONFIG, $clsRewrite, $_SITE_ROOT, $mod, $act;
    global $core, $_LANG_ID;
    require_once DIR_COMMON . "/clsPaging.php";
    //Begin GetVars
    $cat_id = isset($_GET["cat_id"]) ? $_GET["cat_id"] : 0;
    $cat_id = intval($cat_id);
    $slug = isset($_GET["slug"]) ? $_GET["slug"] : "";
    $id_typical = isset($_GET["id_typical"]) ? $_GET["id_typical"] : "";

    $n_key = GET("n_key",'');
    $sort = GET("sort",'');
    //End GetVars
    //Begin Init
    if (isset($_GET["cat_id"]) && "$cat_id" != $_GET["cat_id"] && $_GET["cat_id"] != "") {
        showErrorFatalBox("notfound");
        exit();
    }
    $clsTypical = new Typical();
    $clsCategory = new Category();
    $clsYear = new Year();
    $clsCategory->getParentArray();
    $curCat = array();
    if ($slug != "") {
        $arrTmp = $clsCategory->getBySlug($slug);
        if (is_array($arrTmp) && $arrTmp['cat_id'] != 0) {
            $cat_id = $arrTmp['cat_id'];
        }
    }
    if ($cat_id > 0) {
        $curCat = $clsCategory->getOne($cat_id);
        if ($curCat['parent_id'] > 0) {
            $parCat = $clsCategory->getOne($curCat['parent_id']);
        }
        $h1_title = $curCat['name'];
    } else {
        $h1_title = "Tin tức";
        $parCat = 0;
    }
    //End Init
    //Begin SQl Condition
    $cond = "a.year = 2019 AND a.lang_code='$_LANG_ID' AND a.is_online=1";

    if ($cat_id != "" && $cat_id != "0") {
        $cat_id_str = $clsCategory->getAllCatStr($cat_id) . $cat_id;
        $cond .= (strpos($cat_id_str, ',') !== false) ? " AND a.cat_id in ($cat_id_str)" : " AND a.cat_id=$cat_id";
    }
    if ($sort == 'name'){
        $orderby = " ORDER BY a.title ASC";
    }else{
        $orderby = " ORDER BY a.reg_date DESC";
    }
    //End SQL Condition
    //Begin Paging
    $rowPerPage = 6;
    $curPage = (isset($_GET["page"]) && $_GET["page"] > 0) ? ($_GET["page"]) : 0;
    $clsPaging = new Paging($curPage, $rowPerPage, "news");
    $clsPaging->setBaseURL($clsRewrite->url_category($curCat) . "?sort=$sort");
    $cond_count = str_replace('a.', '', $cond);
    $totalItem = $clsTypical->countItem($cond_count);
    $clsPaging->setTotalRows($totalItem);
    $clsPaging->setShowStatstic(false);
    $clsPaging->setShowGotoBox(false);
    $assign_list["clsPaging"] = $clsPaging;
    //End Paging
    //Begin ListTypical
    $arrListTypical = $clsTypical->getAllSimple($cond . $orderby);
    if ($curCat['parent_id'] == 0) {
        $arrListTypicalByCat = $clsCategory->getSubCatNews($cat_id);
    } else {
        $arrListTypicalByCat = $clsCategory->getSubCatNews($curCat['parent_id'], 0);
        if ($curCat['template'] == "default") {
            $curCat['template'] = "list";
        }
        if ($curCat['image'] == "") {
            $curCat['image'] = $parCat['image'];
        }
    }
    //End ListTypical

    $parCat_id = $parCat['cat_id'];
    if ( $curCat['parent_id'] == 0 ){
        $arrListCatNewsChildren = $clsCategory->getAll("parent_id = $cat_id AND is_online = 1 ORDER BY order_no ASC");
    }else{
        $arrListCatNewsChildren = $clsCategory->getAll("parent_id = $parCat_id AND is_online = 1 ORDER BY order_no ASC");
    }


    $arrCatTypicalApplication = $clsCategory->getAll("ctype = 6 AND is_online = 1 AND parent_id != 0 AND lang_code = '$_LANG_ID' ORDER BY order_no ASC");
    if (is_array($arrCatTypicalApplication)){
        foreach ($arrCatTypicalApplication as $k => $v) {
            $cat_id1 = $v['cat_id'];
            $arrTypical = $clsTypical->getAllSimple2("is_online = 1 AND lang_code = '$_LANG_ID' AND cat_id = $cat_id1 ORDER BY reg_date DESC");
            $arrCatTypicalApplication[$k]['arrTypical'] = $arrTypical;
        }
    }
    $assign_list["arrListCatNewsChildren"] = $arrListCatNewsChildren;
    $assign_list["curCat"] = $curCat;
    $assign_list["parCat"] = $parCat;
    $assign_list["arrListTypical"] = $arrListTypical;
    $assign_list["arrListTypicalByCat"] = $arrListTypicalByCat;
    $assign_list["totalItem"] = $totalItem;
    $assign_list["h1_title"] = $h1_title;
    $assign_list["rowPerPage"] = $rowPerPage;
    $assign_list["core"] = $core;
    $assign_list["id_typical"] = $id_typical;
    $assign_list["sort"] = $sort;
    $assign_list["arrCatTypicalApplication"] = $arrCatTypicalApplication;
    //End Assign
    //Begin SEOmoz
    $page_title = ($curCat['page_title'] != "") ? $curCat['page_title'] : $curCat['name'];
    if ($page_title == "") $site_title = "Tin tức";
    $page_title .= " - " . $_CONFIG['site_title'];
    $tags = $curCat['meta_keywords'];
    $page_keywords = ($tags != "") ? $tags : $_CONFIG['meta_keywords'];
    $des = $curCat['meta_des'];
    $page_description = ($des != "") ? $des : $_CONFIG['site_description'];
    $_CONFIG['page_title'] = $page_title;
    $_CONFIG['page_keywords'] = $page_keywords;
    $_CONFIG['page_description'] = $page_description;
    //End SEOmoz
}

?>