<?php
/**
 * Module: [cart]
 * Home function with $sub=default, $act=prepay
 * Checkout
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_prepay()
{
    global $smarty, $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act, $clsRewrite;
    global $dbconn, $core, $_LANG_ID, $clsCart;
    $clsOrders = new Orders();
    $order_code = GET("order_code", "");
    $payment_method = GET("payment_method", "");
    $process = GET("process", 0);
    if ($order_code == "") {
        redirectURL($clsRewrite->url_home());
    }
    $arrOneOrder = $clsOrders->getOneOrder($order_code);
    if (!is_array($arrOneOrder) || $arrOneOrder['order_id'] == 0) {
        redirectURL($clsRewrite->url_home());
    }

    //Assign to smarty
    $assign_list["url_prepaypaypal"] = url_prepayonline($order_code, 'paypal');
    $assign_list["url_prepayvisa"] = url_prepayonline($order_code, 'visa');
    $assign_list["url_prepayatm"] = url_prepayonline($order_code, 'atm');
    $assign_list["arrOneOrder"] = $arrOneOrder;
    //Begin SEOMoz
    $_CONFIG['page_title'] = $core->getLang("Process_payment") . " - " . $_CONFIG['site_title'];
    //End SEOMoz
}