<?php

function default_showcart()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $smarty;
    $clsProduct = new Product();
    $return = (isset($_GET["return"])) ? base64_decode($_GET["return"]) : "";
    $quantity = isset($_POST["quantity"]) ? $_POST["quantity"] : "";
    $btnUpdate = isset($_POST["btnUpdate"]) ? $_POST["btnUpdate"] : "";
    $clsItem = new Product();
    $clsCart = new Cart();
    $clsCart->updateCart($quantity);
    if ($return == "") $return = "mod=product";
    $returnExp = "return=" . base64_encode($return);
    $arrListItem = $clsCart->getAllItem();
    $subTotal = $clsCart->getTotalPrice(false);
    $total_price = $clsCart->getTotalPrice(FALSE);
    $VAT = $subTotal * 0;
    $assign_list["subTotal"] = $subTotal;
    $assign_list["VAT"] = $VAT;
    $assign_list["totalCost"] = $subTotal + $VAT;
    $assign_list["totalItem"] = $clsCart->totalItem;
    $assign_list["arrListItem"] = $arrListItem;
    $assign_list["clsItem"] = $clsItem;
    $assign_list["return"] = $return;
    $assign_list["clsProduct"] = $clsProduct;
    $assign_list["total_price"] = $total_price;
}

?>