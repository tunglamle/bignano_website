<?php
/**
 * Module: [cart]
 * Home function with $sub=default, $act=default
 * Checkout
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_checkout()
{
    global $assign_list, $_CONFIG, $core, $clsCart;
    //Check cart empty or not?
    $total_quantity = $clsCart->getTotalQuantity();
    $success = GET("success", "");
    $clsProduct = new Product();
    if ($total_quantity > 0) {
        //Lấy danh sách item trong cart
        $arrListItem = $clsCart->getAllItem();

        //Lấy tổng giá trị giỏ hàng
        $total_price = $clsCart->getTotalPrice(FALSE);
        //Gán vào smarty
        $assign_list["arrListItem"] = $arrListItem;
        $assign_list["total_price"] = $total_price;
        $assign_list["listProvince"] = makeListProvince($core->_USER['province_id']);
        $assign_list["listDistrict"] = makeListDistrict($core->_USER['district_id']);
    } else {
        $assign_list["message"] = "Không có sản phẩm nào được chọn!";
    }
    $assign_list["total_item"] = $clsCart->totalItem;
    $assign_list["total_quantity"] = $total_quantity;
    $assign_list["clsProduct"] = $clsProduct;
    $assign_list["success"] = $success;
    //Begin SEOMoz
    $_CONFIG['page_title'] = $core->getLang("Checkout") . " - " . $_CONFIG['site_title'];
    //End SEOMoz
}