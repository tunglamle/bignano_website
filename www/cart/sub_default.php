<?
/**
 * Module: [cart]
 * Home function with $sub=default, $act=default
 * Display cart info
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_default()
{
    global $smarty, $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $dbconn, $core, $_LANG_ID, $clsCart, $clsRewrite;

    //print_r($clsCart->arrItemId);
    //print_r($clsCart->arrItem);

    //If has parameter ?clearcart
    $clearcart = isset($_GET["clearcart"]) ? 1 : 0;
    if ($clearcart) {
        $clsCart->clearCart();
        redirectURL($clsRewrite->url_viewcart());
    }

    //Lấy danh sách Course ID
    $tmp = $clsCart->getAllItemId();
    $list_course_id = array_keys($tmp);

    //Lấy danh sách item trong cart
    $arrListItem = $clsCart->getAllItem();

    //Lấy tổng giá trị giỏ hàng
    $total_price = $clsCart->getTotalPrice(FALSE);
    $total_quantity = $clsCart->getTotalQuantity();

    redirectURL($clsRewrite->url_checkout());
    //Gán vào smarty
    $assign_list["arrListItem"] = $arrListItem;
    $assign_list["total_price"] = $total_price;
    $assign_list["total_quantity"] = $total_quantity;
    //Begin SEOMoz
    $_CONFIG['page_title'] = $core->getLang("Shopping_Cart") . " - " . $_CONFIG['site_title'];
    //End SEOMoz
}

?>