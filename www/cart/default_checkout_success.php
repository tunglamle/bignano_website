<?php
/**
 * Module: [cart]
 * Home function with $sub=default, $act=checkout_success
 * Checkout
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_checkout_success()
{
    global $smarty, $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act, $clsRewrite;
    global $dbconn, $core, $_LANG_ID, $clsCart;
    $clsOrders = new Orders();
    $order_code = GET("order_code", "");
    if ($order_code == "") {
        redirectURL($clsRewrite->url_home());
    }
    $arrOneOrder = $clsOrders->getOneOrder($order_code);
    if (!is_array($arrOneOrder) || $arrOneOrder['order_id'] == 0) {
        redirectURL($clsRewrite->url_home());
    }

    //Assign to smarty
    $assign_list["order_code"] = $order_code;
    $assign_list["arrOneOrder"] = $arrOneOrder;

    //Begin SEOMoz
    $_CONFIG['page_title'] = "Đặt hàng thành công - " . $_CONFIG['site_title'];
    //End SEOMoz
}