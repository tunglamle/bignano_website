<?
/**
 * Module: [cart]
 * Home function with $sub=ajax, $act=default
 * Ajax default action
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function ajax_default()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID;
    exit();
}

/**
 * Book a tour (Add to Cart)
 *
 * @param                : no params
 * @return                : no need return
 */
function ajax_addToCart()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID, $clsCart;
    $clsProduct = new Product();
    $valid = 0;
    $product_id = POST('product_id');
    $quantity = POST('quantity');
    $arr_error = array('status' => 'error', 'message' => $core->getLang('Cannot_do_this_action'));
    if (is_array($_POST) && count($_POST) > 0) {
        $valid = 1;
        extract($_POST);
        $item = array();
        $arrOneItem = $clsCart->getOneItem($product_id);
        if ($valid) {
            $arrOneProduct = $clsProduct->getOne($product_id);
            $item['itemId'] = $product_id;
            $item['name'] = $arrOneProduct['name'];
            $item['image'] = $arrOneProduct['image'];
            $item['price'] = $arrOneProduct['price'];
            $item['quantity'] = ($quantity > 0) ? $quantity : 1;
            $item['discount_type'] = 0;
            $item['discount_value'] = 0;
            if ($arrOneProduct['start_date'] <= time() && $arrOneProduct['end_date'] >= time() && $arrOneProduct['is_start'] == 1) {
                $item['discount_type'] = $arrOneProduct['discount_type'];
                $item['discount_value'] = $arrOneProduct['discount_value'];
            }
            $item['VAT'] = 0;
            $clsCart->addCart($item);
            $arr_error = array('status' => 'success', 'message' => 'Đã thêm vào giỏ hàng thành công!', 'total_quantity' => $clsCart->getTotalQuantity());

        }
    }
    echo json_encode($arr_error);
    exit();
}

function ajax_byNow()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID, $clsCart;
    $clsProduct = new Product();
    $valid = 0;
    $product_id = POST('product_id');
    $quantity = POST('quantity');
    $arr_error = array('status' => 'error', 'message' => $core->getLang('Cannot_do_this_action'));
    if (is_array($_POST) && count($_POST) > 0) {
        $valid = 1;
        extract($_POST);
        $item = array();
        $arrOneItem = $clsCart->getOneItem($product_id);
        if ($valid) {
            $arrOneProduct = $clsProduct->getOne($product_id);
            $item['itemId'] = $product_id;
            $item['name'] = $arrOneProduct['name'];
            $item['image'] = $arrOneProduct['image'];
            $item['price'] = $arrOneProduct['price'];
            $item['quantity'] = ($quantity > 0) ? $quantity : 1;
            $item['discount_type'] = 0;
            $item['discount_value'] = 0;
            if ($arrOneProduct['start_date'] <= time() && $arrOneProduct['end_date'] >= time() && $arrOneProduct['is_start'] == 1) {
                $item['discount_type'] = $arrOneProduct['discount_type'];
                $item['discount_value'] = $arrOneProduct['discount_value'];
            }
            $item['VAT'] = 0;
            $clsCart->addCart($item);
            $arr_error = array('status' => 'success', 'message' => 'Đã thêm vào giỏ hàng thành công!', 'total_quantity' => $clsCart->getTotalQuantity());

        }
    }
    echo json_encode($arr_error);
    exit();
}

/**
 * Create a order
 *
 * @param                : no params
 * @return                : no need return
 */
function ajax_createOrder()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID, $clsCart, $clsRewrite;
    $clsOrders = new Orders();
    $btn = POST("btn", "");
    $valid = -1;
    $arr_error = array('status' => 'error', 'message' => $core->getLang('Đã có lỗi xảy ra'));
    if ($btn == "Order") {
        $location = "";
        $total_quantity = $clsCart->getTotalQuantity();
        if ($total_quantity > 0) {
            $valid = $clsOrders->validateCreateOrder($errors);
        } else {
            $location = $clsRewrite->url_viewcart();
        }
        if ($valid) {
            $order_code = $clsOrders->createNewOrder();
            if ($order_code != "") {
                $location = $clsRewrite->url_home();
                $arr_error = array('status' => 'success', 'message' => 'Đặt hàng thành công!', 'location' => $location);
            } else {
                $arr_error = array('status' => 'error', 'message' => 'Có lỗi sảy ra, vui lòng thử lại sau!');
            }
        } else {
            $arr_error = array('status' => 'error', 'message' => 'Vui lòng điền đầy đủ và đúng thông tin!', 'errors' => $errors);
        }
    }
    echo json_encode($arr_error);
    exit();
}
/**
 * Update quantity of each item on cart
 *
 * @param                : no params
 * @return                : no need return
 */
function ajax_updatecart()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID, $clsCart;
    $quantity = POST("quantity", 0);
    $clsCart->updateCart($quantity);
    $total_quantity = $clsCart->getTotalQuantity();
    echo $total_quantity;
    exit();
}

/**
 * Update quantity of each item on cart
 *
 * @param                : no params
 * @return                : no need return
 */
function ajax_getTotalItem()
{
    $clsCart = new Cart();
    echo $clsCart->totalItem;
    exit;
}

function ajax_getTotalQuantity()
{
    $clsCart = new Cart();
    echo $clsCart->getTotalQuantity();
    exit;
}

/**
 * Remove 1 item from cart
 *
 * @param                : no params
 * @return                : no need return
 */
function ajax_removeItem()
{
    $product_id = isset($_POST["product_id"]) ? $_POST["product_id"] : 0;
    if ($product_id != 0) {
        $clsCart = new Cart();
        $clsCart->removeCart($product_id);
        unset($clsCart);
    }
    $clsCart = new Cart();
    echo json_encode([$clsCart->getTotalQuantity()]);
    exit();
}


/**
 * Clear all cart
 *
 * @param                : no params
 * @return                : no need return
 */
function ajax_removecart()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID, $clsCart;
    $clsCart->clearCart();
    exit();
}

function ajax_showCartHeader()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID, $clsCart, $smarty, $clsRewrite;

    $assign_list["core"] = $core;
    $assign_list["URL_UPLOADS"] = URL_UPLOADS;
    $assign_list["Rewrite"] = $clsRewrite;
    $smarty->assign($assign_list);
    echo $smarty->fetch("cart/act_showCartHeader.tpl");
    exit();
}

function ajax_updateorder()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID, $clsCart;
    $quantity = POST("order_quantity", 0);
    $clsCart->updateCart($quantity);
    echo json_encode($_POST);
    exit();
}



?>