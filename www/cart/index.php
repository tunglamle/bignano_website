<?
/******************************************************
 * SubIndex File of module: [cart]
*
* Control Module depend on 2 vars $sub, $act
*
* Project Name               :  Shopping Themes DVS
* Package Name            		:
* Program ID                 :  index.php
* Environment                :  PHP  version 4, 5
* Author                     :  TuanTA
* Version                    :  1.0
* Creation Date              :  2014/02/10
*
* Modification History     :
* Version    Date            Person Name  		Chng  Req   No    Remarks
* 1.0       	2014/02/10    	TuanTA          -  		-     -     -
*
********************************************************/
//If run alone
if (!defined("VNCMS_DIR")){die("Access denied!");}

$sub = $stdio->GET("sub", "default");
$act = $stdio->GET("act", "default");
//Initialize class Module with param: $mod
$clsModule = new Module($mod);
//Call to run module (home, $sub, $act)
$clsModule->run($sub, $act);
//Assign vars to $assign_list
$assign_list["sub"] = $sub;
$assign_list["act"] = $act;	
?>