<?php
/**
 * Module: [cart]
 * Home function with $sub=default, $act=prepayatm
 * Checkout
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_prepayvisa()
{
    global $smarty, $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act, $clsRewrite;
    global $dbconn, $core, $_LANG_ID, $clsCart;
    $clsOrders = new Orders();
    $order_code = GET("order_code", "");
    $payment_method = GET("payment_method", "");
    $process = GET("process", 0);
    if ($order_code == "" || $payment_method == "") {
        redirectURL($clsRewrite->url_home());
    }
    $arrOneOrder = $clsOrders->getOneOrder($order_code);
    if (!is_array($arrOneOrder) || $arrOneOrder['order_id'] == 0) {
        redirectURL($clsRewrite->url_home());
    }
    $btnSubmit = POST("btnSubmit", "");
    //Begin BaoKim
    require_once(VNCMS_DIR . "/baokim/constants.php");
    require_once(VNCMS_DIR . "/baokim/baokim_payment_pro.php");
    $baokim = new BaoKimPaymentPro();
    $banks = $baokim->get_seller_info();
    $list_bank_images = $baokim->generateBankImage($banks, PAYMENT_METHOD_TYPE_CREDIT_CARD);
    //End BaoKim
    //Sau khi người mua thanh toán tiền, paypal sẽ trả lại kết quả giao dịch
    if ($btnSubmit == "Submit") {
        $_POST["order_id"] = $arrOneOrder['order_id'];
        $_POST["url_success"] = url_prepayonline($order_code, $payment_method) . "?btnSubmit=ReturnSuccess";
        $_POST["url_cancel"] = url_prepayonline($order_code, $payment_method) . "?btnSubmit=ReturnCancel";
        $baokim = new BaoKimPaymentPro();
        $result = $baokim->pay_by_card($_POST);

        $baokim_url = $result['redirect_url'] ? $result['redirect_url'] : $result['guide_url'];
        redirectURL($baokim_url);
    } else
        if ($btnSubmit == "ReturnSuccess") {
            extract($_GET);
            $success = ($created_on > 0 && $customer_email != "" && $customer_name != "" && $fee_amount > 0 && $merchant_id > 0 && $order_id != "" && $payment_type > 0 && $transaction_id != "" && $transaction_status == 4);
            if ($success) {
                $success = $clsOrders->updatePayedOrder($order_id);
            } else {
                $success = 0;
            }
            $assign_list["success"] = $success;
        } else
            if ($btnSubmit == "ReturnCancel") {
                $assign_list["success"] = 0;
            }

    //Assign to smarty
    $assign_list["list_bank_images"] = $list_bank_images;
    $assign_list["url_prepay_process"] = url_prepayonline($order_code, $payment_method);
    $assign_list["order_code"] = $order_code;
    $assign_list["payment_method"] = $payment_method;
    $assign_list["arrOneOrder"] = $arrOneOrder;

    $assign_list["url_prepaypaypal"] = url_prepayonline($order_code, 'paypal');
    $assign_list["url_prepayvisa"] = url_prepayonline($order_code, 'visa');
    $assign_list["url_prepayatm"] = url_prepayonline($order_code, 'atm');
    $assign_list["url_prepay"] = url_prepayonline($order_code);
    //Begin SEOMoz
    $_CONFIG['page_title'] = $core->getLang("Process_payment") . " - " . $_CONFIG['site_title'];
    //End SEOMoz
}