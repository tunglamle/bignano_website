<?php
/**
 * Module: [cart]
 * Home function with $sub=default, $act=prepayvisa
 * Checkout
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_prepaypaypal()
{
    global $smarty, $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act, $clsRewrite;
    global $dbconn, $core, $_LANG_ID, $clsCart;
    $clsOrders = new Orders();
    $order_code = GET("order_code", "");
    $payment_method = GET("payment_method", "");
    $process = GET("process", 0);
    if ($order_code == "" || $payment_method == "") {
        redirectURL($clsRewrite->url_home());
    }
    $arrOneOrder = $clsOrders->getOneOrder($order_code);
    if (!is_array($arrOneOrder) || $arrOneOrder['order_id'] == 0) {
        redirectURL($clsRewrite->url_home());
    }
    //Sau khi người mua thanh toán tiền, paypal sẽ trả lại kết quả giao dịch
    //Cập nhật lại trạng thái của đơn hàng
    if ($process == 1000) {
        $paymentID = GET("paymentID", "");
        $payerID = GET("payerID", "");
        $paymentToken = GET("paymentToken", "");

        $ch = curl_init();
        $clientId = PayPal_CLIENT_ID;
        $secret = PayPal_SECRET;
        //curl_setopt($ch, CURLOPT_SSLVERSION, 5);
        curl_setopt($ch, CURLOPT_URL, PayPal_BASE_URL . 'oauth2/token');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $clientId . ":" . $secret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        $result = curl_exec($ch);
        $accessToken = null;


        if (empty($result)) {
            echo 'Curl error: ' . curl_error($ch);
        } else {
            $json = json_decode($result);
            $accessToken = $json->access_token;
            $curl = curl_init(PayPal_BASE_URL . 'payments/payment/' . $paymentID);
            //curl_setopt($ch, CURLOPT_SSLVERSION, 5);
            curl_setopt($curl, CURLOPT_POST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer ' . $accessToken,
                'Accept: application/json',
                'Content-Type: application/xml'
            ));
            $response = curl_exec($curl);
            $result = json_decode($response);


            $state = $result->state;
            $total = $result->transactions[0]->amount->total;
            $currency = $result->transactions[0]->amount->currency;
            $subtotal = $result->transactions[0]->amount->details->subtotal;
            $recipient_name = $result->transactions[0]->item_list->shipping_address->recipient_name;
            curl_close($ch);
            curl_close($curl);

            if ($state == 'approved') {
                $clsOrders->updatePayedOrder($arrOneOrder['order_id'], $payerID, $paymentID, $paymentToken);
                header("location: " . url_checkout() . "?msgr=done&order_code=$order_code");
                exit();
            } else {
                require_once(NVCMS_DIR . "/error404.php");
                exit();
            }
        }
    }

    $arrOneOrder['total_cost_usd'] = $arrOneOrder['total_cost'] / 23500;
    //Assign to smarty
    $assign_list["url_prepay_process"] = url_prepayonline($order_code, $payment_method);
    $assign_list["order_code"] = $order_code;
    $assign_list["payment_method"] = $payment_method;
    $assign_list["arrOneOrder"] = $arrOneOrder;

    $assign_list["url_prepaypaypal"] = url_prepayonline($order_code, 'paypal');
    $assign_list["url_prepayvisa"] = url_prepayonline($order_code, 'visa');
    $assign_list["url_prepayatm"] = url_prepayonline($order_code, 'atm');
    $assign_list["url_prepay"] = url_prepayonline($order_code);

    $assign_list["PayPal_CLIENT_ID"] = PayPal_CLIENT_ID;
    $assign_list["PayPal_SECRET"] = PayPal_SECRET;
    //Begin SEOMoz
    $_CONFIG['page_title'] = $core->getLang("Process_payment") . " - " . $_CONFIG['site_title'];
    //End SEOMoz
}