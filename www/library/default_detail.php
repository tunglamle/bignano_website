<?php
/**
 * Module: [Library]
 * Posts function with $sub=default, $act=detail
 * Display detail of a post
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_detail()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act;
    global $core, $isMobile, $_LANG_ID;
    //Begin GetVars
    $library_id = isset($_GET["library_id"]) ? $_GET["library_id"] : "";
    $slug = isset($_GET["slug"]) ? $_GET["slug"] : "";
    if (isset($_GET["library_id"]) && "$library_id" != $_GET["library_id"] && $_GET["library_id"] != "") {
        $act = "notfound";
        return;
    }
    //End GetVars
    //Begin Init
    $clsCourses = new Courses();
    $clsCategory = new Category();
    $clsLibrary = new Library();
    $clsUsers = new Users();

    $clsCategory->getParentArray();
    if ($library_id == "" || $library_id == 0) {
        if ($slug != "") {
            $arrTmp = $clsLibrary->getByCond("slug='$slug'");
            if (is_array($arrTmp) && $arrTmp['library_id'] != 0) {
                $library_id = $arrTmp['library_id'];
            }
        } else {
            $act = "notfound";
            return;
        }
    }
    $library_id = intval($library_id);
    //End Init

    $arrOneLibrary = $clsLibrary->getOne($library_id);
    if (!is_array($arrOneLibrary) || $arrOneLibrary["library_id"] != $library_id) {
        $act = "notfound";
        return;
    }
    // Tags
    $tags = $arrOneLibrary['tags'];
    $htmlTags = "";
    $arrListTags = array_filter(explode(",", $tags));
    if (is_array($arrListTags)) {
        foreach ($arrListTags as $k => $v) {
            $url_tags = url_tags($v);
            $htmlTags .= "<li class='nav-item'><a class='nav-link' href='$url_tags'>$v</a></li>";
        }
    }

    //Tăng lượt xem
    $clsLibrary->updateOne($library_id, "view_num=view_num+1");
    $cat_id = $arrOneLibrary["cat_id"];
    if ($cat_id > 0) {
        $curCat = $clsCategory->getOne($cat_id);
        if ($curCat['parent_id'] > 0) {
            $parCat = $clsCategory->getOne($curCat['parent_id']);
        }
    }
    $arrListOtherLibrary = $clsLibrary->getAllSimple("c.cat_id=$cat_id AND a.library_id<>$library_id ORDER BY a.reg_date DESC LIMIT 0,4");


    $arrOneLibrary['list_image'] = explode(',',$arrOneLibrary['list_image']);

    //Begin Right Sidebar
    //End Right Sidebar

    //Begin Assign
    $assign_list["curCat"] = $curCat;
    $assign_list["parCat"] = $parCat;
    $assign_list["arrOneLibrary"] = $arrOneLibrary;
    $assign_list["htmlTags"] = $htmlTags;
    $assign_list["arrListOtherLibrary"] = $arrListOtherLibrary;
    //End Assign
    //Begin SEOmoz
    $page_title = ($arrOneLibrary['page_title'] != "") ? $arrOneLibrary['page_title'] : $arrOneLibrary['title'];
    $page_title .= " - " . $_CONFIG['site_title'];
    $tags = $arrOneLibrary['meta_keywords'];
    $page_keywords = ($tags != "") ? $tags : $_CONFIG['meta_keywords'];
    $des = $arrOneLibrary['meta_des'];
    $page_description = ($des != "") ? $des : $_CONFIG['site_description'];
    $_CONFIG['thumb'] = $arrOneLibrary['image'];
    $_CONFIG['page_title'] = $page_title;
    $_CONFIG['page_keywords'] = $page_keywords;
    $_CONFIG['page_description'] = $page_description;
    unset($tags, $des);
    //End SEOmoz
}

?>