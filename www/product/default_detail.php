<?php
/**
 * Module: [articles]
 * Posts function with $sub=default, $act=detail
 * Display detail of a post
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_detail()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act,$clsRewrite;
    global $core, $isMobile, $_LANG_ID;
    //Begin GetVars
    $product_id = isset($_GET["product_id"]) ? $_GET["product_id"] : "";
    $slug = isset($_GET["slug"]) ? $_GET["slug"] : "";
    if (isset($_GET["product_id"]) && "$product_id" != $_GET["product_id"] && $_GET["product_id"] != "") {
        $act = "notfound";
        return;
    }
    //End GetVars
    //Begin Init
    $clsCategory = new Category();
    $clsProduct = new Product();
    $clsAdver = new Adver();
    $clsArticle = new Articles();
    $clsCategory->getParentArray();
    if ($product_id == "" || $product_id == 0) {
        if ($slug != "") {
            $arrTmp = $clsProduct->getByCond("slug='$slug'");
            if (is_array($arrTmp) && $arrTmp['product_id'] != 0) {
                $product_id = $arrTmp['product_id'];
            }
        } else {
            $act = "notfound";
            return;
        }
    }
    $product_id = intval($product_id);
    //End Init

    $arrOneProduct = $clsProduct->getOne($product_id);

    $clsProduct->updateOne($product_id, "view_num=view_num+1");
    if (!is_array($arrOneProduct) || $arrOneProduct["product_id"] != $product_id) {
        $act = "notfound";
        return;
    }

    $cat_id = $arrOneProduct["cat_id"];
    if ($cat_id > 0) {
        $curCat = $clsCategory->getOne($cat_id);
        $parCat = $clsCategory->getOne($curCat['parent_id']);
        $graCat = $clsCategory->getOne($parCat['parent_id']);
    }
    $arrListOtherProduct = $clsProduct->getAllSimple2("cat_id='$cat_id' AND product_id<>'$product_id' ORDER BY reg_date DESC LIMIT 0,6");

    //Lưu sản phẩm đã xem

    $clsProduct->getIDProductViewed($product_id);
    //SP đã xem
    $strpro_viewed = getCookie("pro_viewing");
    $arrListProductViewed = $clsProduct->getAllSimple2("is_online = 1 AND lang_code = '$_LANG_ID' AND product_id IN ($strpro_viewed) ORDER BY reg_date DESC LIMIT 0,4");

    if ($arrOneProduct['list_image'] != '')
        $arrOneProduct['list_image'] = explode(',', $arrOneProduct['list_image']);

    if ($arrOneProduct['uploads_document'] != '')
        $arrOneProduct['uploads_document'] = explode(',', $arrOneProduct['uploads_document']);


    $arrCat = $clsCategory->getOne($arrOneProduct['cat_id']);
    $catName = $arrCat['name'];
    $arrListArticlePromotion = $clsArticle->getAllSimple2("is_online = 1 and is_promotion = 1 ORDER BY reg_date DESC LIMIT 0,3");
    //Begin Assign
    $assign_list["curCat"] = $curCat;
    $assign_list["parCat"] = $parCat;
    $assign_list["graCat"] = $graCat;
    $assign_list["arrOneProduct"] = $arrOneProduct;
    $assign_list["arrListProductViewed"] = $arrListProductViewed;
    $assign_list["arrListOtherProduct"] = $arrListOtherProduct;
    $assign_list["catName"] = $catName;
    $assign_list["clsCategory"] = $clsCategory;
    $assign_list["arrListArticlePromotion"] = $arrListArticlePromotion;
    //End Assign
    //Begin SEOmoz
    $site_title = ($arrOneProduct['page_title'] != "") ? $arrOneProduct['page_title'] : $arrOneProduct['name'];
    $site_title .= " - " . $_CONFIG['site_title'];
    $tags = $arrOneProduct['meta_keywords'];
    $meta_keywords = ($tags != "") ? $tags : $_CONFIG['meta_keywords'];
    $des = $arrOneProduct['meta_des'];
    $site_description = ($des != "") ? $des : $_CONFIG['site_description'];
    $page_title = $site_title;
    $_CONFIG['thumb'] = $arrOneProduct['image'];
    $_CONFIG['site_title'] = $site_title;
    $_CONFIG['meta_keywords'] = $meta_keywords;
    $_CONFIG['site_description'] = $site_description;



    $og = array();
    $og['title'] = $arrOneProduct['title'];
    $og['description'] = strip_tags(htmlDecode($arrOneProduct['sapo']));
    $og['url'] = $clsRewrite->url_product($arrOneProduct);
    $og['type'] = $arrOneProduct['title'];
    $og['image'] = str_replace(' ', '%20', URL_UPLOADS."/".$arrOneProduct['image']);
    $assign_list['og'] = $og;



    unset($tags, $des);
    //End SEOmoz
}

?>