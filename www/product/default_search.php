<?php
/**
 * Module: [Articles]
 * Posts function with $sub=default, $act=search
 * Display search of a articles
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_search()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID;
    $clsProduct = new Product();
    $clsCategory = new Category;
    $clsSearch_app = new Search_app();
    $clsSearch_pro = new Search_pro();
    $clsCategory->getParentArray();

    $app_id = isset($_GET["app_id"]) ? $_GET["app_id"] : "";
    $pro_id = isset($_GET["pro_id"]) ? $_GET["pro_id"] : "";

    if($app_id != '' && $pro_id == ''){
        $arrSearch = $clsSearch_app->getOne($app_id);
        $arrListSearchResult = $clsProduct->getAllSimple2("application_id = $app_id");
    }elseif ($pro_id != '' && $app_id == ''){
        $arrSearch = $clsSearch_pro->getOne($pro_id);
        $arrListSearchResult = $clsProduct->getAllSimple2("properties_id = $pro_id");
    }
    $countSearchResult = count($arrListSearchResult);

    $assign_list["arrSearch"] = $arrSearch;
    $assign_list["arrListSearchResult"] = $arrListSearchResult;
    $assign_list["countSearchResult"] = $countSearchResult;

}

?>