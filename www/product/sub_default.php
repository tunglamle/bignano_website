<?
/******************************************************
 * Child Module of module [product]
 *
 * Contain functions of child module: [default], each function has prefix is 'default_'
 *
 * Project Name               :  noithatthuanphat.com
 * Package Name                    :
 * Program ID                 :  index.php
 * Environment                :  PHP  version 4, 5
 * Author                     :  TuanTA
 * Version                    :  1.0
 * Creation Date              :  04/10/2017
 *
 * Modification History     :
 * Version    Date            Person Name        Chng  Req   No    Remarks
 * 1.0        04/10/2017        Ducnh          -        -     -     -
 *
 ********************************************************/

/**
 * Module: [Product]
 * Category function with $sub=default, $act=default
 * Display Category Page, display list of products
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_default()
{
    global $assign_list, $_CONFIG, $clsRewrite, $_SITE_ROOT, $mod, $act;
    global $core, $_LANG_ID;
    require_once DIR_COMMON . "/clsPaging.php";
    //Begin GetVars
    $cat_id = isset($_GET["cat_id"]) ? $_GET["cat_id"] : 0;
    $cat_id = intval($cat_id);
    $slug = isset($_GET["slug"]) ? $_GET["slug"] : "";
    $show = isset($_GET["show"]) ? $_GET["show"] : "";
    //End GetVars
    //Begin Init
    if (isset($_GET["cat_id"]) && "$cat_id" != $_GET["cat_id"] && $_GET["cat_id"] != "") {
        showErrorFatalBox("notfound");
        exit();
    }
    $clsProduct = new Product();
    $clsCategory = new Category();
    $clsCategory->getParentArray();
    $clsSearch_app = new Search_app();
    $clsSearch_pro = new Search_pro();
    $clsAdver = new Adver();
    $curCat = array();
    $slug = GET("slug", "");
    if ($slug != "") {
        $arrTmp = $clsCategory->getBySlug($slug);
        if (is_array($arrTmp) && $arrTmp['cat_id'] != 0) {
            $cat_id = $arrTmp['cat_id'];
        }
        unset($arrTmp);
    }
    if ($cat_id > 0) {
        $curCat = $clsCategory->getOne($cat_id);
        $parCat = $clsCategory->getOne($curCat['parent_id']);
        $graCat = $clsCategory->getOne($parCat['parent_id']);
        $arrSubCat = $clsCategory->getAll("is_online=1 AND parent_id=$cat_id");
         if (is_array($arrSubCat)) {
            foreach ($arrSubCat as $key => $value) {
                $hasSubCat++;
                $listProduct = $clsProduct->getAllSimple2("cat_id=" . $value['cat_id'] . " LIMIT 0,12");
                $arrSubCat[$key]['listProduct'] = $listProduct;
            }
        }else {
            $arrSubCat = array();
        }

        $page_title = $curCat['name'];
    } else {
        $page_title = $core->getLang("Product");
    }
    // print_r($arrSubCat);
    //End Init
    //Begin SQl Condition
    $cond = "lang_code='$_LANG_ID' AND is_online=1";
    if ($cat_id != "" && $cat_id != "0") {
        $cat_id_str = $clsCategory->getAllCatStr($cat_id) . $cat_id;
        $cond .= (strpos($cat_id_str, ',') !== false) ? " AND cat_id in ($cat_id_str)" : " AND cat_id=$cat_id";
    }

    $orderby = " ORDER BY reg_date DESC";

    $cat_id_curent = $clsCategory->getRootCat($curCat['cat_id']);//Lấy cat_id cấp cao nhất\
    

    //End SQL Condition
    //Begin Paging
    $rowPerPage = 9;
    $curPage = (isset($_GET["page"]) && $_GET["page"] > 0) ? ($_GET["page"]) : 0;
    $clsPaging = new Paging($curPage, $rowPerPage, "news");
    $clsPaging->setBaseURL($clsRewrite->url_category($curCat));
    $cond_count = $cond;
    $totalItem = $clsProduct->countItem($cond_count);
    $clsPaging->setTotalRows($totalItem);
    $clsPaging->setShowStatstic(false);
    $clsPaging->setShowGotoBox(false);
    $assign_list["clsPaging"] = $clsPaging;
    //End Paging

    //Lấy list nhóm cha
    $assign_list['listCatParent'] = $clsCategory->getAllParentCatArr($curCat['cat_id']);

    //Begin List Product
    $total_product = $clsProduct->countItem("is_online=1 AND lang_code='$_LANG_ID' AND cat_id = $cat_id ");

    $arrListProduct = $clsProduct->getAllSimple2($cond . $orderby);

    $arrListSearchApplication = $clsSearch_app->getAll("is_online = 1 ORDER BY order_no ASC");

    $arrListSearchProperties = $clsSearch_pro->getAll("is_online = 1 ORDER BY order_no ASC");


    $arrListCat = $clsCategory->getAll("is_online = 1 AND lang_code = '$_LANG_ID' AND parent_id = $cat_id ORDER BY order_no ASC");
    if (is_array($arrListCat)) {
        foreach ($arrListCat as $k => $v) {
            $cat_id1 = $v['cat_id'];
            $arrCat = $clsCategory->getAll("is_online = 1 AND lang_code = '$_LANG_ID' AND parent_id = $cat_id1 ORDER BY order_no ASC");
            $arrListCat[$k]['arrCat'] = $arrCat;
            $arrListCat[$k]['list_image'] = explode(',', $v['list_image']);
        }
    }

    if ($curCat['parent_id'] > 0) {
        $arrListCatProduct = $clsCategory->getAll("is_online = 1 AND lang_code = '$_LANG_ID' AND parent_id = $cat_id ORDER BY order_no ASC");
        if (is_array($arrListCatProduct)) {
            foreach ($arrListCatProduct as $k => $v) {
                $cat_id1 = $v['cat_id'];
                $arrProduct = $clsProduct->getAllSimple2("is_online = 1 AND lang_code = '$_LANG_ID' AND cat_id = $cat_id1 ORDER BY order_no ASC");
                $arrListCatProduct[$k]['arrProduct'] = $arrProduct;
                $arrListCatProduct[$k]['list_image'] = explode(',', $v['list_image']);
            }
        }
    }
    $arrListProductNewLaunching = $clsProduct->getAllSimple("a.is_new = 1 AND a.lang_code = '$_LANG_ID' AND a.is_online = 1 AND c.parent_id = $cat_id ORDER BY a.reg_date DESC");

    $arrListCatByCurCat = $clsCategory->getAll("parent_id = $cat_id AND lang_code = '$_LANG_ID' ORDER BY order_no ASC");


    //Eng List Product
//Begin Assign
    $assign_list["curCat"] = $curCat;
    $assign_list["total_product"] = $total_product;
    $assign_list["parCat"] = $parCat;
    $assign_list["graCat"] = $graCat;
    $assign_list["show"] = $show;
    $assign_list["arrListProduct"] = $arrListProduct;
    $assign_list["totalItem"] = $totalItem;
    $assign_list["page_title"] = $page_title;
    $assign_list["rowPerPage"] = $rowPerPage;
    $assign_list["clsCategory"] = $clsCategory;
    $assign_list["arrSubCat"] = $arrSubCat;
    $assign_list["hasSubCat"] = $hasSubCat;
    $assign_list["arrListCat"] = $arrListCat;
    $assign_list["arrListCatProduct"] = $arrListCatProduct;
    $assign_list["arrListSearchApplication"] = $arrListSearchApplication;
    $assign_list["arrListSearchProperties"] = $arrListSearchProperties;
    $assign_list["arrListProductNewLaunching"] = $arrListProductNewLaunching;
    $assign_list["arrListCatByCurCat"] = $arrListCatByCurCat;
//End Assign
//Begin SEOmoz
    $site_title = ($curCat['page_title'] != "") ? $curCat['page_title'] : $curCat['name'];
    if ($site_title == "") $site_title = "Sản phẩm";
    $page_title .= " - " . $_CONFIG['site_title'];
    $tags = $curCat['meta_keywords'];
    $meta_keywords = ($tags != "") ? $tags : $_CONFIG['meta_keywords'];
    $des = $curCat['meta_des'];
    $site_description = ($des != "") ? $des : $_CONFIG['site_description'];
    $_CONFIG['site_title'] = $site_title;
    $_CONFIG['meta_keywords'] = $meta_keywords;
    $_CONFIG['site_description'] = $site_description;
    unset($tags, $des);
//End SEOmoz
}


?>