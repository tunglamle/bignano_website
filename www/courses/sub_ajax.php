<?
function ajax_default()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod;
    global $core, $_LANG_ID;
    exit();
}

function ajax_save_score()
{
    global $core, $_LANG_ID;
    $clsHistory = new History();
    $user_id = $core->_USER['user_id'];
    if (is_array($_POST) && count($_POST) != 0 && $user_id != 0) {
        $arrOneHistory = $clsHistory->getByCond("user_id = $user_id AND lesson_id = $_POST[lesson_id] AND lang_code='$_LANG_ID'");

        $field = "user_id, lang_code, reg_date";
        $value = "$user_id, '$_LANG_ID', " . time();
        $set = "";
        foreach ($_POST as $f => $v) {
            $field .= ", $f";
            $value .= ", '$v'";
            if ($f != "lesson_id") {
                $set .= ($set != "") ? ", $f = '$v'" : "$f = '$v'";
            }
        }
        if (!is_array($arrOneHistory) || count($arrOneHistory) == 0) {
            $clsHistory->insertOne($field, $value);
        } else {
            $clsHistory->updateOne($arrOneHistory['history_id'], $set);
        }
    }
    exit();
}

?>