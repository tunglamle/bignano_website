<?php
/**
 * Module: [Lessons]
 * Posts function with $sub=default, $act=active
 * Display detail of a Lesson
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_active()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act, $clsRewrite;
    global $core, $isMobile, $_LANG_ID;
    //Begin GetVars
    $clsOrderItem = new OrderItem();
    if (is_array($_POST) && count($_POST) > 0) {
        extract($_POST);
        switch ($clsOrderItem->activeCourse($active_key)) {
            case 0:
                $arr_error = array('status' => 'success', 'message' => 'Kích hoạt khóa học thành công!');
                break;
            case 1:
                $arr_error = array('status' => 'error', 'message' => 'Mã kích hoạt đã được sử dụng!');
                break;
            default:
                $arr_error = array('status' => 'error', 'message' => 'Mã kích hoạt không đúng vui lòng thử lại!');
                break;
        }
    }
    $assign_list['arr_error'] = $arr_error;
    //Begin SEOmoz
    $page_title = "Kích hoạt khóa học";
    $page_title .= " - " . $_CONFIG['site_title'];
    $_CONFIG['page_title'] = $page_title;
    unset($tags, $des);
    //End SEOmoz
}

?>