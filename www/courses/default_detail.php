<?php
/**
 * Module: [Lessons]
 * Posts function with $sub=default, $act=detail
 * Display detail of a Lesson
 *
 * @param                : no params
 * @return                : no need return
 * @exception
 * @throws
 */
function default_detail()
{
    global $assign_list, $_CONFIG, $_SITE_ROOT, $mod, $act, $clsRewrite;
    global $core, $isMobile, $_LANG_ID;
    //Begin GetVars
    $user_id = $core->_USER['user_id'];
    $course_id = isset($_GET["course_id"]) ? $_GET["course_id"] : "";
    $slug = isset($_GET["slug"]) ? $_GET["slug"] : "";
    if (isset($_GET["course_id"]) && "$course_id" != $_GET["course_id"] && $_GET["course_id"] != "") {
        $act = "notfound";
        return;
    }
    //End GetVars
    //Begin Init
    $clsCategory = new Category();
    $clsCourses = new Courses();
    $clsLessons = new Lessons();
    $clsStage = new Stage();
    $clsUsers = new Users();
    $clsOrderItem = new OrderItem();

    if ($course_id == "" || $course_id == 0) {
        if ($slug != "") {
            $arrTmp = $clsCourses->getByCond("slug='$slug'");
            if (is_array($arrTmp) && $arrTmp['course_id'] != 0) {
                $course_id = $arrTmp['course_id'];
            }
        } else {
            $act = "notfound";
            return;
        }
    }
    $arrOneCourse = $clsCourses->getOneSimple($course_id);

    $cat_id = $arrOneCourse["cat_id"];
    if ($cat_id > 0) {
        $curCat = $clsCategory->getOne($cat_id);
        if ($curCat['parent_id'] > 0) {
            $parCat = $clsCategory->getOne($curCat['parent_id']);
        }
    }

    $arrListStage = $clsStage->getAll("course_id = $course_id AND is_online = 1 AND lang_code='$_LANG_ID' ORDER BY order_no ASC, reg_date ASC");
    if (is_array($arrListStage)) {
        foreach ($arrListStage as $s => $stage) {
            $list_lesson = $clsLessons->getAll("stage_id = $stage[stage_id] AND is_online = 1 AND lang_code='$_LANG_ID' ORDER BY order_no ASC, reg_date ASC");
            foreach ($list_lesson as $l => $lesson) {
                $list_lesson[$l]['video_id'] = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), 0, 50) . base64_encode($lesson['video_id']);
            }
            $arrListStage[$s]['list_lesson'] = $list_lesson;
        }
    }

    $arrListOtherCourses = $clsCourses->getAll("course_id != $course_id AND lang_code = '$_LANG_ID' AND is_online = 1 ORDER BY reg_date DESC LIMIT 3");
    if (is_array($arrListOtherCourses) && count($arrListOtherCourses) > 0) {
        foreach ($arrListOtherCourses as $c => $course) {
            $arrListOtherCourses[$c]['author'] = $clsUsers->getOne($course['user_id']);
            $arrListOtherCourses[$c]['total_student'] = $clsOrderItem->countItem("course_id = $course[course_id]");
        }
    }
    $arrAuthor = ($arrOneCourse['user_id'] > 0) ? $clsUsers->getOne($arrOneCourse['user_id']) : array();
    $arrOneActive = $clsOrderItem->getOneSimple("o.user_id = $user_id AND a.course_id = $course_id");
    $total_student = $clsOrderItem->countItem("course_id = $course_id");
    //Begin Assign
    $assign_list["curCat"] = $curCat;
    $assign_list["parCat"] = $parCat;
    $assign_list["arrOneCourse"] = $arrOneCourse;
    $assign_list["arrListStage"] = $arrListStage;
    $assign_list["arrListOtherCourses"] = $arrListOtherCourses;
    $assign_list["is_active"] = $arrOneActive['is_active'];
    $assign_list["arrAuthor"] = $arrAuthor;
    $assign_list["total_student"] = $total_student;
    //End Assign
    //Begin SEOmoz
    $page_title = ($arrOneCourse['page_title'] != "") ? $arrOneCourse['page_title'] : $arrOneCourse['name'];
    $page_title .= " - " . $_CONFIG['site_title'];
    $_CONFIG['page_title'] = $page_title;
    $_CONFIG['page_keywords'] = $arrOneCourse['meta_keywords'];
    $_CONFIG['page_description'] = $arrOneCourse['meta_des'];
    unset($tags, $des);
    //End SEOmoz
}

?>