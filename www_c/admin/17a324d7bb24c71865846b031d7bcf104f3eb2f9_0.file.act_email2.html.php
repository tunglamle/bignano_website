<?php
/* Smarty version 3.1.32, created on 2020-03-09 14:07:39
  from '/home/bignao/public_html/admin/templates/settings/act_email2.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e65eb3bc17a13_10688131',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '17a324d7bb24c71865846b031d7bcf104f3eb2f9' => 
    array (
      0 => '/home/bignao/public_html/admin/templates/settings/act_email2.html',
      1 => 1575252076,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_block_inner_head_add.html' => 1,
  ),
),false)) {
function content_5e65eb3bc17a13_10688131 (Smarty_Internal_Template $_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['clsForm']->value->showJS();?>

<?php $_smarty_tpl->_subTemplateRender("file:_block_inner_head_add.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<form name="theForm" action="" method="post" id="theForm">
    <table width="100%" border="0">
        <tr>
            <td style="padding:10px" colspan="4">
                <div style="padding-bottom:3px;font-size:14px; border-bottom:1px solid #999999; padding-left:5px;">
                    <a class="btn-tab " href="?mod=settings&act=email">Đăng ký tài khoản</a>
                    <a class="btn-tab active" href="?mod=settings&act=email2">Quên mật khẩu</a>
                    <a class="btn-tab " href="?mod=settings&act=email3">Cấu hình SMTP</a>
                    <a class="btn-tab " href="?mod=settings&act=email6">Mail Feedback</a>
                    <div style="float:right;font-size:12px; width:30%; color:blue" align="right">
                        Ngôn ngữ: <?php echo $_smarty_tpl->tpl_vars['lang_code_name']->value;?>

                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding:0px 10px" width="100%" valign="top">
                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="girdtable">
                    <tr>
                        <td colspan="2" class="gridheader1">Email Quên mật khẩu</td>
                    </tr>
                    <tr>
                        <td class="gridrow" width="40%">Gửi email quên mật khẩu cho người dùng?</td>
                        <td class="gridrow1">
                            <?php echo $_smarty_tpl->tpl_vars['clsForm']->value->showInput("mail_configs[mail_forgot]");?>

                        </td>
                    </tr>
                    <tr>
                        <td class="gridrow" width="40%">Tiêu đề email:</td>
                        <td class="gridrow1">
                            <?php echo $_smarty_tpl->tpl_vars['clsForm']->value->showInput("mail_forgot_title");?>

                        </td>
                    </tr>
                    <tr>
                        <td class="gridrow" width="40%">Nội dung email:
                            <br><br>
                            <pre>
                                %SITE_NAME% : tên của website
                                %SITE_TITLE% : tiêu đề của website
                                %SITE_HOTLINE% : hotline của website
                                %FULL_NAME% : họ tên người đăng ký
                                %USER_NAME% : tên đăng nhập
                                %URL_FORGOT% : link kích hoạt tài khoản
                            </pre>
                        </td>
                        <td class="gridrow1">
                            <?php echo $_smarty_tpl->tpl_vars['clsForm']->value->showInput("mail_forgot_body");?>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="gridheader1">Email Đổi mật khẩu thành công</td>
                    </tr>
                    <tr>
                        <td class="gridrow" width="40%">Gửi email thông báo khi người dùng đổi mật khẩu thành công?</td>
                        <td class="gridrow1">
                            <?php echo $_smarty_tpl->tpl_vars['clsForm']->value->showInput("mail_configs[mail_forgot_success]");?>

                        </td>
                    </tr>
                    <tr>
                        <td class="gridrow" width="40%">Tiêu đề email:</td>
                        <td class="gridrow1">
                            <?php echo $_smarty_tpl->tpl_vars['clsForm']->value->showInput("mail_forgot_success_title");?>

                        </td>
                    </tr>
                    <tr>
                        <td class="gridrow" width="40%">Nội dung email:
                            <br><br>
                            <pre>
                                %SITE_NAME% : tên của website
                                %SITE_TITLE% : tiêu đề của website
                                %SITE_HOTLINE% : hotline của website
                                %FULL_NAME% : họ tên người đăng ký
                                %USER_NAME% : tên đăng nhập
                                %USER_PASS% : mật khẩu
                            </pre>
                        </td>
                        <td class="gridrow1">
                            <?php echo $_smarty_tpl->tpl_vars['clsForm']->value->showInput("mail_forgot_success_body");?>

                        </td>
                    </tr>
                </table>
                <em><font style="font-size:10px"><?php echo $_smarty_tpl->tpl_vars['core']->value->getLang("Note");?>
: * <?php echo $_smarty_tpl->tpl_vars['core']->value->getLang("isrequired");?>
</font></em>
            </td>
        </tr>
    </table>
</form><?php }
}
