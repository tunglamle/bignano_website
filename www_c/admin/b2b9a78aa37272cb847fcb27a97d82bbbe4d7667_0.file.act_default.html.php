<?php
/* Smarty version 3.1.32, created on 2020-03-31 10:25:13
  from '/var/www/html/bignanotech.com.vn/admin/templates/menu/act_default.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e82b819963085_62734638',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b2b9a78aa37272cb847fcb27a97d82bbbe4d7667' => 
    array (
      0 => '/var/www/html/bignanotech.com.vn/admin/templates/menu/act_default.html',
      1 => 1584441207,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_block_inner_head.html' => 1,
  ),
),false)) {
function content_5e82b819963085_62734638 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:_block_inner_head.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<form name="theForm" action="<?php if ($_GET['menu_id'] == '') {?>?mod=<?php echo $_smarty_tpl->tpl_vars['mod']->value;
}?>" method="post" id="theForm">
<table width="100%" border="0">
<tr>
<td style="padding:10px">
	<div style="font-size:14px; float:left">
	<strong><?php echo $_smarty_tpl->tpl_vars['core']->value->getLang("ListOf");?>
 <?php echo $_smarty_tpl->tpl_vars['clsDataGrid']->value->getTitle();?>
</strong>
	<select name="mtype" id="mtype" onchange="return submitform();">
	<?php echo $_smarty_tpl->tpl_vars['htmlOptionsMenu']->value;?>

	</select>	
	</div>
	<div style="float:right;font-size:12px; color:blue" align="right">
	Ngôn ngữ: <?php echo $_smarty_tpl->tpl_vars['lang_code_name']->value;?>

	</div>
</td>
</tr>
<tr>
	<td style="padding:0px 10px" width="100%" valign="top">
	<div class="navpath"><?php echo $_smarty_tpl->tpl_vars['core']->value->getLang("You_are_at");?>
: <?php echo $_smarty_tpl->tpl_vars['menuPathAdmin']->value;?>
</div>
	<?php echo $_smarty_tpl->tpl_vars['clsDataGrid']->value->showDataGrid("theForm");?>

	</td>
</tr>
<tr>
	<td  style="padding:0px 10px">
	<?php echo $_smarty_tpl->tpl_vars['clsDataGrid']->value->showPaging("theForm");?>

	</td>
</tr>
</table>
</form>
<?php }
}
