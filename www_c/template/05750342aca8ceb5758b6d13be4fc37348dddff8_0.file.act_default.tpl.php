<?php
/* Smarty version 3.1.32, created on 2019-11-30 15:35:08
  from 'D:\Thuan\bignanotech\themes\template\home\act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5de229bc5a0e86_75440758',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '05750342aca8ceb5758b6d13be4fc37348dddff8' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\home\\act_default.tpl',
      1 => 1575102814,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5de229bc5a0e86_75440758 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'D:\\Thuan\\bignanotech\\includes\\smarty3\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?><div class="page__content">
    <!-- main content-->
    <div class="home-video">
        <video src="<?php echo $_smarty_tpl->tpl_vars['URL_MEDIA']->value;?>
/video.mp4" poster="<?php echo $_smarty_tpl->tpl_vars['URL_IMAGES']->value;?>
/poster.jpg" autoplay="autoplay" muted="muted" playsinline="playsinline" loop="loop"></video>
    </div>
    <div class="home-movie">
        <div class="home-movie__wrapper">
            <div class="home-movie__body">
                <div class="home-movie__search">
                    <form class="search" action="#!">
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="Search our site" />
                            <div class="input-group-append">
                                <button class="input-group-text"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="home-movie__aside"><a class="home-movie__card" href="#!">
                    <div class="home-movie__card-frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_IMAGES']->value;?>
/home-movie-card.jpg" alt=""></div>
                    <div class="home-movie__card-body">
                        <div class="home-movie__card-subtitle">High-performance Membrane</div>
                        <div class="home-movie__card-title"><span class="font-italic">miraim</span><span>®</span></div>
                    </div>
                </a><a class="home-movie__card" href="#!">
                    <div class="home-movie__card-frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_IMAGES']->value;?>
/home-movie-card.jpg" alt=""></div>
                    <div class="home-movie__card-body">
                        <div class="home-movie__card-subtitle">High-performance Membrane</div>
                        <div class="home-movie__card-title"><span class="font-italic">miraim</span><span>®</span></div>
                    </div>
                </a>
                <div class="home-movie__card-2"><a href="#!"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_IMAGES']->value;?>
/home-movie-card-2.jpg" alt=""></a></div>
            </div>
        </div>
    </div>
    <!-- notification-->
    <section class="section section--gray">
        <h2 class="section__title">Products &amp; Services</h2>
        <div class="section__body">
            <ul class="cate">
                 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrCatProductHome']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                <li class="cate__item">
                    <a class="cate__link" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
">
                        <div class="cate__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['icon'];?>
" alt="" /></div>
                        <div class="cate__text"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</div>
                    </a>
                </li>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </ul>
            <div class="text-center mt-40"><a class="my-button" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['arrProduct']->value);?>
">More</a></div>
        </div>
    </section>
    <section class="section section--transparent">
        <h2 class="section__title">Teijin's Two Pillar Business Fields</h2>
        <div class="row no-gutters mt-md-20">
                 <div class="col-md-6">
                <div class="home-card">
                    <div class="home-card__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['arrLvHome']->value[0]['image'];?>
" alt=""></div>
                    <div class="home-card__overlay">
                        <div class="home-card__title"><?php echo $_smarty_tpl->tpl_vars['arrLvHome']->value[0]['title'];?>
</div>
                        <div class="home-card__desc"><?php echo htmlDecode($_smarty_tpl->tpl_vars['arrLvHome']->value[0]['des']);?>
</div>
                    </div>
                    <div class="home-card__outer">
                        <div class="home-card__close"></div>
                        <div class="home-card__title2"><?php echo $_smarty_tpl->tpl_vars['arrLvHome']->value[0]['title'];?>
</div>
                        <div class="home-card__desc2"><?php echo htmlDecode($_smarty_tpl->tpl_vars['arrLvHome']->value[0]['content']);?>
</div>
                        <ul class="home-card__nav">
                             <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrCatProductHomeLeft']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                            <li class="home-card__nav-item"><a class="home-card__nav-link" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a></li>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </ul>
                    </div>
                </div>
            </div>
                 <div class="col-md-6">
                     <div class="home-card">
                         <div class="home-card__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['arrLvHome']->value[1]['image'];?>
" alt=""></div>
                         <div class="home-card__overlay">
                             <div class="home-card__title"><?php echo $_smarty_tpl->tpl_vars['arrLvHome']->value[1]['title'];?>
</div>
                             <div class="home-card__desc"><?php echo htmlDecode($_smarty_tpl->tpl_vars['arrLvHome']->value[1]['des']);?>
</div>
                         </div>
                         <div class="home-card__outer">
                             <div class="home-card__close"></div>
                             <div class="home-card__title2"><?php echo $_smarty_tpl->tpl_vars['arrLvHome']->value[1]['title'];?>
</div>
                             <div class="home-card__desc2"><?php echo htmlDecode($_smarty_tpl->tpl_vars['arrLvHome']->value[1]['content']);?>
</div>
                             <ul class="home-card__nav">
                                 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrCatProductHomeRight']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                                     <li class="home-card__nav-item"><a class="home-card__nav-link" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a></li>
                                 <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                             </ul>
                         </div>
                     </div>
                 </div>
        </div>
    </section>
    <section class="section pb-0">
        <h2 class="section__title">About Us</h2>
        <div class="about-cards">
             <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrCatAboutHome']->value, 'about', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['about']->value) {
?>
            <div class="about-cards__item">
                <div class="about-cards__frame">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['about']->value['image'];?>
" alt="">
                </div>
                <a class="about-cards__overlay" href="<?php if ($_smarty_tpl->tpl_vars['about']->value['link']) {
echo $_smarty_tpl->tpl_vars['about']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['about']->value);
}?>">
                    <h3 class="about-cards__title text-white"><?php echo $_smarty_tpl->tpl_vars['about']->value['name'];?>
</h3>
                </a>
            </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

            <div class="about-cards__item">
                <div class="about-cards__frame"></div>
                <div class="about-cards__overlay">
                    <h3 class="about-cards__title">Stock Price</h3>
                    <div class="my-2"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_IMAGES']->value;?>
/about-card-img-3.png" alt=""></div><a class="about-cards__link" href="#!"><span>Chart</span></a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section--gray">
        <h2 class="section__title">Latest Information</h2>
        <div class="section__body">
            <ul class="nav news-tabs">
                <li class="nav-item"><a class="nav-link js-news-tab active" href="#news-tab-1" data-toggle="tab">News</a></li>
                <li class="nav-item"><a class="nav-link js-news-tab" href="#news-tab-2" data-toggle="tab">IR News</a></li>
                <li class="nav-item"><a class="nav-link js-news-tab" href="#news-tab-3" data-toggle="tab">Notifications</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="news-tab-1">
                    <div class="news-slider">
                        <div class="news-slider__prev"></div>
                        <div class="news-slider__next"></div>
                        <div class="news-slider__pagination"></div>
                        <div class="news-slider__container swiper-container">
                            <div class="swiper-wrapper">
                                 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListNews']->value, 'news', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['news']->value) {
?>
                                <div class="swiper-slide">
                                    <a class="news" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_article($_smarty_tpl->tpl_vars['news']->value);?>
">
                                        <div class="news__date"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['reg_date'],"%B %d, %Y");?>
</div>
                                        <div class="news__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['news']->value['image'];?>
" alt="" /></div>
                                        <h3 class="news__title"><?php echo $_smarty_tpl->tpl_vars['news']->value['title'];?>
</h3>
                                    </a>
                                </div>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="news-tab-2">
                    <div class="news-slider">
                        <div class="news-slider__prev"></div>
                        <div class="news-slider__next"></div>
                        <div class="news-slider__pagination"></div>
                        <div class="news-slider__container swiper-container">
                            <div class="swiper-wrapper">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListNewsIr']->value, 'news', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['news']->value) {
?>
                                    <div class="swiper-slide">
                                        <a class="news" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_article($_smarty_tpl->tpl_vars['news']->value);?>
">
                                            <div class="news__date"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['reg_date'],"%B %d, %Y");?>
</div>
                                            <div class="news__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['news']->value['image'];?>
" alt="" /></div>
                                            <h3 class="news__title"><?php echo $_smarty_tpl->tpl_vars['news']->value['title'];?>
</h3>
                                                                                    </a>
                                    </div>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="news-tab-3">
                    <div class="news-slider">
                        <div class="news-slider__prev"></div>
                        <div class="news-slider__next"></div>
                        <div class="news-slider__pagination"></div>
                        <div class="news-slider__container swiper-container">
                            <div class="swiper-wrapper">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListNewsNotifications']->value, 'news', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['news']->value) {
?>
                                    <div class="swiper-slide">
                                        <a class="news" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_article($_smarty_tpl->tpl_vars['news']->value);?>
">
                                            <div class="news__date"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['reg_date'],"%B %d, %Y");?>
</div>
                                            <div class="news__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['news']->value['image'];?>
" alt="" /></div>
                                            <h3 class="news__title"><?php echo $_smarty_tpl->tpl_vars['news']->value['title'];?>
</h3>
                                                                                    </a>
                                    </div>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center mt-30"><a class="my-button" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['arrNews']->value);?>
">News Archives</a></div>
        </div>
    </section>
</div>
<?php }
}
