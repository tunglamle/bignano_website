<?php
/* Smarty version 3.1.32, created on 2019-11-28 15:38:48
  from 'D:\Thuan\bignanotech\themes\template\product\act_detail.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5ddf8798b1bcd8_34704160',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'aab25a9ea442902f881ca62fcde9730372f9b87c' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\product\\act_detail.tpl',
      1 => 1574412587,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ddf8798b1bcd8_34704160 (Smarty_Internal_Template $_smarty_tpl) {
?><article class="banner">
    <img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt=""/>
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a class="link-unstyled" href="/">Trang chủ</a></li>
            <?php if ($_smarty_tpl->tpl_vars['parCat']->value) {?>
                <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['parCat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['parCat']->value['name'];?>
</a></li>
            <?php }?>
            <li class="breadcrumb-item"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</li>

            <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['name'];?>
</li>
        </ol>
    </div>
</article>

<main class="main">

    <section class="product-main product-detail-main">

        <div class="container">
            <h2 class="cat-title">
                <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['parCat']->value['image'];?>
" alt="">
                <?php echo $_smarty_tpl->tpl_vars['parCat']->value['name'];?>

            </h2>
        </div>
        <div class="pd-top">
            <div class="container">
                <div class="pd-top-wrap">
                    <h1 class="pd-top-name"><?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['name'];?>
</h1>
                    <div class="pd-top-price"><?php echo $_smarty_tpl->tpl_vars['core']->value->callfunc("number_format",$_smarty_tpl->tpl_vars['arrOneProduct']->value['price'],0,',','.');?>
 VNĐ</div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row mb-60">
                <div class="col-lg-8">
                    <article class="preview">
                        <div class="preview__thumbs js-thumbs">
                            <div class="preview__thumbs-float">
                                <div class="js-thumbs__prev"><i class="fa fa-angle-up"></i></div>
                                <div class="js-thumbs__next"><i class="fa fa-angle-down"></i></div>
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrOneProduct']->value['list_image'], 'slide', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['slide']->value) {
?>
                                        <div class="swiper-slide">
                                            <div class="thumb-slider__frame">
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['slide']->value;?>
" alt=""/>
                                            </div>
                                        </div>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="preview__frame js-preview">
                            <div class="preview__frame-wrap">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrOneProduct']->value['list_image'], 'slide', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['slide']->value) {
?>
                                        <div class="swiper-slide">
                                            <a class="preview-slider__frame">
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['slide']->value;?>
" class="w-100" alt=""/>
                                            </a>
                                        </div>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-lg-4">
                    <h2 class="pd-title">
                        <?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['name'];?>

                    </h2>
                    <div class="pd-code">
                        <?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['product_code'];?>

                    </div>
                    <div class="pd-sapo">
                        <?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOneProduct']->value['sapo2']);?>

                    </div>
                    <div class="pd-price">
                        Giá tham khảo: <span class="pd-price-span"><?php echo $_smarty_tpl->tpl_vars['core']->value->callfunc("number_format",$_smarty_tpl->tpl_vars['arrOneProduct']->value['price'],0,',','.');?>
 VNĐ</span>
                    </div>
                </div>
            </div>
            <div class="pd-content-wrap">
                <div class="box-title">
                    <h3>CHI TIẾT SẢN PHẨM</h3>
                </div>
                <div class="pd-content">
                    <?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOneProduct']->value['content']);?>

                </div>
            </div>
            <div class="container">
                <div class="product-viewed">
                    <div class="viewed-title">
                        Đã xem gần đây
                    </div>
                    <div class="viewed-wrap">
                        <div class="row">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListProductViewed']->value, 'product', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['product']->value) {
?>
                                <div class="col-lg-3 col-6">
                                    <div class="viewed-box">
                                        <div class="viewed-img">
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);?>
" class="text-default">
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['product']->value['image'];?>
"  class="w-100" alt="">
                                            </a>
                                        </div>
                                        <div class="viewed-content">
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);?>
" class="text-default viewed-link">
                                                <?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>

                                            </a>
                                            <span class="d-block viewed-code">
                                       <?php echo $_smarty_tpl->tpl_vars['product']->value['product_code'];?>

                                </span>
                                        </div>
                                    </div>
                                </div>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main><?php }
}
