<?php
/* Smarty version 3.1.32, created on 2020-02-06 11:47:31
  from '/home/bignao/public_html/themes/template/job/act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e3b9a6334e3f6_44206741',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '346090c9ae9aae0cc0d17d6fb019acbe2357c748' => 
    array (
      0 => '/home/bignao/public_html/themes/template/job/act_default.tpl',
      1 => 1580964449,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e3b9a6334e3f6_44206741 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/home/bignao/public_html/includes/smarty3/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>    <div class="page__content">
        <!-- main content-->
        <div class="banner">
            <div class="banner__wrapper">
                <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
            </div><img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="" />
        </div>
        <section class="section-2">
            <section class="news-section mb-40">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListJob']->value, 'job', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['job']->value) {
?>
                    <a class="news-section__item" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_job($_smarty_tpl->tpl_vars['job']->value);?>
">
                        <div class="news-section__date"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['job']->value['reg_date'],"%B %d, %Y");?>
</div>
                        <h3 class="news-section__text"><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</h3>
                        <span class="news-section__exp-date">Expiration Date: <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['job']->value['exp_date'],"%d/ %m/ %Y");?>
</span>
                    </a>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </section>
            <div class="row gutter-under-md-16">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatChildren']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                    <div class="col-sm-6 col-md-4 col-lg-3 mb-30">
                        <a class="news-2" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
">
                            <div class="news-2__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="" /></div>
                            <div class="news-2__overlay">
                                <div class="news-2__title"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</div>
                            </div>
                        </a>
                        <?php if ($_smarty_tpl->tpl_vars['cat']->value['arrCsr']) {?>
                            <div class="mt-30">
                                <ul class="news-list">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cat']->value['arrCsr'], 'csr', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['csr']->value) {
?>
                                        <li class="news-list__item"><a class="news-list__link" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_csr($_smarty_tpl->tpl_vars['csr']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['csr']->value['title'];?>
</a></li>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </ul>
                            </div>
                        <?php }?>
                    </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
        </section>
    </div>
<?php }
}
