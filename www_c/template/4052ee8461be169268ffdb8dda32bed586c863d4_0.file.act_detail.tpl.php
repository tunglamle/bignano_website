<?php
/* Smarty version 3.1.32, created on 2019-11-29 16:58:00
  from 'D:\Thuan\bignanotech\themes\template\csr\act_detail.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5de0eba854e4a4_49861003',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4052ee8461be169268ffdb8dda32bed586c863d4' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\csr\\act_detail.tpl',
      1 => 1575021475,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5de0eba854e4a4_49861003 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="page__content">
    <!-- main content-->
    <nav class="navigation">
        <div class="navigation__wrapper">
            <div class="navigation__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['parCat']->value);?>
""><?php echo $_smarty_tpl->tpl_vars['parCat']->value['name'];?>
</a></li>
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['curCat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</a></li>
                    <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['arrOneCsr']->value['title'];?>
</li>
                </ol>
            </div>
            <div class="navigation__lang">
                <div class="langs">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                            <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </div>
    </nav>
    <section class="section-2 py-20">
        <article class="post mb-40">
            <h1 class="post-title"><?php echo $_smarty_tpl->tpl_vars['arrOneCsr']->value['title'];?>
</h1>
            <div class="post-content">
                <?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOneCsr']->value['content']);?>

            </div>
        </article>
    </section>
</div><?php }
}
