<?php
/* Smarty version 3.1.32, created on 2020-03-16 16:11:57
  from '/home/bignao/public_html/themes/template/product/act_detail.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e6f42ddb7b630_23956749',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '27603ba1ad195400b97dd30ca34148945dd627a2' => 
    array (
      0 => '/home/bignao/public_html/themes/template/product/act_detail.tpl',
      1 => 1584349871,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e6f42ddb7b630_23956749 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/home/bignao/public_html/includes/smarty3/plugins/modifier.lang.php','function'=>'smarty_modifier_lang',),));
?><div class="page__content">
    <!-- main content-->
                                                    <section class="section-2">
        <div class="row">
            <div class="col-lg-6 mb-30 floating-container">
                <div class="floating">
                    <?php if ($_smarty_tpl->tpl_vars['arrOneProduct']->value['list_image']) {?>
                    <article class="preview-2">
                        <div class="preview-2__main">
                            <div class="preview-slider-2">
                                <?php if (is_array($_smarty_tpl->tpl_vars['arrOneProduct']->value['list_image'])) {?>
                                <div class="preview-slider-2__prev"><i class="fa fa-angle-left"></i></div>
                                <div class="preview-slider-2__next"><i class="fa fa-angle-right"></i></div>
                                <div class="preview-slider-2__pagination"></div>
                                <?php }?>
                                <div class="preview-slider-2__container swiper-container">
                                    <div class="swiper-wrapper">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrOneProduct']->value['list_image'], 'slide', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['slide']->value) {
?>
                                        <div class="swiper-slide">
                                            <a class="preview-slider-2__frame js-zoom" href="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['slide']->value;?>
" data-fancybox="product image">
                                                <?php if ($_smarty_tpl->tpl_vars['slide']->value) {?>
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['slide']->value;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['name'];?>
" />
                                                <?php }?>
                                            </a>
                                        </div>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="preview-2__thumbs">
                            <div class="thumb-slider-2">
                                <div class="thumb-slider-2__container swiper-container">
                                    <div class="swiper-wrapper">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrOneProduct']->value['list_image'], 'slide', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['slide']->value) {
?>
                                        <div class="swiper-slide">
                                            <div class="thumb-slider-2__frame">
                                                <?php if ($_smarty_tpl->tpl_vars['slide']->value) {?>
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['slide']->value;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['name'];?>
" />
                                                <?php }?>
                                            </div>
                                        </div>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <?php } elseif ($_smarty_tpl->tpl_vars['arrOneProduct']->value['embed'] && $_smarty_tpl->tpl_vars['arrOneProduct']->value['list_image'] != '') {?>
                    <iframe width="100%" height="350" src="https://www.youtube.com/embed/<?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['embed'];?>
" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <?php } else { ?>
                    <?php }?>
                </div>
            </div>
            <div class="col-lg-6 mb-30">
                <section class="pd-detail">
                    <h1 class="pd-detail__title"><?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['name'];?>
</h1>
                    <div class="pd-detail__desc">
                        <?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOneProduct']->value['sapo']);?>

                    </div>
                                                                                                        </section>
                <div class="pd-tools">
                    <div class="pd-tools__header">
                        <a class="pd-tools__item" href=".md-sendmail" data-toggle="modal"><i class="fa fa-envelope mr-2"></i><span>Send mail</span>
                        </a>
                        <a class="pd-tools__item" href="#!" onclick="myFunction()"><i class="fa fa-print mr-2"></i><span>Print</span>
                        </a>
                        <?php echo '<script'; ?>
>
                            function myFunction() {
                                window.print();
                            }
                        <?php echo '</script'; ?>
>
                    </div>
                    <nav class="pd-tools__social">
                        <a class="pd-tools__social-item" href="javascript:share_facebook();"><i class="fa fa-facebook"></i></a>
                        <a class="pd-tools__social-item" href="<?php echo $_smarty_tpl->tpl_vars['social']->value['youtube'];?>
"><i class="fa fa-youtube"></i></a>
                        <a class="pd-tools__social-item" href="https://zalo.me/<?php echo $_smarty_tpl->tpl_vars['social']->value['zalo'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_IMAGES']->value;?>
/icon-zalo.png" alt="icon zalo" /></a>
                    </nav>
                </div>
            </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['arrOneProduct']->value['content']) {?>
        <section class="pd-detail mb-30">
            <!-- <h2 class="pd-detail__title">Thông tin sản phẩm</h2>-->
            <div class="row">
                <div class="col-lg-8">
                    <div class="pd-detail__content">
                        <?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOneProduct']->value['content']);?>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="pd-download">
                        <?php if ($_smarty_tpl->tpl_vars['arrOneProduct']->value['embed']) {?>
                        <iframe width="100%" height="250" src="https://www.youtube.com/embed/<?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['embed'];?>
" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <?php }?>
                        
                        <!--   <div class="pd-download__subtitle">UNIVERSAL SORBENT SDS</div>
                            <div class="pd-download__text">Safety Data Sheet for Universal Meltblown Absorbent
                                Products
                            </div>-->
                        <?php if ($_smarty_tpl->tpl_vars['arrOneProduct']->value['uploads_document']) {?>
                        <div class="pd-download__title"><?php echo smarty_modifier_lang('download-catalogue');?>
</div>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrOneProduct']->value['uploads_document'], 'dowloads', false, 'i');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['dowloads']->value) {
?>
                        <a class="pd-download__btn" href="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['dowloads']->value;?>
"><i class="fa fa-arrow-circle-o-down mr-2"></i><span><?php echo $_smarty_tpl->tpl_vars['dowloads']->value;?>
</span></a>
                        <p></p>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        <?php }?>
                        
                    </div>
                </div>
            </div>
        </section>
        <?php }?>
        <section class="pd-detail">
            <h2 class="pd-detail__title">Sản phẩm liên quan</h2>
            <div class="row gutter-20">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListOtherProduct']->value, 'product', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['product']->value) {
?>
                <div class="col-xl-2 col-lg-3 col-md-4 col-6 mb-20">
                    <div class="product-3">
                        <a class="product-3__frame" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);?>
">
                            <?php if ($_smarty_tpl->tpl_vars['product']->value['image']) {?>
                            <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['product']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
" />
                            <?php }?>
                        </a>
                        <h3 class="product-3__title">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
</a></h3>
                        <!-- <div class="product-3__desc">
                                <?php echo htmlDecode($_smarty_tpl->tpl_vars['product']->value['sapo']);?>

                            </div>-->
                        <div class="product-3__footer">
                            <a class="product-3__link" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);?>
">
                                <span>Xem chi tiết</span>
                                <i class="fa fa-long-arrow-right ml-2"></i></a></div>
                    </div>
                </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
        </section>
    </section>
</div>
<div class="md-sendmail modal fade" tabindex="-1">
    <div class="modal-dialog modal-md">
        <form class="modal-content" id="save_email_form" onsubmit="ajax_email();return false;">
            <div class="modal-body md-sendmail__body">
                <button class="md-sendmail__close" data-dismiss="modal"></button>
                <div class="md-sendmail__title">Send mail:</div>
                <div class="media mb-20"><img class="md-sendmail__img" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['name'];?>
" />
                    <div class="md-sendmail__name media-body"><?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['name'];?>

                    </div>
                    <input type="hidden" name="product" value="<?php echo $_smarty_tpl->tpl_vars['arrOneProduct']->value['name'];?>
">
                </div>
                <div class="form-group">
                    <label>Họ và tên (*):</label>
                    <input class="form-control" name="name" type="text"/ required="">
                </div>
                <div class="form-group">
                    <label>Địa chỉ email (*):</label>
                    <input class="form-control" name="email" type="email" />
                </div>
                <div class="form-group">
                    <label>Số điện thoại (*):</label>
                    <input class="form-control" name="phone" type="text" />
                </div>
                <div class="text-muted">(*) Những trường bắt buộc</div>
                <div class="md-sendmail__footer">
                    <button class="md-sendmail__btn" type="submit">Gửi</button>
                    <span class="md-sendmail__btn-divider">hoặc</span>
                    <button class="md-sendmail__btn2" type="button" data-dismiss="modal">Huỷ bỏ</button>
                </div>
            </div>
        </form>
    </div>
</div><?php }
}
