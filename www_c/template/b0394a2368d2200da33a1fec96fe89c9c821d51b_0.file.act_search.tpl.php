<?php
/* Smarty version 3.1.32, created on 2019-11-28 15:57:01
  from 'D:\Thuan\bignanotech\themes\template\product\act_search.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5ddf8bdddac9c1_34461934',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b0394a2368d2200da33a1fec96fe89c9c821d51b' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\product\\act_search.tpl',
      1 => 1574931421,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ddf8bdddac9c1_34461934 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="page__content">
    <!-- main content-->
    <nav class="navigation">
        <div class="navigation__wrapper">
            <div class="navigation__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['arrProduct']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['arrProduct']->value['name'];?>
</a></li>
                    <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['arrSearch']->value['name'];?>
</li>
                </ol>
            </div>
            <div class="navigation__lang">
                <div class="langs">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                            <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </div>
    </nav>
    <section class="section-2 pt-20">
        <article class="post mb-40">
            <div class="post-subtitle">Products Search Results</div>
            <h1 class="post-title"><?php echo $_smarty_tpl->tpl_vars['arrSearch']->value['name'];?>
</h1>
            <span><b class="text-18"><?php echo $_smarty_tpl->tpl_vars['countSearchResult']->value;?>
</b> product</span>
            <div class="post-content">
                <h2 class="post-heading text-center" id="heading-1">Search Results</h2>
                <div class="accordion">
                    <div class="accordion__top"><a class="accordion__switch" href="#!">All open</a></div>
                     <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListSearchResult']->value, 'product', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['product']->value) {
?>
                    <div class="accordion__item">
                        <div class="accordion__header">
                            <h3 class="accordion__title"><a href="<?php if ($_smarty_tpl->tpl_vars['product']->value['link']) {
echo $_smarty_tpl->tpl_vars['product']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);
}?>"><?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
</a></h3>
                            <div class="accordion__toggle"></div>
                        </div>
                        <div class="accordion__body">
                            <div class="accordion__wrapper media">
                                <div class="accordion__left">
                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['list_image']) {?>
                                    <div class="image-slider">
                                        <div class="image-slider__container swiper-container">
                                            <div class="swiper-wrapper">
                                                <?php $_smarty_tpl->_assignInScope('arrSlide', explode(",",$_smarty_tpl->tpl_vars['product']->value['list_image']));?>
                                                 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrSlide']->value, 'slide', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['slide']->value) {
?>
                                                <div class="swiper-slide">
                                                    <div class="image-slider__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['slide']->value;?>
" alt="" /></div>
                                                </div>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                            </div>
                                        </div>
                                        <div class="image-slider__pagination"></div>
                                        <div class="image-slider__nav">
                                            <div class="image-slider__prev"><i class="fa fa-angle-left"></i></div>
                                            <div class="image-slider__next"><i class="fa fa-angle-right"></i></div>
                                        </div>
                                    </div>
                                        <?php } else { ?>
                                        <div class="image-slider__img"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['product']->value['image'];?>
" alt="" /></div>
                                    <?php }?>
                                    <div class="accordion__icons">
                                        <?php $_smarty_tpl->_assignInScope('arrIcon', explode(",",$_smarty_tpl->tpl_vars['product']->value['list_icon']));?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrIcon']->value, 'icon', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['icon']->value) {
?>
                                        <a class="accordion__icon" href="#!">
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['icon']->value;?>
" alt="" /></a>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <div class="accordion__content">
                                        <?php echo htmlDecode($_smarty_tpl->tpl_vars['product']->value['sapo']);?>

                                    </div>
                                    <div class="accordion__btns">
                                        <a class="accordion__btn button button--red" href="<?php if ($_smarty_tpl->tpl_vars['product']->value['link']) {
echo $_smarty_tpl->tpl_vars['product']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);
}?>">Product details</a>
                                        <a class="accordion__btn button button--dark" href="#!">Inquiry</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>

            </div>
        </article>

    </section>
</div><?php }
}
