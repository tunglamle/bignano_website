<?php
/* Smarty version 3.1.32, created on 2019-11-29 09:50:32
  from 'D:\Thuan\bignanotech\themes\template\about\act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5de08778ee2418_65865078',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8d99701f72613435a81e3584eff255847b29ab5e' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\about\\act_default.tpl',
      1 => 1574995832,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5de08778ee2418_65865078 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == 0) {?>
    <?php if ($_smarty_tpl->tpl_vars['curCat']->value['template'] == 'page') {?>
        <div class="page__content">
            <!-- main content-->
            <nav class="navigation">
                <div class="navigation__wrapper">
                    <div class="navigation__breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                            <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</li>
                        </ol>
                    </div>
                    <div class="navigation__lang">
                        <div class="langs">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                                <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                                    <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                                <?php }?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="banner">
                <div class="banner__wrapper">
                    <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
                </div><img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="" />
            </div>
            <section class="section-2">
                <div class="mb-40"><a class="banner-2" href="<?php if ($_smarty_tpl->tpl_vars['arrListCatChildren']->value[0]['link']) {
echo $_smarty_tpl->tpl_vars['arrListCatChildren']->value[0]['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['arrListCatChildren']->value[0]);
}?>">
                        <h2 class="banner-2__title"><?php echo $_smarty_tpl->tpl_vars['arrListCatChildren']->value[0]['name'];?>
</h2>
                        <div class="banner-2__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['arrListCatChildren']->value[0]['banner'];?>
" alt="" /></div>
                    </a>
                </div>
                <div class="row gutter-under-md-16">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatChildren']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value > 0) {?>
                        <div class="col-6 col-md-4 col-lg-3 mb-3 mb-sm-30">
                            <div class="about">
                                <a class="about__frame" href="<?php if ($_smarty_tpl->tpl_vars['cat']->value['link']) {
echo $_smarty_tpl->tpl_vars['cat']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);
}?>">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="" /></a>
                                <div class="about__body">
                                    <h3 class="about__title"><a href="<?php if ($_smarty_tpl->tpl_vars['cat']->value['link']) {
echo $_smarty_tpl->tpl_vars['cat']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);
}?>"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a></h3>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </section>
        </div>
        <?php } else { ?>
        <div class="page__content">
            <!-- main content-->
            <nav class="navigation">
                <div class="navigation__wrapper">
                    <div class="navigation__breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                            <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</li>
                        </ol>
                    </div>
                    <div class="navigation__lang">
                        <div class="langs">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                                <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                                    <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                                <?php }?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="banner">
                <div class="banner__wrapper">
                    <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
                </div><img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="" />
            </div>
            <section class="section-2">
                <div class="row gutter-under-md-16">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatChildren']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                        <div class="col-6 col-md-4 col-lg-3 mb-3 mb-sm-30">
                            <div class="about">
                                <a class="about__frame" href="<?php if ($_smarty_tpl->tpl_vars['cat']->value['link']) {
echo $_smarty_tpl->tpl_vars['cat']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);
}?>">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="" /></a>
                                <div class="about__body">
                                    <h3 class="about__title"><a href="<?php if ($_smarty_tpl->tpl_vars['cat']->value['link']) {
echo $_smarty_tpl->tpl_vars['cat']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);
}?>"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a></h3>
                                </div>
                            </div>
                        </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
                                                                                                            </section>
        </div>
    <?php }?>
    <?php } else { ?>
    <div class="page__content">
        <!-- main content-->
        <nav class="navigation">
            <div class="navigation__wrapper">
                <div class="navigation__breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                        <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['parCat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['parCat']->value['name'];?>
</a></li>
                        <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</li>
                    </ol>
                </div>
                <div class="navigation__lang">
                    <div class="langs">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                            <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                                <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                </div>
            </div>
        </nav>
        <section class="section-2 pt-20">
            <article class="post mb-40">
                <div class="post-subtitle"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
                <h1 class="post-title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</h1>
                <?php if ($_smarty_tpl->tpl_vars['countListAbout']->value > 1) {?>
                <ul class="post-nav">
                     <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListAbout']->value, 'about', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['about']->value) {
?>
                    <li class="post-nav__item"><a class="post-nav__link" href="#<?php echo $_smarty_tpl->tpl_vars['about']->value['slub'];?>
-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['about']->value['title'];?>
</a></li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </ul>
                <?php }?>
                <div class="post-content">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListAbout']->value, 'about', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['about']->value) {
?>
                        <h2 class="post-heading" id="<?php echo $_smarty_tpl->tpl_vars['about']->value['slub'];?>
-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><a href="#!"><?php echo $_smarty_tpl->tpl_vars['about']->value['title'];?>
</a></h2>
                        <?php echo htmlDecode($_smarty_tpl->tpl_vars['about']->value['content']);?>

                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </article>
        </section>
    </div>
<?php }
}
}
