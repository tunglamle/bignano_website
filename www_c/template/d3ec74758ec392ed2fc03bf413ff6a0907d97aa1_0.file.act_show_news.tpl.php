<?php
/* Smarty version 3.1.32, created on 2019-11-29 15:10:08
  from 'D:\Thuan\bignanotech\themes\template\articles\act_show_news.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5de0d260d28969_22002770',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd3ec74758ec392ed2fc03bf413ff6a0907d97aa1' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\articles\\act_show_news.tpl',
      1 => 1575015006,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5de0d260d28969_22002770 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'D:\\Thuan\\bignanotech\\includes\\smarty3\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?><div class="news-ajax-content">
    <?php if ($_smarty_tpl->tpl_vars['arrListArticles']->value[0] == '') {?>
        <span class="text-center d-block mt-3">Please wait for new "News".</span>
    <?php } else { ?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListArticles']->value, 'news', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['news']->value) {
?>
            <a class="news-section__item" href="<?php if ($_smarty_tpl->tpl_vars['news']->value['link']) {
echo $_smarty_tpl->tpl_vars['news']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_article($_smarty_tpl->tpl_vars['news']->value);
}?>">
                <div class="news-section__date"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['reg_date'],"%B %d, %Y");?>
</div>
                <div class="news-section__icon"><?php echo $_smarty_tpl->tpl_vars['news']->value['cat_name'];?>
</div>
                <h3 class="news-section__text"><?php echo $_smarty_tpl->tpl_vars['news']->value['title'];?>
</h3>
            </a>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    <?php }?>
</div><?php }
}
