<?php
/* Smarty version 3.1.32, created on 2020-03-17 17:34:46
  from '/var/www/html/bignanotech.com.vn/themes/template/_header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e70a7c6d73580_19674677',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b0aee51f7c8b772033f83712faa40a1b5a9b5d49' => 
    array (
      0 => '/var/www/html/bignanotech.com.vn/themes/template/_header.tpl',
      1 => 1584441220,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e70a7c6d73580_19674677 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/html/bignanotech.com.vn/includes/smarty3/plugins/modifier.lang.php','function'=>'smarty_modifier_lang',),));
?><!-- header-->
<header class="header">
    <div class="header__inner">
        <a class="header__logo" href="/">
            <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_logo'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_title'];?>
"></a>
        <a class="header__logo-2" href="/">
            <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_logo2'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_title'];?>
" />
        </a>
        <div class="header__elements">
            <form class="h-search" action="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
/search">
                <div class="input-group">
                    <label for="header-search" class="d-none"><?php echo smarty_modifier_lang('Search');?>
</label>
                    <input class="form-control" id="header-search" type="text" name="key" placeholder="<?php echo smarty_modifier_lang('search-our-site');?>
..." />
                    <div class="input-group-append">
                        <button class="input-group-text" type="submit">
                            <i class="fa fa-search"></i>
                            <span class="d-none"><?php echo smarty_modifier_lang('Search');?>
</span>
                        </button>
                    </div>
                </div>
            </form>
            <ul class="langs">
                <li class="langs__item"><a class="langs__link <?php if ($_smarty_tpl->tpl_vars['_LANG_ID']->value == 'vn') {?>active<?php }?>" href="?lang=vn">VI</a></li>
                <li class="langs__item"><a class="langs__link <?php if ($_smarty_tpl->tpl_vars['_LANG_ID']->value == 'en') {?>active<?php }?>" href="?lang=en">EN</a></li>
            </ul>
            <a class="header__element" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['link_email'];?>
">
                <i class="icon icon-envelope">
                    <span class="d-none">Contact</span>
                </i>
            </a>
            <div class="header__hotline">
                <div class="media">
                    <div class="media-body">
                    	<div class="d-flex justify-content-between">
                            <span class="mr-1">Hotline:</span>
                            <a href="tel:+84868939595">(+84) 879.808.080</a>
                        </div>
                        <div class="d-flex justify-content-between">
                            <span class="mr-1">For importer:</span>
                            <a href="tel:+84913560223">(+84) 913.560.223</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="header__menu">
         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListMainMenu']->value, 'menu', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['menu']->value) {
?>
        <a class="header__menu-link" href="<?php echo $_smarty_tpl->tpl_vars['menu']->value['href'];?>
">
            <span class="header__menu-text"><?php echo $_smarty_tpl->tpl_vars['menu']->value['title'];?>
</span>
        </a>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </nav>
</header>
<?php }
}
