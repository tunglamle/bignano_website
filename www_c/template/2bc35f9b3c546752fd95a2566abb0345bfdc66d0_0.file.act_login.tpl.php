<?php
/* Smarty version 3.1.32, created on 2020-04-08 20:37:20
  from '/var/www/html/bignanotech.com.vn/themes/template/account/act_login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e8dd390843eb4_90501406',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2bc35f9b3c546752fd95a2566abb0345bfdc66d0' => 
    array (
      0 => '/var/www/html/bignanotech.com.vn/themes/template/account/act_login.tpl',
      1 => 1584441220,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e8dd390843eb4_90501406 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container pt-40">
    <div class="row mb-30">
        <div class="col-xl-6 offset-xl-3">
            <div class="sign-in card border-primary">
                <div class="card-header bg-primary text-white">
                    <h2 class="card-title">Đăng nhập</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="modal-body" method="POST" id="fLogin" name="fLogin">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-white">
                                                <i class="fa fa-user fa-fw"></i>
                                            </span>
                                        </div>
                                        <input class="form-control" type="text" name="user_name" placeholder="Tên đăng nhập" required/>
                                    </div>
                                    <?php if ($_smarty_tpl->tpl_vars['arr_error']->value['user_name'] != '') {?><div class="text-danger"><?php echo $_smarty_tpl->tpl_vars['arr_error']->value['user_name'];?>
</div><?php }?>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-white">
                                                <i class="fa fa-key fa-fw"></i>
                                            </span>
                                        </div>
                                        <input class="form-control" type="password" name="user_pass" placeholder="Mật khẩu" required/>
                                    </div>
                                    <?php if ($_smarty_tpl->tpl_vars['arr_error']->value['user_pass'] != '') {?> <div class="text-danger"><?php echo $_smarty_tpl->tpl_vars['arr_error']->value['user_pass'];?>
</div><?php }?>
                                    <div class="form-text">
                                        <a class="text-muted js-switch-modal" href=".md-recovery">Quên mật khẩu?</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-block btn-primary" type="submit" value="Login" name="btnLogin">Đăng nhập</button>
                                </div>
                                <div class="form-group form-row">
                                    <div class="col-6">
                                        <a class="btn btn-block text-white btn-facebook" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_fbauth();?>
">
                                            <i class="fa fa-facebook mr-2"></i>
                                            <span>Facebook</span>
                                        </a>
                                    </div>
                                    <div class="col-6">
                                        <a class="btn btn-block text-white btn-google-plus" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_ggauth();?>
">
                                            <i class="fa fa-google mr-2"></i>
                                            <span>Google</span>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <a class="js-switch-modal" href=".md-register">ĐĂNG KÝ NGAY</a> nếu bạn chưa có tài khoản.
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
