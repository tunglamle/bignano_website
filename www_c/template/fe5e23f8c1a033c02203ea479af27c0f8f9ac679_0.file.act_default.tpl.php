<?php
/* Smarty version 3.1.32, created on 2020-02-17 14:03:58
  from '/home/bignao/public_html/themes/template/articles/act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e4a3aded247a4_18094238',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fe5e23f8c1a033c02203ea479af27c0f8f9ac679' => 
    array (
      0 => '/home/bignao/public_html/themes/template/articles/act_default.tpl',
      1 => 1581923038,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4a3aded247a4_18094238 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/home/bignao/public_html/includes/smarty3/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>

<div class="page__content">
    <!-- main content-->
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
        </div>
        <img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
"/>
    </div>
    <section class="section-2">
        <section class="news-section mb-40">
            <div class="news-section__header">
                <h2 class="news-section__title" id="news-title">All</h2>
                <div class="news-section__elements">
                    <span class="menu-news news-section__element active" onclick="showNewsType(999, this)" style="cursor: pointer">All</span>
                    <span class="menu-news news-section__element" onclick="showNewsType(0, this)" style="cursor: pointer">News</span>
                    <span class="menu-news news-section__element" onclick="showNewsType(1, this)" style="cursor: pointer">Job</span>
                </div>
            </div>
            <div class="news-type">
                <div class="news-type_content">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListArticles']->value, 'news', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['news']->value) {
?>
                    <a class="news-section__item"
                       href="<?php if ($_smarty_tpl->tpl_vars['news']->value['link']) {
echo $_smarty_tpl->tpl_vars['news']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_article($_smarty_tpl->tpl_vars['news']->value);
}?>">
                        <div class="news-section__date"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['reg_date'],"%B %d, %Y");?>
</div>
                        <div class="news-section__icon">
                            <?php if ($_smarty_tpl->tpl_vars['news']->value['news_type'] == 0) {?>
                                News
                            <?php } else { ?>
                                Job
                            <?php }?>
                        </div>
                        <h3 class="news-section__text"><?php echo $_smarty_tpl->tpl_vars['news']->value['title'];?>
</h3>
                    </a>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </section>
    </section>
</div>
<?php }
}
