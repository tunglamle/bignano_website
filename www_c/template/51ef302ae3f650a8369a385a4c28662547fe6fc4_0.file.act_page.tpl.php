<?php
/* Smarty version 3.1.32, created on 2019-11-30 11:46:07
  from 'D:\Thuan\bignanotech\themes\template\home\act_page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5de1f40fa46ad0_92600846',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '51ef302ae3f650a8369a385a4c28662547fe6fc4' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\home\\act_page.tpl',
      1 => 1575089166,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5de1f40fa46ad0_92600846 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="page__content">
    <!-- main content-->
    <nav class="navigation">
        <div class="navigation__wrapper">
            <div class="navigation__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                    <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['name'];?>
</li>
                </ol>
            </div>
            <div class="navigation__lang">
                <div class="langs">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                            <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </div>
    </nav>
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['name'];?>
</div>
        </div><img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['banner'];?>
" alt="" />
    </div>
    <section class="section-2">
        <article class="post">
            <div class="post-content">
                <h2 class="post-heading"><a href="#!"><?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['name'];?>
</a></h2>
                <?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOnePage']->value['content']);?>


            </div>
        </article>
    </section>
</div><?php }
}
