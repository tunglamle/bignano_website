<?php
/* Smarty version 3.1.32, created on 2020-02-05 09:12:57
  from '/home/bignao/public_html/themes/template/typical/act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e3a24a9b48843_38146829',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4597802d83e5942ae3f8c1f72d7161a401188139' => 
    array (
      0 => '/home/bignao/public_html/themes/template/typical/act_default.tpl',
      1 => 1580868775,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e3a24a9b48843_38146829 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="page__content">
    <!-- main content-->
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
        </div><img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="" />
    </div>
    <section class="section-2">
        <ul class="nav pd-tabs">
             <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrCatTypicalApplication']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
            <li class="nav-item">
                <a class="nav-link <?php if ($_smarty_tpl->tpl_vars['cat']->value['cat_id'] == $_smarty_tpl->tpl_vars['id_typical']->value) {?>active<?php }
if (!$_smarty_tpl->tpl_vars['id_typical']->value && $_smarty_tpl->tpl_vars['k']->value == 0) {?> active<?php }?>" href="#product-tab-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a></li>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </ul>
        <div class="tab-content">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrCatTypicalApplication']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
            <div class="tab-pane <?php if ($_smarty_tpl->tpl_vars['cat']->value['cat_id'] == $_smarty_tpl->tpl_vars['id_typical']->value) {?>active<?php }
if (!$_smarty_tpl->tpl_vars['id_typical']->value && $_smarty_tpl->tpl_vars['k']->value == 0) {?> active<?php }?>" id="product-tab-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">
                <div class="launching-slider">
                    <div class="launching-slider__pagination"></div>
                    <div class="swiper-container launching-slider__container">
                        <div class="swiper-wrapper">
                             <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cat']->value['arrTypical'], 'typical', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['typical']->value) {
?>
                            <div class="swiper-slide">
                                <div class="launching-slider__item">
                                    <div class="launching-slider__frame">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['typical']->value['image'];?>
" alt="" />
                                    </div>
                                    <div class="launching-slider__desc"> <?php echo $_smarty_tpl->tpl_vars['typical']->value['title'];?>
 </div>
                                </div>
                            </div>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
    </section>
</div><?php }
}
