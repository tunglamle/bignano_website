<?php
/* Smarty version 3.1.32, created on 2020-03-17 17:34:46
  from '/var/www/html/bignanotech.com.vn/themes/template/home/act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e70a7c6dddf12_95251094',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '341c3d509d0e5a02ee102311b2f17e93a0c5e3c6' => 
    array (
      0 => '/var/www/html/bignanotech.com.vn/themes/template/home/act_default.tpl',
      1 => 1584441220,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e70a7c6dddf12_95251094 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/html/bignanotech.com.vn/includes/smarty3/plugins/modifier.lang.php','function'=>'smarty_modifier_lang',),));
?><h1 class="d-none"><?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_title'];?>
</h1>
<div class="page__content">
    <div class="home-movie">
        <div class="home-movie__body">
            <div class="embed-responsive embed-responsive-16by9 home-video">
                <video class="embed-responsive-item" src="<?php echo $_smarty_tpl->tpl_vars['URL_MEDIA']->value;?>
/video5.mp4" poster="" autoplay="autoplay"
                       muted="muted" playsinline="playsinline" loop="loop">
            </div>
        </div>
        <div class="home-movie__aside">
            <div class="home-movie__close"><i class="fa fa-angle-double-right"></i></div>
            <div class="home-movie__aside-item">
                <?php echo htmlDecode($_smarty_tpl->tpl_vars['_CONFIG']->value['content_slider']);?>

                <!-- div class="product-5">
                    <div class="product-5__part">
                        <div class="product-5__frame">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['right_slide_link'];?>
" class="text-default">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['right_slide_image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['right_slide_alt'];?>
"/>
                            </a>
                        </div>
                    </div>
                    <div class="product-5__part">
                        <div class="product-5__body">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListRightMenu']->value, 'right', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['right']->value) {
?>
                                <div class="product-5__item">
                                    <a class="product-5__link text-uppercase" href="<?php echo $_smarty_tpl->tpl_vars['right']->value['href'];?>
">
                                        <i class="fa fa-angle-right mr-2"></i>
                                        <span><?php echo $_smarty_tpl->tpl_vars['right']->value['title'];?>
</span></a>
                                </div>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <div style="border-top: 1px solid white"></div>
    <!-- notification-->
    <section class="section" id="products">
        <h2 class="section__title"><?php echo smarty_modifier_lang('Product');?>
</h2>
        <div class="section__body">
            <div class="row gutter-under-sm-16 gutter-over-xl-60">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrCatProductHome']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                    <div class="col-6 col-md-4 mb-20 mb-md-40">
                        <div class="about">
                            <a class="about__frame"
                               href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
"/>
                            </a>
                            <div class="about__body">
                                <h3 class="about__title text-center">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a>
                                </h3>
                            </div>
                        </div>
                                            </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
        </div>
    </section>
    <section class="section section--gray">
        <h2 class="section__title"><?php echo smarty_modifier_lang('typical-application');?>
</h2>
        <div class="section__body">
            <div class="row gutter-under-sm-16">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrCatTypicalApplication']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                    <div class="col-6 col-lg-3 mb-3 mb-sm-30">
                        <div class="application">
                            <a class="application__frame"
                               href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['arrTypical']->value);?>
?id_typical=<?php echo $_smarty_tpl->tpl_vars['cat']->value['cat_id'];?>
">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
"/></a>
                            <div class="application__desc">
                                <?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>

                            </div>
                        </div>
                    </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
        </div>
    </section>
    <section class="section">
        <h2 class="section__title"><?php echo smarty_modifier_lang('about_us');?>
</h2>
        <div class="section__body">
            <div class="row gutter-under-md-16 gutter-over-xl-60">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrCatAboutHome']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                    <div class="col-sm-6 col-lg-4 col-lg-4 mb-20">
                        <div class="about about--2">
                                                        <div class="about__body">
                                <h3 class="about__title text-center">
                                    <a href="<?php if ($_smarty_tpl->tpl_vars['cat']->value['link']) {
echo $_smarty_tpl->tpl_vars['cat']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);
}?>"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
        </div>
    </section>
</div>
<?php }
}
