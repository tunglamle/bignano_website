<?php
/* Smarty version 3.1.32, created on 2020-02-14 10:04:40
  from '/home/bignao/public_html/themes/template/articles/act_show_news_type.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e460e48af6a60_66248142',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd4462b2645fcc8c1dd7507279299f5edb602088c' => 
    array (
      0 => '/home/bignao/public_html/themes/template/articles/act_show_news_type.tpl',
      1 => 1581649475,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e460e48af6a60_66248142 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/home/bignao/public_html/includes/smarty3/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?><div class="news-type_content">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListArticles']->value, 'news', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['news']->value) {
?>
        <a class="news-section__item"
           href="<?php if ($_smarty_tpl->tpl_vars['news']->value['link']) {
echo $_smarty_tpl->tpl_vars['news']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_article($_smarty_tpl->tpl_vars['news']->value);
}?>">
            <div class="news-section__date"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['reg_date'],"%B %d, %Y");?>
</div>
            <div class="news-section__icon">
                <?php if ($_smarty_tpl->tpl_vars['news']->value['news_type'] == 0) {?>
                    News
                <?php } else { ?>
                    Job
                <?php }?>
            </div>
            <h3 class="news-section__text"><?php echo $_smarty_tpl->tpl_vars['news']->value['title'];?>
</h3>
        </a>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div><?php }
}
