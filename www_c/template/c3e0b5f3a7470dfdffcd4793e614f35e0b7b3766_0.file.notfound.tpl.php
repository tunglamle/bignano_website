<?php
/* Smarty version 3.1.32, created on 2020-03-20 17:33:22
  from '/var/www/html/bignanotech.com.vn/themes/template/notfound.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e749bf2421a07_21340993',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c3e0b5f3a7470dfdffcd4793e614f35e0b7b3766' => 
    array (
      0 => '/var/www/html/bignanotech.com.vn/themes/template/notfound.tpl',
      1 => 1584441221,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e749bf2421a07_21340993 (Smarty_Internal_Template $_smarty_tpl) {
?>
    <style>
        .notfound-wrap {
            padding: 50px 0;
            text-align: center;
        }

        .notfound-des {
            padding: 10px 0;
        }

        .notfound-title {
            font-weight: bold;
        }

        .notfound-link {
            background: #0082C4;
            color: white;
            padding: 10px 15px;
            border-radius: 8px;
            margin: 0 15px;
        }

        .notfound-box {
            margin-top: 30px;
        }
    </style>

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="notfound-wrap my-30" style="background-color: #edf2f2;">
                <h1 class="text-center text-uppercase notfound-title"
                    style="color:#0082C4;"><?php echo $_smarty_tpl->tpl_vars['core']->value->getLang("PAGE NOT FOUND... !!!");?>
 !!!</h1>
                <div class="notfound-des">
                    <?php echo $_smarty_tpl->tpl_vars['core']->value->getLang("SORRY NOT FOUND...");?>

                </div>
                <div class="notfound-box">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
" class="text-default notfound-link">
                        <?php echo $_smarty_tpl->tpl_vars['core']->value->getLang("HOME PAGE");?>

                    </a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
/lien-he" class="text-default notfound-link">
                        <?php echo $_smarty_tpl->tpl_vars['core']->value->getLang("CONTACT");?>

                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
<?php }
}
