<?php
/* Smarty version 3.1.32, created on 2020-03-18 15:43:31
  from '/var/www/html/bignanotech.com.vn/themes/template/articles/act_detail.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e71df339c50b1_05062527',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9682d6e98f7cbe04ad7cf9652e88744f01b0e0c7' => 
    array (
      0 => '/var/www/html/bignanotech.com.vn/themes/template/articles/act_detail.tpl',
      1 => 1584441220,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e71df339c50b1_05062527 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="page__content">
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
        </div><img class="banner__bg" src="<?php if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == 0) {
echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];
} else {
echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['parCat']->value['banner'];
}?>" alt="" />
    </div>
    <section class="section-2">
        <article class="post mb-40">
            <h1 class="post-title"><?php echo $_smarty_tpl->tpl_vars['arrOneArticle']->value['title'];?>
</h1>
            <div class="post-content">
                <?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOneArticle']->value['content']);?>

            </div>
        </article>
    </section>
</div>
<?php }
}
