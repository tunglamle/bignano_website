<?php
/* Smarty version 3.1.32, created on 2019-11-29 16:57:55
  from 'D:\Thuan\bignanotech\themes\template\csr\act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5de0eba3a7e446_61033846',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cbf223cceb829ddf1b47a5d889ae50ce6de8b9fb' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\csr\\act_default.tpl',
      1 => 1575021475,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5de0eba3a7e446_61033846 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'D:\\Thuan\\bignanotech\\includes\\smarty3\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == 0) {?>
    <div class="page__content">
    <!-- main content-->
    <nav class="navigation">
        <div class="navigation__wrapper">
            <div class="navigation__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                    <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</li>
                </ol>
            </div>
            <div class="navigation__lang">
                <div class="langs">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                            <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </div>
    </nav>
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
        </div><img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="" />
    </div>
    <section class="section-2">
        <section class="news-section mb-40">
            <div class="news-section__header">
                <h2 class="news-section__title">CSR News</h2>
                <div class="news-section__elements">
                    <a class="news-section__element news-section__element--rss" href="#!">RSS</a>
                    <a class="news-section__element news-section__element--arrow" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['arrCatNewsCsr']->value);?>
">CSR News Archives</a></div>
            </div>
             <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListNewsCsr']->value, 'news', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['news']->value) {
?>
            <a class="news-section__item" href="<?php if ($_smarty_tpl->tpl_vars['news']->value['link']) {
echo $_smarty_tpl->tpl_vars['news']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_article($_smarty_tpl->tpl_vars['news']->value);
}?>">
                <div class="news-section__date"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['reg_date'],"%B %d, %Y");?>
</div>
                <div class="news-section__icon"><?php echo $_smarty_tpl->tpl_vars['news']->value['cat_name'];?>
</div>
                <h3 class="news-section__text"><?php echo $_smarty_tpl->tpl_vars['news']->value['title'];?>
</h3>
            </a>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </section>
        <div class="row gutter-under-md-16">
             <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatChildren']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
            <div class="col-sm-6 col-md-4 col-lg-3 mb-30">
                <a class="news-2" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
">
                    <div class="news-2__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="" /></div>
                    <div class="news-2__overlay">
                        <div class="news-2__title"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</div>
                    </div>
                </a>
                <?php if ($_smarty_tpl->tpl_vars['cat']->value['arrCsr']) {?>
                <div class="mt-30">
                    <ul class="news-list">
                         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cat']->value['arrCsr'], 'csr', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['csr']->value) {
?>
                        <li class="news-list__item"><a class="news-list__link" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_csr($_smarty_tpl->tpl_vars['csr']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['csr']->value['title'];?>
</a></li>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </ul>
                </div>
                <?php }?>
            </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
    </section>
    <section class="relate">
        <div class="relate__body">
            <h2 class="relate__title">Related information</h2>
            <div class="relate__grid">
                 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListRelatedCsr']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                <div class="relate__col"><a class="relate__frame" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['link'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['adver']->value['image'];?>
" alt=""></a></div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
        </div>
    </section>
    <section class="partner-section">
        <div class="partner-section__body"><a class="partner-section__btn" href="#!">Ethical Reporting Desk</a>
            <div class="partner-section__list">
                <ul class="partners">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListPartner']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                    <li class="partners__item"><a class="partners__link" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['link'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['adver']->value['image'];?>
" alt=""></a></li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </ul>
            </div>
        </div>
    </section>
</div>
<?php } else { ?>
    <div class="page__content">
        <!-- main content-->
        <nav class="navigation">
            <div class="navigation__wrapper">
                <div class="navigation__breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                        <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['parCat']->value);?>
""><?php echo $_smarty_tpl->tpl_vars['parCat']->value['name'];?>
</a></li>
                        <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</li>
                    </ol>
                </div>
                <div class="navigation__lang">
                    <div class="langs">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                            <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                                <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                </div>
            </div>
        </nav>
        <section class="section-2 py-20">
            <article class="post mb-40">
                <h1 class="post-title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</h1>
                <div class="post-content">
                    <?php echo htmlDecode($_smarty_tpl->tpl_vars['curCat']->value['des']);?>

                </div>
            </article>
        </section>
    </div>
<?php }
}
}
