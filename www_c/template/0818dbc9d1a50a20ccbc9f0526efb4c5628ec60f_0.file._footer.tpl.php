<?php
/* Smarty version 3.1.32, created on 2019-11-29 09:24:26
  from 'D:\Thuan\bignanotech\themes\template\_footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5de0815ae3e5a3_08571945',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0818dbc9d1a50a20ccbc9f0526efb4c5628ec60f' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\_footer.tpl',
      1 => 1574994265,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5de0815ae3e5a3_08571945 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- footer-->
<div class="sitemap">
    <div class="sitemap__wrapper">
        <ul class="sitemap__list">
             <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListMainMenu']->value, 'menu', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['menu']->value) {
?>
                 <?php if ($_smarty_tpl->tpl_vars['menu']->value['children']) {?>
                     <li class="sitemap__item sitemap__dropdown">
                         <a class="sitemap__link sitemap__dropdown-toggle <?php if ($_smarty_tpl->tpl_vars['k']->value == 0) {?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['menu']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['menu']->value['title'];?>
</a>
                         <div class="sitemap__dropdown-menu"  <?php if ($_smarty_tpl->tpl_vars['k']->value == 0) {?> style="display: block" <?php }?>><a class="sitemap__dropdown-title" href="<?php echo $_smarty_tpl->tpl_vars['menu']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['menu']->value['title'];?>
</a>
                             <ul class="sitemap__sub">
                                  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['menu']->value['children'], 'submenu', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['submenu']->value) {
?>
                                 <li class="sitemap__sub-item">
                                     <a class="sitemap__sub-link" href="<?php if ($_smarty_tpl->tpl_vars['submenu']->value['link']) {
echo $_smarty_tpl->tpl_vars['submenu']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['submenu']->value['href'];
}?>"><?php echo $_smarty_tpl->tpl_vars['submenu']->value['title'];?>
</a>
                                 </li>
                                 <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                             </ul>
                         </div>
                     </li>
                     <?php } else { ?>
                     <li class="sitemap__item"><a class="sitemap__link" href="<?php echo $_smarty_tpl->tpl_vars['menu']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['menu']->value['title'];?>
</a></li>
                 <?php }?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </ul>
        <nav class="sitemap__nav">
            <a class="sitemap__nav-link" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['arrNews']->value);?>
">News</a>
            <a class="sitemap__nav-link" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['arrJobCareers']->value);?>
">Jobs & Careers</a>
        </nav>
    </div>
</div>

<footer class="footer">
    <div class="footer__wrapper">
        <div class="footer__content">
            <ul class="footer__nav">
                 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListBottomMenu']->value, 'menu', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['menu']->value) {
?>
                <li class="footer__nav-item">
                    <a class="footer__nav-link" href="<?php echo $_smarty_tpl->tpl_vars['menu']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['menu']->value['title'];?>
</a>
                </li>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </ul>
            <div class="footer__copyright"><?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['copyright'];?>
</div>
        </div>
        <nav class="footer__elements">
            <a class="footer__element" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
/contact"><i class="icon icon-envelope"></i></a>
            <a class="footer__element js-md-lang" href="#!"><i class="icon icon-globe"></i></a></nav>
    </div><a class="footer__btn js-movetop" href="#!"></a>
</footer><?php }
}
