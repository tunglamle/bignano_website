<?php
/* Smarty version 3.1.32, created on 2019-11-27 17:28:40
  from 'D:\Thuan\bignanotech\themes\template\_header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5dde4fd82bc2d6_51421959',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f33f693c307a5e6f9e166830d7ba965ddd0968f7' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\_header.tpl',
      1 => 1574850518,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dde4fd82bc2d6_51421959 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- header-->
<header class="header">
    <div class="header__inner">
        <a class="header__logo" href="/">
            <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_logo'];?>
" alt="" /></a>
        <a class="header__logo-2" href="/">
            <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_logo2'];?>
" alt="" />
        </a>
        <div class="header__elements"><a class="header__element" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
/contact"><i class="icon icon-envelope"></i></a>
            <a class="header__element js-md-lang" href="#!"><i class="icon icon-globe"></i></a></div>
    </div>
    <div class="md-lang">
        <div class="md-lang__wrapper">
            <div class="md-lang__container">
                <div class="md-lang__header">
                    <div class="md-lang__title">TEIJIN Corporate Site</div>
                    <div class="md-lang__close"></div>
                </div>
                <div class="md-lang__nav">
                     <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                      <a class="md-lang__btn" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['link'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </div>
    </div>
    <nav class="header__menu">
         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListMainMenu']->value, 'menu', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['menu']->value) {
?>
        <a class="header__menu-link" href="<?php echo $_smarty_tpl->tpl_vars['menu']->value['href'];?>
">
            <span class="header__menu-text"><?php ob_start();
echo $_smarty_tpl->tpl_vars['menu']->value['title'];
$_prefixVariable1 = ob_get_clean();
echo $_prefixVariable1;?>
</span>
        </a>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </nav>
</header><?php }
}
