<?php
/* Smarty version 3.1.32, created on 2020-03-17 19:05:08
  from '/var/www/html/bignanotech.com.vn/themes/template/for_investor/act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e70bcf4aa7745_32492014',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8493e227f33d88fe763e53387f5095ac170f0c89' => 
    array (
      0 => '/var/www/html/bignanotech.com.vn/themes/template/for_investor/act_default.tpl',
      1 => 1584441220,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e70bcf4aa7745_32492014 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == 0) {?>
    <div class="page__content">

        <div class="banner">
            <div class="banner__wrapper">
                <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
            </div>
            <img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt=""/>
        </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <div class="container">
            <div class="for_investor_des mt-40">
                <?php echo htmlDecode($_smarty_tpl->tpl_vars['curCat']->value['des']);?>

            </div>
        </div>
        <section class="partner-section pt-0">
            <div class="partner-section__body">
                <ul class="partners">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListPartner']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                        <li class="partners__item"><a class="partners__link" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['link'];?>
"><img
                                        src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['adver']->value['image'];?>
" alt=""></a></li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </ul>
            </div>
        </section>
    </div>
<?php } else { ?>
    <div class="page__content">
        <!-- main content-->
                <section class="section-2 py-20">
            <article class="post mb-40">
                <h1 class="post-title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</h1>
                <div class="post-content">
                    <?php echo htmlDecode($_smarty_tpl->tpl_vars['curCat']->value['des']);?>

                </div>
            </article>
        </section>
    </div>
<?php }
}
}
