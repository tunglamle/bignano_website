<?php
/* Smarty version 3.1.32, created on 2020-03-16 13:33:53
  from '/home/bignao/public_html/themes/template/product/act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e6f1dd170f564_94504205',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5e8ab7e08ab03c886f1c265f4b691bdca5a45bdd' => 
    array (
      0 => '/home/bignao/public_html/themes/template/product/act_default.tpl',
      1 => 1584340397,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e6f1dd170f564_94504205 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/home/bignao/public_html/includes/smarty3/plugins/modifier.lang.php','function'=>'smarty_modifier_lang',),));
if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == 0) {?>
<div class="page__content">
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['curCat']->value['banner']) {?>
        <img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
" />
        <?php }?>
    </div>
    <section class="section-2">
        <!--  <h2 class="heading">Product Search</h2> -->
        <ul class="nav tabs">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCat']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
            <li class="nav-item">
                <?php if ($_smarty_tpl->tpl_vars['cat']->value['is_show_logo']) {?>
                <a class="nav-link" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a></li>
            <?php } else { ?>
            <a class="nav-link <?php if ($_smarty_tpl->tpl_vars['cat']->value['cat_id'] == $_smarty_tpl->tpl_vars['show']->value) {?>active<?php }
if (!$_smarty_tpl->tpl_vars['show']->value && $_smarty_tpl->tpl_vars['k']->value == 0) {?>active<?php }?>" href="#product-tab-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a></li>
            <?php }?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </ul>
        <div class="tab-content">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCat']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
            <div class="tab-pane <?php if ($_smarty_tpl->tpl_vars['cat']->value['cat_id'] == $_smarty_tpl->tpl_vars['show']->value) {?>active<?php }
if (!$_smarty_tpl->tpl_vars['show']->value && $_smarty_tpl->tpl_vars['k']->value == 0) {?> active<?php }?>" id="product-tab-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">
                <div class="row gutter-under-sm-16">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cat']->value['arrCat'], 'subcat', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['subcat']->value) {
?>
                    <div class="col-6 col-md-3 mb-20 mb-md-40">
                        <div class="about">
                            <a class="about__frame" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['subcat']->value);?>
">
                                <?php if ($_smarty_tpl->tpl_vars['subcat']->value['image']) {?>
                                <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['subcat']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['subcat']->value['name'];?>
" />
                                <?php }?>
                            </a>
                            <div class="about__body">
                                <h3 class="about__title">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['subcat']->value);?>
"><?php if ($_smarty_tpl->tpl_vars['subcat']->value['name']) {
echo $_smarty_tpl->tpl_vars['subcat']->value['name'];
} else { ?>Bignanotech<?php }?></a>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['curCat']->value['phone']) {?>
        <div class="mt-30 p-30 p-lg-40 bg-light text-18 text-center">
            Contact: <a class="text-default phone" href="tel:<?php echo $_smarty_tpl->tpl_vars['curCat']->value['phone'];?>
"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['phone'];?>
</a>
        </div>
        <?php }?>
    </section>
</div>
<?php } elseif ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == $_smarty_tpl->tpl_vars['arrProduct']->value['cat_id'] && $_smarty_tpl->tpl_vars['curCat']->value['is_show_logo'] == 0) {?>
<div class="page__content">
    <!-- main content-->
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['curCat']->value['banner']) {?>
        <img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
" />
        <?php }?>
    </div>
    <section class="section-2">
        <h2 class="heading"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</h2>
        <div class="row gutter-under-sm-16">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatByCurCat']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
            <div class="col-6 col-md-3 mb-20 mb-md-40">
                <div class="about">
                    <a class="about__frame" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
" />
                    </a>
                    <div class="about__body">
                        <h3 class="about__title">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a></h3>
                    </div>
                </div>
            </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['curCat']->value['phone']) {?>
        <div class="mt-30 p-30 p-lg-40 bg-light text-18 text-center">
            Contact: <a class="text-default phone" href="tel:<?php echo $_smarty_tpl->tpl_vars['curCat']->value['phone'];?>
"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['phone'];?>
</a>
        </div>
        <?php }?>
    </section>
</div>
<?php } else {
if ($_smarty_tpl->tpl_vars['curCat']->value['is_show_logo'] == 1) {?>
<div class="container mt-40">
    <section class="show_logo_text">
        <?php echo htmlDecode($_smarty_tpl->tpl_vars['curCat']->value['des']);?>

    </section>
    <section class="partner-section pt-0">
        <div class="partner-section__body">
            <ul class="partners">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListPartner']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                <li class="partners__item"><a class="partners__link" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['link'];?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['adver']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
"></a></li>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </ul>
        </div>
    </section>
</div>
<?php } else { ?>
<div class="page__content">
    <!-- main content-->
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['curCat']->value['banner']) {?>
        <img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
" />
        <?php }?>
    </div>
    <section class="section-2 pt-20">
        <ul class="nav pd-tabs my-30">
             <?php if ($_smarty_tpl->tpl_vars['arrListProductNewLaunching']->value) {?>
            <li class="nav-item"><a class="nav-link active" href="#pd-tab-0" data-toggle="tab">NEW LAUNCHING</a>
            </li>
            <?php }?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatProduct']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
            <li class="nav-item">
                <a class="nav-link" href="#pd-tab-<?php echo $_smarty_tpl->tpl_vars['k']->value+2;?>
" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a>
            </li>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </ul>
        <div class="tab-content mb-40">
            <?php if ($_smarty_tpl->tpl_vars['arrListProductNewLaunching']->value) {?>
            <div class="tab-pane active" id="pd-tab-0">
                <div class="launching-slider">
                    <div class="launching-slider__pagination"></div>
                    <div class="swiper-container launching-slider__container">
                        <div class="swiper-wrapper">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListProductNewLaunching']->value, 'product', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['product']->value) {
?>
                            <div class="swiper-slide">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);?>
" class="text-default">
                                    <div class="launching-slider__item">
                                        <div class="launching-slider__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['product']->value['image_newlaunching'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
" />
                                        </div>
                                        <div class="launching-slider__desc"><?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
</div>
                                    </div>
                                </a>
                            </div>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatProduct']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
            <div class="tab-pane" id="pd-tab-<?php echo $_smarty_tpl->tpl_vars['k']->value+2;?>
">
                <article class="post mb-40">
                    <div class="product-intro media">
                        <div class="product-intro__left">
                            <?php if ($_smarty_tpl->tpl_vars['cat']->value['image']) {?>
                            <img class="d-block w-100" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
">
                            <?php }?>
                        </div>
                        <div class="product-intro__body media-body">
                            <?php echo htmlDecode($_smarty_tpl->tpl_vars['cat']->value['des']);?>

                            <?php if ($_smarty_tpl->tpl_vars['cat']->value['catalouge']) {?>
                            <div class="pd-download ml-auto" style="max-width: 400px;">
                                <div class="pd-download__title"><?php echo smarty_modifier_lang('download-catalogue');?>
</div>
                                <a class="pd-download__btn" href="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['catalouge'];?>
" download><i class="fa fa-arrow-circle-o-down mr-2"></i><span><?php echo smarty_modifier_lang('download-catalogue');?>
</span></a>
                                <p></p>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </article>
                <?php if ($_smarty_tpl->tpl_vars['cat']->value['arrProduct'][0]['product_id'] > 0) {?>
                <div class="accordion">
                    <div class="accordion__top">
                        <a class="accordion__switch active" href="#!"><?php echo smarty_modifier_lang('all-open');?>
</a>
                    </div>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cat']->value['arrProduct'], 'product', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['product']->value) {
?>
                    <div class="accordion__item">
                        <div class="accordion__header">
                            <h3 class="accordion__title">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
</a></h3>
                            <div class="accordion__toggle active"></div>
                        </div>
                        <div class="accordion__body" style="display: block">
                            <div class="accordion__wrapper media">
                                <div class="accordion__left">
                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['image']) {?>
                                    <img class="d-block w-100" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['product']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
" />
                                    <?php }?>
                                </div>
                                <div class="media-body">
                                    <div class="accordion__content">
                                        <?php echo htmlDecode($_smarty_tpl->tpl_vars['product']->value['sapo']);?>

                                    </div>
                                    <div class="accordion__btns">
                                        <a class="accordion__btn button button--red button--sm" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);?>
"><?php echo smarty_modifier_lang('product-details');?>
</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
                <?php }?>
            </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
    </section>
</div>
<?php }
}
}
}
