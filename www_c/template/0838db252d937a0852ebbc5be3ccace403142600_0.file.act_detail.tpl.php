<?php
/* Smarty version 3.1.32, created on 2019-12-06 11:24:22
  from '/home/bignao/public_html/themes/template/for_investor/act_detail.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5de9d7f6a91112_00225754',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0838db252d937a0852ebbc5be3ccace403142600' => 
    array (
      0 => '/home/bignao/public_html/themes/template/for_investor/act_detail.tpl',
      1 => 1575347231,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5de9d7f6a91112_00225754 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="page__content">
    <!-- main content-->
    <nav class="navigation">
        <div class="navigation__wrapper">
            <div class="navigation__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['parCat']->value);?>
""><?php echo $_smarty_tpl->tpl_vars['parCat']->value['name'];?>
</a></li>
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['curCat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</a></li>
                    <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['arrOneFor_investor']->value['title'];?>
</li>
                </ol>
            </div>
            <div class="navigation__lang">
                <div class="langs">
                    <a class="md-lang__btn" href="?lang=vn">Tiếng Việt</a>
                    <a class="md-lang__btn" href="?lang=en">Tiếng Anh</a>
                </div>
            </div>
        </div>
    </nav>
    <section class="section-2 py-20">
        <article class="post mb-40">
            <h1 class="post-title"><?php echo $_smarty_tpl->tpl_vars['arrOneFor_investor']->value['title'];?>
</h1>
            <div class="post-content">
                <?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOneFor_investor']->value['content']);?>

            </div>
        </article>
    </section>
</div><?php }
}
