<?php
/* Smarty version 3.1.32, created on 2019-11-26 14:52:44
  from 'D:\Thuan\bignanotech\themes\template\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5ddcd9cc26b146_15322648',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '222fadb3ce6e23cbfa89ed305987c1fdc7e10bac' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\index.tpl',
      1 => 1574754763,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_header.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5ddcd9cc26b146_15322648 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="vi">
<head>
    <title><?php if ($_smarty_tpl->tpl_vars['_CONFIG']->value['page_title']) {
echo $_smarty_tpl->tpl_vars['_CONFIG']->value['page_title'];
} else {
echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_title'];
}?></title>
    <!-- REQUIRED meta tags -->
    <meta charset="utf-8"/>
    <meta name="viewport"
          content="width=device-width, height=device-height, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <!-- Favicon tag -->
    <?php if ($_smarty_tpl->tpl_vars['_CONFIG']->value['site_favicon'] != '') {?>
        <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_favicon'];?>
" type="image/x-icon"/>
        <link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_favicon'];?>
" type="image/x-icon">
    <?php } else { ?>
        <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_favicon'];?>
" type="image/x-icon">
    <?php }?>
    <!-- SEO meta tags -->
    <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['page_keywords'];?>
"/>
    <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['page_description'];?>
"/>
    <meta name="author" content="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_title'];?>
"/>
    <meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['thumb'];?>
"/>
    <meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['title'];?>
"/>
    <meta property="og:description" content="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['description'];?>
"/>
    <meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['url'];?>
"/>
    <meta property="og:type" content="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['type'];?>
"/>
    <!-- Social meta tags -->
    <?php if ($_smarty_tpl->tpl_vars['og']->value['title'] != '') {?>
        <meta name="DC.title" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['title'];?>
"/>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['og']->value['fbadmin'] != '') {?>
        <meta property="fb:admins" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['fbadmin'];?>
"/>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['og']->value['url'] != '') {?>
        <meta itemprop="url" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['url'];?>
"/>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['og']->value['image'] != '') {?>
        <meta itemprop="image" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['image'];?>
"/>
        <meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['image'];?>
"/>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['og']->value['title'] != '') {?>
        <meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['title'];?>
"/>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['og']->value['description'] != '') {?>
        <meta property="og:description" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['description'];?>
"/>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['og']->value['type'] != '') {?>
        <meta property="og:type" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['type'];?>
"/>
    <?php }?>
    <meta property="og:locale" content="vi_vn">
    <?php if ($_smarty_tpl->tpl_vars['og']->value['published'] != '') {?>
        <meta property="article:published_time" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['published'];?>
"/>
        <meta property="article:modified_time" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['modified'];?>
"/>
        <meta property="article:author" content="<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['site_name'];?>
"/>
        <meta property="article:section" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['section'];?>
"/>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['og']->value['tag'], 'tag', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['tag']->value) {
?>
            <meta property="article:tag" content="<?php echo $_smarty_tpl->tpl_vars['tag']->value;?>
"/>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        <meta name="twitter:card" content="summary"/>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['og']->value['site_name'] != '') {?>
        <meta property="og:site_name" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['site_name'];?>
"/>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['og']->value['url'] != '') {?>
        <meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['url'];?>
"/>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['og']->value['alternate1'] != '') {?>
        <link rel="alternate" href="<?php echo $_smarty_tpl->tpl_vars['og']->value['alternate1'];?>
" media="handheld"/>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['og']->value['canonical']) {?>
        <link rel="canonical" href="<?php echo $_smarty_tpl->tpl_vars['og']->value['canonical'];?>
"/>
    <?php }?>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['URL_VENDOR']->value;?>
/font-awesome-4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['URL_VENDOR']->value;?>
/swiper/swiper.min.css"/>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css'>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['URL_CSS']->value;?>
/style.css"/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['URL_CSS']->value;?>
/custom.css"/>


    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['URL_VENDOR']->value;?>
/jquery/jquery-3.3.1.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src = "https://cdn.polyfill.io/v2/polyfill.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js'><?php echo '</script'; ?>
>

    <!-- ![endif]-->
    <?php echo htmlDecode($_smarty_tpl->tpl_vars['_CONFIG']->value['jscode_head']);?>

</head>
<body <?php if ($_smarty_tpl->tpl_vars['mod']->value == 'home' && $_smarty_tpl->tpl_vars['act']->value == 'default') {?>class="menu-show"<?php }?>>
<!-- Custom JS Body -->
<?php echo htmlDecode($_smarty_tpl->tpl_vars['_CONFIG']->value['jscode_openbody']);?>

<div class="page">
<?php $_smarty_tpl->_subTemplateRender("file:_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['mod']->value)."/index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->_subTemplateRender("file:_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
<div class="loader-overlay">
    <div class="loader-box">
        <div class="loader"></div>
    </div>
</div>

<!-- modals + sticky widgets-->
<div class="sticky">
    <div class="sticky__btns">
        <ul class="sticky-btns">
            <li class="sticky-btns__item"><a class="sticky-btns__link sticky-btns__link--up js-movetop" href="#!"></a></li>
            <li class="sticky-btns__item"><a class="sticky-btns__link sticky-btns__link--search js-md-search" href="#!"></a></li>
            <li class="sticky-btns__item"><a class="sticky-btns__link sticky-btns__link--menu js-menu-toggle" href="#!"><span></span><span></span><span></span></a></li>
        </ul>
    </div>
    <div class="sticky__sitemap">
        <div class="sticky__sitemap-body">
            <div class="sitemap">
                <div class="sitemap__wrapper">
                    <ul class="sitemap__list">
                        <li class="sitemap__item sitemap__dropdown"><a class="sitemap__link sitemap__dropdown-toggle active" href="#!">About Us</a>
                            <div class="sitemap__dropdown-menu" style="display: block"><a class="sitemap__dropdown-title" href="#!">About Us</a>
                                <ul class="sitemap__sub">
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Message from the President</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Coporate Data</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Our Philosophy</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Teijin Brand</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Board of Directors</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">History</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Facilities in Japan</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Facilities in Workwide</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">TEIJIN Chanel</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="sitemap__item sitemap__dropdown"><a class="sitemap__link sitemap__dropdown-toggle" href="#!">Products & Services</a>
                            <div class="sitemap__dropdown-menu"><a class="sitemap__dropdown-title" href="#!">Products & Services</a>
                                <ul class="sitemap__sub">
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Our Philosophy</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Teijin Brand</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Message from the President</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Coporate Data</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Board of Directors</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">History</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Facilities in Japan</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">Facilities in Workwide</a></li>
                                    <li class="sitemap__sub-item"><a class="sitemap__sub-link" href="#!">TEIJIN Chanel</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="sitemap__item"><a class="sitemap__link" href="#!">Research & Development</a></li>
                        <li class="sitemap__item"><a class="sitemap__link" href="#!">Corporate Social Responsibility</a></li>
                        <li class="sitemap__item"><a class="sitemap__link" href="#!">For Investors</a></li>
                        <li class="sitemap__item"><a class="sitemap__link" href="#!">Jobs & Careers</a></li>
                        <li class="sitemap__item"><a class="sitemap__link" href="#!">About Us</a></li>
                    </ul>
                    <nav class="sitemap__nav"><a class="sitemap__nav-link" href="#!">News</a><a class="sitemap__nav-link" href="#!">Jobs & Careers</a></nav>
                </div>
            </div>
        </div>
    </div>
</div>
<article class="md-search">
    <div class="md-search__wrapper">
        <div class="md-search__container">
            <div class="md-search__inner">
                <form class="search" action="#!">
                    <div class="input-group">
                        <input class="form-control" type="text" placeholder="Search our site" />
                        <div class="input-group-append">
                            <button class="input-group-text"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
                <button class="md-search__close"></button>
            </div>
        </div>
    </div>
</article>


<?php echo '<script'; ?>
>
    var VNCMS_URL = "<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
";
    var URL_IMAGES = "<?php echo $_smarty_tpl->tpl_vars['URL_IMAGES']->value;?>
";
    var URL_MEDIAS = "<?php echo $_smarty_tpl->tpl_vars['URL_MEDIAS']->value;?>
";
    var URL_CSS = "<?php echo $_smarty_tpl->tpl_vars['URL_CSS']->value;?>
";
    var URL_UPLOAD = "<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
";
    var IS_LOGIN = <?php echo $_smarty_tpl->tpl_vars['isLogin']->value;?>
;
<?php echo '</script'; ?>
>
<!-- :::::-[ Vendors JS ]-:::::: -->

<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['URL_VENDOR']->value;?>
/popper/popper.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['URL_VENDOR']->value;?>
/bootstrap/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['URL_VENDOR']->value;?>
/swiper/swiper.min.js"><?php echo '</script'; ?>
>
<!-- custom script-->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['URL_JS']->value;?>
/cart.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['URL_JS']->value;?>
/globals.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['URL_JS']->value;?>
/custom.js"><?php echo '</script'; ?>
>
<!-- custom script-->
<!-- Custom JS Bottom -->


<?php echo htmlDecode($_smarty_tpl->tpl_vars['_CONFIG']->value['jscode_closebody']);?>

<?php if ($_smarty_tpl->tpl_vars['arr_error']->value) {?>
    <?php echo '<script'; ?>
>
        Swal.queue([{
            title: "Thông báo",
            type: "<?php echo $_smarty_tpl->tpl_vars['arr_error']->value['status'];?>
",
            text: "<?php echo $_smarty_tpl->tpl_vars['arr_error']->value['message'];?>
",
            showLoaderOnConfirm: true,
            preConfirm: () => {
                var location = "<?php echo $_smarty_tpl->tpl_vars['arr_error']->value['location'];?>
";
                if (location !== "") {
                    return window.location.href = "<?php echo $_smarty_tpl->tpl_vars['arr_error']->value['location'];?>
";
                }
            }
        }])
    <?php echo '</script'; ?>
>
<?php }?>
</body>
<div id="fb-root"></div>
<?php echo '<script'; ?>
 async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=538897933198331&autoLogAppEvents=1"><?php echo '</script'; ?>
>
</html><?php }
}
