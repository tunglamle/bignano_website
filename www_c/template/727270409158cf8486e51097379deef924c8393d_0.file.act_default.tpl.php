<?php
/* Smarty version 3.1.32, created on 2019-11-28 16:25:58
  from 'D:\Thuan\bignanotech\themes\template\product\act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5ddf92a6adeca8_87961789',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '727270409158cf8486e51097379deef924c8393d' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\product\\act_default.tpl',
      1 => 1574933156,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ddf92a6adeca8_87961789 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == 0) {?>
    <div class="page__content">
    <!-- main content-->
    <nav class="navigation">
        <div class="navigation__wrapper">
            <div class="navigation__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                    <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</li>
                </ol>
            </div>
            <div class="navigation__lang">
                <div class="langs">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                            <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </div>
    </nav>
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
        </div><img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="" />
    </div>
    <section class="section-2">
        <h2 class="heading">Product Search</h2>
        <ul class="nav tabs">
            <li class="nav-item"><a class="nav-link active" href="#product-tab-1" data-toggle="tab"><span class="d-none d-md-inline">Search by Category</span><span class="d-md-none">Category</span></a></li>
            <li class="nav-item"><a class="nav-link" href="#product-tab-2" data-toggle="tab"><span class="d-none d-md-inline">Search by Application</span><span class="d-md-none">Application</span></a></li>
            <li class="nav-item"><a class="nav-link" href="#product-tab-3" data-toggle="tab"><span class="d-none d-md-inline">Search by Properties</span><span class="d-md-none">Properties</span></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="product-tab-1">
                <div class="product-tab-text">Clicking on a category moves to the product list.</div>
                <div class="product-grid">
                     <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatProduct']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                    <div class="product-grid__col">
                        <div class="product">
                            <a class="product__frame" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="" /></a>
                            <h3 class="product__title"><a href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a></h3>
                        </div>
                    </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
            <div class="tab-pane" id="product-tab-2">
                <div class="product-tab-text">Please click the application you wish to search. (multiple selection is not available)</div>
                <div class="product-grid-2">
                     <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListSearchApplication']->value, 'search', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['search']->value) {
?>
                    <div class="product-grid-2__col">
                        <div class="product-2 media">
                            <div class="product-2__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['search']->value['image'];?>
" alt="" /></div>
                            <div class="product-2__body media-body">
                                <h3 class="product-2__title"><a href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
/product-search?app_id=<?php echo $_smarty_tpl->tpl_vars['search']->value['search_app_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['search']->value['name'];?>
</a></h3>
                            </div>
                        </div>
                    </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
            <div class="tab-pane" id="product-tab-3">
                <div class="product-tab-text">Please click the property you wish to search. (multiple selection is not available)</div>
                <div class="product-grid-2">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListSearchProperties']->value, 'search', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['search']->value) {
?>
                        <div class="product-grid-2__col">
                            <div class="product-2 media">
                                <div class="product-2__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['search']->value['image'];?>
" alt="" /></div>
                                <div class="product-2__body media-body">
                                    <h3 class="product-2__title"><a href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
/product-search?pro_id=<?php echo $_smarty_tpl->tpl_vars['search']->value['search_pro_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['search']->value['name'];?>
</a></h3>
                                </div>
                            </div>
                        </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php } else { ?>
    <div class="page__content">
        <!-- main content-->
        <nav class="navigation">
            <div class="navigation__wrapper">
                <div class="navigation__breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                        <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['parCat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['parCat']->value['name'];?>
</a></li>
                        <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</li>
                    </ol>
                </div>
                <div class="navigation__lang">
                    <div class="langs">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                            <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                                <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                </div>
            </div>
        </nav>
        <section class="section-2 pt-20">
            <article class="post mb-40">
                <div class="post-subtitle"><?php echo $_smarty_tpl->tpl_vars['parCat']->value['name'];?>
</div>
                <h1 class="post-title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</h1>
                <div class="product-intro media">
                    <div class="product-intro__left">
                        <div class="image-slider__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['image'];?>
" alt="" /></div>
                    </div>
                    <div class="product-intro__body media-body">
                        <?php echo htmlDecode($_smarty_tpl->tpl_vars['curCat']->value['des']);?>

                    </div>
                </div>
                <ul class="post-nav">
                     <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatProduct']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                    <li class="post-nav__item"><a class="post-nav__link" href="#<?php echo $_smarty_tpl->tpl_vars['cat']->value['slug'];?>
-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a></li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </ul>
                <div class="post-content">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatProduct']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                    <h2 class="post-heading text-center" id="<?php echo $_smarty_tpl->tpl_vars['cat']->value['slug'];?>
-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</h2>
                    <div class="accordion">
                        <div class="accordion__top"><a class="accordion__switch" href="#!">All open</a></div>
                         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cat']->value['arrProduct'], 'product', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['product']->value) {
?>
                        <div class="accordion__item">
                            <div class="accordion__header">
                                <h3 class="accordion__title"><a href=""<?php if ($_smarty_tpl->tpl_vars['product']->value['link']) {
echo $_smarty_tpl->tpl_vars['product']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);
}?>"><?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
</a></h3>
                                <div class="accordion__toggle"></div>
                            </div>
                            <div class="accordion__body">
                                <div class="accordion__wrapper media">
                                    <div class="accordion__left">
                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['list_image']) {?>
                                            <div class="image-slider">
                                                <div class="image-slider__container swiper-container">
                                                    <div class="swiper-wrapper">
                                                        <?php $_smarty_tpl->_assignInScope('arrSlide', explode(",",$_smarty_tpl->tpl_vars['product']->value['list_image']));?>
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrSlide']->value, 'slide', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['slide']->value) {
?>
                                                            <div class="swiper-slide">
                                                                <div class="image-slider__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['slide']->value;?>
" alt="" /></div>
                                                            </div>
                                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                    </div>
                                                </div>
                                                <div class="image-slider__pagination"></div>
                                                <div class="image-slider__nav">
                                                    <div class="image-slider__prev"><i class="fa fa-angle-left"></i></div>
                                                    <div class="image-slider__next"><i class="fa fa-angle-right"></i></div>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="image-slider__img"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['product']->value['image'];?>
" alt="" /></div>
                                        <?php }?>
                                        <div class="accordion__icons">
                                            <?php $_smarty_tpl->_assignInScope('arrIcon', explode(",",$_smarty_tpl->tpl_vars['product']->value['list_icon']));?>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrIcon']->value, 'icon', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['icon']->value) {
?>
                                                <a class="accordion__icon" href="#!">
                                                    <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['icon']->value;?>
" alt="" /></a>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <div class="accordion__content">
                                            <?php echo htmlDecode($_smarty_tpl->tpl_vars['product']->value['sapo']);?>

                                        </div>
                                        <div class="accordion__btns">
                                            <a class="accordion__btn button button--red" href="<?php if ($_smarty_tpl->tpl_vars['product']->value['link']) {
echo $_smarty_tpl->tpl_vars['product']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);
}?>">Product details</a>
                                            <a class="accordion__btn button button--dark" href="#!">Inquiry</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </article>
            <div class="visit mb-40">
                <div class="visit__label">Please visit the website below for details.</div>
                <div class="text-center"><a class="visit__btn button" href="<?php echo $_smarty_tpl->tpl_vars['curCat']->value['link_visit'];?>
"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name_visit'];?>
</a></div>
            </div>
            <div class="contact-combo mb-40">
                <div class="contact-combo__left">
                    <div class="contact-combo__title">Inquiry about <?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
                </div>
                <div class="contact-combo__right"><a class="contact-combo__btn" href="#!">Inquiry</a></div>
            </div>
            <ul class="cate-2">

                 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatProductMain']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                <li class="cate-2__item"><a class="cate-2__link" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
">
                        <div class="cate-2__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['icon'];?>
" alt="" /></div>
                        <div class="cate-2__text"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</div>
                    </a>
                </li>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

            </ul>
        </section>
    </div>
<?php }
}
}
