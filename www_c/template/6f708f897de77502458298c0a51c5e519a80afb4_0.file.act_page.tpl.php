<?php
/* Smarty version 3.1.32, created on 2020-03-18 17:20:24
  from '/var/www/html/bignanotech.com.vn/themes/template/home/act_page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e71f5e866bdd4_24245483',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6f708f897de77502458298c0a51c5e519a80afb4' => 
    array (
      0 => '/var/www/html/bignanotech.com.vn/themes/template/home/act_page.tpl',
      1 => 1584441210,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e71f5e866bdd4_24245483 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="page__content">
    <!-- main content-->
        <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['name'];?>
</div>
        </div><img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['banner'];?>
" alt="" />
    </div>
    <section class="section-2">
        <article class="post">
            <div class="post-content">
                <h2 class="post-heading"><a href="#!"><?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['name'];?>
</a></h2>
                <?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOnePage']->value['content']);?>


            </div>
        </article>
    </section>
</div>
<?php }
}
