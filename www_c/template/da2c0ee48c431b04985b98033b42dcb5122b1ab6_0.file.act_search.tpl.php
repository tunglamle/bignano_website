<?php
/* Smarty version 3.1.32, created on 2020-02-10 17:17:57
  from '/home/bignao/public_html/themes/template/home/act_search.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e412dd53f0476_37459999',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'da2c0ee48c431b04985b98033b42dcb5122b1ab6' => 
    array (
      0 => '/home/bignao/public_html/themes/template/home/act_search.tpl',
      1 => 1581329874,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e412dd53f0476_37459999 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container my-40">
    <div class="accordion">
        <div class="accordion__top justify-content-md-between flex-md-row flex-column">
            <h3 class="">Kết quả tìm kiếm cho từ khóa: <span style="color:#cf0212;"><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</span></h3>
            <a class="accordion__switch active" href="#!" style="max-width: 136px; margin-left: auto">All open</a>
        </div>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListProduct']->value, 'product', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['product']->value) {
?>
            <div class="accordion__item">
                <div class="accordion__header">
                    <h3 class="accordion__title">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
</a></h3>
                    <div class="accordion__toggle active"></div>
                </div>
                <div class="accordion__body" style="display: block">
                    <div class="accordion__wrapper media">
                        <div class="accordion__left">
                            <div class="">
                                <?php if ($_smarty_tpl->tpl_vars['product']->value['image']) {?>
                                <img class="d-block w-100" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['product']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
"/>
                                <?php }?>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="accordion__content">
                                <?php echo htmlDecode($_smarty_tpl->tpl_vars['product']->value['sapo']);?>

                            </div>
                            <div class="accordion__btns">
                                <a class="accordion__btn button button--red button--sm"
                                   href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_product($_smarty_tpl->tpl_vars['product']->value);?>
">Product
                                    details</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </div>
</div><?php }
}
