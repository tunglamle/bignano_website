<?php
/* Smarty version 3.1.32, created on 2020-02-20 11:59:31
  from '/home/bignao/public_html/themes/template/about/act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e4e123356d797_59004375',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8f6fab72e98a4849625668a422ca0174da233fb0' => 
    array (
      0 => '/home/bignao/public_html/themes/template/about/act_default.tpl',
      1 => 1582174764,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4e123356d797_59004375 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/home/bignao/public_html/includes/smarty3/plugins/modifier.lang.php','function'=>'smarty_modifier_lang',),));
if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == 0) {?>
    <?php if ($_smarty_tpl->tpl_vars['curCat']->value['template'] == 'page') {?>
        <div class="page__content">
            <div class="banner">
                <div class="banner__wrapper">
                    <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
                </div>
                <img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
"/>
            </div>
            <section class="section-2">
                <div class="research_developement_des mb-30">
                    <?php echo htmlDecode($_smarty_tpl->tpl_vars['curCat']->value['des']);?>

                </div>
                                                                                                                                <div class="row gutter-under-md-16">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatChildren']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                        <div class="col-6 col-md-4 col-lg-4 mb-3 mb-sm-30">
                            <div class="about">
                                <a class="about__frame"
                                   href="<?php if ($_smarty_tpl->tpl_vars['cat']->value['link']) {
echo $_smarty_tpl->tpl_vars['cat']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);
}?>">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
"/></a>
                                <div class="about__body">
                                    <h3 class="about__title"><a
                                                href="<?php if ($_smarty_tpl->tpl_vars['cat']->value['link']) {
echo $_smarty_tpl->tpl_vars['cat']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);
}?>"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a>
                                    </h3>
                                    <div class="mt-2">
                                        <?php echo htmlDecode($_smarty_tpl->tpl_vars['cat']->value['des']);?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </section>
        </div>
    <?php } else { ?>
        <div class="page__content">
            <div class="banner">
                <div class="banner__wrapper">
                    <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
                </div>
                <img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
"/>
            </div>
            <section class="section-2">
                <div class="row gutter-under-md-16 gutter-over-xl-60">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatChildren']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                        <div class="col-6 col-md-4 col-lg-4 mb-3 mb-sm-30">
                            <div class="about">
                                <a class="about__frame"
                                   href="<?php if ($_smarty_tpl->tpl_vars['cat']->value['link']) {
echo $_smarty_tpl->tpl_vars['cat']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);
}?>">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
"/></a>
                                <div class="about__body">
                                    <h3 class="about__title text-center">
                                        <a href="<?php if ($_smarty_tpl->tpl_vars['cat']->value['link']) {
echo $_smarty_tpl->tpl_vars['cat']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);
}?>"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </section>
        </div>
    <?php }
} else { ?>
    <?php if ($_smarty_tpl->tpl_vars['curCat']->value['template'] == 'page') {?>
        <div class="page__content">
            <div class="banner">
                <div class="banner__wrapper">
                    <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
                </div>
                <img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
"/>
            </div>
            <section class="section-2">
                <div class="research_developement_des mb-30">
                    <?php echo htmlDecode($_smarty_tpl->tpl_vars['curCat']->value['des']);?>

                </div>

                <div class="row gutter-under-md-16">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatChildren']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
                        <div class="col-6 col-md-4 col-lg-4 mb-3 mb-sm-30">
                            <div class="about">
                                <a class="about__frame"
                                   href="#!">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
"/></a>
                                <div class="about__body">
                                    <h3 class="about__title">
                                        <a href="#!"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a>
                                    </h3>
                                    <div class="mt-2">
                                        <?php echo htmlDecode($_smarty_tpl->tpl_vars['cat']->value['des']);?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </section>
        </div>
    <?php } else { ?>
        <div class="page__content">
            <!-- main content-->
            <section class="section-2 pt-20">
                <article class="post mb-40">
                    <?php if ($_smarty_tpl->tpl_vars['countListAbout']->value > 1) {?>
                        <ul class="post-nav">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListAbout']->value, 'about', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['about']->value) {
?>
                                <li class="post-nav__item"><a class="post-nav__link"
                                                              href="#<?php echo $_smarty_tpl->tpl_vars['about']->value['slub'];?>
-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['about']->value['title'];?>
</a></li>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </ul>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['curCat']->value['is_show_contact'] == 1) {?>
                        <div class="post-content">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListAbout']->value, 'about', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['about']->value) {
?>
                                <h2 class="post-heading text-primary" id="<?php echo $_smarty_tpl->tpl_vars['about']->value['slub'];?>
-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['about']->value['title'];?>
</h2>
                                <div class="row">
                                    <div class="col-lg-6 mb-30">
                                        <form id="form-contact" method="post">
                                            <h2 class="text-20 mb-3"><?php echo smarty_modifier_lang('gui-thong-tin-cho-chung-toi');?>
</h2>
                                            <div class="form-group">
                                                <label for="ct-name" class="d-none">Name</label>
                                                <input id="ct-name" name="name" type="text" class="form-control" placeholder="<?php echo smarty_modifier_lang('Full_name');?>
">
                                            </div>
                                            <div class="form-group">
                                                <label for="ct-email" class="d-none">Email</label>
                                                <input id="ct-email" name="email" type="email" class="form-control" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="ct-phone" class="d-none"><?php echo smarty_modifier_lang('Phone');?>
</label>
                                                <input id="ct-phone" name="phone" type="text" class="form-control" placeholder="<?php echo smarty_modifier_lang('Phone');?>
">
                                            </div>
                                            <div class="form-group">
                                                <label for="ct-title" class="d-none">Title</label>
                                                <input id="ct-title" name="title" type="text" class="form-control" placeholder="<?php echo smarty_modifier_lang('tieu-de');?>
">
                                            </div>
                                            <div class="form-group">
                                                <label for="ct-content" class="d-none"><?php echo smarty_modifier_lang('Content');?>
</label>
                                                <textarea id="ct-content" name="content" class="form-control" rows="4"
                                                          placeholder="<?php echo smarty_modifier_lang('Content');?>
"></textarea>
                                            </div>
                                            <button class="btn btn-primary" type="button" onclick="submitContact()"><?php echo smarty_modifier_lang('gui-cau-hoi');?>
</button>
                                        </form>
                                    </div>
                                    <div class="col-lg-6 mb-30">
                                        <div class="d-none d-lg-block text-20 mb-2">&nbsp;</div>
                                        <?php echo htmlDecode($_smarty_tpl->tpl_vars['about']->value['content']);?>

                                    </div>
                                    <div class="col-12">
                                        <div class="embed-responsive" style="height: 350px;">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d931.1138261316368!2d105.7757454!3d21.0144606!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345302b915c71d%3A0xbca488cecc456c5d!2sBIG%20CAPITAL.%2C%20JSC!5e0!3m2!1svi!2s!4v1580900249007!5m2!1svi!2s"
                                                    width="600" height="450" frameborder="0" style="border:0;"
                                                    allowfullscreen=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </div>
                    <?php } else { ?>
                        <div class="post-content">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListAbout']->value, 'about', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['about']->value) {
?>
                                <h2 class="post-heading text-primary" id="<?php echo $_smarty_tpl->tpl_vars['about']->value['slub'];?>
-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['about']->value['title'];?>
</h2>
                                <div class="">
                                    <?php echo htmlDecode($_smarty_tpl->tpl_vars['about']->value['content']);?>

                                </div>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </div>
                    <?php }?>
                </article>

            </section>
        </div>
    <?php }
}
}
}
