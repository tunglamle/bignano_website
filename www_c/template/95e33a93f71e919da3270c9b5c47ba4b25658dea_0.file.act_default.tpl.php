<?php
/* Smarty version 3.1.32, created on 2019-11-30 15:15:46
  from 'D:\Thuan\bignanotech\themes\template\for_investor\act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5de22532a951b5_99286978',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '95e33a93f71e919da3270c9b5c47ba4b25658dea' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\for_investor\\act_default.tpl',
      1 => 1575101745,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5de22532a951b5_99286978 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'D:\\Thuan\\bignanotech\\includes\\smarty3\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == 0) {?>
    <div class="page__content">
        <!-- main content-->
        <nav class="navigation">
            <div class="navigation__wrapper">
                <div class="navigation__breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                        <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</li>
                    </ol>
                </div>
                <div class="navigation__lang">
                    <div class="langs">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                            <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                                <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                </div>
            </div>
        </nav>
        <div class="banner">
            <div class="banner__wrapper">
                <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
            </div><img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];?>
" alt="" />
        </div>
        <section class="section-2">
            <div class="ir-layout mb-40">
                <div class="ir-layout__left">
                    <a class="ir-layout__frame" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_for_investor($_smarty_tpl->tpl_vars['arrFISpecial']->value);?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['arrFISpecial']->value['image'];?>
" alt="">
                        <h2 class="ir-layout__title"><?php echo $_smarty_tpl->tpl_vars['arrFISpecial']->value['title'];?>
</h2>
                    </a>
                </div>
                <div class="ir-layout__right">
                    <div class="ir-layout__title2">Stock Price</div>
                    <div class="ir-layout__content"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_IMAGES']->value;?>
/ir-layout-img.png" alt=""></div>
                    <div class="ir-layout__footer"><a class="ir-layout__link" href="#!">Chart</a></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 mb-40">
                    <div class="news-section__header">
                        <h2 class="news-section__title">IR News</h2>
                        <div class="news-section__elements">
                            <a class="news-section__element news-section__element--rss" href="#!">RSS</a>
                            <a class="news-section__element news-section__element--arrow" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['arrCatNewsFor_investor']->value);?>
">view all</a>
                        </div>
                    </div>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListNewsFor_investor']->value, 'news', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['news']->value) {
?>
                        <a class="news-section__item" href="<?php if ($_smarty_tpl->tpl_vars['news']->value['link']) {
echo $_smarty_tpl->tpl_vars['news']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_article($_smarty_tpl->tpl_vars['news']->value);
}?>">
                            <div class="news-section__date"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['reg_date'],"%B %d, %Y");?>
</div>
                            <div class="news-section__icon"><?php echo $_smarty_tpl->tpl_vars['news']->value['cat_name'];?>
</div>
                            <h3 class="news-section__text"><?php echo $_smarty_tpl->tpl_vars['news']->value['title'];?>
</h3>
                        </a>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
                <div class="col-lg-4 mb-40">
                    <div class="download-zip">
                        <a class="download-zip__btn" href="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['_CONFIG']->value['download_zip'];?>
" download>
                            <img class="download-zip__icon" src="<?php echo $_smarty_tpl->tpl_vars['URL_IMAGES']->value;?>
/icon-zip-file.png" alt="">
                            <div class="download-zip__text">
                                <div>Download the latest IR materials in batch</div>
                                <div>(ZIP: <?php echo $_smarty_tpl->tpl_vars['sizeFile']->value;?>
 KB)</div>
                            </div>
                        </a>
                        <div class="download-zip__detail">
                            <p>The following materials are included</p>
                            <ul>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkForInvestor']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['link'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a></li>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-xl-9 mb-40">
                    <div class="row gutter-under-md-16">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatChildrenLeft']->value, 'ir', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['ir']->value) {
?>
                            <div class="col-sm-6 col-md-4 col-lg-4 mb-30"><a class="news-2" href="<?php if ($_smarty_tpl->tpl_vars['ir']->value['link']) {
echo $_smarty_tpl->tpl_vars['ir']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['ir']->value);
}?>">
                                    <div class="news-2__frame"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['ir']->value['image'];?>
" alt="" /></div>
                                    <div class="news-2__overlay">
                                        <div class="news-2__title"><?php echo $_smarty_tpl->tpl_vars['ir']->value['name'];?>
</div>
                                    </div>
                                </a>
                                <div class="mt-30">
                                    <ul class="news-list">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ir']->value['arrFor_investor'], 'sub_ir', false, 'j');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['sub_ir']->value) {
?>
                                            <li class="news-list__item"><a class="news-list__link" href="<?php if ($_smarty_tpl->tpl_vars['sub_ir']->value['link']) {
echo $_smarty_tpl->tpl_vars['sub_ir']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_for_investor($_smarty_tpl->tpl_vars['sub_ir']->value);
}?>"><?php echo $_smarty_tpl->tpl_vars['sub_ir']->value['title'];?>
</a></li>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </ul>
                                </div>
                            </div>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-3 mb-40">
                    <ul class="as-menu">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatChildrenRight']->value, 'ir', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['ir']->value) {
?>
                            <li class="as-menu__item"><a class="as-menu__link" href="<?php if ($_smarty_tpl->tpl_vars['ir']->value['link']) {
echo $_smarty_tpl->tpl_vars['ir']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['ir']->value);
}?>">
                                    <div class="as-menu__frame">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['ir']->value['icon'];?>
" alt="">
                                    </div>
                                    <div class="as-menu__text"><?php echo $_smarty_tpl->tpl_vars['ir']->value['name'];?>
</div>
                                </a>
                            </li>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </ul>
                </div>
            </div>
        </section>
        <section class="partner-section pt-0">
            <div class="partner-section__body">
                <ul class="partners">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListPartner']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                        <li class="partners__item"><a class="partners__link" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['link'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['adver']->value['image'];?>
" alt=""></a></li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </ul>
            </div>
        </section>
    </div>
<?php } else { ?>
    <div class="page__content">
        <!-- main content-->
        <nav class="navigation">
            <div class="navigation__wrapper">
                <div class="navigation__breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                        <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['parCat']->value);?>
""><?php echo $_smarty_tpl->tpl_vars['parCat']->value['name'];?>
</a></li>
                        <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</li>
                    </ol>
                </div>
                <div class="navigation__lang">
                    <div class="langs">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                            <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                                <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                </div>
            </div>
        </nav>
        <section class="section-2 py-20">
            <article class="post mb-40">
                <h1 class="post-title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</h1>
                <div class="post-content">
                    <?php echo htmlDecode($_smarty_tpl->tpl_vars['curCat']->value['des']);?>

                </div>
            </article>
        </section>
    </div>
<?php }
}
}
