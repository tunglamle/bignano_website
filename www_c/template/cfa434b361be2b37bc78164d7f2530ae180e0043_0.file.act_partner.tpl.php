<?php
/* Smarty version 3.1.32, created on 2020-05-05 11:01:33
  from '/var/www/html/bignanotech.com.vn/themes/template/home/act_partner.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5eb0e51d3edd88_89041469',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cfa434b361be2b37bc78164d7f2530ae180e0043' => 
    array (
      0 => '/var/www/html/bignanotech.com.vn/themes/template/home/act_partner.tpl',
      1 => 1584441210,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5eb0e51d3edd88_89041469 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="page-content">
    <!-- main content-->
    <section class="banner"><img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['image'];?>
" alt="" />
        <div class="container">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['name'];?>
</div>
            <div class="banner__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Trang chủ</a></li>
                    <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['name'];?>
</li>
                </ol>
            </div>
        </div>
    </section>
    <div class="container pt-40 pb-60">
        <div class="mb-4">
            <p><?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOnePage']->value['content']);?>
</p>
        </div>
        <div class="row gutter-under-sm-16">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListPartner']->value, 'partner', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['partner']->value) {
?>
            <div class="col-lg-2 col-md-3 col-4 mb-3 mb-sm-30">
                <div class="partner">
                    <a class="partner__frame" href="<?php echo $_smarty_tpl->tpl_vars['partner']->value['link'];?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['partner']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['partner']->value['title'];?>
" /></a>
                    <h3 class="partner__title"><?php echo $_smarty_tpl->tpl_vars['partner']->value['title'];?>
</h3>
                </div>
            </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
        <nav class="d-flex justify-content-center">
            <ul class="pagination">
                <?php echo $_smarty_tpl->tpl_vars['clsPaging']->value->showPagingNew2();?>

            </ul>
        </nav>
    </div>
</div><?php }
}
