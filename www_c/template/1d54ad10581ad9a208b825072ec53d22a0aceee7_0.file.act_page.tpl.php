<?php
/* Smarty version 3.1.32, created on 2020-01-17 17:03:01
  from '/home/bignao/public_html/themes/template/home/act_page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e21865540db71_76624653',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1d54ad10581ad9a208b825072ec53d22a0aceee7' => 
    array (
      0 => '/home/bignao/public_html/themes/template/home/act_page.tpl',
      1 => 1579244397,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e21865540db71_76624653 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="page__content">
    <!-- main content-->
        <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['name'];?>
</div>
        </div><img class="banner__bg" src="<?php echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['banner'];?>
" alt="" />
    </div>
    <section class="section-2">
        <article class="post">
            <div class="post-content">
                <h2 class="post-heading"><a href="#!"><?php echo $_smarty_tpl->tpl_vars['arrOnePage']->value['name'];?>
</a></h2>
                <?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOnePage']->value['content']);?>


            </div>
        </article>
    </section>
</div>
<?php }
}
