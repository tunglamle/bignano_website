<?php
/* Smarty version 3.1.32, created on 2019-11-29 15:10:25
  from 'D:\Thuan\bignanotech\themes\template\articles\act_default.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5de0d27150cff7_23144749',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '65ba3161bb0ffd84303a4007dca02ac365cbaf7f' => 
    array (
      0 => 'D:\\Thuan\\bignanotech\\themes\\template\\articles\\act_default.tpl',
      1 => 1575015024,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5de0d27150cff7_23144749 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'D:\\Thuan\\bignanotech\\includes\\smarty3\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?><div class="page__content">
    <!-- main content-->
    <nav class="navigation">
        <div class="navigation__wrapper">
            <div class="navigation__breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['VNCMS_URL']->value;?>
">Home</a></li>
                    <?php if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] != 0) {?>
                        <li class="breadcrumb-item"><a class="link-unstyled" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['parCat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['parCat']->value['name'];?>
</a></li>
                    <?php }?>
                    <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</li>
                </ol>
            </div>
            <div class="navigation__lang">
                <div class="langs">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListLinkLanguage']->value, 'adver', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['adver']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value != 1) {?>
                            <a class="langs__item" href="<?php echo $_smarty_tpl->tpl_vars['adver']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['adver']->value['title'];?>
</a>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </div>
    </nav>
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == 0) {
echo $_smarty_tpl->tpl_vars['curCat']->value['name'];
} else {
echo $_smarty_tpl->tpl_vars['parCat']->value['name'];
}?></div>
        </div><img class="banner__bg" src="<?php if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == 0) {
echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];
} else {
echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['parCat']->value['banner'];
}?>" alt="" />
    </div>
    <section class="section-2">
        <ul class="news-tabs">
            <li class="news-tabs__item"><a class="news-tabs__link <?php if ($_smarty_tpl->tpl_vars['curCat']->value['cat_id'] == $_smarty_tpl->tpl_vars['arrNews']->value['cat_id']) {?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['arrNews']->value);?>
">Show All</a></li>
             <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListCatNewsChildren']->value, 'cat', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['cat']->value) {
?>
            <li class="news-tabs__item"><a class="news-tabs__link <?php if ($_smarty_tpl->tpl_vars['curCat']->value['cat_id'] == $_smarty_tpl->tpl_vars['cat']->value['cat_id']) {?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_category($_smarty_tpl->tpl_vars['cat']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</a></li>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </ul>
        <section class="news-section mb-40">
            <div class="news-section__header">
                <form action="" id="form_news">
                    <input type="hidden" name="cat_id" value="<?php echo $_smarty_tpl->tpl_vars['curCat']->value['cat_id'];?>
">
                <select class="form-control rounded-0 my-12" name="year" onchange="showNews()" style="width: 250px; max-width: 100%; height: 45px">
                     <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListYear']->value, 'year', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['year']->value) {
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value['name'];?>
"><?php echo $_smarty_tpl->tpl_vars['year']->value['name'];?>
</option>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </select>
                </form>
                <div class="news-section__elements"><a class="news-section__element news-section__element--rss" href="#!">RSS</a></div>
            </div>
            <div class="news-ajax-wrap">
                <div class="news-ajax-content">
                    <?php if ($_smarty_tpl->tpl_vars['arrListArticles']->value[0] == '') {?>
                        <span class="text-center d-block mt-3">Please wait for new "News".</span>
                        <?php } else { ?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arrListArticles']->value, 'news', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['news']->value) {
?>
                        <a class="news-section__item" href="<?php if ($_smarty_tpl->tpl_vars['news']->value['link']) {
echo $_smarty_tpl->tpl_vars['news']->value['link'];
} else {
echo $_smarty_tpl->tpl_vars['Rewrite']->value->url_article($_smarty_tpl->tpl_vars['news']->value);
}?>">
                            <div class="news-section__date"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['news']->value['reg_date'],"%B %d, %Y");?>
</div>
                            <div class="news-section__icon"><?php echo $_smarty_tpl->tpl_vars['news']->value['cat_name'];?>
</div>
                            <h3 class="news-section__text"><?php echo $_smarty_tpl->tpl_vars['news']->value['title'];?>
</h3>
                        </a>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    <?php }?>
                </div>
            </div>
        </section>
    </section>
</div><?php }
}
