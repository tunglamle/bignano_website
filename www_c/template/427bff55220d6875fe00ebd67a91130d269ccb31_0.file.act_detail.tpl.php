<?php
/* Smarty version 3.1.32, created on 2020-02-06 11:59:09
  from '/home/bignao/public_html/themes/template/job/act_detail.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5e3b9d1d75efe1_34021315',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '427bff55220d6875fe00ebd67a91130d269ccb31' => 
    array (
      0 => '/home/bignao/public_html/themes/template/job/act_detail.tpl',
      1 => 1580965147,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e3b9d1d75efe1_34021315 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="page__content">
    <div class="banner">
        <div class="banner__wrapper">
            <div class="banner__title"><?php echo $_smarty_tpl->tpl_vars['curCat']->value['name'];?>
</div>
        </div><img class="banner__bg" src="<?php if ($_smarty_tpl->tpl_vars['curCat']->value['parent_id'] == 0) {
echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['curCat']->value['banner'];
} else {
echo $_smarty_tpl->tpl_vars['URL_UPLOADS']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['parCat']->value['banner'];
}?>" alt="" />
    </div>
    <section class="section-2">
        <article class="post mb-40">
            <h1 class="post-title"><?php echo $_smarty_tpl->tpl_vars['arrOneJob']->value['title'];?>
</h1>
            <div class="post-content">
                <?php echo htmlDecode($_smarty_tpl->tpl_vars['arrOneJob']->value['content']);?>

            </div>
        </article>
    </section>
</div>
<?php }
}
